#!/usr/bin/env python3
import sys
import socket

# create dgram udp socket
try:
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
except socket.error:
	print('Failed to create socket')
	sys.exit()

projectSrl = 9
sessionSrl = 14
setSrl = 0
sessionType = 0 # 0 == RTI, 1 == PG, 2 == DIS
l = [projectSrl, sessionSrl, setSrl, sessionType]

host = '127.0.0.1'
port = 5240
try :
    data=b''.join(i.to_bytes(4, byteorder='big', signed=False) for i in l)
    s.sendto(data, (host, port))
    # receive data from client (data, addr)
    print("Request sent, waiting for reply...")
    s.settimeout(5)
    reply, addr = s.recvfrom(1024)
    responseCode = int.from_bytes(reply, 'big')
    print('Server reply : {}'.format(responseCode))
except socket.timeout:
    print("Timeout! Server did not respond..")
except socket.error as msg:
    print('Error Code : ' + str(msg[0]) + ' Message ' + msg[1])
    sys.exit()
