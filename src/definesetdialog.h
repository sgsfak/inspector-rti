/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

#ifndef DEFINESETDIALOG_H
#define DEFINESETDIALOG_H

// Local headers

#include "ienums.h"
#include "imageset.h"
#include "modelutils.h"

// Qt headers

#include <QComboBox>
#include <QDialog>
#include <QGridLayout>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QListView>
#include <QPushButton>
#include <QRadioButton>
#include <QStringListModel>

class DefineSetDialog : public QDialog, protected ModelUtils
{
    Q_OBJECT
public:
    DefineSetDialog(QWidget *parent = 0);
    ~DefineSetDialog();

    void setImageSet(const ImageSet &imageSet);
    ImageSet getImageSet();

public slots:
    void fileTypeButtonChanged();
    void addProcessedFiles();
    void addArchivalFiles();
    void removeProcessedFiles();
    void removeArchivalFiles();
    void okClicked();

signals:

private:

    // GUI member variables

    QLineEdit * nameEditBox;
    QRadioButton * jpegButton;
    QRadioButton * tiffButton;
//    QRadioButton * pngButton;
    QGroupBox * processedGroupBox;
    QListView * processedFilesList;
    QPushButton * processedAddButton;
    QPushButton * processedRemoveButton;
    QListView * archivalFilesList;
    QPushButton * archivalAddButton;
    QPushButton * archivalRemoveButton;
    QPushButton * okButton;
    QPushButton * cancelButton;

    // Non-GUI member variables

    int imageSetSrl;
    QString archivalPath;
    QString processedPath;
    QStringListModel * archivalFilesModel;
    QStringListModel * processedFilesModel;

    // GUI initialization methods

    void initGUI();
    QHBoxLayout * initTitleLayout();
    QGridLayout *initNameAndFileTypeLayout();
    QHBoxLayout * initFileListsLayout();
    QGroupBox * initFilesGroupBox(const IFileType::Enum fileType, QStringListModel *& filesModel, QListView *& filesList, QPushButton *& addButton, QPushButton *& removeButton);
    QHBoxLayout * initButtonsLayout();

    // Connect slots

    void setConnections();

    // Utility methods

    void init();
    IFileType::Enum getProcessedType();
    QString getProcessedTypeName();
    void addFiles(QStringListModel * filesModel, IFileType::Enum fileType);
};

#endif // DEFINESETDIALOG_H
