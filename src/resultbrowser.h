/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

#ifndef RESULTBROWSER_H
#define RESULTBROWSER_H

// Local headers

#include "dbutils.h"
#include "modelutils.h"
#include "results.h"

// Qt headers

#include <QComboBox>
#include <QDialog>
#include <QGridLayout>
#include <QList>
#include <QListView>
#include <QPushButton>
#include <QStringListModel>
#include <QVBoxLayout>

class ResultBrowser : public QDialog, protected ModelUtils, protected DBUtils
{
    Q_OBJECT
public:
    ResultBrowser(InspectorType::Enum inspectorType);
    ~ResultBrowser() override;
    void selectBundle(int project_srl, int bundle_srl);

public slots:
    void projectChanged(int projectIndex);
    void bundleChanged(int groupIndex);
    void viewInspections();
    void deleteCheckedInspections();

private:

    // GUI variables

    QComboBox * projectComboBox;
    QComboBox * bundleComboBox;
    QListView * inspectionsListView;
    QPushButton * resultsButton;
    QPushButton * deleteCheckedButton;
    QPushButton * closeButton;

    // Non-GUI variables

    InspectorType::Enum inspectorType;
    class CheckableStringListModel * inspectionsModel;
    QHash<QString, int> inspectionsHash;

    // GUI initialization methods

    void initGUI();
    QGridLayout *initComboBoxLayout();
    QVBoxLayout * initListLayout();
    QVBoxLayout * initButtonLayout();

    void setConnections();

    // Methods to support the GUI

    void initComboBox(ITableType::Enum tableType, int parentSrl, QComboBox * comboBox);
    void updateComboBoxes(int index, QComboBox * comboBox);
    void updateInspectionsList();
    void deleteCheckedButtonEnabled(int count);

    // Utility methods

    int getSerial(const QComboBox * comboBox);
    QString getPath(const QComboBox * comboBox);
    QString getNullableValue(QSqlQuery & q, int index);

};

#endif // RESULTBROWSER_H
