/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

// Local headers

#include "ienums.h"
#include "ierrorcontext.h"

/********************************************************************/
/*                                                                  */
/*  IErrorContext class                                             */
/*                                                                  */
/********************************************************************/

/********************************************************************/
/*                                                                  */
/*  Constructor and destructor                                      */
/*                                                                  */
/********************************************************************/

IErrorContext::IErrorContext()
{
    reset();
}

IErrorContext::~IErrorContext()
{

}

/********************************************************************/
/*                                                                  */
/*  Public methods                                                  */
/*                                                                  */
/********************************************************************/

void IErrorContext::reset()
{
    // Clear / reset the error context fields.

    projectDirectory.clear();
    imageSetName.clear();
    imageSetNum = -1;
    testGroup = ITestGroup::None;
    fileInfo1.setFile("");
    fileInfo2.setFile("");
    msgType = IMessageType::None;

    // Clear / reset the stacks.

    fileInfo1Stack.clear();
    fileInfo2Stack.clear();
    msgTypeStack.clear();
}

void IErrorContext::pushFileInfos(const QFileInfo &newFileInfo1, const QFileInfo &newFileInfo2)
{
    // Occasionally, when the error context has already been set
    // to use two filenames, we need to perform a check on a
    // single file. This method overrides the filenames
    // currently stored in the error context and sets fileInfo1
    // to that single file. For example, see Inspector.getNonNullSynonyms().

    fileInfo1Stack.push(fileInfo1);
    fileInfo2Stack.push(fileInfo2);
    fileInfo1 = newFileInfo1;
    fileInfo2 = newFileInfo2;
}

void IErrorContext::popFileInfos()
{
    // This method restores the original state of filename1
    // and filename2. See pushFilenames().

    fileInfo1 = fileInfo1Stack.pop();
    fileInfo2 = fileInfo2Stack.pop();
}

void IErrorContext::pushMsgType(const IMessageType::Enum newMsgType)
{
    // Occasionally, when the error context has already been set
    // to use a particular message type, we need to use a different
    // message type. This method overrides the current message type.
    // For example, see Inspector.checkSynonymsAgainstDLN().

    msgTypeStack.push(msgType);
    msgType = newMsgType;
}

void IErrorContext::popMsgType()
{
    // This method restores the original state of msgType. See
    // pushMsgType().

    msgType = msgTypeStack.pop();
}

