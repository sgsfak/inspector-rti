/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

#ifndef SETCHOOSER_H
#define SETCHOOSER_H

// Local headers

#include "dbutils.h"
#include "dirpattern.h"
#include "ienums.h"
#include "iexception.h"
#include "imageset.h"
#include "modelutils.h"

#include "setmanager.h"

// Qt headers

#include <QComboBox>
#include <QGroupBox>
#include <QHash>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QList>
#include <QListView>
#include <QModelIndex>
#include <QPushButton>
#include <QRadioButton>
#include <QStringListModel>
#include <QStringList>
#include <QTabWidget>
#include <QVariant>
#include <QVBoxLayout>
#include <QWidget>

/********************************************************************/
/*                                                                  */
/*  FileSet struct                                                  */
/*                                                                  */
/********************************************************************/

// This struct is used to contain information about a set of image
// files (archival or processed) that we use to build an image set.

struct FileSet
{
    QString name;
    bool isArchival; // The file set can be archival or processed
    QString path;
    IFileType::Enum fileType;
    QList<QFileInfo> fileInfos;

    FileSet() :
        isArchival(false),
        fileType(IFileType::UNKNOWN)
    {

    }

    FileSet(QString name, bool isArchival, QString path, IFileType::Enum imageType, QList<QFileInfo> fileInfos) :
        name(name),
        isArchival(isArchival),
        path(path),
        fileType(imageType),
        fileInfos(fileInfos)
    {

    }
};

/********************************************************************/
/*                                                                  */
/*  ClickableLabel class                                            */
/*                                                                  */
/********************************************************************/

// This class is used on the Custom tab of the Photogrammetry Inspector
// to provide a clickable label for the directory pattern examples.

class ClickableLabel : public QLabel {
    Q_OBJECT

public:
    explicit ClickableLabel(QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
    ~ClickableLabel() override;

signals:
    void clicked();

protected:
    void mousePressEvent(QMouseEvent* event) override;

};

/********************************************************************/
/*                                                                  */
/*  SetChooser class                                                */
/*                                                                  */
/********************************************************************/

class SetChooser : public QGroupBox, protected ModelUtils, protected DBUtils
{
    Q_OBJECT

public:
    SetChooser(InspectorType::Enum inspectorType, const QList<DirPattern> & dirPatterns, bool withEditSet = true, QWidget *parent = nullptr);
    ~SetChooser() override;

    void selectBundle(int project_srl, int bundle_srl);


    // Accessor methods

    QString getProjectDirectory();
    QList<ImageSet> getSelectedImageSets();

    // Method overrides

    void showEvent(QShowEvent* e) override;

signals:

public slots:

    // Without database

    void chooseDirectory();
    void checkDirectory();
    void radioButtonClicked();
    void exampleClicked();
    void toggleChanged(int index);

    // With database

    void projectChanged(int projectIndex);
    void groupChanged(int groupIndex);

private:

    // GUI member variables

    // Inspector Type:
    InspectorType::Enum inspectorType;

    // Without database

    QComboBox * toggleComboBox;   // Photogrammetry only
    QFrame * browserFrame;
    QLineEdit * directoryEditBox;
    QPushButton * dirBrowseButton;
    QList<QRadioButton *> radioButtons;
    QList<ClickableLabel *> exampleLabels;
    QHash<ClickableLabel *, int> examplesHash;
    SetManager * setManager;

    bool withEditSet; // Enable setManager? Active only for PG..

    // With database

    QFrame * comboBoxFrame;
    QComboBox * projectComboBox;
    QComboBox * groupComboBox;

    // Independent of database use

    QListView * imageSetsListView;

    // Non-GUI member variables

    const QList<DirPattern> & dirPatterns;
    QStringListModel * imageSetsModel;
    QString projectDirectory;
    QHash<QString, ImageSet> imageSetsHash;
    int currRadioIndex;

    // GUI initialization methods

    void initGUI();

    // Toggle layout

    QHBoxLayout * initToggleLayout(); // Photogrammetry only

    // Top area - directory browser or project/group combo boxes

    QFrame * initBrowserFrame();
    QGroupBox * initDirectoryPatterns();

    QFrame * initComboBoxFrame();
    void initComboBox(ITableType::Enum tableType, int parentSrl, QComboBox *comboBox);

    // Sets list

    QLayout * initListLayout();

    // Connect slots

    void setConnections();

    // Methods to support GUI slots.

    void updateGroupComboBox(int projectIndex, const QComboBox * projectComboBox, QComboBox * groupComboBox);
    int getSelectedRadioButton();
    DirPattern getSelectedPattern();

    // Methods to update the chooser - without database.

    void getImageSetsByDir(const QString &dirName, DirPattern pattern, IFileType::Enum archivalType);
    void searchDir(QHash<QString, FileSet> & fileSets, const QString & dirName, const QList<Pattern> & patterns, int patternIndex, const QString & currentSetName, IFileType::Enum currentFileType, IFileType::Enum archivalType);
    QString getSetName(const QDir & dir, const Pattern & pattern, const QString & currentSetName);
    IFileType::Enum getFileType(const QDir &dir, const Pattern & pattern, IFileType::Enum currentFileType);
    void addFileSet(QHash<QString, FileSet> & fileSets, const QDir & dir, const QString & setName, IFileType::Enum fileType, IFileType::Enum archivalType);
    bool getFileSets(const QString & setName, const QList<FileSet> & candidateSets, FileSet & archivalSet, FileSet & processedSet);

    // Methods to update the chooser - with database.

    static ImageSet newImageSet(int imageSetSrl, const QString & setName, const QString & archivalPath, const QList<QFileInfo> & archivalInfos, IFileType::Enum archivalType, const QString & processedPath, const QList<QFileInfo> & processedInfos, IFileType::Enum processedType);
    void getImageSetsByDB(const QComboBox * projectComboBox, const QComboBox * groupComboBox);
    int getSerial(const QComboBox * comboBox);
    QString getPath(const QComboBox * comboBox);
    void addImageSet(const ImageSet&);
    static IFileType::Enum getTypeFromFiles(const QString & path, const QString & setName);

    // Utility methods.

    void clearModels();
public:
    static QList<ImageSet> getImageSetsByDB(int projectSrl, int groupSrl, QString& projectDir);
    static QString getProjectPathDB(int projectSrl);
};

#endif // SETCHOOSER_H
