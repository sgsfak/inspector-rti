/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

#ifndef IEXCEPTION_H
#define IEXCEPTION_H

#include <QString>
#include <QSqlError>

// We use different exceptions to identify what caused an error. We
// probably don't need to do this, but we're adding it in case it
// becomes relevant in the future. (Currently, all processing is
// done on IException.)

class IException
{
public:
    IException(){}
    IException(QString title, QString msg);
    ~IException();

    QString title;
    QString msg;
};

class IOException : public IException
{
public:
    IOException(QString title, QString msg);
    ~IOException() {}
};

class XMLException : public IException
{
public:
    XMLException(QString title, QString msg);
    ~XMLException() {}
};

class XMPException : public IException
{
public:
    XMPException(QString title, QString msg);
    ~XMPException() {}
};

class DBException : public IException
{
public:
    DBException(QString title, QString msg);
    DBException(QString title, QSqlError e);

    ~DBException() {}
};

class InspectorException : public IException
{
public:
    InspectorException(QString title, QString msg);
    ~InspectorException() {}
};

#endif // IEXCEPTION_H
