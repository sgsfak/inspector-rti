/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

// Local headers

#include "dirpattern.h"
//#include "ienums.h"
#include "iexception.h"
#include "iglobals.h"

// Qt headers

#include <QByteArray>
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonParseError>
#include <QJsonValue>
#include <QStack>
#include <QCoreApplication>

/********************************************************************/
/*                                                                  */
/*  DirPatternFactory class                                         */
/*                                                                  */
/********************************************************************/

QList<DirPattern> DirPatternFactory::getDirPatterns(InspectorType::Enum inspectorType)
{
    // Build the name of the JSON file and open it.

    QString suffix = (inspectorType == InspectorType::RTI) ? "-RTI" : "-Photo";

#if defined(Q_OS_OSX)
    QString resourcesDir = QCoreApplication::applicationDirPath() +"/../Resources";
#else
    QString resourcesDir = QCoreApplication::applicationDirPath();
#endif
    QString filename("DirPatterns" + suffix + ".json");
    QFile file(resourcesDir + "/" + filename);
    if (!file.open(QIODevice::ReadOnly))
    {
        QString exeName = (inspectorType == InspectorType::RTI) ?
                    "DLN-Inspector-RTI.exe" : "DLN-Inspector-PG.exe";
        throw IOException(QString("Directory patterns initialization error"), QString("Required file ").append(filename).append(" could not be opened. This file must be in the same directory as ").append(exeName).append("."));
    }

    // Read the JSON file and parse it into a QJsonDocument.

    QByteArray docByteArray = file.readAll();
    QJsonParseError error;
    QJsonDocument jsonDoc = QJsonDocument::fromJson(docByteArray, &error);
    if (error.error != QJsonParseError::NoError)
    {
        throw IOException("JSON parse error", "Error parsing " + filename + ": " + error.errorString());
    }

    // Traverse the QJsonDocument and build the list of DirPatterns.

    QList<DirPattern> dirPatterns;
    try
    {
        dirPatterns = parseDocument(jsonDoc);
        for (int i = 0; i < dirPatterns.size(); i++)
        {
            checkDirPattern(dirPatterns[i]);
        }
    }
    catch (IOException e)
    {
        throw IOException(e.title, "Error parsing " + filename + ". " + e.msg);
    }

    // Return the list of DirPatterns.

    return dirPatterns;
}

QList<DirPattern> DirPatternFactory::parseDocument(const QJsonDocument & doc)
{
    // Check that the JSON document is an object, not an array.

    if (!doc.isObject())
    {
        throw IOException("JSON error", "The top level must be a single JSON object that contains the DirPatterns name/value pair.");
    }

    // Parse the object.

    return parseDirPatterns(doc.object());
}

QList<DirPattern> DirPatternFactory::parseDirPatterns(const QJsonObject & topObject)
{
    // Get the DirPatterns property value and convert it to an array.

    QJsonValue dirPatternsValue = getPropertyValue(topObject, "DirPatterns", "The top-level object");
    QJsonArray dirPatternsArray = getArray(dirPatternsValue, 1, "The value of the DirPatterns property");

    // Parse each object in the array.

    QList<DirPattern> dirPatterns;
    for (int i = 0; i < dirPatternsArray.size(); i++)
    {
        DirPattern dirPattern = parseDirPattern(dirPatternsArray[i]);
        dirPatterns.append(dirPattern);
    }

    // Return the list of DirPatterns.

    return dirPatterns;
}

DirPattern DirPatternFactory::parseDirPattern(const QJsonValue & dirPatternValue)
{
    // Convert the array item to an object.

    QJsonObject dirPatternObject = getObject(dirPatternValue, "Each item in the DirPatterns array");

    // Get the properties in the object.

    DirPattern dirPattern;
    dirPattern.summary = getStringProperty(dirPatternObject, "Summary", "Each object in the DirPatterns array");
    dirPattern.example = parseExample(dirPatternObject);
    dirPattern.patterns = parsePatterns(dirPatternObject);

    // Return the DirPattern object.

    return dirPattern;
}

QString DirPatternFactory::parseExample(const QJsonObject & dirPatternObject)
{
    // Get the Patterns property value and convert it to an array.

    QJsonValue exampleValue = getPropertyValue(dirPatternObject, "Example", "Each object in the DirPatterns array");
    QJsonArray exampleArray = getArray(exampleValue, 1, "Each Example property");

    // Concatenate the items in the Example array, separated by newlines.

    QString example;
    for (int i = 0; i < exampleArray.size(); i++)
    {
        QString exampleStr = getString(exampleArray[i], "Each value in the Example property array");
        if (i != 0)
        {
            example.append("\n");
        }
        example.append(exampleStr);
    }

    // Return the string.

    return example;
}

QList<Pattern> DirPatternFactory::parsePatterns(const QJsonObject & dirPatternObject)
{
    // Get the Patterns property value and convert it to an array.

    QJsonValue patternsValue = getPropertyValue(dirPatternObject, "Patterns", "Each object in the DirPatterns array");
    QJsonArray patternsArray = getArray(patternsValue, 1, "Each Patterns property");

    // Parse each object in the array.

    QList<Pattern> patterns;
    for (int i = 0; i < patternsArray.size(); i++)
    {
        Pattern pattern = parsePattern(patternsArray[i]);
        patterns.append(pattern);
    }

    // Return the list of Patterns.

    return patterns;
}

Pattern DirPatternFactory::parsePattern(const QJsonValue & patternValue)
{
    // Convert the item in the array to an object.

    QJsonObject patternObject = getObject(patternValue, "Each item in the Patterns array");

    // Get the properties in the object.

    Pattern pattern;
    QString contains = getStringProperty(patternObject, "Contains", "Each object in the Patterns array");
    checkContains(contains);
    pattern.hasFiles = (contains == "Files");

    QString hasSetName = getStringProperty(patternObject, "HasSetName", "Each object in the Patterns array");
    checkYesNo("HasSetName", hasSetName);
    pattern.hasSetName = (hasSetName == "Yes");

    QString hasTypeName = getStringProperty(patternObject, "HasTypeName", "Each object in the Patterns array");
    checkYesNo("HasTypeName", hasTypeName);
    pattern.hasTypeName = (hasTypeName == "Yes");

    pattern.typeRegexes = parseRegexes(patternObject);

    // Return the Pattern.

    return pattern;
}

QHash<QString, QRegExp> DirPatternFactory::parseRegexes(const QJsonObject & patternObject)
{
    // Get the TypeRegexes property value. Note that this property is
    // optional, so we don't throw an error if it isn't present.

    QHash<QString, QRegExp> regexes;
    QJsonValue regexesValue;
    try
    {
        regexesValue = getPropertyValue(patternObject, "TypeRegexes", "");
    }
    catch (IOException e)
    {
        // The TypeRegexes property is optional.

        return regexes;
    }

    // Convert the value to an array and parse each object in the array.

    QJsonArray regexesArray = getArray(regexesValue, 1, "The value of the TypeRegexes property");
    for (int i = 0; i < regexesArray.size(); i++)
    {
        // Get the TypeRegex.

        TypeRegex typeRegex = parseTypeRegex(regexesArray[i]);

        // Check that there isn't already a TypeRegex for this file type.
        if (regexes.contains(typeRegex.typeName))
        {
            throw IOException("JSON error", "The TypeRegexes property contains more than one regular expression for the type " + typeRegex.typeName);
        }

        // Insert the type name and regex in the hash table.

        regexes.insert(typeRegex.typeName, typeRegex.regex);
    }

    // Return the hash table.

    return regexes;
}

TypeRegex DirPatternFactory::parseTypeRegex(const QJsonValue & typeRegexValue)
{
    // Convert the item value to an object.
    QJsonObject typeRegexObject = getObject(typeRegexValue, "Each item in the TypeRegexes array");

    // Get the properties of the object.

    TypeRegex typeRegex;
    QString typeName = getStringProperty(typeRegexObject, "Type", "Each object in the TypeRegexes array");
    checkTypeName("Type", typeName);
    typeRegex.typeName = typeName;

    QString regexStr = getStringProperty(typeRegexObject, "Regex", "Each object in the TypeRegexes array");
    typeRegex.regex.setPattern(regexStr);
    typeRegex.regex.setCaseSensitivity(Qt::CaseInsensitive);

    // Return the object.

    return typeRegex;
}

/********************************************************************/
/*                                                                  */
/*  Utility methods                                                 */
/*                                                                  */
/********************************************************************/

QJsonObject DirPatternFactory::getObject(const QJsonValue & value, const QString & description)
{
    if (!value.isObject())
    {
        throw IOException("JSON error", description + " must be an object.");
    }
    return value.toObject();
}

QJsonArray DirPatternFactory::getArray(const QJsonValue & value, int minSize, const QString & description)
{
    if (!value.isArray())
    {
        throw IOException("JSON error", description + " must be an array.");
    }

    QJsonArray array = value.toArray();
    if (array.size() < minSize)
    {
        throw IOException("JSON error", description + " must contain at least one item.");
    }

    return array;
}

QJsonValue DirPatternFactory::getPropertyValue(const QJsonObject & object, const QString & propName, const QString & description)
{
    if (!object.contains(propName))
    {
        throw IOException("JSON error", description + " must contain the " + propName + " property.");
    }

    return object[propName];
}

QString DirPatternFactory::getStringProperty(const QJsonObject & object, const QString & propName, const QString & description)
{
    QJsonValue value = getPropertyValue(object, propName, description);
    return getString(value, "The value of the " + propName + " property");
}

QString DirPatternFactory::getString(const QJsonValue & value, const QString & description)
{
    if (!value.isString())
    {
        throw IOException("JSON error", description + " must be a string.");
    }

    return value.toString();
}

/********************************************************************/
/*                                                                  */
/*  Checking methods                                                */
/*                                                                  */
/********************************************************************/

void DirPatternFactory::checkContains(const QString & contains)
{
    if ((contains == "Dirs") || (contains == "Files")) return;
    throw IOException("JSON error", "The value of the Contains property must be 'Dirs' or 'Files'.");
}

void DirPatternFactory::checkYesNo(const QString & propName, const QString & yesNo)
{
    if ((yesNo == "No") || (yesNo == "Yes")) return;
    throw IOException("JSON error", "The value of the " + propName + " property must be 'Yes' or 'No'.");
}

void DirPatternFactory::checkTypeName(const QString & propName, const QString & typeName)
{
    if ((typeName == "JPEG") || (typeName == "DNG") || (typeName == "TIFF")) return;
    throw IOException("JSON error", "The value of the " + propName + " property must be 'JPEG', 'TIFF', or 'DNG'.");
}

void DirPatternFactory::checkDirPattern(const DirPattern & dirPattern)
{
    // Check the summary and example.

    if (dirPattern.summary.isEmpty())
    {
        throw IOException("JSON error", "The Summary property must not be empty.");
    }
    if (dirPattern.example.isEmpty())
    {
        throw IOException("JSON error", "The " + dirPattern.summary + " pattern has an empty Example property.");
    }

    // Check the patterns.

    bool typeNameFound(false), setNameFound(false);
    for (int i = 0; i < dirPattern.patterns.size(); i++)
    {
        // Check that only the last pattern has a value of "Files".

        Pattern pattern = dirPattern.patterns[i];
        if (pattern.hasFiles && (i != dirPattern.patterns.size() - 1))
        {
            throw IOException("JSON error", "Only the last object in the Patterns array can have a Contains property whose value is 'Files'");
        }

        if (!pattern.hasFiles && (i == dirPattern.patterns.size() - 1))
        {
            throw IOException("JSON error", "The last object in the Patterns array must have a Contains property whose value is 'Files'");
        }

        // HasTypeName tests.

        if (pattern.hasTypeName)
        {
            if (typeNameFound)
            {
                throw IOException("JSON error", "More than one object in the Patterns array has a HasTypeName property with a value of 'Yes'.");
            }
            else
            {
                typeNameFound = true;
            }
        }

        if (pattern.hasTypeName && (pattern.typeRegexes.size() == 0))
        {
            throw IOException("JSON error", "An object in the Patterns array has a HasTypeName property with a value of 'Yes' but does not have a TypeRegexes property.");
        }

        if (!pattern.hasTypeName && (pattern.typeRegexes.size() != 0))
        {
            throw IOException("JSON error", "An object in the Patterns array has a HasTypeName property with a value of 'No' and has a TypeRegexes property.");
        }

        // HasSetName tests.

        if (pattern.hasSetName)
        {
            if (setNameFound)
            {
                throw IOException("JSON error", "Only one item in the Patterns array may have a HasSetName attribute with the value 'Yes'.");
            }
            else
            {
                setNameFound = true;
            }
        }

        // Regex tests

        QList<QRegExp> typeRegexes = pattern.typeRegexes.values();
        for (int j = 0; j < typeRegexes.size(); j++)
        {
            QRegExp regex = typeRegexes[j];
            if (!regex.isValid())
            {
                throw IOException("JSON error", "An item in the TypeRegexes array contains an invalid regular expression: " + regex.errorString());
            }

            if (regex.isEmpty())
            {
                throw IOException("JSON error", "An item in the TypeRegexes array has an empty regular expression.");
            }
        }
    }

    // Check that we have a type name and a set name.

    if (!setNameFound)
    {
        throw IOException("JSON error", "Exactly one item in the Patterns array must have a HasSetName property with the value 'Yes'.");
    }

    if (!typeNameFound)
    {
        throw IOException("JSON error", "Exactly one item in the Patterns array must have a HasTypeName property with the value 'Yes'.");
    }
}
