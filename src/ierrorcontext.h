/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

#ifndef IERRORCONTEXT_H
#define IERRORCONTEXT_H

// Local headers

#include "ienums.h"

// Qt headers

#include <QFileInfo>
#include <QStack>

/********************************************************************/
/*                                                                  */
/*  IErrorContext class                                             */
/*                                                                  */
/********************************************************************/

// IErrorContext tracks the context in which the Inspector tests
// properties: the current project directory, image set, test group
// and names of the file(s) from which the property(ies) were taken,
// as well as the type of message a given error wil create.

class IErrorContext
{

public:
    IErrorContext();
    ~IErrorContext();

    // Methods

    void reset();
    void pushFileInfos(const QFileInfo & newFileInfo1, const QFileInfo & newFileInfo2);
    void popFileInfos();
    void pushMsgType(const IMessageType::Enum newMsgType);
    void popMsgType();

    // Variables

    QString projectDirectory;     // Set in inspect()
    QString imageSetName;         // Set in inspectSet(), updateDLNFile()
    int imageSetNum;              // Set in inspectSet(), updateDLNFile
    ITestGroup::Enum testGroup;   // Set in inspectFilenames(), inspectCaptureProps(), inspectXformProps(), updateDLNFile()
    QFileInfo fileInfo1;          // Set in getDLNValues(), getValues(), validateValues(), compareValueLists(), getNonNullSynonyms()
    QFileInfo fileInfo2;          // Set in compareValueLists()
    IMessageType::Enum msgType;   // Set in getDLNValues(), getValues(), validateValue(), checkRequiredValue(), compareValueLists(), getNonNullSynonyms()
                                  // Overridden in checkSynonymsAgainstDLN()

private:

    QStack<QFileInfo> fileInfo1Stack;
    QStack<QFileInfo> fileInfo2Stack;
    QStack<IMessageType::Enum> msgTypeStack;
};

#endif // IERRORCONTEXT_H
