/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2015                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

#ifndef RESULTBOX_H
#define RESULTBOX_H

// Local headers

#include "iresult.h"

// Qt headers

#include <QDialog>
#include <QHBoxLayout>

class ResultBox : public QDialog
{
    Q_OBJECT
public:
    ResultBox(const IResult &result, const QString &projectDirectory, QWidget *parent = 0);
    ~ResultBox();
    
signals:
    
public slots:

private:
    // Private variables

    IResult result;
    QString projectDirectory;

    // GUI initialization methods

    void initGUI();
    QHBoxLayout * initTitle();
    QHBoxLayout * initInfoLayout();
    QHBoxLayout * initDescriptionLayout();
    QHBoxLayout * initButtonLayout();
    
};

#endif // RESULTBOX_H
