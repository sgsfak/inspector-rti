/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

#ifndef IRESULT_H
#define IRESULT_H

// Local headers

#include "ienums.h"

// Qt headers

#include <QFileInfo>
#include <QString>

/********************************************************************/
/*                                                                  */
/*  IResult struct                                                  */
/*                                                                  */
/********************************************************************/

struct IResult
{
    // IResult is a simple structure to hold information about a
    // single inspection result.

    QString imageSetName;
    int imageSetNum;
    ITestGroup::Enum testGroup;       // File, Capture, Transformation, UpdateDLN
    IResultType::Enum resultType;     // OK, Warning, FileError, CaptureError, TransformationError, FatalError
    IMessageType::Enum msgType;       // CompareValues, RequiredValue, InvalidValue, Text, None
    QString title;
    QString description;
    QString propQName;                // For fields in structs, this is the full path.
    int severity;                     // The severity of the property check
    QFileInfo fileInfo1;              // When checking a single file, fileInfo1 is used.
    QFileInfo fileInfo2;              // When checking a single file, fileInfo2 is null.
    QString value1;                   // When checking required values, file value is here.
    QString value2;                   // When checking required values, required value is here.

    // Constructors

    IResult();

    IResult(const QString & imageSetName,
            int imageSetNum,
            ITestGroup::Enum testGroup,
            IResultType::Enum resultType,
            const QString & title,
            const QString & description);

    IResult(const QString & imageSetName,
            int imageSetNum,
            ITestGroup::Enum testGroup,
            IResultType::Enum resultType,
            IMessageType::Enum msgType,
            const QString & title,
            const QString & description,
            const QString & propQName,
            int severity,
            const QFileInfo & fileInfo1,
            const QFileInfo & fileInfo2,
            const QString & value1,
            const QString & value2);

    // Methods

    QString getMessage(bool html, bool lineBreaks) const;
    QString stripHTML(const QString & str1, bool lineBreaks) const;

    // Operators

    friend bool operator<(const IResult & lhs, const IResult & rhs);
    friend QString getLocalName(const QString & propQName);

};

#endif // IRESULT_H
