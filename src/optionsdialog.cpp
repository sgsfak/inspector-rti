/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

// Local headers

#include "optionsdialog.h"

// Qt headers

#include <QGroupBox>
#include <QIntValidator>
#include <QLabel>
#include <QMessageBox>
#include <QPushButton>

/********************************************************************/
/*                                                                  */
/*  Constructor and destructor                                      */
/*                                                                  */
/********************************************************************/

OptionsDialog::OptionsDialog(bool isStandalone, InspectorType::Enum inspectorType, Options & options, QWidget *parent) :
    QDialog(parent),
    isStandalone_(isStandalone),
    inspectorType(inspectorType),
    options(options)
{
    setModal(true);
    setWindowTitle("Options");

    initGUI();
}

OptionsDialog::~OptionsDialog()
{

}

/********************************************************************/
/*                                                                  */
/*  Methods to initialize the GUI                                   */
/*                                                                  */
/********************************************************************/

void OptionsDialog::initGUI()
{
    // Create the individual layouts.

    QVBoxLayout * checkBoxLayout = initCheckBoxLayout();
    QVBoxLayout * writeDLNButtonLayout = initRadioButtonLayout();
    QHBoxLayout * buttonLayout = initButtonLayout();

    // Create the dialog box layout.

    QVBoxLayout * layout = new QVBoxLayout();
    layout->addLayout(checkBoxLayout);
    layout->addSpacing(20);
    layout->addStretch();
    layout->addLayout(writeDLNButtonLayout);

    if (this->isStandalone_) {
        QHBoxLayout * epsilonLayout = initEpsilonLayout();
        layout->addSpacing(20);
        layout->addStretch();
        layout->addLayout(epsilonLayout);
    }
    layout->addSpacing(20);
    layout->addStretch();
    layout->addLayout(buttonLayout);

    // Set the layout.

    setLayout(layout);
}

QVBoxLayout * OptionsDialog::initCheckBoxLayout()
{
    // Create and set the check boxes for logging and stop on error.

    useDatabaseCheckBox = new QCheckBox("Use the DLN");
    useDatabaseCheckBox->setChecked(options.useDatabase);
    connect(useDatabaseCheckBox, SIGNAL(clicked()), this, SLOT(useDatabaseClicked()));

    loggingCheckBox = new QCheckBox("Log all tests to a file");
    loggingCheckBox->setChecked(options.logging);
    stopOnErrorCheckBox = new QCheckBox("Stop on error");
    stopOnErrorCheckBox->setChecked(options.stopOnError);

    // Add the check boxes to a layout.

    QVBoxLayout * checkBoxLayout = new QVBoxLayout();
    if (this->isStandalone_) {
        // If the Inspector runs in "integrated" mode (i.e. inside DLN)
        // then we always use the database. So the "Use the DLN"
        // option makes sense only in the standalone version:
        checkBoxLayout->addWidget(useDatabaseCheckBox);
    }
    checkBoxLayout->addWidget(loggingCheckBox);
    checkBoxLayout->addWidget(stopOnErrorCheckBox);

    // Return the layout.

    return checkBoxLayout;
}

QVBoxLayout * OptionsDialog::initRadioButtonLayout()
{
    // Create the radio buttons.

    successWarningErrorButton = new QRadioButton("Always (on success, warnings, or errors)");
    successWarningButton = new QRadioButton("On success or warnings");
    successButton = new QRadioButton("On success only");

    // Set the radio buttons

    if (options.writeToDLN & IStatus::Error)
        successWarningErrorButton->setChecked(true);
    else if (options.writeToDLN & IStatus::Warning)
        successWarningButton->setChecked(true);
    else
        successButton->setChecked(true);

    // Add the radio buttons to a group box. This makes them mutually exclusive.

    QGroupBox * writeButtonGroup = new QGroupBox("Write inspection results to DLN");
    QVBoxLayout * writeButtonLayout = new QVBoxLayout();
    writeButtonLayout->addWidget(successWarningErrorButton);
    writeButtonLayout->addWidget(successWarningButton);
    writeButtonLayout->addWidget(successButton);
    writeButtonGroup->setLayout(writeButtonLayout);

    // Create and set the remove results check box.

    removePreviousResultsCheckBox = new QCheckBox("Remove previous inspection results from DLN");
    removePreviousResultsCheckBox->setChecked(options.removePreviousResults);

    // Add everything to a layout.

    QVBoxLayout * radioButtonLayout = new QVBoxLayout();
    if (this->isStandalone_) {
        // If the Inspector runs in "integrated" mode (i.e. inside DLN)
        // then we always use the database. So the "write inspection results to DLN"
        // options make sense only in the standalone version:
        radioButtonLayout->addWidget(writeButtonGroup);
    }
    radioButtonLayout->addWidget(removePreviousResultsCheckBox);

    // Return the layout.

    return radioButtonLayout;
}

QHBoxLayout * OptionsDialog::initEpsilonLayout()
{
    // Create an edit box for the maximum allowable date-time difference.

    QLabel * epsilonLabel = new QLabel("Maximum hours between two date-times still considered equal.");
    epsilonLineEdit = new QLineEdit(QString::number(options.epsilon/(60 * 60)));
    epsilonLineEdit->setMaxLength(2);
    epsilonLineEdit->setMaximumWidth(30);
    QIntValidator * validator = new QIntValidator(0, 49);
    epsilonLineEdit->setValidator(validator);

    // Lay out the date-time difference.

    QHBoxLayout * epsilonLayout = new QHBoxLayout();
    epsilonLayout->addWidget(epsilonLabel);
    epsilonLayout->addWidget(epsilonLineEdit);

    // Return the layout.

    return epsilonLayout;
}

QHBoxLayout * OptionsDialog::initButtonLayout()
{
    QPushButton * saveButton = new QPushButton("Save");
    saveButton->isDefault();
    QPushButton * cancelButton = new QPushButton("Cancel");

    connect(saveButton, SIGNAL(clicked()), this, SLOT(saveClicked()));
    connect(cancelButton, SIGNAL(clicked()), this, SLOT(close()));

    QHBoxLayout * buttonLayout = new QHBoxLayout();
    buttonLayout->addStretch();
    buttonLayout->addWidget(saveButton);
    buttonLayout->addWidget(cancelButton);

    return buttonLayout;
}

/********************************************************************/
/*                                                                  */
/*  Slots                                                           */
/*                                                                  */
/********************************************************************/

void OptionsDialog::useDatabaseClicked()
{
    QMessageBox::warning(this, "Restart required", "You must restart the DLN:Inspector for this change to take effect.");
}

void OptionsDialog::saveClicked()
{
    // Set the values in the Options struct.

    options.useDatabase = useDatabaseCheckBox->isChecked();
    options.logging = loggingCheckBox->isChecked();
    options.stopOnError = stopOnErrorCheckBox->isChecked();

    if (successWarningErrorButton->isChecked())
        options.writeToDLN = IStatus::Error | IStatus::Warning | IStatus::Success;
    else if (successWarningButton->isChecked())
        options.writeToDLN = IStatus::Warning | IStatus::Success;
    else if (successButton->isChecked())
        options.writeToDLN = IStatus::Success;

    options.removePreviousResults = removePreviousResultsCheckBox->isChecked();

    // We convert epsilon to seconds because our date difference
    // calculation returns seconds.

    if (this->isStandalone_) {
        options.epsilon = epsilonLineEdit->text().toInt() * 60 * 60;
    }

    // Save the options to the options-(RTI|PG).db file.

    OptionsFactory factory(this->inspectorType);
    factory.toOptionsDB(options);

    // Close the Options dialog.

    close();
}

