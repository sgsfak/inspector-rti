#-------------------------------------------------
#
# Project created by QtCreator 2015-01-22T11:07:34
#
#-------------------------------------------------

QT      += core gui xmlpatterns xml sql
DEFINES += PHOTO_BUILD

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DLN-Inspector-PG
TEMPLATE = app

SOURCES += \
    dbutils.cpp \
    definesetdialog.cpp \
    dirpattern.cpp \
    ienums.cpp \
    ierrorcontext.cpp \
    iexception.cpp \
    imageset.cpp \
    inspector.cpp \
    iresult.cpp \
    main.cpp\
    mainwindow.cpp \
    modelutils.cpp \
    options.cpp \
    optionsdialog.cpp \
    propertyinfo.cpp \
    resultbox.cpp \
    resultbrowser.cpp \
    results.cpp \
    resultsdialog.cpp \
    setchooser.cpp \
    setmanager.cpp \
    xmpvalue.cpp

HEADERS  += \
    dbutils.h \
    definesetdialog.h \
    dirpattern.h \
    ienums.h \
    ierrorcontext.h \
    iexception.h \
    iglobals.h \
    imageset.h \
    inspector.h \
    inspectorxmp.h \
    iresult.h \
    mainwindow.h \
    modelutils.h \
    options.h \
    optionsdialog.h \
    propertyinfo.h \
    resultbox.h \
    resultbrowser.h \
    results.h \
    resultsdialog.h \
    setchooser.h \
    setmanager.h \
    xmpvalue.h

# These defines are required by the XMP SDK.

win32:DEFINES += WIN_ENV=1
mac:DEFINES += MAC_ENV=1

win32 {
win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../lib/xmpsdk/XMP-Toolkit-SDK-CC201412/public/libraries/windows/release/ -lXMPFilesStatic
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../lib/xmpsdk/XMP-Toolkit-SDK-CC201412/public/libraries/windows/debug/ -lXMPFilesStatic

INCLUDEPATH += $$PWD/../../../lib/xmpsdk/XMP-Toolkit-SDK-CC201412/public/include
DEPENDPATH += $$PWD/../../../lib/xmpsdk/XMP-Toolkit-SDK-CC201412/public/include

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../lib/xmpsdk/XMP-Toolkit-SDK-CC201412/public/libraries/windows/release/libXMPFilesStatic.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../lib/xmpsdk/XMP-Toolkit-SDK-CC201412/public/libraries/windows/debug/libXMPFilesStatic.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../lib/xmpsdk/XMP-Toolkit-SDK-CC201412/public/libraries/windows/release/XMPFilesStatic.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../lib/xmpsdk/XMP-Toolkit-SDK-CC201412/public/libraries/windows/debug/XMPFilesStatic.lib

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../lib/xmpsdk/XMP-Toolkit-SDK-CC201412/public/libraries/windows/release/ -lXMPCoreStatic
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../lib/xmpsdk/XMP-Toolkit-SDK-CC201412/public/libraries/windows/debug/ -lXMPCoreStatic

# INCLUDEPATH += $$PWD/../../../lib/xmpsdk/XMP-Toolkit-SDK-CC201412/public/include
# DEPENDPATH += $$PWD/../../../lib/xmpsdk/XMP-Toolkit-SDK-CC201412/public/include

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../lib/xmpsdk/XMP-Toolkit-SDK-CC201412/public/libraries/windows/release/libXMPCoreStatic.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../lib/xmpsdk/XMP-Toolkit-SDK-CC201412/public/libraries/windows/debug/libXMPCoreStatic.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../lib/xmpsdk/XMP-Toolkit-SDK-CC201412/public/libraries/windows/release/XMPCoreStatic.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../lib/xmpsdk/XMP-Toolkit-SDK-CC201412/public/libraries/windows/debug/XMPCoreStatic.lib
}

macx {
QMAKE_LFLAGS += -F$$PWD/../../../../../XMP-Toolkit-SDK-CC201607/public/libraries/macintosh/intel_64/Debug -framework XMPFiles
QMAKE_LFLAGS += -F$$PWD/../../../../../XMP-Toolkit-SDK-CC201607/public/libraries/macintosh/intel_64/Debug -framework XMPCore
INCLUDEPATH += $$PWD/../../../../../XMP-Toolkit-SDK-CC201607/public/include

COPY_FILES = $$files(*.xml)
COPY_FILES += $$files(*.json)
COPY_FILES += $$files(*.db)
COPY_FILES += $$files(*.sql)
COPY_FILES += $$files(*.sh)
APP_FILES.files = $$COPY_FILES
APP_FILES.path = Contents/MacOS
QMAKE_BUNDLE_DATA += APP_FILES

}
