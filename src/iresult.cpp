/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

// Local headers

#include "iresult.h"

/********************************************************************/
/*                                                                  */
/*  IResult methods                                                 */
/*                                                                  */
/********************************************************************/

/********************************************************************/
/*                                                                  */
/*  Constructors                                                    */
/*                                                                  */
/********************************************************************/

IResult::IResult()
{

}

IResult::IResult(const QString & imageSetName,
        int imageSetNum,
        ITestGroup::Enum testGroup,
        IResultType::Enum resultType,
        const QString & title, const QString &description) :
    imageSetName(imageSetName),
    imageSetNum(imageSetNum),
    testGroup(testGroup),
    resultType(resultType),
    msgType(IMessageType::None),
    title(title),
    description(description),
    propQName(),
    severity(1),
    fileInfo1(),
    fileInfo2(),
    value1(),
    value2()
{
}

IResult::IResult(const QString & imageSetName,
        int imageSetNum, ITestGroup::Enum testGroup,
        IResultType::Enum resultType,
        IMessageType::Enum msgType,
        const QString & title,
        const QString & description,
        const QString & propQName,
        int severity,
        const QFileInfo &filename1,
        const QFileInfo &filename2,
        const QString & value1,
        const QString & value2) :
    imageSetName(imageSetName),
    imageSetNum(imageSetNum),
    testGroup(testGroup),
    resultType(resultType),
    msgType(msgType),
    title(title),
    description(description),
    propQName(propQName),
    severity(severity),
    fileInfo1(filename1),
    fileInfo2(filename2),
    value1(value1),
    value2(value2)
{
}

/********************************************************************/
/*                                                                  */
/*  Public methods                                                  */
/*                                                                  */
/********************************************************************/

QString IResult::getMessage(bool html, bool lineBreaks) const
{
    // If we haven't stored any data (filenames or values),
    // just return the description.

    if ((msgType == IMessageType::Text) || (msgType == IMessageType::None))
        return html ? description : stripHTML(description, lineBreaks);

    // If we have stored data, build an error message in one of
    // the following formats, depending on the value of msgType:
    //
    // CompareValues (one property, two filenames, two values)
    // -------------------------------------------------------
    // <title>. <description>
    //
    // Property: <propQName>
    //
    // Filename 1: <filename1>
    // Value 1: <value1>
    //
    // Filename 2: <filename2>
    // Value 2: <value2>
    //
    // Restriction (one property, one filename, one value, one restriction)
    // --------------------------------------------------------------------
    // <title>. <description>
    //
    // Property: <propQName>
    //
    // Filename: <filename1>
    // Value: <value1>
    // Restriction: <value2>
    //
    // InvalidValue (one property, one filename, one value)
    // ----------------------------------------------------
    // <title>. <description>
    //
    // Property: <propQName>
    //
    // Filename: <filename1>
    // Value: <value1>
    //
    // Depending on the html and lineBreaks flags, the message will be
    // an HTML table, plain text with line breaks, or a single line
    // of plain text.

    QString lineEnd = lineBreaks ? "\n" : " ";
    QString fileLabel1 = IMessageType::getFilenameLabel(msgType, 1);
    QString fileLabel2 = IMessageType::getFilenameLabel(msgType, 2);
    QString valueLabel1 = IMessageType::getValueLabel(msgType, 1);
    QString valueLabel2 = IMessageType::getValueLabel(msgType, 2);

    QString msg;
    if (html)
    {
        // Build the introduction.

        msg.append("<p>").append(title).append(".");
        if (description.startsWith("<p>"))
            msg.append("</p>").append(description);
        else
            msg.append(" ").append(description).append("</p>");
        msg.append("<p></p>");

        // Start the table.

        msg.append("<table>");

        // List the property.

        msg.append("<tr><td>Property:</td><td>").append(propQName).append("</td></tr>");
        msg.append("<tr><td>&nbsp;</td><td>&nbsp;</td></tr>");

        // Build the first filename/value line. All message types have this line.

        msg.append("<tr><td>").append(fileLabel1).append(":</td><td>").append(fileInfo1.fileName()).append("</td></tr>");
        msg.append("<tr><td>").append(valueLabel1).append(":</td><td>").append(value1).append("</td></tr>");

        // Build the second filename line. Only CompareValues has this line.

        if (msgType == IMessageType::CompareValues)
        {
            msg.append("<tr><td>&nbsp;</td><td>&nbsp;</td></tr>");
            msg.append("<tr><td>").append(fileLabel2).append(":</td><td>").append(fileInfo2.fileName()).append("</td></tr>");
        }

        // Build the second property line. Only CompareValues and
        // Restriction have this line.

        if ((msgType == IMessageType::CompareValues) || (msgType == IMessageType::Restriction))
        {
            msg.append("<tr><td>").append(valueLabel2).append(":</td><td>").append(value2).append("</td></tr>");
        }

        // End the table.

        msg.append("</table>");
    }
    else
    {
        // Build the introduction.

        msg.append(title).append(". ").append(stripHTML(description, lineBreaks)).append(lineEnd);
        if (lineBreaks) msg.append(lineEnd).append(lineEnd);

        // List the property.

        msg.append("Property: ").append(propQName).append(lineEnd);
        if (lineBreaks) msg.append(lineEnd);

        // Build the first filename/value line. All message types have this line.

        msg.append(fileLabel1).append(": ").append(fileInfo1.fileName()).append(lineEnd);
        msg.append(valueLabel1).append(": ").append(stripHTML(value1, lineBreaks)).append(lineEnd);

        // Build the second filename line. Only CompareValues has this line.

        if (msgType == IMessageType::CompareValues)
        {
            if (lineBreaks) msg.append(lineEnd);
            msg.append(fileLabel2).append(": ").append(fileInfo2.fileName()).append(lineEnd);
        }

        // Build the second property line. Only CompareValues and
        // Restriction have this line.

        if ((msgType == IMessageType::CompareValues) || (msgType == IMessageType::Restriction))
        {
            msg.append(valueLabel2).append(": ").append(stripHTML(value2, lineBreaks)).append(lineEnd);
        }
    }

    return msg;
}

QString IResult::stripHTML(const QString & str1, bool lineBreaks) const
{
    // Make a copy of the string so we don't modify it.

    QString str2 = str1;

    // Strip HTML out of the QString. This is necessary for output
    // formats such as the log file that don't understand HTML.

    str2.remove("<p>")
        .remove("<table>")
        .remove("<tr>")
        .remove("<td>");

    // Replace HTML line-ending tags with newline or a space.

    if (lineBreaks)
        str2.replace("</p>", "\n")
            .replace("</table>", "\n")
            .replace("</tr>", "\n")
            .replace("</td>", " ")
            .replace("<br />", "\n");
    else
        str2.replace("</p>", " ")
            .replace("</table>", " ")
            .replace("</tr>", " ")
            .replace("</td>", " ")
            .replace("<br />", " ");

    // Replace lt entity reference with <.

    str2.replace("&lt;", "<");

    // Return the modified string.

    return str2;
}

/********************************************************************/
/*                                                                  */
/*  Friend functions                                                */
/*                                                                  */
/********************************************************************/

QString getLocalName(const QString & propQName)
{
    // This returns the local (non-namespace) part of the property
    // name. We use it to sort properties in local name order, rather
    // than namespace prefix and then local name order. Most users don't
    // care about or understand namespaces, so this is more natural
    // for them.

    return propQName.right(propQName.size() - propQName.lastIndexOf(":") - 1).toLower();
}
bool operator<(const IResult & lhs, const IResult & rhs)
{
    // The < operator is used to sort results.

    if (lhs.imageSetNum < rhs.imageSetNum)
        return true;
    else if (lhs.imageSetNum > rhs.imageSetNum)
        return false;
    else if (lhs.testGroup < rhs.testGroup)
        return true;
    else if (lhs.testGroup > rhs.testGroup)
        return false;
    else if (lhs.resultType < rhs.resultType)
        return true;
    else if (lhs.resultType > rhs.resultType)
        return false;
    else if (getLocalName(lhs.propQName) < getLocalName(rhs.propQName))
        return true;
    else if (getLocalName(lhs.propQName) > getLocalName(rhs.propQName))
        return false;
    else if (lhs.title < rhs.title)
        return true;
    else if (lhs.title > rhs.title)
        return false;
    else if (lhs.fileInfo1.fileName() < rhs.fileInfo1.fileName())
        return true;
    else if (lhs.fileInfo1.fileName() > rhs.fileInfo1.fileName())
        return false;
    else if (lhs.fileInfo2.fileName() < rhs.fileInfo2.fileName())
        return true;
    else if (lhs.fileInfo2.fileName() > rhs.fileInfo2.fileName())
        return false;
    else
        return false;
}

