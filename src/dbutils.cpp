/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

// Local headers

#include "dbutils.h"
#include "iexception.h"

// Qt headers

#include <QCryptographicHash>
#include <QUuid>
#include <QVariant>

#include <QDebug>

/********************************************************************/
/*                                                                  */
/*  Public methods                                                  */
/*                                                                  */
/********************************************************************/

DBUtils::DBUtils()
{

}

DBUtils::~DBUtils()
{

}

/********************************************************************/
/*                                                                  */
/*  Database methods                                                */
/*                                                                  */
/********************************************************************/

QSqlDatabase DBUtils::openDatabase(const QString &type, const QString &connectionName, const QString & dbName)
{
    QSqlDatabase db = QSqlDatabase::addDatabase(type, connectionName);
    db.setDatabaseName(dbName);
    if (!db.open())
    {
        throw DBException("Error opening " + dbName, db.lastError());
    }

    return db;
}

void DBUtils::startTransaction(QSqlDatabase & db)
{
    startTransaction(db, "DLN database");
}

void DBUtils::startTransaction(QSqlDatabase & db, const QString & dbName)
{
    if (!db.transaction())
    {
        throw DBException("Error starting transaction on " + dbName, db.lastError());
    }
}

void DBUtils::commitTransaction(QSqlDatabase & db)
{
    commitTransaction(db, "DLN database");
}

void DBUtils::commitTransaction(QSqlDatabase & db, const QString & dbName)
{
    if (!db.commit())
    {
        throw DBException("Error committing transaction on " + dbName, db.lastError());
    }
}

/********************************************************************/
/*                                                                  */
/*  Query methods                                                   */
/*                                                                  */
/********************************************************************/

void DBUtils::prepareQuery(QSqlQuery & q, const QString & query)
{
    prepareQuery(q, query, "DLN database");
}

void DBUtils::prepareQuery(QSqlQuery & q, const QString & query, const QString & dbName)
{
    q.setForwardOnly(true);
    if (!q.prepare(query))
    {
        qDebug() << __FILE__ << __LINE__ << q.lastError().text();
        throw DBException("Error preparing query for " + dbName, q.lastError());
    }
}

void DBUtils::execPrepared(QSqlQuery & q)
{
    execPrepared(q, "DLN database");
}

void DBUtils::execPrepared(QSqlQuery & q, const QString & dbName)
{
    if (!q.exec())
    {
        qDebug() << __FILE__ << __LINE__ << q.lastError().text();
        throw DBException("Error executing prepared query on " + dbName, q.lastError());
    }
}

void DBUtils::execQuery(QSqlQuery & q, const QString & query)
{
    execQuery(q, query, "DLN database");
}

void DBUtils::execQuery(QSqlQuery & q, const QString & query, const QString & dbName)
{
    q.setForwardOnly(true);
    if (!q.exec(query))
    {
        qDebug().noquote() << "DB error: " << q.lastError() << " Query:\n" << q.lastQuery();
        throw DBException("Error executing query on " + dbName, q.lastError());
    }
}

/********************************************************************/
/*                                                                  */
/*  Methods for working with filelists                              */
/*                                                                  */
/********************************************************************/

/********************************************************************/
/*  Adding filelists                                                */
/********************************************************************/

int DBUtils::addFilelist(QSqlQuery & q, const QString & hash, const QList<QFileInfo> &fileInfos, const QHash<QFileInfo, ImageSet::DocumentIds> &docIds, int fileType, const QString &filelistType)
{
    // Insert a new row into filelist.

    QString query = "INSERT INTO filelist "
                    "(filepath_hash, filelist_uuid, file_type, filelist_type) "
                    "VALUES ('" + hash + "', '" + QUuid::createUuid().toString() + "', " +
                             QString::number(fileType) + ", '" + filelistType + "')";
    execQuery(q, query);

    // Retrieve the key of the newly inserted row. For more information,
    // see the documentation and:
    //
    //   https://stackoverflow.com/questions/48342280/qt-qpsql-table-insert-return-column

    int filelistSrl = q.lastInsertId().toInt();
    q.finish();

    // Loop through the QFileInfos and add rows for each file
    // to filepaths.

    query = "INSERT INTO filepath (filelist_srl, filepath, filepath_uuid, origdoc_uuid) "
            "VALUES (" + QString::number(filelistSrl) + ", ?, ?, ?)";
    prepareQuery(q, query);
    for (const QFileInfo& finfo: fileInfos)
    {
        q.bindValue(0, finfo.canonicalFilePath());
        ImageSet::DocumentIds dids = docIds.value(finfo);
        if (dids.origDocumentId != "") {
            q.bindValue(1, dids.documentId );
            q.bindValue(2, dids.origDocumentId);
        }
        else {
            // If no XMP DocumentIds found, create a random one (?)
            QString uuid = QUuid::createUuid().toString();
            q.bindValue(1, uuid);
            q.bindValue(2, uuid);
        }

        execPrepared(q);
    }
    q.finish();

    // Return the filelist srl.

    return filelistSrl;
}

void DBUtils::addFilelistVsOther(QSqlQuery & q, const QString & vsTable, const QString & otherColumn, int otherSrl, int filelistSrl)
{
    // Insert a row into the table used to join cptset and filelist.

    QString query = "INSERT INTO " + vsTable + "(" + otherColumn + ", filelist_srl) "
                    "VALUES (" + QString::number(otherSrl) + ", " +
                             QString::number(filelistSrl) + ")";
    execQuery(q, query);
    q.finish();
}

/********************************************************************/
/*  Deleting filelists                                               */
/********************************************************************/

void DBUtils::deleteFilelistVsOther(QSqlQuery & q, const QString & vsTable, int vsSrl)
{
    // Remove the association between the image set or inspection and the filelist.

    QString query = "DELETE FROM " + vsTable + " WHERE srl = " + QString::number(vsSrl);
    execQuery(q, query);
    q.finish();
}

void DBUtils::deleteFilelist(QSqlQuery & q, int filelistSrl)
{
    // Delete the rows for the paths in the file list from filepath.

    QString query = "DELETE FROM filepath "
                    "WHERE filelist_srl = " + QString::number(filelistSrl);
    execQuery(q, query);
    q.finish();

    // Delete the row for the file list from filelist.

    query = "DELETE FROM filelist "
            "WHERE srl = " + QString::number(filelistSrl);
    execQuery(q, query);
    q.finish();
}

/********************************************************************/
/*  Filelist utilities                                              */
/********************************************************************/

bool DBUtils::getFilelistForOther(QSqlQuery & q, const QString & vsTable, const QString & otherColumn, int otherSrl, const QString & filelistType, QString & hash, int & vsSrl, int & filelistSrl)
{
    // Query the association and filelist tables to see if this
    // image set or inspection already has a list of (archival or
    // processed) files.

    QString query = "SELECT fl.filepath_hash, vs.srl, fl.srl "
                    "FROM "+ vsTable + " vs, filelist fl "
                    "WHERE vs." + otherColumn + " = " + QString::number(otherSrl) + " AND "
                    "vs.filelist_srl = fl.srl AND "
                    "fl.filelist_type = '" + filelistType + "'";
    execQuery(q, query);

    // If there is a row, get the stored hash, the srl of the row in
    // the vs table, and the srl of the filelist.

    if (q.first())
    {
        hash = q.value(0).toString();
        vsSrl = q.value(1).toInt();
        filelistSrl = q.value(2).toInt();
    }
    q.finish();

    // Return whether there is an existing filelist.

    return (!hash.isEmpty());
}

int DBUtils::getFilelistSrl(QSqlQuery & q, const QString & hash, const QString &filelistType)
{
    // Check if a filelist exists with this hash and type.

    QString query = "SELECT srl FROM filelist "
                    "WHERE filepath_hash = '" + hash +
                    "' AND filelist_type = '" + filelistType + "'";
    execQuery(q, query);

    // If there is a row, get the filelist srl.

    int filelistSrl(-1);
    if (q.first())
    {
        filelistSrl = q.value(0).toInt();
    }
    q.finish();

    // Return the filelist srl.

    return filelistSrl;
}

int DBUtils::getFilelistRefCount(QSqlQuery & q, int filelistSrl)
{
    // Check if the filelist is associated with another image set or
    // inspection results.

    QString query = "SELECT COUNT(filelist_srl) FROM "
                    "(SELECT filelist_srl FROM cptset_vs_filelist "
                      "WHERE filelist_srl = " + QString::number(filelistSrl) +
                      " UNION ALL "
                      "SELECT filelist_srl FROM inspection_vs_filelist "
                      "WHERE filelist_srl = " + QString::number(filelistSrl) +
                    ") AS a";
    execQuery(q, query);

    // Get the count.

    int count(0);
    if (q.first())
    {
        count = q.value(0).toInt();
    }
    else
    {
        // If the query does not return a row, it's a bug.

        throw InspectorException("Programming error", "Invalid query getting reference count for filelist.");
    }
    q.finish();

    // Return the count.

    return count;
}

/********************************************************************/
/*                                                                  */
/*  Utility methods                                                 */
/*                                                                  */
/********************************************************************/

// Pass fileInfos by value _on purpose_ since we alter it (sort it):
QString DBUtils::getHash(QList<QFileInfo> fileInfos)
{
    if (fileInfos.size() == 0) return QString();

    // Compute a hash for a set of files by:
    //
    // a) Lower-casing the file paths (Windows only)
    // b) Sorting the file paths alphabetically
    // c) Concatenating the file paths.
    // d) Computing the hash.
    //
    // Computing a hash allows us to quickly determine whether a
    // particular set of files is already stored in the filelist
    // and filepath tables. (The hash is stored in filelist.)
    //
    // The alternative -- retrieving paths from the filepath table
    // and comparing them one-by-one -- would take much longer.

    // Sort the file paths by their canonical paths. (Use lower-case paths
    // on Windows. For more information, see Inspector.sortFiles().)

    std::sort(fileInfos.begin(), fileInfos.end(), comparePaths);

    // Concatenate the file paths.

    QString s, p;
    for (int i = 0; i < fileInfos.size(); i++)
    {
        p = fileInfos[i].canonicalFilePath();
#ifdef Q_OS_WIN
        p = p.toLower();
#endif
        s.append(p);
    }

    // Return the hash.

    return QString(QCryptographicHash::hash(s.toUtf8(), QCryptographicHash::Md5).toHex());
}

bool DBUtils::comparePaths(const QFileInfo &fileInfo1, const QFileInfo &fileInfo2)
{
    return (fileInfo1.canonicalFilePath().compare(fileInfo2.canonicalFilePath()) < 0);
}
