/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

// Local headers

#include "definesetdialog.h"
#include "iglobals.h"

// Qt headers

#include <QFileDialog>
#include <QLabel>
#include <QMessageBox>

#include <QDebug>

/********************************************************************/
/*                                                                  */
/*  Public methods                                                  */
/*                                                                  */
/********************************************************************/

/********************************************************************/
/*                                                                  */
/*  Constructor and destructor                                      */
/*                                                                  */
/********************************************************************/

DefineSetDialog::DefineSetDialog(QWidget *parent) :
    QDialog(parent)
{
    init();
}

DefineSetDialog::~DefineSetDialog()
{
    // Delete the model(s) used by the view class(es). Because multiple view classes
    // can share the same model, the view classes do not delete model objects.
    // Instead, the programmer must delete model objects explicitly.

    delete archivalFilesModel;
    archivalFilesModel = nullptr;
    delete processedFilesModel;
    processedFilesModel = nullptr;
}

/********************************************************************/
/*                                                                  */
/*  Accessor methods                                                */
/*                                                                  */
/********************************************************************/

void DefineSetDialog::setImageSet(const ImageSet & imageSet)
{
    // Set the image set name and set it to read-only

    nameEditBox->setText(imageSet.name);
    nameEditBox->setEnabled(false);

    // Save the archival and processed paths. We only care about these
    // when we are using the DLN database and want to constrain file
    // selection to the directories stored in the database, if any.
    //
    // We are on somewhat shaky ground here as we are counting on
    // the calling module (SetManager) to call this method, rather
    // than passing the paths on the constructor. This works now
    // because, when using the DLN database, we only support the
    // SetManager.Edit button, which calls this when clicked. It
    // would not work if the Add button wanted to constrain files
    // to a particular database. In other words, we are not black-
    // boxing this functionality -- DefineSetDialog depends on SetManager
    // doing the right thing.

    if (usingDB)
    {
        imageSetSrl = imageSet.srl;
        archivalPath = imageSet.archivalPath;
        processedPath = imageSet.processedPath;
    }

    // Set the image type.

    if (imageSet.processedType == IFileType::JPEG)
    {
        jpegButton->setChecked(true);
    }
    else if (imageSet.processedType == IFileType::TIFF)
    {
        tiffButton->setChecked(true);
    }
//    else if (imageSet.imageType == IFileType::PNG)
//    {
//        pngButton->setChecked(true);
//    }

    // Add the processed files to the processed files list, then sort it.

    for (int i = 0; i < imageSet.processedFileInfos.size(); i++)
    {
        addRow(processedFilesModel, 0, QVariant(imageSet.processedFileInfos[i].filePath()));
    }
    processedFilesModel->sort(0);

    // Add the archival files to the archival files list, then sort it.

    for (int i = 0; i < imageSet.archivalFileInfos.size(); i++)
    {
        addRow(archivalFilesModel, 0, QVariant(imageSet.archivalFileInfos[i].filePath()));
    }
    archivalFilesModel->sort(0);
}

ImageSet DefineSetDialog::getImageSet()
{
    // Create an ImageSet object. We create a new image set because:
    //
    // a) When called to add a new image set, there is no existing image set.
    //
    // b) When called to edit an existing image set, we do not want to
    //    modify the passed-in image set because the user could cancel and
    //    we don't want to back out changes.

    ImageSet imageSet;
    imageSet.srl = imageSetSrl;
    imageSet.name = nameEditBox->text();
    imageSet.archivalPath = archivalPath;
    imageSet.archivalType = IFileType::DNG;
    imageSet.processedPath = processedPath;
    imageSet.processedType = getProcessedType();

    // Get the list of processed files and add a QFileInfo
    // object for each.

    QStringList processedFilePaths = getAllData(processedFilesModel);
    for (int i = 0; i < processedFilePaths.size(); i++)
    {
        imageSet.processedFileInfos.append(QFileInfo(processedFilePaths[i]));
    }

    // Get the list of archival files and add a QFileInfo
    // object for each.

    QStringList archivalFilePaths = getAllData(archivalFilesModel);
    for (int i = 0; i < archivalFilePaths.size(); i++)
    {
        imageSet.archivalFileInfos.append(QFileInfo(archivalFilePaths[i]));
    }

    // Return the ImageSet.

    return imageSet;
}

/********************************************************************/
/*                                                                  */
/*  Slots                                                           */
/*                                                                  */
/********************************************************************/

void DefineSetDialog::fileTypeButtonChanged()
{
    // When the file type button changes, clear the list of
    // processed files and change the list title.

    clearModel(processedFilesModel);
    processedGroupBox->setTitle(getProcessedTypeName() + " Files");
}

void DefineSetDialog::addProcessedFiles()
{
    addFiles(processedFilesModel, getProcessedType());
}

void DefineSetDialog::addArchivalFiles()
{
    addFiles(archivalFilesModel, IFileType::DNG);
}

void DefineSetDialog::removeProcessedFiles()
{
    removeSelectedRows(processedFilesList, processedFilesModel);
}

void DefineSetDialog::removeArchivalFiles()
{
    removeSelectedRows(archivalFilesList, archivalFilesModel);
}

void DefineSetDialog::okClicked()
{
    // Check that the user entered an image set name.

    if (nameEditBox->text().size() == 0)
    {
        QMessageBox::warning(this, "Need " + imageSetTermLC + " set name", "You must enter an " + imageSetTermLC + " set name.");
        return;
    }

    // Check that the name does not include an asterisk. We use
    // asterisks as field delimiters when saving image set
    // definitions to a file.

    QString name = nameEditBox->text();
    if (name.indexOf("*") != -1)
    {
        QMessageBox::warning(this, "Invalid " + imageSetTermLC + " set name", "The " + imageSetTermLC + " set name may not include an asterisk (*).");
        return;
    }

    // Check that the user entered one or more processed files. (Archival
    // files are optional.)

    if (processedFilesModel->rowCount() == 0)
    {
        QMessageBox::warning(this, "Need " + getProcessedTypeName() + " files", "You must add one or more " + getProcessedTypeName() + " files to the " + imageSetTermLC + " set.");
        return;
    }

    // NOTE: This check has been removed because this is a common
    // and acceptable situation. For example, it is common -- and
    // even desirable -- for the list of archival files to include a
    // color card. The discrepancy in numbers of files will result
    // in a warning during the inspection.

    // If the user entered archival files, check that they entered
    // the same number of archival and processed files.

/*    if (archivalFilesModel->rowCount() != 0)
    {
        if (archivalFilesModel->rowCount() != processedFilesModel->rowCount())
        {
            QMessageBox::warning(this, "Different numbers of files", "If you add archival files, you must add the same number of archival files as " + getImageTypeName() + " files.");
            return;
        }
    }*/

    // Close the dialog.

    accept();
}

/********************************************************************/
/*                                                                  */
/*  Private methods                                                 */
/*                                                                  */
/********************************************************************/

/********************************************************************/
/*                                                                  */
/*  Methods to initialize the GUI                                   */
/*                                                                  */
/********************************************************************/

void DefineSetDialog::initGUI()
{
    // Create the layouts in the dialog.

    QHBoxLayout * titleLayout = initTitleLayout();
    QGridLayout * nameAndFileTypeLayout = initNameAndFileTypeLayout();
    QHBoxLayout * fileListsLayout = initFileListsLayout();
    QHBoxLayout * buttonLayout = initButtonsLayout();

    // Create a vertical layout for the dialog.

    QVBoxLayout * defineLayout = new QVBoxLayout();
    defineLayout->addLayout(titleLayout);
    defineLayout->addLayout(nameAndFileTypeLayout);
    defineLayout->addLayout(fileListsLayout);
    defineLayout->addLayout(buttonLayout);

    // Set the dialog's layout.

    setLayout(defineLayout);
}

QHBoxLayout * DefineSetDialog::initTitleLayout()
{
    // Create the title and set the font.

    QFont font("Times New Roman", 16);
    QLabel * title = new QLabel("Define " + imageSetTermIC + " Set");
    title->setFont(font);

    // Lay out the title and center it.

    QHBoxLayout * titleLayout = new QHBoxLayout();
    titleLayout->addStretch();
    titleLayout->addWidget(title);
    titleLayout->addStretch();

    // Return the title layout.

    return titleLayout;
}

QGridLayout * DefineSetDialog::initNameAndFileTypeLayout()
{
    // Create the image set name label and edit box.

    QLabel * nameLabel = new QLabel("Name:");
    nameEditBox = new QLineEdit();

    // Create the file type label.

    QLabel * fileTypeLabel = new QLabel("File type:");

    // Create the file type radio buttons.

    // NOTE: PNG files do not directly support EXIF metadata, so we
    // are not supporting them for now. We are leaving the processing
    // code in place, but commenting out the GUI code. To support PNG,
    // search through this file (definesetdialog.cpp) and uncomment
    // the PNG-specific code. You will also need to uncomment one line
    // of code in SetChooser::setDirName(). Note that all of the PNG
    // code has been lightly tested.

    jpegButton = new QRadioButton("JPEG");
    jpegButton->setChecked(true);
    tiffButton = new QRadioButton("TIFF");
//    pngButton = new QRadioButton("PNG");

    // Add the radio buttons to a group box. This makes them mutually exclusive.

    QGroupBox * fileTypeButtonGroup = new QGroupBox();
    fileTypeButtonGroup->setStyleSheet("border:0;");
    QVBoxLayout * fileTypeButtonLayout = new QVBoxLayout();
    fileTypeButtonLayout->addWidget(jpegButton);
    fileTypeButtonLayout->addWidget(tiffButton);
//    fileTypeButtonLayout->addWidget(pngButton);
    fileTypeButtonGroup->setLayout(fileTypeButtonLayout);

    // Lay out the name label, name edit box, file type label, and
    // file type radio buttons in a grid. Note that we center-align
    // the file type label rather than vertically aligning it. This
    // is because it doesn't line up well with the radio buttons when
    // vertically aligned -- the radio buttons have space above them
    // and no amount of playing with settings can get rid of it.
    //
    // We stretch the second column so the labels take as little
    // room as possible.

    QGridLayout * layout = new QGridLayout();
    layout->addWidget(nameLabel, 0, 0);
    layout->addWidget(nameEditBox, 0, 1);
    layout->addWidget(fileTypeLabel, 1, 0);
    layout->addWidget(fileTypeButtonGroup, 1, 1);
    layout->setColumnStretch(1, 1);

    // Return the name and file type layout.

    return layout;
}

QHBoxLayout * DefineSetDialog::initFileListsLayout()
{
    // Create the group boxes for processed and archival file lists

    processedGroupBox = initFilesGroupBox(getProcessedType(), processedFilesModel, processedFilesList, processedAddButton, processedRemoveButton);
    QGroupBox * archivalGroupBox = initFilesGroupBox(IFileType::DNG, archivalFilesModel, archivalFilesList, archivalAddButton, archivalRemoveButton);

    // Lay out the group boxes.

    QHBoxLayout * fileListsLayout = new QHBoxLayout();
    fileListsLayout->addWidget(processedGroupBox);
    fileListsLayout->addWidget(archivalGroupBox);

    // Return the file lists layout.

    return fileListsLayout;
}

QGroupBox * DefineSetDialog::initFilesGroupBox(const IFileType::Enum fileType, QStringListModel *& filesModel, QListView *& filesList, QPushButton *& addButton, QPushButton *& removeButton)
{
    // Get the file type name.

    QString fileTypeName = IFileType::toString(fileType);

    // Create the list model and scrolling list.

    filesModel = new QStringListModel();
    filesList = new QListView();
    filesList->setModel(filesModel);
    filesList->setEditTriggers(QAbstractItemView::NoEditTriggers);
    filesList->setToolTip("List of " + fileTypeName + " files in image set");
    filesList->setSelectionMode(QAbstractItemView::ExtendedSelection);

    // Create the buttons.

    addButton = new QPushButton("Add...");
    removeButton = new QPushButton("Remove");

    // Create the buttons layout.

    QHBoxLayout * buttonsLayout = new QHBoxLayout();
    buttonsLayout->addStretch();
    buttonsLayout->addWidget(addButton);
    buttonsLayout->addWidget(removeButton);

    // Create the group box layout.

    QVBoxLayout * groupBoxLayout = new QVBoxLayout();
    groupBoxLayout->addWidget(filesList);
    groupBoxLayout->addLayout(buttonsLayout);

    // Create the group box.

    QGroupBox * filesGroupBox = new QGroupBox(fileTypeName.left(1).toUpper() + fileTypeName.mid(1) + " Files");
    filesGroupBox->setLayout(groupBoxLayout);

    // Return the group box.

    return filesGroupBox;
}

QHBoxLayout * DefineSetDialog::initButtonsLayout()
{
    // Create the OK and Cancel buttons.

    okButton = new QPushButton("OK");
    cancelButton = new QPushButton("Cancel");

    // Lay out the OK and Cancel buttons.

    QHBoxLayout * buttonLayout = new QHBoxLayout();
    buttonLayout->addStretch();
    buttonLayout->addWidget(okButton);
    buttonLayout->addWidget(cancelButton);

    // Return the button layout.

    return buttonLayout;
}

void DefineSetDialog::setConnections()
{
    // Change the file list label when a file type button is clicked.

    connect(jpegButton, SIGNAL(clicked()), this, SLOT(fileTypeButtonChanged()));
    connect(tiffButton, SIGNAL(clicked()), this, SLOT(fileTypeButtonChanged()));
//    connect(pngButton, SIGNAL(clicked()), this, SLOT(fileTypeButtonChanged()));

    // Display a file chooser dialog when either Add button is clicked.

    connect(processedAddButton, SIGNAL(clicked()), this, SLOT(addProcessedFiles()));
    connect(archivalAddButton, SIGNAL(clicked()), this, SLOT(addArchivalFiles()));

    // Remove files when either Remove button is clicked.

    connect(processedRemoveButton, SIGNAL(clicked()), this, SLOT(removeProcessedFiles()));
    connect(archivalRemoveButton, SIGNAL(clicked()), this, SLOT(removeArchivalFiles()));

    // Exit the dialog when OK or cancel clicked.

    connect(cancelButton, SIGNAL(clicked()), this, SLOT(reject()));
    connect(okButton, SIGNAL(clicked()), this, SLOT(okClicked()));
}

/********************************************************************/
/*                                                                  */
/*  Utility methods                                                 */
/*                                                                  */
/********************************************************************/

void DefineSetDialog::init()
{
    setModal(true);
    setWindowTitle("Define " + imageSetTermIC + " Set");
    initGUI();
    setConnections();
}

IFileType::Enum DefineSetDialog::getProcessedType()
{
    // Get the currently selected file type.

    if (jpegButton->isChecked())
        return IFileType::JPEG;
    else if (tiffButton->isChecked())
        return IFileType::TIFF;
//    else if (pngButton->isChecked())
//        return IFileType::PNG;
    else
        return IFileType::UNKNOWN;
}

QString DefineSetDialog::getProcessedTypeName()
{
    return IFileType::toString(getProcessedType());
}

void DefineSetDialog::addFiles(QStringListModel * filesModel, IFileType::Enum fileType)
{
    // Set up a dialog to get a list of file paths.

    QString typeName = IFileType::toString(fileType);

    QString extensionStr;
    QStringList extensions = IFileType::getExtensions(fileType);
    for (int i = 0; i < extensions.size(); i++)
    {
        if (i != 0) extensionStr.append(" ");
        extensionStr.append(extensions[i]);
    }

    QString filter;
    filter.append(typeName);
    filter.append(" files (");
    filter.append(extensionStr);
    filter.append(")");

    // Set the path. (a) This won't work when we support other
    // archival file types (e.g. TIFF). (b) Except when using the
    // DLN database, this will be an empty string.

    QString path((fileType == IFileType::DNG) ? archivalPath : processedPath);

    // Display the dialog.

    QStringList filePaths = QFileDialog::getOpenFileNames(this,
                                           "Get " + typeName + " files",
                                           path,
                                           filter);

    // When using the database, check if the database contains
    // a directory. If so, make sure that the chosen files are
    // in that directory.

    if (usingDB)
    {
        // If the path is not empty, check that the files
        // are in the correct directory. Display a warning
        // and return if they are not.

        if (!path.isEmpty())
        {
            for (int i = 0; i < filePaths.size(); i++)
            {
                if (QFileInfo(filePaths[i]).canonicalPath() != path)
                {
                    QMessageBox::warning(this,
                                         "Invalid directory",
                                         "Files must be in the " + path +
                                         " directory, which is specified in the DLN.");
                    return;
                }
            }
        }

    }

    // Add the files to the model.

    for (int i = 0; i < filePaths.size(); i++)
    {
        addRow(filesModel, 0, QVariant(filePaths[i]));
    }

    // Sort the model.

    filesModel->sort(0);
}
