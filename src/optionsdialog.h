/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

#ifndef OPTIONSDIALOG_H
#define OPTIONSDIALOG_H

// Local headers

#include "options.h"

// Qt headers

#include <QCheckBox>
#include <QDialog>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QRadioButton>
#include <QVBoxLayout>

/********************************************************************/
/*                                                                  */
/*  OptionsDialog class                                             */
/*                                                                  */
/********************************************************************/

class OptionsDialog : public QDialog
{
    Q_OBJECT
public:
    OptionsDialog(bool isStandalone, InspectorType::Enum inspectorType, Options & options, QWidget *parent = 0);
    ~OptionsDialog() override;

signals:
    
public slots:
    void saveClicked();
    void useDatabaseClicked();

private:

    const bool isStandalone_;
    // Member variables for options

    InspectorType::Enum inspectorType;
    Options & options;

    // GUI member variables

    QCheckBox * useDatabaseCheckBox;
    QCheckBox * loggingCheckBox;
    QCheckBox * stopOnErrorCheckBox;
    QRadioButton * successWarningErrorButton;
    QRadioButton * successWarningButton;
    QRadioButton * successButton;
    QCheckBox * removePreviousResultsCheckBox;
    QLineEdit * epsilonLineEdit;

    // GUI initialization methods

    void initGUI();
    QVBoxLayout * initCheckBoxLayout();
    QVBoxLayout * initRadioButtonLayout();
    QHBoxLayout * initEpsilonLayout();
    QHBoxLayout * initButtonLayout();
};

#endif // OPTIONSDIALOG_H
