/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

// Local headers

#include "ienums.h"

/********************************************************************/
/*                                                                  */
/*  IComparisonType methods                                         */
/*                                                                  */
/********************************************************************/

IComparisonType::Enum IComparisonType::fromString(const QString & str)
{
    if ((str.compare("Restrictions") == 0))
        return IComparisonType::Restrictions;
    else if ((str.compare("WithinFileType") == 0))
        return IComparisonType::WithinFileType;
    else if (str.compare("AcrossFileTypes") == 0)
        return IComparisonType::AcrossFileTypes;
    else if (str.compare("AgainstDLN") == 0)
        return IComparisonType::AgainstDLN;
    else if (str.compare("All") == 0)
        return IComparisonType::All;
    else if (str.compare("None") == 0)
        return IComparisonType::None;
    else
        return IComparisonType::Unknown;
}

/********************************************************************/
/*                                                                  */
/*  IErrorCode methods                                              */
/*                                                                  */
/********************************************************************/

QString IErrorCode::toString(IErrorCode::Enum code)
{
    // Return the standard error message.

    switch (code)
    {
        case IErrorCode::OK:
            return QString("Property values OK");

        case IErrorCode::DiffIsNull:
            return QString("Different null values");

        case IErrorCode::DiffTypes:
            return QString("Different property types");

        case IErrorCode::DiffIsArray:
            return QString("Different array types");

        case IErrorCode::DiffArraySizes:
            return QString("Different array sizes");

        case IErrorCode::DiffStructSizes:
            return QString("Different structure sizes");

        case IErrorCode::Equal:
            return QString("Same property values");

        case IErrorCode::Unequal:
            return QString("Different property values");

        case IErrorCode::DateDiffTooBig:
            return QString("Date difference too big");

        case IErrorCode::LessThan:
            return QString("Property value too low");

        case IErrorCode::GreaterThan:
            return QString("Property value too high");

        case IErrorCode::InvalidValue:
            return QString("Invalid property value");

        case IErrorCode::OpNotSupported:
            return QString("Operator not supported for data type");

        case IErrorCode::UnknownOperator:
            return QString("Unknown operator");

        default:
            return QString("Unknown error");
    }
}

/********************************************************************/
/*                                                                  */
/*  IFileType methods                                               */
/*                                                                  */
/********************************************************************/

QString IFileType::toString(IFileType::Enum type)
{
    switch (type)
    {
        case IFileType::JPEG:
            return QString("JPEG");

        case IFileType::TIFF:
            return QString("TIFF");

//        case IFileType::PNG:
//            return QString("PNG");

        case IFileType::DNG:
            return QString("DNG");

//        case IFileType::RDF:
//            return QString("RDF");

        default:
            return QString("Unknown file type");
    }
}

IFileType::Enum IFileType::fromString(const QString & str)
{
    if ((str.compare("JPEG", Qt::CaseInsensitive) == 0) ||
        (str.compare("JPG", Qt::CaseInsensitive) == 0))
        return IFileType::JPEG;
    else if ((str.compare("TIFF", Qt::CaseInsensitive) == 0) ||
        (str.compare("TIF", Qt::CaseInsensitive) == 0))
        return IFileType::TIFF;
//    else if (str.compare("PNG", Qt::CaseInsensitive) == 0)
//        return IFileType::PNG;
    else if (str.compare("DNG", Qt::CaseInsensitive) == 0)
        return IFileType::DNG;
//    else if (str.compare("RDF", Qt::CaseInsensitive) == 0)
//        return IFileType::RDF;
    else
        return IFileType::UNKNOWN;
}

QStringList IFileType::getExtensions(IFileType::Enum type)
{
    QStringList list;
    switch (type)
    {
        case IFileType::JPEG:
            list << "*.jpeg" << "*.jpg";
            break;

        case IFileType::TIFF:
            list << "*.tiff" << "*.tif";
            break;

//        case IFileType::PNG:
//            list << "*.png";
//            break;

        case IFileType::DNG:
            list << "*.dng";
            break;

//        case IFileType::RDF:
//            list << "*.rdf";
//            break;

        default:
            break;
    }

    return list;
}

QStringList IFileType::getProcessedExtensions()
{
    QStringList list;
    list << "*.jpeg" << "*.jpg" << "*.tiff" << "*.tif";
    return list;
}

QStringList IFileType::getArchivalExtensions()
{
    QStringList list;
    list << "*.dng";
    return list;
}


/********************************************************************/
/*                                                                  */
/*  IMessageType methods                                            */
/*                                                                  */
/********************************************************************/

IMessageType::Enum IMessageType::fromString(const QString & str)
{
    if (str == "Two values")
        return IMessageType::CompareValues;
    else if (str == "Restriction")
        return IMessageType::Restriction;
    else if (str == "Invalid value")
        return IMessageType::InvalidValue;
    else if (str == "Text")
        return IMessageType::Text;
    else // if (str == "None")
        return IMessageType::None;
}

QString IMessageType::toString(IMessageType::Enum msgType)
{
    switch (msgType)
    {
        case IMessageType::CompareValues:
            return QString("Two values");

        case IMessageType::Restriction:
            return QString("Restriction");

        case IMessageType::InvalidValue:
            return QString("Invalid value");

        case IMessageType::Text:
            return QString("Text");

        case IMessageType::None:
        default:
            return QString("None");
    }
}

QString IMessageType::getFilenameLabel(Enum msgType, int num)
{
    QString label;
    switch (msgType)
    {
        case IMessageType::CompareValues:
            label.append("Filename ").append(QString::number(num));
            return label;

        case IMessageType::Restriction:
        case IMessageType::InvalidValue:
            return (num == 1) ? QString("Filename") : QString();

        case IMessageType::None:
        default:
            return QString();
    }
}

QString IMessageType::getValueLabel(Enum msgType, int num)
{
    QString label;
    switch (msgType)
    {
        case IMessageType::CompareValues:
            label.append("Value ").append(QString::number(num));
            return label;

        case IMessageType::Restriction:
            if (num == 1)
                return QString("Value");
            else if (num == 2)
                return QString("Restrictions");
            else
                return QString();

        case IMessageType::InvalidValue:
            return (num == 1) ? QString("Value") : QString();

        case IMessageType::None:
        default:
            return QString();
    }
}

/********************************************************************/
/*                                                                  */
/*  InspectorType methods                                               */
/*                                                                  */
/********************************************************************/

QString InspectorType::toString(InspectorType::Enum inspectorType)
{
    // Return the name of the specified inspector type.

    switch (inspectorType)
    {
        case InspectorType::RTI:
            return "RTI";

        case InspectorType::Photogrammetry:
            return "Photogrammetry";

        case InspectorType::Documentary:
            return "Documentary";

        case InspectorType::MultiSpectral:
            return "MultiSpectral";

        default:
            return "Unknown";
    }
}


QString InspectorType::toDBString(InspectorType::Enum inspectorType)
{
    // Return the name of the specified inspector type as stored in the DLN DB

    switch (inspectorType)
    {
        case InspectorType::RTI:
            return "RTI";

        case InspectorType::Photogrammetry:
            return "PG";

        case InspectorType::Documentary:
            return "DIS";

        case InspectorType::MultiSpectral:
            return "MS";
    }
}

/********************************************************************/
/*                                                                  */
/*  IOperator methods                                               */
/*                                                                  */
/********************************************************************/

QString IOperator::toString(IOperator::Enum op)
{
    // Return the name of the specified operator.

    switch (op)
    {
        case IOperator::EQ:
            return "EQ";

        case IOperator::NE:
            return "NE";

        case IOperator::LT:
            return "LT";

        case IOperator::LE:
            return "LE";

        case IOperator::GT:
            return "GT";

        case IOperator::GE:
            return "GE";

        case IOperator::Unknown:
        default:
            return "Unknown";
    }
}

IOperator::Enum IOperator::fromString(const QString &str)
{
    if (str == "EQ")
        return IOperator::EQ;
    else if (str == "NE")
        return IOperator::NE;
    else if (str == "LT")
        return IOperator::LT;
    else if (str == "LE")
        return IOperator::LE;
    else if (str == "GT")
        return IOperator::GT;
    else if (str == "GE")
        return IOperator::GE;
    else
        return IOperator::Unknown;
}

QString IOperator::toSymbol(Enum op, bool html)
{
    // Return the name of the specified operator.

    switch (op)
    {
        case IOperator::EQ:
            return "=";

        case IOperator::NE:
            return "!=";

        case IOperator::LT:
            return html ? "&lt;" : "<";

        case IOperator::LE:
            return html ? "&lt;=" : "<=";

        case IOperator::GT:
            return ">";

        case IOperator::GE:
            return ">=";

        case IOperator::Unknown:
        default:
            return "";
    }
}

/********************************************************************/
/*                                                                  */
/*  IResultType methods                                             */
/*                                                                  */
/********************************************************************/

QString IResultType::toString(IResultType::Enum resultType)
{
    // Return the name of the specified result type.

    switch (resultType)
    {
        case IResultType::OK:
            return "OK";

        case IResultType::Warning:
            return "Warning";

        case IResultType::FileError:
            return "File error";

        case IResultType::CaptureError:
            return "Capture error";

        case IResultType::TransformationError:
            return "Transformation error";

        case IResultType::FatalError:
            return "Fatal error";

        case IResultType::Unknown:
        default:
            return "Unknown";
    }
}

IResultType::Enum IResultType::fromString(const QString &str)
{
    if (str == "OK")
        return IResultType::OK;
    else if (str == "Warning")
        return IResultType::Warning;
    else if (str == "File error")
        return IResultType::FileError;
    else if (str == "Capture error")
        return IResultType::CaptureError;
    else if (str == "Transformation error")
        return IResultType::TransformationError;
    else if (str == "Fatal error")
        return IResultType::FatalError;
    else
        return IResultType::Unknown;
}

QColor IResultType::toColor(IResultType::Enum resultType)
{
    // Return a color for each result type.

    switch (resultType)
    {
        case IResultType::OK:
            return QColor(2, 200, 2); // Green

        case IResultType::Warning:
            return QColor(255, 165, 0); // Orange

        case IResultType::FileError:
        case IResultType::CaptureError:
        case IResultType::TransformationError:
        case IResultType::FatalError:
            return QColor(Qt::red);

        default:
            return QColor(Qt::black);
    }
}

/********************************************************************/
/*                                                                  */
/*  IStatus methods                                                 */
/*                                                                  */
/********************************************************************/

// None.

/********************************************************************/
/*                                                                  */
/*  ITestGroup methods                                              */
/*                                                                  */
/********************************************************************/

QString ITestGroup::toString(ITestGroup::Enum type)
{
    // Return the name of the specified test group.

    switch (type)
    {
        case ITestGroup::File:
            return "File tests";

        case ITestGroup::Capture:
            return "Capture tests";

        case ITestGroup::Transformation:
            return "Transformation tests";

        case ITestGroup::UpdateDLN:
            return "Update DLN";

        case ITestGroup::None:
        default:
            return "None";
    }
}

ITestGroup::Enum ITestGroup::fromString(const QString &str)
{
    if (str == "Capture tests")
        return ITestGroup::Capture;
    else if (str == "Transformation tests")
        return ITestGroup::Transformation;
    else if (str == "File tests")
        return ITestGroup::File;
    else if (str == "Update DLN")
        return ITestGroup::UpdateDLN;
    else // if (str == "None")
        return ITestGroup::None;
}

/********************************************************************/
/*                                                                  */
/*  PropAttrToken methods                                           */
/*                                                                  */
/********************************************************************/

PropAttrToken::Enum PropAttrToken::fromString(const QStringRef & str)
{
    // Return a token representing the element name.

    if (str == "Version")
        return PropAttrToken::Version;
    else
        return PropAttrToken::Unknown;
}

QString PropAttrToken::toString(PropAttrToken::Enum token)
{
    switch (token)
    {
        case PropAttrToken::Version:
            return QString("Version");

        case PropAttrToken::Unknown:
        default:
            return QString("Invalid type: ").append(QString::number(token));
    }
}

/********************************************************************/
/*                                                                  */
/*  PropElementToken methods                                        */
/*                                                                  */
/********************************************************************/

PropElementToken::Enum PropElementToken::fromString(const QStringRef & str)
{
    // Return a token representing the element name.

    if (str == "PropInfo")
        return PropElementToken::PropInfo;
    else if (str == "Namespace")
        return PropElementToken::Namespace;
    else if (str == "Name")
        return PropElementToken::Name;
    else if (str == "SimpleType")
        return PropElementToken::SimpleType;
    else if (str == "Type")
        return PropElementToken::Type;
    else if (str == "Restrictions")
        return PropElementToken::Restrictions;
    else if (str == "ResultType")
        return PropElementToken::ResultType;
    else if (str == "Restriction")
        return PropElementToken::Restriction;
    else if (str == "Operator")
        return PropElementToken::Operator;
    else if (str == "Value")
        return PropElementToken::Value;
    else if (str == "AndOr")
        return PropElementToken::AndOr;
    else if (str == "Message")
        return PropElementToken::Message;
    else if (str == "ErrorMessage")
        return PropElementToken::ErrorMessage;
    else if (str == "IsArray")
        return PropElementToken::IsArray;
    else if (str == "Severity")
        return PropElementToken::Severity;
    else if (str == "Compare")
        return PropElementToken::Compare;
    else if (str == "StructType")
        return PropElementToken::StructType;
    else if (str == "DLNQuery")
        return PropElementToken::DLNQuery;
    else if (str == "PropInfoDoc")
        return PropElementToken::PropInfoDoc;
    else
        return PropElementToken::Unknown;
}

/********************************************************************/
/*                                                                  */
/*  XMPValueType methods                                            */
/*                                                                  */
/********************************************************************/

QString XMPUnionType::toString(XMPUnionType::Enum type)
{
    switch (type)
    {
        case XMPUnionType::Simple:
            return QString("Simple");

        case XMPUnionType::Array:
            return QString("Array");

        case XMPUnionType::Struct:
            return QString("Struct");

        case XMPUnionType::Unknown:
        default:
            return QString("Invalid type: ").append(QString::number(type));
    }
}

bool XMPUnionType::fromString(const QString & str, XMPUnionType::Enum & type)
{
    if (str == "Simple")
        type = XMPUnionType::Simple;
    else if (str == "Array")
        type = XMPUnionType::Array;
    else if (str == "Struct")
        type = XMPUnionType::Struct;
    else
        type = XMPUnionType::Unknown;

    return (type != XMPUnionType::Unknown);
}

/********************************************************************/
/*                                                                  */
/*  XMPType methods                                                 */
/*                                                                  */
/********************************************************************/

QString XMPType::toString(XMPType::Enum type)
{
    switch (type)
    {
        case XMPType::Boolean:
            return QString("Boolean");

        case XMPType::Datetime:
            return QString("Date");

        case XMPType::Real:
            return QString("Real");

        case XMPType::Integer:
            return QString("Integer");

        case XMPType::Rational:
            return QString("Rational");

        case XMPType::Text:
            return QString("Text");

        case XMPType::Unknown:
        default:
            return QString("Invalid type: ").append(QString::number(type));
    }
}

bool XMPType::fromString(const QString & str, XMPType::Enum & type)
{
    if (str == "Integer")
        type = XMPType::Integer;
    else if (str == "Boolean")
        type = XMPType::Boolean;
    else if (str == "Rational")
        type = XMPType::Rational;
    else if (str == "Date")
        type = XMPType::Datetime;
    else if (str == "Text")
        type = XMPType::Text;
    else if (str == "Real")
        type = XMPType::Real;
    else
        type = XMPType::Unknown;

    return (type != XMPType::Unknown);
}
