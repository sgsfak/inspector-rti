/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

#ifndef DIRPATTERN_H
#define DIRPATTERN_H

// Local headers

#include "ienums.h"

// Qt headers

#include <QHash>
#include <QList>
#include <QRegExp>
#include <QString>
#include <QXmlStreamReader>

#include <QDebug>

// TypeRegex struct. Used for parsing objects in TypeRegexes array.

struct TypeRegex
{
    QString typeName;
    QRegExp regex;
};

/********************************************************************/
/*                                                                  */
/*  Pattern struct                                                  */
/*                                                                  */
/********************************************************************/

struct Pattern
{
    bool hasFiles;
    bool hasSetName;
    bool hasTypeName;
    QHash<QString, QRegExp> typeRegexes;

    QString toString()
    {
        QString s;
        s.append("hasFiles ").append(hasFiles ? "T" : "F");
        s.append(" isSetName ").append(hasSetName ? "T" : "F");
        s.append(" hasTypeName ").append(hasTypeName ? "T" : "F");
        s.append(" regex size ").append(QString::number(typeRegexes.size()));
        return s;
    }

    Pattern() :
        hasFiles(false),
        hasSetName(false),
        hasTypeName(false)
    {
    }

    ~Pattern()
    {
    }
};

/********************************************************************/
/*                                                                  */
/*  DirPattern struct                                               */
/*                                                                  */
/********************************************************************/

struct DirPattern
{
    QString summary;
    QString example;
    QList<Pattern> patterns;

    DirPattern()
    {
    }

    ~DirPattern()
    {
    }
};

/********************************************************************/
/*                                                                  */
/*  DirPatternFactory class                                         */
/*                                                                  */
/********************************************************************/

class DirPatternFactory
{
public:
    static QList<DirPattern> getDirPatterns(InspectorType::Enum);

private:
    static QList<DirPattern> parseDocument(const QJsonDocument & doc);
    static QList<DirPattern> parseDirPatterns(const QJsonObject & topObject);
    static DirPattern parseDirPattern(const QJsonValue & dirPatternValue);
    static QString parseExample(const QJsonObject & dirPatternObject);
    static QList<Pattern> parsePatterns(const QJsonObject & dirPatternObject);
    static Pattern parsePattern(const QJsonValue & patternValue);
    static QHash<QString, QRegExp> parseRegexes(const QJsonObject & patternObject);
    static TypeRegex parseTypeRegex(const QJsonValue & typeRegexValue);

    static QJsonObject getObject(const QJsonValue & value, const QString & description);
    static QJsonArray getArray(const QJsonValue & value, int minSize, const QString & description);
    static QJsonValue getPropertyValue(const QJsonObject & object, const QString & propName, const QString & description);
    static QString getStringProperty(const QJsonObject & object, const QString & propName, const QString & description);
    static QString getString(const QJsonValue & value, const QString & description);

    static void checkContains(const QString & contains);
    static void checkYesNo(const QString & propName, const QString & yesNo);
    static void checkTypeName(const QString & propName, const QString & typeName);
    static void checkDirPattern(const DirPattern & dirPattern);
};

#endif // DIRPATTERN_H
