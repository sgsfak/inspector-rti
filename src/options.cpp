/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

// Local headers

#include "iexception.h"
#include "iglobals.h"
#include "options.h"

// Qt headers

#include <QDir>
#include <QFile>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QStandardPaths>
#include <QVariant>

#include <QDebug>

/********************************************************************/
/*                                                                  */
/*  Public methods                                                  */
/*                                                                  */
/********************************************************************/

// The options information is stored in a SQLite database named
// options-(RTI|PG).db, which is created by the DLN:Inspector in
// the directory returned by:
//
//    QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)
//
// For example, on Windows this is:
//
//    C:\Users\<user>\AppData\Roaming\CHI\Inspector
//
// You can browse and modify this file with a SQLite browser. For
// example, an online browser is available at:
//
//    https://sqliteonline.com/

Options OptionsFactory::fromOptionsDB()
{
    // Get the full path name of the options file.

    QString optionsPath = getOptionsDBPath();

    // If the file exists, get the options from it. Otherwise, create a new
    // file, save the default option values to it, and return the default values.

    if (QFile::exists(optionsPath))
    {
        return getExistingOptions(optionsPath);
    }
    else
    {
        return getNewOptions(optionsPath);
    }
}

void OptionsFactory::toOptionsDB(const Options & options)
{
    // Get the path to the options file.

    QString optionsPath = getOptionsDBPath();

    // Check that the options-(RTI|PG).db file exists. If it does not
    // exist, we want to throw an exception. (The SQLite driver creates
    // it if it does not exist.)

    if (!QFile::exists(optionsPath))
    {
        throw IException("Programming error", "Trying to write to " + optionsPath + " and it does not exist.");
    }

    // The following code is inside a tighter scope so that the QSqlDatabase
    // and QSqlQuery objects will be deleted when leaving the scope. This is
    // necessary to close them and prevent "resource leaks". For details, see:
    //
    //    http://doc.qt.io/qt-5/qsqldatabase.html#removeDatabase

    {
        // Add a connection to the SQLite database and open that connection.

        QSqlDatabase db = openDatabase("QSQLITE", "options", optionsPath);

        // Prepare the UPDATE statement.

        QSqlQuery q(db);
        prepareQuery(q, "UPDATE options SET useDatabase = ?, logging = ?, stopOnError = ?, writeOnSuccess = ?, writeOnWarning = ?, writeOnError = ?, remove = ?, epsilon = ?");

        // Bind values to the UPDATE statement.

        q.bindValue(0, QVariant(intFromBool(options.useDatabase)));
        q.bindValue(1, QVariant(intFromBool(options.logging)));
        q.bindValue(2, QVariant(intFromBool(options.stopOnError)));
        q.bindValue(3, QVariant(intFromBool((options.writeToDLN & IStatus::Success) != 0)));
        q.bindValue(4, QVariant(intFromBool((options.writeToDLN & IStatus::Warning) != 0)));
        q.bindValue(5, QVariant(intFromBool((options.writeToDLN & IStatus::Error) != 0)));
        q.bindValue(6, QVariant(intFromBool(options.removePreviousResults)));
        q.bindValue(7, QVariant(options.epsilon));

        // Start a transaction, execute the query, and commit the transaction.

        startTransaction(db, "options");
        execPrepared(q, "options");
        commitTransaction(db, "options");

        // Close the database.

        db.close();
    }

    // Remove the database.

    QSqlDatabase::removeDatabase("options");
}

/********************************************************************/
/*                                                                  */
/*  Private methods                                                 */
/*                                                                  */
/********************************************************************/

Options OptionsFactory::getExistingOptions(QString optionsPath)
{

    // The following code is inside a tighter scope so that the QSqlDatabase
    // and QSqlQuery objects will be deleted when leaving the scope. This is
    // necessary to close them and prevent "resource leaks". For details, see:
    //
    //    http://doc.qt.io/qt-5/qsqldatabase.html#removeDatabase

    Options options;
    {
        // Add a connection to the SQLite database and open that connection.

        QSqlDatabase db = openDatabase("QSQLITE", "options", optionsPath);

        // Get the data from the options table.

        QSqlQuery q(db);
        execQuery(q, "SELECT useDatabase, logging, stopOnError, writeOnSuccess, writeOnWarning, writeOnError, remove, epsilon FROM options");

        // Get the first row of data. There should be exactly one row.

        if (!q.next())
        {
            throw DBException("Error retrieving row from " + optionsPath, q.lastError());
        }

        // Set the Options values.

        options.useDatabase = (q.value(0).toInt() == 1);
        options.logging = (q.value(1).toInt() == 1);
        options.stopOnError = (q.value(2).toInt() == 1);

        options.writeToDLN = 0;
        if (q.value(3).toInt() == 1) options.writeToDLN = options.writeToDLN | IStatus::Success;
        if (q.value(4).toInt() == 1) options.writeToDLN = options.writeToDLN | IStatus::Warning;
        if (q.value(5).toInt() == 1) options.writeToDLN = options.writeToDLN | IStatus::Error;

        options.removePreviousResults = (q.value(6).toInt() == 1);
        options.epsilon = q.value(7).toInt();

        // Close the database.

        db.close();
    }

    // Remove the database.

    QSqlDatabase::removeDatabase("options");

    // Return the options.

    return options;
}

Options OptionsFactory::getNewOptions(QString optionsPath)
{
    // Create a new Options element. This sets the default option
    // values in its constructor.

    Options options;

    // Create a new options file, create the options table and
    // save the options.

    // The following code is inside a tighter scope so that the QSqlDatabase
    // and QSqlQuery objects will be deleted when leaving the scope. This is
    // necessary to close them and prevent "resource leaks". For details, see:
    //
    //    http://doc.qt.io/qt-5/qsqldatabase.html#removeDatabase

    {
        // Add a connection to the SQLite database and open that connection.

        QSqlDatabase db = openDatabase("QSQLITE", "options", optionsPath);

        // Create the options table.

        QSqlQuery q(db);
        QString query = "CREATE TABLE options "
                        "(useDatabase INT, logging INT, stopOnError INT, "
                         "writeOnSuccess INT, writeOnWarning INT, writeOnError INT, "
                         "remove INT, epsilon INT)";
        startTransaction(db, "options");
        execQuery(q, query);
        commitTransaction(db, "options");

        // Insert the data into the options table.

        query = "INSERT INTO options "
                "(useDatabase, logging, stopOnError, writeOnSuccess, "
                 "writeOnWarning, writeOnError, remove, epsilon) "
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        prepareQuery(q, query);

        // Bind values to the INSERT statement.

        q.bindValue(0, QVariant(intFromBool(options.useDatabase)));
        q.bindValue(1, QVariant(intFromBool(options.logging)));
        q.bindValue(2, QVariant(intFromBool(options.stopOnError)));
        q.bindValue(3, QVariant(intFromBool((options.writeToDLN & IStatus::Success) != 0)));
        q.bindValue(4, QVariant(intFromBool((options.writeToDLN & IStatus::Warning) != 0)));
        q.bindValue(5, QVariant(intFromBool((options.writeToDLN & IStatus::Error) != 0)));
        q.bindValue(6, QVariant(intFromBool(options.removePreviousResults)));
        q.bindValue(7, QVariant(options.epsilon));

        // Start a transaction, execute the query, and commit the transaction.

        startTransaction(db, "options");
        execPrepared(q, "options");
        commitTransaction(db, "options");

        // Close the database.

        db.close();
    }

    // Remove the database.

    QSqlDatabase::removeDatabase("options");

    // Return the options.

    return options;
}

QString OptionsFactory::getOptionsDBPath()
{
    // Get the filename of the options-(RTI|PG|DIS|MS).db file.

    QString filename;
    switch (inspectorType) {
    case InspectorType::RTI: filename = "options-RTI.db"; break;
    case InspectorType::Photogrammetry: filename = "options-PG.db"; break;
    case InspectorType::Documentary: filename = "options-DIS.db"; break;
    case InspectorType::MultiSpectral: filename = "options-MS.db"; break;
    }
    // Get the path and create it if it does not exist.

    QString path = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    QDir dir(path);
    if (!dir.exists())
    {
        dir.mkpath(path);
    }

    // Return the full path of the file.

    return path + "/" + filename;
}

int OptionsFactory::intFromBool(bool b)
{
    return b ? 1 : 0;
}
