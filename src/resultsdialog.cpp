/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2017                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

// Local headers

#include "iglobals.h"
#include "iresult.h"
#include "resultbox.h"
#include "resultsdialog.h"
#include "treemodel.h"
#include "treeitem.h"

// Qt headers

#include <QAbstractItemView>
#include <QDesktopServices>
#include <QFileDialog>
#include <QFont>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QMessageBox>
#include <QPoint>
#include <QPushButton>
#include <QScrollBar>
#include <QVariant>
#include <QCoreApplication>

#include <QDebug>
#include <QStack>

/********************************************************************/
/*                                                                  */
/*  ITableView methods                                              */
/*                                                                  */
/********************************************************************/

ITableView::ITableView(QWidget *parent) :
    QTableView(parent)
{
    // Track mouse move events.

    this->setMouseTracking(true);
}

void ITableView::mouseMoveEvent(QMouseEvent* event)
{
    // When the mouse moves, find its position and get the
    // index of the cell it is on.

    QPoint pos = event->pos();
    QModelIndex index = indexAt(pos);

    if (index.isValid())
    {
        // When the mouse is in the fourth column, use the pointing
        // hand cursor. Otherwise, use the arrow cursor.

        if (index.column() == 3)
        {
            setCursor(Qt::PointingHandCursor);
        }
        else
        {
            setCursor(Qt::ArrowCursor);
        }
    }

    // Pass the mouse move event to QTableView.

    QTableView::mouseMoveEvent(event);
}

/********************************************************************/
/*                                                                  */
/*  ResultsDialog methods                                           */
/*                                                                  */
/********************************************************************/

/********************************************************************/
/*                                                                  */
/*  Constructor and destructor                                      */
/*                                                                  */
/********************************************************************/

ResultsDialog::ResultsDialog(QWidget *parent) :
    QDialog(parent)
{
    // Allocate a new item model.

    tableModel = new QStandardItemModel();

    // Set up the dialog

    setModal(true);
    setWindowTitle("Results");
    setMinimumWidth(1000);
    setMinimumHeight(400);
    initGUI();
}

ResultsDialog::~ResultsDialog()
{
    // Delete the model used by the view class. Because multiple view classes
    // can share the same model, the view classes do not delete model objects.
    // Instead, the programmer must delete model objects explicitly.

    delete tableModel;
    tableModel = nullptr;
    delete treeModel;
    treeModel = nullptr;
}

/********************************************************************/
/*                                                                  */
/*  Public methods                                                  */
/*                                                                  */
/********************************************************************/

/********************************************************************/
/*                                                                  */
/*  Methods to display results                                      */
/*                                                                  */
/********************************************************************/

void ResultsDialog::setProject(const QString & projectDirectory)
{
    Results::setProject(projectDirectory);
    if (projectDirectory.isEmpty())
    {
        directoryLabel->hide();
    }
    else
    {
        directoryLabel->setText(projectTermIC + " directory: " + projectDirectory);
        directoryLabel->show();
    }
}

void ResultsDialog::setInspectionTime(const QDateTime &inspectionTime)
{
    Results::setInspectionTime(inspectionTime);
    dateTimeLabel->setText("Inspection performed: " + inspectionTime.toString("yyyy-MM-dd hh:mm ap"));
}

void ResultsDialog::clear()
{
    Results::clear();
    tableModel->removeRows(0, tableModel->rowCount());

    // Couldn't find a way to use `removeRows` properly
    // So I destroy the model and recreate it:

    // treeModel->removeRows(0, treeModel->rowCount());
    if (treeModel->rowCount() > 0) {
        delete this->treeModel;
        initTreeView();
        qDebug() << "treemodel cleared, now #rows=" << treeModel->rowCount();
    }

}

void ResultsDialog::showDialog()
{
    // Call this method to display the dialog.

    // First, sort the results.

    sort();

    // Add the rows to the table model. We don't add
    // rows to the table model as they are added to Results
    // because we need to sort them first.

    for (const IResult& result: qAsConst(results))
    {
        // Get the number of rows in the table model and insert a new
        // row at the end of the table model.

        int rowNum = tableModel->rowCount();
        tableModel->insertRow(rowNum);

        // Add data to the row.

        if (result.propQName.endsWith("Exposure2012")) {
            qDebug() << "Exposure" << result.severity;
        }
        addCell(tableModel, rowNum, 0, result.imageSetName);
        addCell(tableModel, rowNum, 1, IResultType::toString(result.resultType));
        addCell(tableModel, rowNum, 2, result.propQName);
        addCell(tableModel, rowNum, 3, result.resultType == IResultType::OK ? "--" : QString::number(result.severity));
        addCell(tableModel, rowNum, 4, result.title);

        // In the second (result) column, set the result color to green,
        // orange, or red.

        setTextColor(tableModel, rowNum, 1, toColor(result.resultType));

        // In the fifth (description) column, set to font to use an
        // underline and the color to blue. This imitates a link in a browser.

        setUnderline(tableModel, rowNum, 4);
        setTextColor(tableModel, rowNum, 4, QColor(Qt::blue));
    }

    // Reset the vertical scroll bar to the top.

    // For reasons not clear to me, the following line of code
    // causes the Inspector to crash. The pointer to the scrollbar
    // is non-null. In spite of this, calls to a handful of different
    // methods on the scrollbar all crashed. It is not clear what
    // is happening... Leave unfixed for now.

//    resultsTable->verticalScrollBar()->setValue(0);


    // Build the TreeModel by iterating over the Results:
    buildTreeModel();

    // Show the dialog.

    exec();
}

/********************************************************************/
/*                                                                  */
/*  Slots                                                           */
/*                                                                  */
/********************************************************************/

void ResultsDialog::displayResult(const QModelIndex & index)
{
    // If the user clicks on the table, check which column
    // was clicked. If column 5 (the underlined result title)
    // was clicked, display the detailed result in a ResultBox.

    int colNum = index.column();
    if (colNum == 4)
    {
        ResultBox box(results.at(index.row()), projectDirectory);
        box.exec();
    }
}

void ResultsDialog::saveResults()
{
    // Prompt the user for the name of a file in which to
    // save the results. If the user clicks Cancel, just return.

    QString selectedFilter("Result files (*.txt)");
    QString filename = getFilename("Save Results", ".txt", "Result files (*.txt);;All files (*.*)", &selectedFilter);
    if (filename.isEmpty()) return;

    // Write the results to the file.

    try
    {
        writeResults(filename);
    }
    catch (IOException io)
    {
        // If there is an exception, display it to the user
        // and return.

        QMessageBox::warning(this, io.title, io.msg);
        return;
    }
}

void ResultsDialog::saveHTML()
{
    // Prompt the user for the name of a file in which to
    // save the report. If the user clicks Cancel, just return.

    QString selectedFilter("HTML files (*.htm *.html)");
    QString filename = getFilename("Save Report", ".htm", "HTML files (*.htm *.html);;All files (*.*)", &selectedFilter);
    if (filename.isEmpty()) return;

    // Write the HTML report to the file.

    try
    {
        writeHTMLFile(filename);
    }
    catch (IOException io)
    {
        // If there is an exception, display it to the user
        // and return.

        QMessageBox::warning(this, io.title, io.msg);
        return;
    }
}

void ResultsDialog::closeDialog()
{
    // Close the dialog box.

    close();
}

void ResultsDialog::displayHelp()
{
    // Display help. Note that the jump to the named destination does not
    // work on all browsers or PDF viewers. (And because it uses the file:
    // protocol, it might not even work on any browsers or viewers. See note in
    // https://helpx.adobe.com/acrobat/kb/link-html-pdf-page-acrobat.html.)

#if defined(Q_OS_OSX)
    QString appDir = QCoreApplication::applicationDirPath() + "/../Resources";
#else
    QString appDir = QCoreApplication::applicationDirPath();
#endif
    QString filePath = appDir + "/manual.pdf";

    if (!QFileInfo::exists(filePath)) {
        QMessageBox::information(this, QCoreApplication::applicationName(),
                                 "PDF manual not found in application's folder!");
        return;
    }

    QUrl fileUrl = QUrl::fromLocalFile(filePath);
    fileUrl.setFragment("namedest=ResultsHelp"); // i.e. "../manual.pdf#namedest=ResultsHelp"
    QDesktopServices::openUrl(fileUrl);
}

/********************************************************************/
/*                                                                  */
/*  Private methods                                                 */
/*                                                                  */
/********************************************************************/

/********************************************************************/
/*                                                                  */
/*  Methods to initialize the GUI                                   */
/*                                                                  */
/********************************************************************/

void ResultsDialog::initGUI()
{
    // Initialize the main GUI components.

    QVBoxLayout * infoLayout = initInfoLayout();
    ITableView * resultsTable = initResultsTable();
    QVBoxLayout * buttonsLayout = initButtonsLayout();

    this->treeView = new QTreeView();
    initTreeView();
    connect(this->treeView,SIGNAL(doubleClicked(const QModelIndex &)),this,SLOT(doubleClickOnTree(const QModelIndex &)));


    // Place the results and buttons layouts side-by-side in a horizontal
    // box layout. This will form the main part of the dialog.

    QHBoxLayout * hLayout = new QHBoxLayout();
    hLayout->addSpacing(10);
    hLayout->addWidget(resultsTable);
    hLayout->addSpacing(20);
    hLayout->addLayout(buttonsLayout);
    hLayout->addSpacing(10);

    // Place the information and the horizontal layouts in a vertical box layout.
    // This will place the information above the results and the buttons.

    QVBoxLayout * vLayout = new QVBoxLayout();
    vLayout->addLayout(infoLayout);
    vLayout->addSpacing(10);
    vLayout->addLayout(hLayout);
    vLayout->addSpacing(10);
    vLayout->addWidget(treeView);
    vLayout->addSpacing(10);

    // Set the dialog box's layout.

    setLayout(vLayout);
}

QVBoxLayout * ResultsDialog::initInfoLayout()
{
    // Create the information labels. We set the actual
    // directory and date/time through setter methods.

    directoryLabel = new QLabel(projectTermIC + " directory: ");
    dateTimeLabel = new QLabel("Inspection performed: ");

    // Lay out the directory and date/time labels in a vertical layout.

    QVBoxLayout * infoLayout = new QVBoxLayout();
    infoLayout->addWidget(directoryLabel);
    infoLayout->addWidget(dateTimeLabel);

    // Return the layout.

    return infoLayout;
}

ITableView * ResultsDialog::initResultsTable()
{
    // Initialize the table model with four columns. This will force the
    // headers to be displayed before any rows have been added.

    tableModel->insertColumns(0, 5);

    // Set the headers.

    tableModel->setHeaderData(0, Qt::Horizontal, QVariant(imageSetTermIC + " set"), Qt::DisplayRole);
    tableModel->setHeaderData(1, Qt::Horizontal, QVariant("Result"), Qt::DisplayRole);
    tableModel->setHeaderData(2, Qt::Horizontal, QVariant("Property"), Qt::DisplayRole);
    tableModel->setHeaderData(3, Qt::Horizontal, QVariant("Severity"), Qt::DisplayRole);
    tableModel->setHeaderData(4, Qt::Horizontal, QVariant("Description"), Qt::DisplayRole);

    // Create a header and set it to use the table model.

    QHeaderView * resultsHeader = new QHeaderView(Qt::Horizontal);
    resultsHeader->setModel(tableModel);
    resultsHeader->setStretchLastSection(true);

    // Create the ITableView and configure its appearance.

    ITableView * resultsTable = new ITableView();
    resultsTable->setModel(tableModel);
    resultsTable->setHorizontalHeader(resultsHeader);
    resultsTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
    resultsTable->setColumnWidth(0, 150);
    resultsTable->setColumnWidth(1, 150);
    resultsTable->setColumnWidth(2, 150);
    resultsTable->setColumnWidth(3, 70);
    resultsTable->setToolTip("Click on the result to see a detailed description");

    // Connect the ITableView's clicked() signal to the displayResult() method.

    connect(resultsTable, SIGNAL(clicked(QModelIndex)), this, SLOT(displayResult(QModelIndex)));

    // Return the ITableView.

    return resultsTable;
}

QVBoxLayout * ResultsDialog::initButtonsLayout()
{
    // Create the buttons.

    QPushButton * saveButton = new QPushButton("Save...");
    saveButton->setToolTip("Save the results for later review");
    QPushButton * reportButton = new QPushButton("Report...");
    reportButton->setToolTip("Save an HTML report of the results");
    QPushButton * helpButton = new QPushButton("Help...");
    helpButton->setToolTip("Get help");
    QPushButton * closeButton = new QPushButton("Close");
    closeButton->setToolTip("Close this window");

    // Connect the buttons to methods that will take action.

    connect(saveButton, SIGNAL(clicked()), this, SLOT(saveResults()));
    connect(reportButton, SIGNAL(clicked()), this, SLOT(saveHTML()));
    connect(helpButton, SIGNAL(clicked()), this, SLOT(displayHelp()));
    connect(closeButton, SIGNAL(clicked()), this, SLOT(closeDialog()));

    // Lay out the buttons vertically.

    QVBoxLayout * buttonsLayout = new QVBoxLayout();
//    buttonsLayout->addStretch();
    buttonsLayout->addWidget(saveButton);
    buttonsLayout->addWidget(reportButton);
    buttonsLayout->addWidget(helpButton);
    buttonsLayout->addStretch();
    buttonsLayout->addWidget(closeButton);

    // Return the button layout.

    return buttonsLayout;
}

void ResultsDialog::initTreeView()
{
    QList<QVariant> rootData;
    rootData << "Inspection target" << "Result" << "Severity" << "Description";

    // Allocate a new Tree model.
    // Root Item is not associated with any Result:
    TreeItem* rootItem = new TreeItem(rootData);
    this->treeModel = new TreeModel(rootItem, this);

    this->treeView->setModel(this->treeModel);
    //    view->header()->setStretchLastSection(false);
    this->treeView->header()->setSectionResizeMode(0, QHeaderView::Stretch);
}

/*
 * Determine the background color of a Result based
 * on the Severity of the Error. If it was an OK Result
 * it returns Green
 */
static QRgb colorCodeResult(const IResult* result)
{
    // Color definitions for setting the background of items:
    static const QRgb purple = qRgb(239, 55, 245);
    static const QRgb red = qRgb(255, 38, 0);
    static const QRgb yellow = qRgb(255, 251, 0);
    static const QRgb green = qRgb(60, 216, 60);
    static const QRgb white = qRgb(255, 255, 255);

    if (!result || result->resultType == IResultType::OK)
        return green;

    // Check severity to determine background color:
    QRgb bgcolor = white;
    switch (result->severity) {
    case PropInfo::SEVERITY_ERROR:
        bgcolor = purple;
        break;
    case PropInfo::SEVERITY_MINOR_ERROR:
        bgcolor = red;
        break;
    case PropInfo::SEVERITY_WARNING:
        bgcolor = yellow;
        break;
    default:
        break;
    }
    return bgcolor;
}

static const IResult* mostSevereResult(const IResult* r1, const IResult* r2)
{
    if (!r1 || r1->resultType == IResultType::OK)
        return r2;
    if (!r2 || r2->resultType == IResultType::OK)
        return r1;
    if (r1->resultType == IResultType::Warning)
        return r2;
    if (r2->resultType == IResultType::Warning)
        return r1;
    if (r2->severity < r1->severity)
        return r2;
    return r1;
}

void ResultsDialog::buildTreeModel() {


    TreeItem* root = treeModel->root();

    std::map< int, QString > setNames;
    std::map< int, std::map<ITestGroup::Enum, QList<const IResult*> > > map;
    std::map< std::pair<int, ITestGroup::Enum>, std::map<IResultType::Enum, int> > mapErrors;
    std::map< std::pair<int, ITestGroup::Enum>, const IResult* > signifResult; // The result with the most severe error

    for(const IResult& result: qAsConst(results)) {
        // Ignore the Update DLN Database test:
        if (result.testGroup == ITestGroup::UpdateDLN)
            continue;
        setNames.insert({result.imageSetNum, result.imageSetName});
        map[result.imageSetNum][result.testGroup].append(&result);

        auto p = std::make_pair(result.imageSetNum, result.testGroup);
        mapErrors[p][result.resultType]++;
        signifResult[p] = mostSevereResult(&result, signifResult[p]);

        mapErrors[{result.imageSetNum, ITestGroup::None}][result.resultType]++;
        mapErrors[{-1, ITestGroup::None}][result.resultType]++;

    }

    for(const auto& v: map) {

        int currentImageSetNum = v.first;
        QString currentImageSetName = setNames[currentImageSetNum];

        std::map<IResultType::Enum, int>& errors = mapErrors[std::make_pair(currentImageSetNum, ITestGroup::None)];
        int errorCount = errors[IResultType::FileError] + errors[IResultType::TransformationError] + errors[IResultType::CaptureError];
        int warnCount = errors[IResultType::Warning];
        QString title = QString("Errors: %1, Warnings: %2").arg(errorCount).arg(warnCount);

        QList<QVariant> data;
        data << currentImageSetName
             << title
             << ""
             << "";
        TreeItem* imageSetRoot = new TreeItem(data, root);
        root->appendChild(imageSetRoot);

        for(const auto& vm: v.second) {
            ITestGroup::Enum currentTestGroup = vm.first;
            QString currentTestGroupName = ITestGroup::toString(currentTestGroup);
            const QList<const IResult*>& currentResults = vm.second;

            std::map<IResultType::Enum, int>& errors = mapErrors[{currentImageSetNum, currentTestGroup}];


            int errorCount = errors[IResultType::FileError] +
                    errors[IResultType::TransformationError] +
                    errors[IResultType::CaptureError] +
                    errors[IResultType::FatalError];
            int warnCount = errors[IResultType::Warning];
            QString title = QString("Errors: %1, Warnings: %2").arg(errorCount).arg(warnCount);

            QList<QVariant> data;
            data << currentTestGroupName
                 << title
                 << ""
                 << "";

            TreeItem* tgItem = new TreeItem(data, imageSetRoot);
            imageSetRoot->appendChild(tgItem);


            QRgb tgColor = colorCodeResult(signifResult[{currentImageSetNum, currentTestGroup}]);
            tgItem->setBgColor(tgColor);

            // if (errorCount == 0 && warnCount == 0) {
            //     continue;
            // }

            for (const IResult* result: currentResults) {
                // In the 3rd level, show only non-OK results:
                if (result->resultType == IResultType::OK)
                    continue;
                QList<QVariant> data;

                data << result->propQName
                     << IResultType::toString(result->resultType)
                     << (result->resultType == IResultType::OK ? "--" : QString::number(result->severity))
                     << result->title;

                TreeItem* item = new TreeItem(data, tgItem);
                tgItem->appendChild(item);
                item->setOpaque(const_cast<IResult*>(result));

                item->setBgColor(colorCodeResult(result));
            }
        }
    }
    /*
    for(int i=0; i<results.size(); ++i) {
        IResult& result = results[i];


        std::tie(currentImageSetNum,currentTestGroup,currentRoot) = parents.top();

        while (currentRoot != root && result.imageSetNum != currentImageSetNum) {
            parents.pop();
            std::tie(currentImageSetNum,currentTestGroup,currentRoot) = parents.top();
        }
        for (const IResult* p = parents.top()->result();
             p && p->imageSetName!=result.imageSetName && p->testGroup != result.testGroup;
             )
        {
            parents.pop();
            p = parents.top()->result();
        }
        TreeItem* parent = parents.top();

        QList<QVariant> data;

        data << result.imageSetName
             << IResultType::toString(result.resultType)
             << result.propQName
             << (result.resultType == IResultType::OK ? "--" : QString::number(result.severity))
             << result.title;

        TreeItem* item = new TreeItem(data, parent);
        item->setOpaque(&result);

        qDebug() << "Created " << result.title;
        parent->appendChild(item);
        if (! (parent->result() &&
                parent->result()->imageSetNum == result.imageSetNum &&
                parent->result()->testGroup == result.testGroup))
        {
            parents.push(item);
        }
    }
    */
}

void ResultsDialog::doubleClickOnTree(const QModelIndex& index)
{
    QVariant d = this->treeModel->data(index, Qt::DisplayRole);
    TreeItem* item = this->treeModel->item(index);
    IResult* result = static_cast<IResult*>(item->opaque());

    qDebug() << "dbl click on " << d << " Data: " << (result ? result->propQName : "");
    if (result) {
        ResultBox box(*result, projectDirectory);
        box.exec();
    }
}

/********************************************************************/
/*                                                                  */
/*  Methods to work with table models                               */
/*                                                                  */
/********************************************************************/

void ResultsDialog::addCell(QStandardItemModel * model, int rowNum, int colNum, const QString & data)
{
    // Add a cell to the specified model. To do this, get an index
    // for the cell and then set the data using that index.

    QModelIndex index = model->index(rowNum, colNum);
    model->setData(index, QVariant(data), Qt::DisplayRole);
}

void ResultsDialog::setUnderline(QStandardItemModel * model, int rowNum, int colNum)
{
    // Set the font in a cell to use an underline. This is done by setting
    // the Qt::FontRole data for the cell to a QFont object with the
    // underline property set to true.

    QModelIndex index = model->index(rowNum, colNum);
    QFont font;
    font.setUnderline(true);

    model->setData(index, QVariant(font), Qt::FontRole);
}

void ResultsDialog::setTextColor(QStandardItemModel * model, int rowNum, int colNum, const QColor & color)
{
    // Set the text in a cell to use a particular color. This is done by
    // setting the Qt::ForegroundRole data for the cell to use a QBrush
    // with that color.

    QModelIndex index = model->index(rowNum, colNum);
    QBrush brush;
    brush.setColor(color);

    model->setData(index, QVariant(brush), Qt::ForegroundRole);
}

/********************************************************************/
/*                                                                  */
/*  Utility methods                                                 */
/*                                                                  */
/********************************************************************/

QString ResultsDialog::getFilename(const QString & caption, const QString & extension, const QString & filters, QString * selectedFilter)
{
    // Create the suggested filename.

    QString suggested("inspect" + inspectionTime.toString("yyyy-MM-ddThh-mm") + extension);
    if (!projectDirectory.isEmpty() && (projectDirectory != "Custom"))
        suggested.prepend(projectDirectory + "/");

    // Prompt the user for the file name they want.

    QString filename = QFileDialog::getSaveFileName(this, caption, suggested, filters, selectedFilter);

    // Return the file name.

    return filename;
}

