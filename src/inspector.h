/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

#ifndef INSPECTOR_H
#define INSPECTOR_H

// Local headers

#include "ienums.h"
#include "ierrorcontext.h"
#include "imageset.h"
#include "inspectorxmp.h"
#include "iresult.h"
#include "options.h"
#include "propertyinfo.h"
#include "results.h"

// Qt headers

//#include <QAbstractMessageHandler>
#include <QDateTime>
#include <QDir>
#include <QHash>
#include <QProgressDialog>
#include <QStack>
#include <QString>
#include <QStringList>

#include <utility> // for std::pair

/********************************************************************/
/*                                                                  */
/*  Various structures                                              */
/*                                                                  */
/********************************************************************/

// The InspectionStatus struct is used to track the status
// of the inspection for each different test group. This allows
// us to return an OK result separately for each test group.

struct InspectionStatus
{
    IStatus::Enum file;
    IStatus::Enum capture;
    IStatus::Enum xform;
    IStatus::Enum updateDLN;

    void reset()
    {
        file = IStatus::Success;
        capture = IStatus::Success;
        xform = IStatus::Success;
        updateDLN = IStatus::Success;
    }
};

/********************************************************************/
/*                                                                  */
/*  Inspector class                                                 */
/*                                                                  */
/********************************************************************/

class Inspector
{
public:
    Inspector();
    ~Inspector();

    void inspect(PropInfoLists *propInfoLists, Results * results, const Options &options, const QString &projectDirectory, QList<ImageSet> &imageSets);

private:
    // Private variables

    // GUI member variables

    bool gui;
    QProgressDialog * progressDialog;
    int progress;

    // General member variables. These are variables that are
    // set at the start of the inspection and never change.

    PropInfoLists * propInfoLists;
    Results * results;
    Options options;
    QFile logFile;

    // Error handling variables. For more information, see the
    // full description at the addResults methods in inspector.cpp

    InspectionStatus status;
    IErrorContext e;

    QStack<PropInfo *> propStack;     // Set in getStructValue(), compareStructValues()

    // Lists used for special handling. We use these lists when we need
    // to retrieve the value of one property (e.g. aux:SerialNumber)
    // when processing a different property (e.g. exifEX:BodySerialNumber).
    //
    // WARNING! Keeping these lists at a global level instead
    // of passing them down through the methods is brittle. We
    // do it to avoid cluttering up method signatures. Because
    // these are set only for the top-level values, using them
    // for lower-level values may or may not work. Also, calling
    // compareValueLists() recursively will break the code.

    QList<PropInfo *> currPropInfoList;  // Set in compareValueLists()
    QList<XMPValue *> currValues1;       // Set in compareValueLists()
    QList<XMPValue *> currValues2;       // Set in compareValueLists()

    // Constants

    // Hashtable for converting namespace URIs to prefixes

    QHash<QString, QString> prefixes;

    // Hashtables of special property namespaces and names

    QHash<QString, QHash<QString, QString> *> specialProps;
    QHash<QString, QString> specialAuxProps;
    QHash<QString, QString> specialExifProps;
    QHash<QString, QString> specialExifEXProps;
    QHash<QString, QString> specialTiffProps;

    // Lists of synonym namespaces and names

    QList<TXMP_STRING_TYPE> lensNamespaces;
    QList<TXMP_STRING_TYPE> lensNames;
    QList<TXMP_STRING_TYPE> serialNumberNamespaces;
    QList<TXMP_STRING_TYPE> serialNumberNames;

    // Private methods

    // Initialize GUI

    void initProgressDialog(const QString & imageSetName, int size);

    // Inspect inspection sets

    void inspectSet(const ImageSet &imageSet, const int imageSetNum);
    void inspectFilenames(const ImageSet &imageSet, QFileInfo &firstArchivalFileInfo, QFileInfo &firstProcessedFileInfo);
    void inspectCaptureProps(const ImageSet &imageSet, const QFileInfo & firstArchivalFileInfo, const QFileInfo & firstProcessedFileInfo);
    void inspectXformProps(const ImageSet &imageSet, const QFileInfo &firstArchivalFileInfo, const QFileInfo &firstProcessedFileInfo);
    void inspectImages(const QList<PropInfo *> &currPropInfoList, const ImageSet &imageSet, const QFileInfo &firstArchivalFileInfo, const QFileInfo &firstProcessedFileInfo);
    void inspectDLN(const QList<PropInfo *> &propInfoList, const ImageSet & imageSet);
    void compareWithinFileType(const QList<PropInfo *> &currPropInfoList, const QList<QFileInfo> &fileInfos);
    void compareAcrossFileTypes(const QList<PropInfo *> &currPropInfoList, const QFileInfo &archivalFileInfo, const QFileInfo &processedFileInfo);

    void updateDatabase(const QList<ImageSet> &imageSets);

    // Get properties

    QList<XMPValue *> getDLNValues(const QList<PropInfo *> &currPropInfoList, const ImageSet &imageSet);
    QList<XMPValue *> getValues(const QList<PropInfo *> &currPropInfoList, const QFileInfo &fileInfo);
    XMPValue * getValue(SXMPMeta & meta, PropInfo * info, const XMP_StringPtr rootNS, const XMP_StringPtr parentPath);
    XMPValue * getSimpleValue(SXMPMeta & meta, PropInfo * info, const XMP_StringPtr rootNS, const XMP_StringPtr path);
    XMPValue * getStructValue(SXMPMeta & meta, PropInfo * info, const XMP_StringPtr rootNS, const XMP_StringPtr path);
    XMPValue * getArrayValues(SXMPMeta & meta, PropInfo * info, const XMP_StringPtr rootNS, const XMP_StringPtr path);

    // Get Document (1st element) and Instance (2nd element) Ids, which are actually GUIDs
    // See https://github.com/adobe/xmp-docs/blob/e2573ad7e7959e657b1aed704546e19319cb4f5d/XMPNamespaces/xmpMM.md
    ImageSet::DocumentIds getMediaManagementIds(const QFileInfo &fileInfo);

    // Validate properties

    void validateValues(const QList<PropInfo *> &propInfoList, const QFileInfo &fileInfo, const QList<XMPValue *> &values);
    void validateValue(const PropInfo *info, const XMPValue *value);
    bool evalRestriction(const PropInfo *info, const XMPValue *value, const Restriction *restriction);
    bool evalRestriction(const PropInfo *info, const XMPValue *value, const Restriction *restriction, int index);

    // Compare properties

    void compareValueLists(const QList<PropInfo *> &currPropInfoList, const QFileInfo &fileInfo1, const QList<XMPValue *> &values1, const QFileInfo &fileInfo2, const QList<XMPValue* > &values2, IComparisonType::Enum comparisonType);
    void compareValues(PropInfo * info, const XMPValue *value1, const XMPValue *value2);
    void compareSimpleValues(PropInfo *info, const XMPValue *value1, const XMPValue *value2);
    void compareStructValues(PropInfo * info, const XMPValue *value1, const XMPValue *value2);
    void compareArrayValues(PropInfo * info, const XMPValue *value1, const XMPValue *value2);
    bool checkNulls(const PropInfo *info, const XMPValue * value1, const XMPValue * value2);

    // Evaluate predicates

    IErrorCode::Enum evalPredicate(const PropInfo *info, const XMPValue * value1, IOperator::Enum op, const XMPValue * value2);
    IErrorCode::Enum evalBooleanPred(bool value1, IOperator::Enum op, bool value2);
    IErrorCode::Enum evalDatetimePred(const XMP_DateTime & value1, IOperator::Enum op, const XMP_DateTime & value2);
    IErrorCode::Enum evalIntegerPred(XMP_Int32 value1, IOperator::Enum op, XMP_Int32 value2);
    IErrorCode::Enum evalRationalPred(const XMPValue::rational & value1, IOperator::Enum op, const XMPValue::rational & value2);
    IErrorCode::Enum evalDoublePred(double value1, IOperator::Enum op, double value2);
    IErrorCode::Enum evalTextPred(const TXMP_STRING_TYPE & value1, IOperator::Enum op, const TXMP_STRING_TYPE & value2);

    // Predicate utilities

    void reduce(int & num, int & den);
    int greatestCommonDivisor(const int v1, const int v2);
    long safeProduct(const int value1, const int value2, bool & ok);
    bool doubleEquals(const double d1, const double d2, const double epsilon);
    bool doubleLessThan(const double v1, const double v2, const double epsilon);
    qint64 dateTimeDiff(const XMP_DateTime & dt1, const XMP_DateTime & dt2);
    QDateTime createUTCQDateTime(const XMP_DateTime & dt);

    // Special handling of properties

    void initSpecialHandling();
    bool specialHandling(PropInfo *info, const QFileInfo &fileInfo1, const XMPValue *value1, const QFileInfo &fileInfo2, const XMPValue *value2);
    bool checkDimensions(const PropInfo * info, const QFileInfo &fileInfo1, const XMPValue * value1, const QFileInfo &fileInfo2, const XMPValue * value2);
    bool getDimensions(const QString & filename, const QList<XMPValue *> &values, const PropInfo * info, const XMPValue * xValue, int & x, int & y);
    bool checkSynonymsAgainstDLN(const QString & mainSynonym, const QList<TXMP_STRING_TYPE> & synNamespaces, const QList<TXMP_STRING_TYPE> & synNames, PropInfo *info, const QFileInfo &fileInfo1, const XMPValue * value1, const QFileInfo &fileInfo2, const XMPValue * value2);
    void getNonNullSynonyms(const QFileInfo & fileInfo, const QList<XMPValue *> &values, const QList<TXMP_STRING_TYPE> & synNamespaces, const QList<TXMP_STRING_TYPE> & synNames, QList<PropInfo *> & synPropInfoList, QList<XMPValue *> & synValues);
    bool checkSynonymValues(const QList<PropInfo *> & synPropInfoList, const QList<XMPValue *> & synValues);
    XMPValue * getOtherValue(const QList<XMPValue *> &values, const TXMP_STRING_TYPE ns, const TXMP_STRING_TYPE name);
    PropInfo * getOtherInfo(const TXMP_STRING_TYPE ns, const TXMP_STRING_TYPE name);

    // Property utilities

    void toLowerCase(XMPValue * value);

    // Compare filenames

    void compareFilenames(const QString& imageSetName, const QList<QFileInfo> &archivalFileInfos, const QList<QFileInfo> &processedFileInfos, IFileType::Enum processedType, QFileInfo & firstArchivalFileInfo, QFileInfo & firstProcessedFileInfo);

    // File utilities

    void sortFiles(QList<ImageSet> & imageSets);
    static bool compareBaseNames(const QFileInfo &fileInfo1, const QFileInfo &fileInfo2);
    void lowerFileCase(QList<QFileInfo> & fileInfos);
    void write(QFile & file, const QString str);
    void write(QFile & file, const char * ptr_to_char);

    // Report results

    void addResult(IResultType::Enum resultType, IErrorCode::Enum code, const PropInfo * info, const XMPValue * value1, const XMPValue * value2);
    void addResult(IResultType::Enum resultType, const QString & title, const QString & htmlDescription, const QString & propPath);
    void addResult(IResultType::Enum resultType, const QString & title, const QString & description, const PropInfo *info, const XMPValue * value1, const XMPValue * value2);
    void addResult(IResultType::Enum resultType, const QString & title, const QString & description, const QString & propPath, const QString & value1, const QString & value2);
    void addResult(const IResult &result);
    void upgradeStatus(IStatus::Enum & status, IResultType::Enum resultType);
    QString propPath(const PropInfo *info);
    IResultType::Enum getResultType(ITestGroup::Enum testGroup);

};

#endif // INSPECTOR_H
