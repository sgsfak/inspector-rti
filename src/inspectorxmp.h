/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2015                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

#ifndef INSPECTORXMP_H
#define INSPECTORXMP_H

// The XMP SDK requires a number of include files and definitions.
// Rather than duplicate them in multiple compilation units, we
// include them in this header, which can then be included as needed.
//
// The XMP SDK has one include file not included here: XMP.incl_cpp.
// The XMP SDK documentation says that this file must be included in
// "exactly one of your source files". We include it in main.cpp.

// Specify the string type that will be used in the XMP SDK
// template classes. IMPORTANT: If you change this string type,
// you must also change the macros below as needed.

#include <string>
#define TXMP_STRING_TYPE std::string

// Tell the XMP SDK that we will be using the XMPFiles classes

#define XMP_INCLUDE_XMPFILES 1

// This is the main include for the XMP SDK ...

#include "XMP.hpp"

// Macros to convert between TXMP_STRING_TYPE and QString. These
// are necessary because our code is not supposed to know what type
// is specified by TXMP_STRING_TYPE and therefore does not know how
// to convert this type to/from QString. (Although unlikely, we could
// change it in the future.)

#define CSTRING_TO_TXMPSTRING(cstr) std::string(cstr)
#define QSTRING_TO_TXMPSTRING(qstr) (qstr).toStdString()
#define TXMPSTRING_TO_QSTRING(xmpstr) QString((xmpstr).c_str())
#define TXMPSTRING_TO_CSTRING(xmpstr) xmpstr.c_str()
#define NEW_TXMP_STRING_TYPE_Q(qstr) new std::string((qstr).toStdString().c_str())
#define NEW_TXMP_STRING_TYPE new std::string()

// Macro to compare two TXMP_STRING_TYPEs.

#define TXMP_STRING_EQUALS(str1, str2) (str1 == str2) // Returns boolean

#endif // INSPECTORXMP_H

