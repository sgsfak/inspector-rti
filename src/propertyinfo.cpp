/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

// Local headers

#include "ienums.h"
#include "iexception.h"
#include "iglobals.h"
#include "propertyinfo.h"

#include <iostream>
// Qt headers

#include <QCoreApplication>
#include <QFileInfo>
#include <QFile>
#include <QSqlDatabase>
#include <QStack>
#include <QUuid>

#include <QDebug>

/********************************************************************/
/*                                                                  */
/*  RestrictionSet methods                                          */
/*                                                                  */
/********************************************************************/

RestrictionSet::RestrictionSet() :
    resultType(IResultType::Unknown),
    restriction(nullptr),
    message(nullptr)
{
}

RestrictionSet::~RestrictionSet()
{
    delete restriction;
    delete message;
}


/********************************************************************/
/*                                                                  */
/*  Restriction methods                                             */
/*                                                                  */
/********************************************************************/

Restriction::Restriction() :
    isExpr(false),
    op(IOperator::Unknown),
    value(nullptr),
    useAnd(false),
    restriction1(nullptr),
    restriction2(nullptr)
{

}

Restriction::~Restriction()
{
    // Note to the confused (like me): If the restriction value is
    // an array -- that is, Restriction.value points to an
    // XMPValue that points to a QList -- the destructor for XMPValue
    // will delete the XMPValues pointed to by the QList.

    if (isExpr)
    {
        delete value;
    }
    else
    {
        delete restriction1;
        delete restriction2;
    }
}

QString Restriction::toString(QString propName, bool html)
{
    QString str;

    str.append("(");

    if (isExpr)
    {
        str.append(propName);
        str.append(" ");
        str.append(IOperator::toSymbol(op, html));
        str.append(" ");
        str.append(value->toString(false));
    }
    else
    {
        str.append(restriction1->toString(propName, html));
        if (useAnd)
            str.append(" and ");
        else
            str.append(" or ");
        str.append(restriction2->toString(propName, html));
    }
    str.append(")");

    return str;
}

/********************************************************************/
/*                                                                  */
/*  Factory function                                                */
/*                                                                  */
/********************************************************************/

QHash<QString, QString> initPrefixes()
{
    // Factory function to initialize the (constant, static)
    // hashtable of prefixes.

    QHash<QString, QString> prefixes;

    prefixes.insert("http://ns.adobe.com/exif/1.0/aux/", "aux");
    prefixes.insert("http://ns.adobe.com/camera-raw-settings/1.0/", "crs");
    prefixes.insert("http://ns.adobe.com/exif/1.0/", "exif");
    prefixes.insert("http://cipa.jp/exif/1.0/", "exifEX");
    prefixes.insert("http://ns.adobe.com/tiff/1.0/", "tiff");
    prefixes.insert("http://ns.adobe.com/xap/1.0/", "xmp");
    prefixes.insert("http://ns.adobe.com/xap/1.0/mm/", "xmpMM");
    prefixes.insert("http://culturalheritageimaging.org/resources/ns/inspector/properties/1.0", "chi");

    return prefixes;
}

static PropInfo* createMMIdPropInfo_(const char* const name)
{
    // See https://github.com/adobe/xmp-docs/blob/master/XMPNamespaces/xmpMM.md
    PropInfo * docIdPropInfo = new PropInfo();
    docIdPropInfo->ns = QSTRING_TO_TXMPSTRING(QString("http://ns.adobe.com/xap/1.0/mm/"));
    docIdPropInfo->name = QSTRING_TO_TXMPSTRING(QString(name));
    docIdPropInfo->simpleInfo = new SimpleTypeInfo();
    docIdPropInfo->simpleInfo->type = XMPType::Text;
    docIdPropInfo->severity = PropInfo::SEVERITY_OK;
    return docIdPropInfo;
}

/*
   The following method produces deterministic UUIDs (version 3, per RFC-4122)
   using the `type` and `name` arguments. The type corresponds to the specific
   entity type (e.g. "property") and the name is for example the qualified name
   of the property. To these, we mix the `version` of the properties read from
   the XML config files.

   So this function works as follows:

   * A "namespace" UUID (again v3, using createUuidV3) is created using the string "inspector" and
     the "nil" UUID (i.e. the one with all zeros)
   * The type, version, and given name are joined together with ':' as the separator
     and this is given to createUuidV3 again to produce the final "name-based" UUID.
 */
QString PropInfoLists::newNameUuid(const QString& type, const QString& name)
{
    // The following always returns {ad248e72-08ac-3e93-98d9-e5b8282727f5}
    // The same in Postgres: select uuid_generate_v3(uuid_nil(), 'inspector');
    static QUuid uuid_ns = QUuid::createUuidV3(QUuid(), QString("inspector"));

    QUuid nameUuid = QUuid::createUuidV3(uuid_ns, QString("%1:%2:%3").arg(type, this->version, name).toUtf8());
    /* The above cound be done in Postgres as follows (using
       the property table as an example):

          UPDATE property SET property_uuid =
            uuid_generate_v3(
                        uuid_generate_v3(uuid_nil(), 'inspector'),
                        'property:1.0:' || property_name);

    */
    qDebug().noquote() << "Produced" << nameUuid << "for string" << name;
    return nameUuid.toString(QUuid::WithoutBraces);
}

PropInfo* PropInfo::docIdPropInfo = createMMIdPropInfo_("DocumentID");
PropInfo* PropInfo::origDocIdPropInfo = createMMIdPropInfo_("OriginalDocumentID");

/********************************************************************/
/*                                                                  */
/*  PropInfo methods and variables                                  */
/*                                                                  */
/********************************************************************/

const QHash<QString, QString> PropInfo::prefixes = initPrefixes();

QString PropInfo::getQName() const
{
    return getQName(ns, name);
}

QString PropInfo::getPrefix() const
{
    return getPrefix(ns);
}

bool PropInfo::setCompare(const QString &compareTypes)
{
    // In the PropInfo constructor, compare is set to IComparisonType::All.
    // Reset it now so we can add the requested types.

    compare = IComparisonType::None;

    // Parse the string containing types separated by pipes
    // and loop through the types. For example, the string
    // might be "Restrictions|WithinFileType".

    QList<QString> types = compareTypes.split('|');
    for (int i = 0; i < types.size(); i++)
    {
        // Get the type from the substring.

        IComparisonType::Enum compareType = IComparisonType::fromString(types[i]);
        if (compareType == IComparisonType::Unknown) return false;

        // Bitwise OR the current value of compare with the
        // new type. Note that the enum is implicitly cast to
        // an int (widening) but we must explicitly cast it
        // to the enum (narrowing). (Guess who's confused
        // about casting?)

        compare = static_cast<IComparisonType::Enum>(compare | compareType);
    }

    return true;
}

QString PropInfo::getQName(const TXMP_STRING_TYPE ns, const TXMP_STRING_TYPE name)
{
    return getPrefix(ns) + ":" + TXMPSTRING_TO_QSTRING(name);
}

QString PropInfo::getPrefix(const TXMP_STRING_TYPE ns)
{
    return prefixes.value(TXMPSTRING_TO_QSTRING(ns));
}

/*
QString PropInfo::fingerprint() const
{
    QString f;

    f += TXMPSTRING_TO_QSTRING(ns);
    f +=":";
    f += TXMPSTRING_TO_QSTRING(name);
    f += ":";
    f += QString::number(this->severity);
    if (this->simpleInfo && this->simpleInfo->hasRestrictions) {
        f += this->simpleInfo->restrictionSet->restriction->toString(this->getQName(), false);
    }
    if (this->structInfo) {
        f += '[';
        const QList<PropInfo*>& infos = * (this->structInfo->propInfoList);
        for(const PropInfo* info: infos ) {
            f += info->fingerprint();
            f += ",";
        }
        f += ']';
    }

    QUuid nameUuid = newNameUuid(f);

    // Return a UUID format of the MD5 :
    return nameUuid.toString(QUuid::WithoutBraces);
}
*/

/********************************************************************/
/*                                                                  */
/*  PropInfoLists                                                   */
/*                                                                  */
/********************************************************************/

/********************************************************************/
/*                                                                  */
/*  Constructor and destructor                                      */
/*                                                                  */
/********************************************************************/

PropInfoLists::PropInfoLists(InspectorType::Enum inspectrType) :
    inspectorType(inspectrType),
    filePropInfoList(nullptr),
    dlnPropInfoList(nullptr),
    cameraIDPropInfoList(nullptr),
    capturePropInfoList(nullptr),
    xformPropInfoList(nullptr)
{

}

PropInfoLists::~PropInfoLists()
{
    // Release memory used by property information lists.

    releasePropInfoList(filePropInfoList);
    releasePropInfoList(dlnPropInfoList);
    releasePropInfoList(cameraIDPropInfoList);
    releasePropInfoList(capturePropInfoList);
    releasePropInfoList(xformPropInfoList);
}

/********************************************************************/
/*                                                                  */
/*  Public methods                                                  */
/*                                                                  */
/********************************************************************/

/********************************************************************/
/*                                                                  */
/*  Property information initialization                             */
/*                                                                  */
/********************************************************************/

void PropInfoLists::initialize()
{
    // Return if the lists are already initialized.

    if (filePropInfoList != nullptr) return;

    // Initialize the file property information list.

    initFilePropInfoList();

    // Initialize the property information lists.

    QXmlStreamReader reader;

    QString suffix;
    switch (inspectorType) {
    case InspectorType::RTI: suffix = "-RTI"; break;
    case InspectorType::Photogrammetry: suffix = "-Photo"; break;
    case InspectorType::Documentary: suffix = "-DIS"; break;
    case InspectorType::MultiSpectral: suffix = "-MS"; break;
    }
    dlnPropInfoList = initPropInfoList(reader, "PropsDLN" + suffix + ".xml");
    cameraIDPropInfoList = initPropInfoList(reader, "PropsCameraID" + suffix + ".xml");
    capturePropInfoList = initPropInfoList(reader, "PropsCapture" + suffix + ".xml");
    xformPropInfoList = initPropInfoList(reader, "PropsXform" + suffix + ".xml");

    if (usingDB)
    {
        initDatabaseProps();
    }

//    debugList(capturePropInfoList);
}

/********************************************************************/
/*                                                                  */
/*  Private methods                                                 */
/*                                                                  */
/********************************************************************/

void PropInfoLists::initFilePropInfoList()
{
    // The Files tests do not compare properties. Instead, they compare
    // filenames. For completeness (and to avoid the inevitable bugs
    // that would occur if we have a test group in the database without
    // a property group or properties), we create a fake property named
    // chi:Filename. Note that this is currently only used if we are
    // using the database, but we always initialize it just for safety.

    SimpleTypeInfo * simpleInfo = new SimpleTypeInfo();
    simpleInfo->type = XMPType::Text;

    PropInfo * propInfo = new PropInfo();
    propInfo->ns = QSTRING_TO_TXMPSTRING(QString("http://culturalheritageimaging.org/resources/ns/inspector/properties/1.0"));
    propInfo->name = QSTRING_TO_TXMPSTRING(QString("Filename"));
    propInfo->simpleInfo = simpleInfo;
    propInfo->severity = PropInfo::SEVERITY_WARNING; // Was 0, which maps to ??

    filePropInfoList = new QList<PropInfo *>();
    filePropInfoList->append(propInfo);
}

/********************************************************************/
/*                                                                  */
/*  Read property information file                                  */
/*                                                                  */
/********************************************************************/

QList<PropInfo *> * PropInfoLists::initPropInfoList(QXmlStreamReader & reader, const QString & filename)
{
    // Allocate the new property info list.

    QList<PropInfo *> *propInfoList = new QList<PropInfo *>();

    // Open the XML file and pass it to the QXmlStreamReader.

#if defined(Q_OS_OSX)
    QString resourcesDir = QCoreApplication::applicationDirPath() +"/../Resources";
#else
    QString resourcesDir = QCoreApplication::applicationDirPath();
#endif

    QFile file(resourcesDir + "/" + filename);
    if (!file.open(QIODevice::ReadOnly))
    {
        throw IOException(QString("Property info initialization error"),
                          QString("Required file %1 could not be opened. This file should be in directory %2.").arg(filename, resourcesDir));
    }
    reader.setDevice(&file);

    // Initialize the variables used to construct the property information list.
    // Note that we initially set the current property information list to
    // the list variable passed to this method.

    PropInfo * currPropInfo = nullptr;
    SimpleTypeInfo * currSimpleInfo = nullptr;
    StructTypeInfo * currStructInfo = nullptr;
    QList<PropInfo *> * currPropInfoList = propInfoList;
    Restriction * currRestriction = nullptr;
    Restriction * tmpRestriction = nullptr;
    QXmlStreamAttributes attrs;
    QString version;

    // Create stack variables to store the current state when
    // we encounter nested structures.

    QStack<PropInfo *> propInfoStack;
    QStack<StructTypeInfo *> structInfoStack;
    QStack<QList<PropInfo *> *> propListStack;
    QStack<Restriction *> restrictionStack;

    // Variables for PCDATA, to track the nesting level,
    // and to track array sizes.

    QString content;
    int level = 0;
    int prevArraySize = -1;
    bool ok;

    // Read the property information document and construct
    // the list.

    while (!reader.atEnd())
    {
        QXmlStreamReader::TokenType tokenType = reader.readNext();
        switch (tokenType)
        {
            case QXmlStreamReader::Invalid:

                // A read error occurred. Stop.

                break;

            case QXmlStreamReader::StartElement:

                // Process element start tags.

                // Check that the element uses the correct namespace.

                if (reader.namespaceUri() != "http://culturalheritageimaging.org/resources/ns/inspector/properties/1.0") reader.raiseError(QString("Invalid namespace: ").append(reader.namespaceUri()).append("."));

                // Switch based on the element type.

                switch (PropElementToken::fromString(reader.name()))
                {

                case PropElementToken::PropInfoDoc:
                    attrs = reader.attributes();
                    version = attrs.value(PropAttrToken::toString(PropAttrToken::Version)).toString();
                    if (this->version.isEmpty())
                    {
                        this->version = version;
                    }
                    else
                    {
                        if (this->version != version)
                        {
                            reader.raiseError(QString("Property version in this file (").append(version).append(") different from version in previous file (").append(this->version).append(")."));
                        }
                    }
                    break;

                    case PropElementToken::PropInfo:

                        // If we are inside a structure, push the current PropInfo
                        // on the PropInfo stack.

                        if (level) propInfoStack.push(currPropInfo);

                        // Allocate a new PropInfo structure and add it to the list.

                        currPropInfo = new PropInfo();
                        currPropInfoList->append(currPropInfo);
                        break;

                    case PropElementToken::Namespace:

                        // Store the property's namespace in the PropInfo structure.
                        // For information about the QSTRING_TO_TXMPSTRING macro, see
                        // inspectorxmp.h.

                        currPropInfo->ns = QSTRING_TO_TXMPSTRING(reader.readElementText());
                        break;

                    case PropElementToken::Name:

                        // Store the property's name in the PropInfo structure. For
                        // information about the QSTRING_TO_TXMPSTRING macro, see
                        // inspectorxmp.h.


                        currPropInfo->name = QSTRING_TO_TXMPSTRING(reader.readElementText());
                        break;

                    case PropElementToken::IsArray:

                        // Store whether the property is an array in the PropInfo structure.

                        content = reader.readElementText();
                        if (!toBool(content, currPropInfo->isArray))
                        {
                            reader.raiseError(QString("Invalid IsArray value: ").append(content).append("."));
                        }
                        break;

                    case PropElementToken::Severity:

                        // Store whether the severity in the PropInfo structure.

                        content = reader.readElementText();
                        currPropInfo->severity = static_cast<PropInfo::SEVERITY>(content.toInt(&ok));
                        if (!ok)
                        {
                            reader.raiseError(QString("Invalid Severity value: ").append(content).append("."));
                        }
                        break;

                    case PropElementToken::Compare:

                        // Comparisons that the property can be used in. The Types
                        // attribute is required and its value must be "All", "None",
                        // or any of the following separated by pipes (|) with no
                        // spaces:
                        //
                        //     Restrictions
                        //     WithinFileType
                        //     AcrossFileTypes
                        //     AgainstDLN
                        //
                        // For example, if you want a property only to be compared
                        // against its restrictions, use:
                        //
                        //     <Compare Types="Restrictions" />
                        //
                        // If you want a property to be compared against its
                        // restriction and within a file type -- for example, JPEG
                        // vs. JPEG, but not JPEG vs. DNG -- use:
                        //
                        //     <Compare Types="Restrictions|WithinFileType" />
                        //
                        // If this element is missing, the code assumes a type of
                        // "All" -- this is set by the PropInfo constructor.

                        attrs = reader.attributes();
                        if (!currPropInfo->setCompare(attrs.value("Types").toString()))
                            reader.raiseError("Invalid value in Types attribute of Compare element: \"" + attrs.value("Types").toString() + "\".");
                        break;

                    case PropElementToken::SimpleType:

                        // Allocate a new SimpleTypeInfo and add it to the PropInfo.

                        currSimpleInfo = new SimpleTypeInfo();
                        currPropInfo->simpleInfo = currSimpleInfo;
                        break;

                    case PropElementToken::StructType:

                        // If we are already inside a structure, push the current
                        // StructTypeInfo on the StructTypeInfo stack.

                        if (level) structInfoStack.push(currStructInfo);

                        // Increment the level so we know we are inside a structure.

                        level++;

                        // Allocate a new StructTypeInfo, set the PropInfo type
                        // to XMPType::Struct and add the StructTypeInfo to the PropInfo.

                        currStructInfo = new StructTypeInfo();
                        currPropInfo->isStruct = true;
                        currPropInfo->structInfo = currStructInfo;

                        // Push the current property information list on the QList
                        // stack, then allocate a new QList for the structure and
                        // add it to the StructTypeInfo.

                        propListStack.push(currPropInfoList);
                        currPropInfoList = new QList<PropInfo *>();
                        currStructInfo->propInfoList = currPropInfoList;
                        break;

                    case PropElementToken::Type:
                    {
                        // Store the property's type in the SimpleTypeInfo structure.

                        content = reader.readElementText();
                        if (!XMPType::fromString(content, currSimpleInfo->type))
                        {
                            reader.raiseError(QString("Invalid XMP data type name: ").append(content).append("."));
                        }
                        break;
                    }

                    case PropElementToken::Restrictions:
                        currSimpleInfo->hasRestrictions = true;
                        currSimpleInfo->restrictionSet = new RestrictionSet();
                        break;

                    case PropElementToken::ResultType:
                        content = reader.readElementText();
                        currSimpleInfo->restrictionSet->resultType = IResultType::fromString(content);
                        switch (currSimpleInfo->restrictionSet->resultType)
                        {
                            case IResultType::CaptureError:
                            case IResultType::FileError:
                            case IResultType::TransformationError:
                            case IResultType::Warning:
                                break;

                            default:
                                reader.raiseError(QString("Invalid ResultType value: ").append(content).append("."));
                                break;
                        }
                        break;

                    case PropElementToken::Restriction:
                        // Create a new restriction.

                        tmpRestriction = new Restriction();

                        // If no restriction already exists, set this restriction
                        // on currSimpleInfo. Otherwise, figure out if this is
                        // the first or second nested restriction and set the
                        // corresponding field in currRestriction, as well as
                        // the isExpr flag.

                        if (currRestriction == nullptr)
                        {
                            currSimpleInfo->restrictionSet->restriction = tmpRestriction;
                        }
                        else
                        {
                            if (currRestriction->restriction1 == nullptr)
                            {
                                currRestriction->restriction1 = tmpRestriction;
                                currRestriction->isExpr = false;
                            }
                            else
                            {
                                currRestriction->restriction2 = tmpRestriction;
                            }
                        }

                        // Push the current restriction (if any) on the stack and
                        // and set the new current restriction.

                        if (currRestriction != nullptr) restrictionStack.push(currRestriction);
                        currRestriction = tmpRestriction;

                        break;

                    case PropElementToken::Message:
                        currSimpleInfo->restrictionSet->message = new QString(reader.readElementText());
                        break;

                    case PropElementToken::Operator:
                        // Get the operator

                        content = reader.readElementText();
                        currRestriction->op = IOperator::fromString(content);
                        if (currRestriction->op == IOperator::Unknown)
                        {
                            reader.raiseError(QString("Invalid operator value: ").append(content).append("."));
                        }

                        // Set the isExpr flag.

                        currRestriction->isExpr = true;
                        break;

                    case PropElementToken::Value:
                    {
                        XMPValue * value = new XMPValue();
                        content = reader.readElementText();
                        if (!XMPValue::fromString(currSimpleInfo->type, content, *value))
                        {
                            reader.raiseError(QString("Invalid value of type ").append(XMPType::toString(currSimpleInfo->type)).append(": ").append(content).append("."));
                        }

                        // Set the restriction.

                        if (currPropInfo->isArray)
                        {
                            // Handle array-valued properties.

                            if (currRestriction->value == nullptr)
                            {
                                // Check if an XMPValue has been allocated to hold
                                // the array of values. If it has not, allocate an
                                // XMPValue, set its isArray flag to true, and
                                // allocate a QList to hold the array of values.

                                currRestriction->value = new XMPValue();
                                currRestriction->value->unionType = XMPUnionType::Array;
                                currRestriction->value->isNull = false;
                                currRestriction->value->ptr_array = new QList<XMPValue *>();
                            }

                            // Add the value to the QList.

                            currRestriction->value->ptr_array->append(value);
                        }
                        else
                        {
                            // For non-array-valued properties, just set the value.

                            currRestriction->value = value;
                        }
                        break;
                    }

                    case PropElementToken::AndOr:
                        content = reader.readElementText();
                        if (content == "AND")
                            currRestriction->useAnd = true;
                        else if (content == "OR")
                            currRestriction->useAnd = false;
                        else
                            reader.raiseError(QString("Invalid AndOr value: ").append(content).append("."));
                        break;

                    case PropElementToken::DLNQuery:
                        currSimpleInfo->dlnQuery = new QString(reader.readElementText());
                        break;

                    case PropElementToken::ErrorMessage:
                        currSimpleInfo->errorMessage = new QString(reader.readElementText());
                        break;

                    case PropElementToken::Unknown:

                        // Raise an error if we encounter an element with an unknown tag.

                        reader.raiseError(QString("Invalid element type name: ").append(reader.name()).append("."));
                        break;

                    default:
                        break;
                }

                break;

            case QXmlStreamReader::EndElement:

                // Process element end tags.

                // Check that the element uses the correct namespace.

                if (reader.namespaceUri() != "http://culturalheritageimaging.org/resources/ns/inspector/properties/1.0") reader.raiseError(QString("Invalid namespace: ").append(reader.namespaceUri()).append("."));

                // Switch based on the element type.

                switch (PropElementToken::fromString(reader.name()))
                {
                    case PropElementToken::PropInfo:

                        // If we were in a structure, pop the top PropInfo
                        // off the PropInfo stack.

                        if (level) currPropInfo = propInfoStack.pop();
                        break;

                    case PropElementToken::StructType:

                        // Decrement the level. If this structure is inside another
                        // structure, pop the top StructTypeInfo off the stack
                        // and return to that previous structure.

                        level--;
                        if (level) currStructInfo = structInfoStack.pop();

                        // Pop this structure's QList off the property list stack.

                        currPropInfoList = propListStack.pop();
                        break;

                    case PropElementToken::Restriction:

                        // If the current restriction is an expression and the property
                        // is array-valued, check that the number of values entered for
                        // the restriction value are the same as the number of values
                        // entered for the previous array. If not, return an error.

                        if (currRestriction->isExpr && currPropInfo->isArray)
                        {
                            int currArraySize = currRestriction->value->ptr_array->size();
                            if ((prevArraySize != -1) && (currArraySize != prevArraySize))
                            {
                                QString msg;
                                msg.append("All arrays used in a restriction must have the same size. For a restriction on the ");
                                msg.append(currPropInfo->getQName());
                                msg.append(" property, one array has ");
                                msg.append(QString().setNum(currRestriction->value->ptr_array->size()));
                                msg.append(" elements and another array has ");
                                msg.append(QString().setNum(prevArraySize));
                                msg.append(" elements.");
                                reader.raiseError(msg);
                            }
                            else
                            {
                                prevArraySize = currArraySize;
                            }
                        }

                        // We are finished processing the current restriction. Pop the
                        // top restriction off the stack and set current restriction to that.
                        // If we are done processing the restrictions for a property, reset
                        // the current restriction and the previous array size.

                        if (restrictionStack.isEmpty())
                        {
                            currRestriction = nullptr;
                            prevArraySize = -1;
                        }
                        else
                        {
                            currRestriction = restrictionStack.pop();
                        }
                        break;

                    case PropElementToken::Unknown:

                        // Raise an error if we encounter an element with an unknown tag.

                        reader.raiseError(QString("Invalid element type name: ").append(reader.name()).append("."));
                        break;

                    default:

                        // No end tag processing for other elements.

                        break;
                }
                break;

            default:
                break;
        }
    }

    // Close the XML document.

    file.close();

    // If there was an error during processing, throw an exception.

    if (reader.hasError())
    {
        throw XMLException(QString("Property info initialization error"), reader.errorString().append(" In file ").append(filename).append(". Please contact CHI."));
    }

    // Return the property information list.

    /* // STELIOS DEBUG properties
    for (auto p: qAsConst(*propInfoList)) {
        qDebug() << p->getPrefix() << p->name.c_str() << " " << ((p->simpleInfo != nullptr) ? XMPType::toString(p->simpleInfo->type) : "");
        // std::cout << p->prefixes[p->ns.c_str()].toStdString() << ":" << p->name << std::endl;
    }
    */
    return propInfoList;
}

bool PropInfoLists::toBool(const QString &str, bool & b)
{
    // Convert a string to a bool.

    if (str == "true")
    {
        b = true;
    }
    else if (str == "false")
    {
        b = false;
    }
    else
    {
        return false;
    }

    // Return true if we made it here.

    return true;
}

/********************************************************************/
/*                                                                  */
/*  Initialize properties in the databsae                           */
/*                                                                  */
/********************************************************************/

void PropInfoLists::initDatabaseProps()
{
    // Get the database and get a query.

    QSqlDatabase db = QSqlDatabase::database(dlnDBName);
    QSqlQuery q(db);

    // Retrieve the property information for the current versions
    // of the properties. getPropHashes() returns true if we were
    // successful -- that is, any rows were found. If no rows were
    // found, store the current properties.

    // WARNING: This code depends on the state of the database being
    // correct. That is, it assumes that if one property group matching
    // the current version is in the database, all the test groups,
    // property groups, and properties for that version are in the
    // database. Fixing this would require adding version at a level
    // above the testgroup table, which is ugly.

    QString query = "SELECT tg.srl, tg.testgroup_name, p.property_name, p.srl "
                    "FROM testgroup tg, propertygroup pg, property p "
                    "WHERE tg.srl = pg.testgroup_srl AND "
                           "pg.propertygroup_version='" + version + "' AND "
                           "pg.srl = p.propertygroup_srl";
    execQuery(q, query);

    // Attempt to build the property hash tables. If no rows were found,
    // store the properties (and build the property hash tables).

    if (!buildPropHashes(q))
    {
        storeProps(db);
    }
}

bool PropInfoLists::buildPropHashes(QSqlQuery & q)
{
    bool found_any{false};
    int prevTestgroupSrl(-1), testgroupSrl;

    while (q.next())
    {
        found_any = true;

        // If we have a new test group, store its srl.

        testgroupSrl = q.value(0).toInt();
        if (testgroupSrl != prevTestgroupSrl)
        {
            prevTestgroupSrl = testgroupSrl;
            testgroupSrlsHash.insert(q.value(1).toString(), testgroupSrl);
        }

        // Store the property srl, hashed by the property's
        // hierarchical name (e.g. /foo:bar or /foo:bar/foo.baz).

        propSrlsHash.insert(q.value(2).toString(), q.value(3).toInt());
    }
    q.finish();

    // Return whether we found any rows -- that is, we are not
    // on the first row.

    return found_any;
}

void PropInfoLists::storeProps(QSqlDatabase & db)
{
    qDebug() << "Storing properties in the DB...";
    // I apologize for the datatype of listOfPropInfoLists...

    QList<QString> propGroupNames;
    QList<QList<PropInfo *> *> listOfPropInfoLists;

    // Start a transaction.

    startTransaction(db);

    // Store the test group, property group (none), and properties (none)
    // for the Files test group.

    propGroupNames << "File";
    listOfPropInfoLists << filePropInfoList;
    storePropGroup(db, ITestGroup::File, propGroupNames, listOfPropInfoLists);

    // Store the test group, property group (none), and properties (none)
    // for the Capture test group.

    propGroupNames.clear();
    listOfPropInfoLists.clear();
    propGroupNames << "DLN" << "Camera ID" << "Capture";
    listOfPropInfoLists << dlnPropInfoList << cameraIDPropInfoList << capturePropInfoList;
    storePropGroup(db, ITestGroup::Capture, propGroupNames, listOfPropInfoLists);

    // Store the test group, property group (none), and properties (none)
    // for the Transformation test group.

    propGroupNames.clear();
    listOfPropInfoLists.clear();
    propGroupNames << "Xform";
    listOfPropInfoLists << xformPropInfoList;
    storePropGroup(db, ITestGroup::Transformation, propGroupNames, listOfPropInfoLists);

    // Commit the transaction.

    commitTransaction(db);
}

void PropInfoLists::storePropGroup(QSqlDatabase &db, ITestGroup::Enum testGroup, const QList<QString> & propGroupNames, const QList<QList<PropInfo *> *> & listOfPropInfoLists)
{
    // Insert the test group.

    QString tgName = ITestGroup::toString(testGroup);
    QString tgUuidName = newNameUuid("testgroup", tgName);
    QString query = "WITH ins AS ("
                    "  INSERT INTO testgroup(testgroup_name, testgroup_uuid) "
                    "  VALUES (:tgName, :tgUuidName) "
                    "  ON CONFLICT (testgroup_uuid) DO NOTHING "
                    "  RETURNING srl"
                    ") "
                    "SELECT srl, 'inserted' as t FROM ins "
                    " UNION ALL "
                    "SELECT srl, 'existing' as t FROM testgroup "
                    " WHERE testgroup_uuid=:testgroup_uuid";

    QSqlQuery q{db};
    prepareQuery(q, query);
    q.bindValue(":tgName", tgName);
    q.bindValue(":tgUuidName", tgUuidName);
    execPrepared(q);

    // Get the srl of the newly inserted row and add it to the
    // list of test group srls.
    q.next();

    int testgroupSrl = q.value(0).toInt();
    testgroupSrlsHash.insert(ITestGroup::toString(testGroup), testgroupSrl);
    q.finish();

    // Loop through the property groups and insert them and their properties.

    for (int i = 0; i < propGroupNames.size(); i++)
    {
        // Insert the property group.

        QString pgName = propGroupNames[i];
        QString pgUuidName = newNameUuid("propertygroup", pgName);

        QString query = "INSERT INTO propertygroup "
                        " (testgroup_srl, propertygroup_name, propertygroup_uuid, propertygroup_version) "
                        "VALUES (:tgSrl, :pgName, :pgUuidName, :version) "
                        "ON CONFLICT (propertygroup_uuid) DO UPDATE SET propertygroup_version=:version "
                        "RETURNING srl";

        QSqlQuery q{db};
        prepareQuery(q, query);
        q.bindValue(":tgSrl", testgroupSrl);
        q.bindValue(":pgName", pgName);
        q.bindValue(":pgUuidName", pgUuidName);
        q.bindValue(":version", this->version);
        execPrepared(q);


        // Get the srl of the newly inserted row or the updated one:
        q.next();

        int propgroupSrl = q.value(0).toInt();
        q.finish();

        // Prepare the statement for inserting properties.

        query = "INSERT INTO property "
                "(propertygroup_srl, property_name, restrictions, compare, property_uuid, severity) "
                "VALUES (" + QString::number(propgroupSrl) + ", :propName, :restrictions, :compare, :propUuid, :severity) "
                "ON CONFLICT (property_uuid) DO UPDATE SET restrictions=:restrictions, compare=:compare, severity=:severity ";
        prepareQuery(q, query);

        // Loop through the properties and insert them.

        QList<PropInfo *> * propInfoList = listOfPropInfoLists[i];
        storePropList(q, propInfoList, QString());
        q.finish();
    }
}

void PropInfoLists::storePropList(QSqlQuery & q, const QList<PropInfo *> * propInfoList, const QString & parentHName)
{
    for (int i = 0; i < propInfoList->size(); i++)
    {
        // Get the PropInfo. Note that we construct a hierarchical
        // name for the property. This is so the property name
        // shows the hierarchical structure of properties without
        // having to directly store that structure itself, such
        // as by having a parent column in property. (SQL cannot
        // easily query abitrarily deep hierarchical structures.)
        //
        // One consequence of this decision is that we don't store
        // the prefix or namespace in a separate column from the name.

        PropInfo * propInfo = propInfoList->at(i);
        QString hName = parentHName + "/" + propInfo->getQName();
        QString restriction;
        QString resultType;
        if (!propInfo->isStruct &&
            propInfo->simpleInfo->hasRestrictions)
        {
            restriction = propInfo->simpleInfo->restrictionSet->restriction->toString(propInfo->getQName(), false);
            resultType = IResultType::toString(propInfo->simpleInfo->restrictionSet->resultType);
        }

        // Set the parameters.

        q.bindValue(":propName", hName);
        q.bindValue(":restrictions", restriction);
        q.bindValue(":compare", propInfo->compare);
        q.bindValue(":propUuid", newNameUuid("property", hName));
        q.bindValue(":severity", propInfo->severity);

        // Insert the property.

        execPrepared(q);

        // Get the srl of the newly inserted row and store it
        // in the hash of property srls.

        propSrlsHash.insert(hName, q.lastInsertId().toInt());

        // If the property is a structure, recursively call this method
        // to insert the child properties. We maintain hierarchical
        // information about the property by passing in the parent's
        // hierarchical name.

        if (propInfo->isStruct)
        {
            storePropList(q, propInfo->structInfo->propInfoList, hName);
        }
    }
}

/********************************************************************/
/*                                                                  */
/*  Release PropInfo memory                                         */
/*                                                                  */
/********************************************************************/

void PropInfoLists::releasePropInfoList(QList<PropInfo *> *propInfoList)
{
    // If the PropInfoList is NULL, just return.

    if (propInfoList == nullptr) return;

    // Traverse the list and release memory.

    for (int i = 0; i < propInfoList->size(); i++)
    {
        PropInfo * propInfo = propInfoList->at(i);

        if (propInfo->isStruct)
        {
            // For structures, recursively call releasePropInfoList
            // to release the memory in the list, then delete the StructInfo.

            releasePropInfoList(propInfo->structInfo->propInfoList);
            delete propInfo->structInfo;
        }
        else // if (propInfo->type != XMPType::Unknown)
        {
            // For simple types (everything else except Unknown), delete
            // the restriction set (if any), the query (if any), the
            // error message (if any), and the SimpleInfo structure.

            if (propInfo->simpleInfo->hasRestrictions)
            {
                delete propInfo->simpleInfo->restrictionSet;
            }
            delete propInfo->simpleInfo->dlnQuery;
            delete propInfo->simpleInfo->errorMessage;
            delete propInfo->simpleInfo;
        }

        // Delete the PropInfo structure.

        delete propInfo;
    }

    // Delete the QList.

    delete propInfoList;
}

/*void PropInfoLists::debugList(QList<PropInfo *> * propInfoList)
{
    // This method prints PropInfos. Not sure if it is out
    // of date -- check against the current PropInfo structure.

    if (propInfoList == nullptr) return;

    for (int i = 0; i < propInfoList->size(); i++)
    {
        PropInfo * p = propInfoList->at(i);

        qDebug(p->ns.c_str());
        qDebug(p->name.c_str());
        if (p->isArray)
            qDebug("is array");
        else
            qDebug("not array");
        qDebug(XMPType::toString(p->type).toStdString().c_str());
        if (p->type == XMPType::Struct) debugList(p->structInfo->propInfoList);
    }
}*/
