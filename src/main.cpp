/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

// Local headers

#include "dirpattern.h"
#include "iexception.h"
#include "iglobals.h"
#include "mainwindow.h"
#include "options.h"
#include "propertyinfo.h"

// Qt headers

#include <QApplication>
#include <QList>
#include <QMessageBox>
#include <QString>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QVariant>
#include <QDir>

#include <QDebug>

// C++ headers

#include <stdio.h>

// XMP SDK headers

// Our include file to set up the XMP SDK:

#include "inspectorxmp.h"

// Mysterious include file that the XMP SDK documentation says must
// be included in "exactly one of your source files". Bleagh.

#include "XMP.incl_cpp"

// Set globals based on #define in .pro file.

#ifdef RTI_BUILD
    InspectorType::Enum globalInspectorType = InspectorType::RTI;
    QString projectTermIC = "Project";
    QString projectTermLC = projectTermIC.toLower();
    QString projectTermUC = projectTermIC.toUpper();
    QString imageSetTermIC = "Image";
    QString imageSetTermLC = imageSetTermIC.toLower();
    QString imageSetTermUC = imageSetTermIC.toUpper();
#endif

#ifdef PHOTO_BUILD
    InspectorType::Enum globalInspectorType = InspectorType::Photogrammetry;
    QString projectTermIC = "Group";
    QString projectTermLC = projectTermIC.toLower();
    QString projectTermUC = projectTermIC.toUpper();
    QString imageSetTermIC = "Image";
    QString imageSetTermLC = imageSetTermIC.toLower();
    QString imageSetTermUC = imageSetTermIC.toUpper();
#endif

QString dlnDBName("dln");
bool usingDB(true);

// Set the version number

const QString inspectorVersionNumber("1.0");

// Declare functions.

void initializeXMP();
void terminateXMP();
int openDatabase(QSqlError &e);
void getConnectionInfo(QString & userName, QString & password, QString & databaseName, QString & driverName, QString & hostName);
bool continueWithoutDatabase(Options & options, const QSqlError & e);
bool continueWithoutDatabase(Options & options);
void turnOffDatabase(Options & options);
void closeDatabase();

/********************************************************************/
/*                                                                  */
/* main() function                                                  */
/*                                                                  */
/********************************************************************/

int main(int argc, char *argv[])
{
    // Create the Qt GUI application and set the organization and
    // application names. These are used to create the directory
    // for the options file. See:
    //
    // https://stackoverflow.com/questions/32525196/how-to-get-a-settings-storage-path-in-a-cross-platform-way-in-qt

    QApplication a(argc, argv);
    a.setOrganizationName("CHI");
    a.setApplicationName("DLN-Inspector");

    // Initialize the property lists, directory patterns, options,
    // and database connection.

    PropInfoLists propInfoLists(globalInspectorType);
    QList<DirPattern> dirPatterns;
    Options options;

    QDir::setCurrent(QCoreApplication::applicationDirPath());

    try
    {
        // Initialize the XMP SDK.

        initializeXMP();

        // Retrieve the options from the options-(RTI|PG).db file.

        OptionsFactory optionsFactory(globalInspectorType);
        options = optionsFactory.fromOptionsDB();

        // If the options say the user wants to use the
        // DLN database, open the database.

        if (options.useDatabase)
        {
            QSqlError e;
            int open = openDatabase(e);
            if (open == 0)
            {
                usingDB = true;
            }
            else if (open == 1)
            {
                // If we are unable to open the database, warn the
                // user and ask if they want to continue. If not,
                // exit and let the user fix the problem.

                if (!continueWithoutDatabase(options, e)) return 0;
                usingDB = false;
            }
            else // if (open == 2)
            {
                // If we are able to open the database but can't find
                // the inspection table, it means the database is
                // installed and running but the Inspector tables have
                // not been installed. In this case, we want to tell
                // the user to install the Inspector tables and give
                // them the option of continuing without the DLN.

                if (!continueWithoutDatabase(options)) return 0;
                usingDB = false;
            }
        }
        else
        {
            usingDB = false;
        }

        // Initialize the information about properties.

        propInfoLists.initialize();

        // Retrieve the directory pattern information.

        if (!usingDB)
        {
            dirPatterns = DirPatternFactory::getDirPatterns(globalInspectorType);
        }
    }
    catch (IException e)
    {
        // If there was an error, display the error information.

        QMessageBox::critical(nullptr, e.title, e.msg);

        // Terminate the XMP SDK.

        terminateXMP();

        // Return an error.

        return 1;
    }

    // Create the main window and show it.

    int returnCode;
    try
    {
        MainWindow w(globalInspectorType, &propInfoLists, options, dirPatterns);
        w.show();

        // Start the event-processing loop.

        returnCode = a.exec();
    }
    catch (IException e)
    {
        QMessageBox::critical(nullptr, e.title, e.msg);
    }

    // Terminate the XMP SDK.

    terminateXMP();

    // Close the database.

    closeDatabase();

    // Return.

    return returnCode;
}

/********************************************************************/
/*                                                                  */
/*  XMP SDK initialization and termination methods                  */
/*                                                                  */
/********************************************************************/

void initializeXMP()
{
    // Initialize the XMP SDK. We only want to do this once, so we
    // do it in main(), rather than later.

    try
    {
        if (!SXMPMeta::Initialize())
        {
            throw XMP_Error(kXMPErr_Unknown, "An unknown error occurred while initializing the XMP SDK. Please contact CHI for more information.");
        }

        if (!SXMPFiles::Initialize())
        {
            throw XMP_Error(kXMPErr_Unknown, "An unknown error occurred while initializing the XMP SDK. Please contact CHI for more information.");
        }
    }
    catch(XMP_Error e)
    {
        throw XMPException("XMP SDK initialization error", "XMP error ID: " + QString::number(e.GetID()) + " XMP error message: " + e.GetErrMsg());
    }
}

void terminateXMP()
{
    // Terminate the XMP SDK.

    SXMPFiles::Terminate();
    SXMPMeta::Terminate();
}

/********************************************************************/
/*                                                                  */
/*  Qt database methods                                             */
/*                                                                  */
/********************************************************************/

// For someone coming from the ODBC/JDBC world, Qt's database
// functionality is confusing. The way objects are used is
// unexpected, and critical functionality, such as opening and closing
// connections, is often hidden behind the scenes, such as in
// factory functions or destructors.
//
// The main database classes are QSqlDatabase and QSqlQuery.
//
// QSqlDatabase plays a dual role. First, it represents a named
// connection to a database. Second, it maintains a global,
// static list (hash table) of named connections.
//
// To get a connection (QSqlDatabase object) and add that connection
// to the list of connections, call the QSqlDatabase::addDatabase()
// factory method. The arguments specify the name of the driver to
// use and the name of the connection. (If no connection name is
// given, an undocumented (but publicly accessible) default name is used.)
//
// Next, set the connection information (host, user name, password,
// database name) on the connection. This is so it will be stored in
// the list and can be reused when reconnecting to the database.
//
// You now have two choices. You can either open the connection and
// pass the connection object to methods that will use it, or allow the
// connection object to go out of scope and get a new copy later with
// the QSqlDatabase::database() method. The latter method accepts a
// connection name (or uses the default name) and returns the
// connection from the internal list.
//
// It is important to understand the internals of QSqlDatabase. QSqlDatabase
// maintains a reference count of connection objects it has returned.
// This is incremented when a new object is returned and decremented
// when the destructor is called on that object. The destructor closes
// the connection when the count reaches 0.
//
// By default, QSqlDatabase::database() opens closed connections. Thus,
// if you continually retrieve QSqlDatabase objects via QSqlDatabase::database()
// and destroy them by allowing them to go out of scope -- which is what
// the design encourages you to do -- you will continually connect to /
// disconnect from the database, which is expensive. The only way to
// avoid this is to have a QSqlDatabase object that remains in scope,
// such as in a global or a local variable in the main() function.
//
// It is also important to understand that QSqlDatabase does not perform
// connection pooling, which would keep a pool of open connections and
// hand them out as needed. It simply has a list of named connections that
// can be used one at a time. Any connection pooling is done at the
// driver level.
//
// For more information, see:
//
//    https://stackoverflow.com/questions/7669987/what-is-the-correct-way-of-qsqldatabase-qsqlquery
//    http://www.informit.com/articles/article.aspx?p=1405550
//
// QSqlQuery is confusing mostly with respect to its construction. In
// ODBC and JDBC, the hierarchy is clear -- you get a connection and
// use it to allocate a statement (query). With QSqlQuery, you pass a
// connection (QSqlDatabase object) to the QSqlQuery constructor,
// somewhat inverting the hierarchy:
//
//    // Allocate a query on the my_conn connection.
//
//    QSqlQuery q(QSqlDatabase::database("my_conn"));
//
// What is more confusing is that, if you are using the default
// connection, you don't even need to specify the connection object:
//
//    // Allocate a query on the default connection.
//
//    QSqlQuery q;
//
// It is not entirely clear to me how this works. QSqlQuery does
// not have a zero-argument constructor, but does have a constructor
// with two default arguments:
//
//    QSqlQuery(const QString &query = QString(), QSqlDatabase db = QSqlDatabase())
//
// If you don't pass any arguments, you get an empty string and an
// invalid connection. Somehow, this translates into using the
// default connection.
//
// Note also that there is no need to close the query. Although there
// is a finish() method to release resources, the documentation says
// it doesn't need to be called. Instead, the destructor seems to
// handle this.
//
// Finally, the 32-bit Windows Postgres driver (qsqlpsql.dll) requires
// the following DLLs:
//
//    libeay32.dll
//    libpq.dll
//    ssleay32.dll
//
// Note that you can use any combination of 32- and 64-bit driver and
// Postgres executable. For example, you can use a 32-bit driver with
// either a 32-bit or 64-bit Postgres executable. The only requirement
// is that all of the driver DLLs must be the same (all 32-bit or all
// 64-bit). Since the Inspector is 32-bit, we use a 32-bit driver.

int openDatabase(QSqlError & e)
{
    // Get the connection information from the config.db database.

    QString userName, password, databaseName, qtDriverName, hostName;
    getConnectionInfo(userName, password, databaseName, qtDriverName, hostName);

    // Add a connection to QSqlDatabase's internal list of database
    // connections, get a connection object representing this connection,
    // and set the connection information.

    QSqlDatabase db = QSqlDatabase::addDatabase(qtDriverName, dlnDBName);
    db.setHostName(hostName);
    db.setUserName(userName);
    db.setPassword(password);
    db.setDatabaseName(databaseName);

    // Open the database to open the connection. Note that the connection
    // should close when the db object goes out of scope at the end of
    // this method. For reasons not clear to me, the connection stays
    // open. See:
    //
    //    https://stackoverflow.com/questions/48479130/connection-still-open-after-qsqldatabase-goes-out-of-scope

    if (db.open())
    {
        // The database exists and is running. Check that the tables
        // used by the Inspector have been installed.

        QSqlQuery q(db);
        QString query = "SELECT * FROM inspection";
        if (q.exec(query))
        {
            return 0;  // Database running and Inspector tables installed.
        }
        else
        {
            return 2;  // Database running and Inspector tables not installed.
        }
    }
    else
    {
        e = db.lastError();
        return 1;  // Database not running.
    }
}

void getConnectionInfo(QString & userName, QString & password, QString & databaseName, QString & qtDriverName, QString & hostName)
{
    // The database connection information is stored in a SQLite database
    // named config.db, which is shipped with the DLN:Inspector. You can
    // browse and modify this file with a SQLite browser. For example, an
    // online browser is available at:
    //
    //    https://sqliteonline.com/

    // Check that the config.db file exists. If it does not exist,
    // we want to throw an exception. (The SQLite driver creates
    // it if it does not exist.)

    if (!QFile::exists("config.db"))
    {
        throw DBException("config.db does not exist", "The config.db file does not exist.");
    }

    // The following code is inside a tighter scope so that the QSqlDatabase
    // and QSqlQuery objects will be deleted when leaving the scope. This is
    // necessary to close them and prevent "resource leaks". For details, see:
    //
    //    http://doc.qt.io/qt-5/qsqldatabase.html#removeDatabase

    {
        // Add a connection to the SQLite database and open that connection.

        QSqlDatabase db = DBUtils::openDatabase("QSQLITE", "config", "config.db");

        // Get the data from the options table.

        QSqlQuery q(db);
        DBUtils::execQuery(q, "SELECT user, pwd, name, qtdrivername, host FROM cptdb", "config.db");

        // Get the first row of data. There should be exactly one row.

        if (!q.next())
        {
            throw DBException("Error retrieving row from config.db", q.lastError());
        }

        // Get the database connection values.

        userName = q.value(0).toString();
        password = q.value(1).toString();
        databaseName = q.value(2).toString();
        qtDriverName = q.value(3).toString();
        hostName = q.value(4).toString();

        // Close the database.

        db.close();
    }

    // Remove the database.

    QSqlDatabase::removeDatabase("config");
}

bool continueWithoutDatabase(Options & options, const QSqlError & e)
{
    // Display the error and ask if the user wants to continue
    // without the DLN database.

    QString msg("<p>An error occurred opening the DLN database:</p><p></p>");
    msg.append("<pre>   Database error: ").append(e.databaseText()).append("</pre>");
    msg.append("<pre>   Driver error: ").append(e.driverText()).append("</pre>");
    msg.append("<pre>   Error code: ").append(e.nativeErrorCode()).append("</pre>");
    msg.append("<p></p><p>Do you want to continue without the DLN?</p>");

    QMessageBox::StandardButton answer = QMessageBox::critical(nullptr, "Error opening DLN database", msg, QMessageBox::Yes | QMessageBox::No, QMessageBox::No);

    // If the user does not want to continue without the DLN, return false.

    if (answer == QMessageBox::No) return false;

    // If the user wants to continue, check if they ever want to
    // use the DLN database. (This case applies to users who have
    // not installed the DLNCC tool.)

    turnOffDatabase(options);

    // Return true, meaning continue without the database.

    return true;
}

bool continueWithoutDatabase(Options & options)
{
    // Tell the user to install the tables used by the Inspector and
    // ask if they want to continue without the database.

    QString msg = "<p>The DLN database is installed and running but has not "
                  "been updated for use with the DLN:Inspector. Please see "
                  "the documentation for how to update the database.</p>"
                  "<p></p><p>Do you want to continue now without the DLN?</p>";

    QMessageBox::StandardButton answer = QMessageBox::critical(nullptr, "Database not updated", msg, QMessageBox::Yes | QMessageBox::No, QMessageBox::No);

    // If the user does not want to continue without the DLN, return false.

    if (answer == QMessageBox::No) return false;

    // If the user wants to continue, check if they ever want to
    // use the DLN database. (This case applies to users who have
    // not updated the DLN database.)

    turnOffDatabase(options);

    // Return true, meaning continue without the database.

    return true;
}

void turnOffDatabase(Options & options)
{
    // If the user does not want to use the DLN database, update
    // the options so we do not attempt to open the database in
    // the future.

    QMessageBox::StandardButton answer = QMessageBox::question(nullptr, "Use DLN in the future?", "<p>Do you want to use the DLN database in the future?</p><p>(You can change your answer later with the Options dialog.)</p>", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);

    if (answer == QMessageBox::No)
    {
        options.useDatabase = false;
        OptionsFactory optionsFactory(globalInspectorType);
        optionsFactory.toOptionsDB(options);
    }
}

void closeDatabase()
{
    // Close the database connection and remove it from
    // QSqlDatabase's internal list of connections.

    QSqlDatabase::database(dlnDBName).close();
    QSqlDatabase::removeDatabase(dlnDBName);
}
