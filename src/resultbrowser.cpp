/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2017                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

// Local headers

#include "iglobals.h"
#include "resultbrowser.h"
#include "resultsdialog.h"

#include "scope_guard.hpp"

// Qt headers

#include <QGuiApplication>
#include <QDateTime>
#include <QHBoxLayout>
#include <QLabel>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>

#include <QDebug>

//#include <algorithm>
//#include <vector>

/*
 * The following is inspired from
 * https://www.walletfox.com/course/qtcheckablelist.php
 *
 */
class CheckableStringListModel : public QStringListModel
{
public:
    CheckableStringListModel(QObject* parent = nullptr):
        QStringListModel(parent) {}

    CheckableStringListModel(const QStringList & strings, QObject* parent = nullptr):
        QStringListModel(strings, parent) {}

    Qt::ItemFlags flags (const QModelIndex& index) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role) override;
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

    void setCheckStates(Qt::CheckState checkState);
    QModelIndexList checkedIndexes() const;
    int countChecked() const;
    int count() const { return this->userData.count(); }

private:
    // The following stores the check states:
    QHash<QPersistentModelIndex, Qt::CheckState> checkedItems;
    // QStringListModel ignores the Qt::UserRole, so we add support here:
    QHash<QPersistentModelIndex, QVariant> userData;
};

Qt::ItemFlags CheckableStringListModel::flags (const QModelIndex & index) const
{
    Qt::ItemFlags defaultFlags = QStringListModel::flags(index);
    if (index.isValid()){
        return defaultFlags | Qt::ItemIsUserCheckable;
    }
    return defaultFlags;
}

bool CheckableStringListModel::setData(const QModelIndex &index,
                                       const QVariant &value,
                                       int role)
{

    if(!index.isValid())
        return false;

    Qt::CheckState checkState = role == Qt::CheckStateRole && value == Qt::Checked ? Qt::Checked : Qt::Unchecked;
    checkedItems.insert(index, checkState);

    if (role == Qt::UserRole) {
        userData.insert(index, value);
        return true;
    }

    if (role == Qt::CheckStateRole) {
        // We check if after checking or unchecking a non-first row
        // we should change the state of the first row. The first row
        // contains the "select all" toggle.
        if (index.row() != 0) {
            int cnt = this->countChecked();
            Qt::CheckState checkState = Qt::PartiallyChecked;
            if (cnt == 0)
                checkState = Qt::Unchecked;
            else if (cnt == this->rowCount() - 1)
                checkState = Qt::Checked;
            checkedItems.insert(index.siblingAtRow(0), checkState);
        }
        emit dataChanged(index, index);
        return true;
    }

    return QStringListModel::setData(index, value, role);

}


QVariant CheckableStringListModel::data(const QModelIndex &index,
                                        int role) const {

    if (!index.isValid())
        return QVariant();

    if (role == Qt::CheckStateRole)
        return checkedItems.value(index);

    if (role == Qt::UserRole)
        return userData.value(index);

    if (role == Qt::FontRole) {
       QFont defFont = QGuiApplication::font();
       if (index.row() == 0)
           defFont.setItalic(true);
       else if (checkedItems.value(index, Qt::Unchecked) == Qt::Checked)
           defFont.setStrikeOut(true);
       return defFont;
    }

    return QStringListModel::data(index, role);
/*
    else if(role == Qt::BackgroundColorRole)
        return checkedItems.contains(index) ?
                    QColor("#ffffb2") : QColor("#ffffff"); */

}


bool CheckableStringListModel::removeRows(int row, int count, const QModelIndex & parent)
{
    // Use a QMutableHashIterator since we need to modify the
    // iterated checkedItems hash:
    QMutableHashIterator<QPersistentModelIndex, Qt::CheckState> i(checkedItems);
    while (i.hasNext()) {
        i.next();
        int currentRow = i.key().row();
        if (currentRow >= row && currentRow < row + count) {
            checkedItems.remove(i.key());
            userData.remove(i.key());
        }
    }
    return QStringListModel::removeRows(row, count, parent);
}


void CheckableStringListModel::setCheckStates(Qt::CheckState checkState)
{
    QHash<QPersistentModelIndex, Qt::CheckState>::iterator i;
    for (i = checkedItems.begin(); i != checkedItems.end(); ++i) {
        checkedItems.insert(i.key(), checkState);
    }
}

/*
 * Returns a list with the ModelIndex-es that have been "checked"
 * i.e. their status is Qt::Checked
 */
QModelIndexList CheckableStringListModel::checkedIndexes() const
{
    QModelIndexList list;
    QHash<QPersistentModelIndex, Qt::CheckState>::const_iterator i;
    for (i = checkedItems.cbegin(); i != checkedItems.cend(); ++i) {
        if (i.value() == Qt::Checked && i.key().row() > 0)
            list.append(i.key());
    }
    return list;
}

int CheckableStringListModel::countChecked() const
{
    int count = 0;
    QHash<QPersistentModelIndex, Qt::CheckState>::const_iterator i;
    for (i = checkedItems.constBegin(); i != checkedItems.constEnd(); ++i) {
        if (i.key().row() == 0)
            continue; // Ignore the first row that contains the toggle
        count += i.value() == Qt::Checked ? 1 : 0;
    }
    return count;
}
/********************************************************************/
/*                                                                  */
/*  ResultBrowser class                                             */
/*                                                                  */
/********************************************************************/

/********************************************************************/
/*                                                                  */
/*  Public methods                                                  */
/*                                                                  */
/********************************************************************/

/********************************************************************/
/*                                                                  */
/*  Constructor and destructor                                      */
/*                                                                  */
/********************************************************************/

ResultBrowser::ResultBrowser(InspectorType::Enum type):
    inspectorType(type)
{
    setModal(true);
    setWindowTitle("View results in DLN");
    initGUI();
    setConnections();
}

ResultBrowser::~ResultBrowser()
{
    // Delete the model used by the view class. Because multiple view classes
    // can share the same model, view classes do not delete model objects.
    // Instead, the programmer must delete model objects explicitly.

    delete inspectionsModel;
    inspectionsModel = nullptr;
}

/********************************************************************/
/*                                                                  */
/*  Slots                                                           */
/*                                                                  */
/********************************************************************/

void ResultBrowser::projectChanged(int projectIndex)
{
    updateComboBoxes(projectIndex, projectComboBox);
}

void ResultBrowser::bundleChanged(int)
{
    updateInspectionsList();
}

void ResultBrowser::viewInspections()
{
    QModelIndexList indices = inspectionsListView->selectionModel()->selectedIndexes();
    if (indices.empty()) {
        QMessageBox::warning(this, "No date/times selected", "Select an inspection date/time to see its results.");
        return;
    }
    QModelIndex selectedIndex = indices.first();
    QString mySrls = inspectionsModel->data(selectedIndex, Qt::UserRole).toString();

    qDebug() << "mysrl=" << mySrls;


    QStringList dateTimes = getSelectedData(inspectionsListView, inspectionsModel);
    if (dateTimes.size() == 0)
    {
        QMessageBox::warning(this, "No date/times selected", "Select an inspection date/time to see its results.");
        return;
    }

    // Get the srl for the image set.

    // QString dateTime = dateTimes[0];
    // int srl = inspectionsHash.value(dateTime);

    // Get the paths and build the project path.

    // Build the base path for the image set. For the RTI
    // Inspector, use the project path if it is not empty.
    // (RTI projects do not use the group path.) For the PG
    // Inspector, use the project and group paths. (If we are
    // using the database, these must not be empty.)

    QString projectPath = getPath(projectComboBox);
    QString bundlePath = getPath(bundleComboBox);

    QString basePath;
    if (!projectPath.isEmpty())
    {
        if (this->inspectorType == InspectorType::RTI)
        {
            basePath = projectPath.append('/');
        }
        else if (!bundlePath.isEmpty())
        {
            basePath = projectPath.append('/').append(bundlePath).append('/');
        }
    }

    // Allocate a ResultsDialog and set the project directory.

    ResultsDialog resultsDialog;
    resultsDialog.setProject(basePath);

    // Retrieve the inspection results for the image set.

    QString query = "SELECT c.cptset, i.inspection_datetime, tg.testgroup_name, "
                           "ir.result_type, ir.msg_type, ir.title, ir.description, "
                           "p.property_name, fp1.filepath, fp2.filepath, ir.value1, "
                           "ir.value2, c.srl, p.severity "
                    "FROM cptset c, inspection i, inspresult ir "
                    "LEFT OUTER JOIN testgroup tg ON ir.testgroup_srl = tg.srl "
                    "LEFT OUTER JOIN property p ON ir.property_srl = p.srl "
                    "LEFT OUTER JOIN filepath fp1 ON ir.filepath1_srl = fp1.srl "
                    "LEFT OUTER JOIN filepath fp2 ON ir.filepath2_srl = fp2.srl "
                    "WHERE i.srl IN (" + mySrls + ") AND "
                           "i.cptset_srl = c.srl AND "
                           "ir.inspection_srl = i.srl "
                    "ORDER BY i.srl ASC";
    QSqlDatabase db = QSqlDatabase::database(dlnDBName);
    QSqlQuery q(db);
    execQuery(q, query);

    // Loop through the query results and build IResults.

    QDateTime inspDateTime;
    while (q.next())
    {
        QString imageSetName = q.value(0).toString();
        inspDateTime = q.value(1).toDateTime();

        // Build the IResult.

        IResult result;
        result.imageSetName = imageSetName;
        result.imageSetNum = q.value(12).toInt();

        bool ok {false};
        result.severity = q.value(13).toInt(&ok);
        if (!ok) result.severity = PropInfo::SEVERITY_WARNING;

        result.testGroup = ITestGroup::fromString(q.value(2).toString());
        result.resultType = static_cast<IResultType::Enum>(q.value(3).toInt());
        result.msgType = static_cast<IMessageType::Enum>(q.value(4).toInt());
        result.title = q.value(5).toString();
        result.description = getNullableValue(q, 6);
        result.propQName = getNullableValue(q, 7);
        result.fileInfo1 = QFileInfo(getNullableValue(q, 8));
        result.fileInfo2 = QFileInfo(getNullableValue(q, 9));
        result.value1 = getNullableValue(q, 10);
        result.value2 = getNullableValue(q, 11);

        // Add it to the results.

        resultsDialog.addResult(result);
    }

    // Show the results.

    resultsDialog.setInspectionTime(inspDateTime);
    resultsDialog.showDialog();
}


void ResultBrowser::deleteCheckedInspections()
{
    QModelIndexList indices = inspectionsModel->checkedIndexes();
    if (indices.empty()) {
        QMessageBox::warning(this, QCoreApplication::applicationName(), "Check one or more inspection date/times to delete.");
        return;
    }

    int count = indices.count();
    QMessageBox::StandardButton answer =
            QMessageBox::question(this, QCoreApplication::applicationName(),
                                  QString("Are you sure you want to delete %1 inspection%2?")
                                  .arg(count)
                                  .arg(count<2 ? "" : "s"));
    if (answer != QMessageBox::StandardButton::Yes)
        return;

    QSqlDatabase db = QSqlDatabase::database(dlnDBName);
    startTransaction(db);

    // Rollback the transaction automatically (through "RAAI")
    // when a exception is thrown:
    auto guard = sg::make_scope_guard([&db]{ db.rollback(); });

    QSqlQuery q(db);

    QStringList inspsToBeDropped;
    for (const QModelIndex& c: qAsConst(indices)) {
        const QStringList inspection_srls = inspectionsModel->data(c, Qt::UserRole).toString().split(",");
        for(const QString& srlStr: inspection_srls ) {
            inspsToBeDropped.push_back(srlStr);
        }
    }
    if (inspsToBeDropped.isEmpty())
        return;

    QString inSrls = "(" + inspsToBeDropped.join(",") + ")";

    execQuery(q, "DELETE FROM inspection_vs_testgroup WHERE inspection_srl IN " + inSrls);
    execQuery(q, "DELETE FROM inspection_vs_filelist WHERE inspection_srl IN " + inSrls);

    // Delete the associated documents returning their ids:
    execQuery(q, "DELETE FROM cptset_vs_documt AS d USING inspection AS i "
                "WHERE d.cptset_srl=i.cptset_srl AND d.documt_srl=i.documt_srl AND i.srl IN " + inSrls +
                " RETURNING d.documt_srl");
    QStringList docsToBeDropped;
    while (q.next()) {
        docsToBeDropped.append(q.value(0).toString());
    }

    execQuery(q, "DELETE FROM inspresult WHERE inspection_srl IN "+inSrls);
    execQuery(q, "DELETE FROM inspection WHERE srl IN " + inSrls);

    // Finally drop the associated documents:
    if (!docsToBeDropped.isEmpty()) {
        QString inDocs = "(" + docsToBeDropped.join(",") + ")";
        execQuery(q, "DELETE FROM cptsess_vs_documt WHERE documt_srl IN " + inDocs);
        execQuery(q, "DELETE FROM documt WHERE srl IN " + inDocs);
    }

    commitTransaction(db);
    guard.dismiss();

    updateInspectionsList();
}

/********************************************************************/
/*                                                                  */
/*  Private methods                                                 */
/*                                                                  */
/********************************************************************/

/********************************************************************/
/*                                                                  */
/*  Methods to initialize the GUI                                   */
/*                                                                  */
/********************************************************************/

void ResultBrowser::initGUI()
{
    // Get the layouts.

    QGridLayout * controlsLayout = initComboBoxLayout();
    QVBoxLayout * listLayout = initListLayout();
    QVBoxLayout * buttonsLayout = initButtonLayout();

    // Create the list label.

    QLabel * inspectionsLabel = new QLabel("Inspection date/times:");

    // Build the bottom layout.

    QHBoxLayout * bottomLayout = new QHBoxLayout();
    bottomLayout->addLayout(listLayout);
    bottomLayout->addLayout(buttonsLayout);

    // Build an overall layout.

    QVBoxLayout * layout = new QVBoxLayout();
    layout->addLayout(controlsLayout);
    layout->addWidget(inspectionsLabel);
    layout->addLayout(bottomLayout);

    // Set the layout.

    setLayout(layout);
}

QGridLayout * ResultBrowser::initComboBoxLayout()
{
    // Create the project, group, and image set combo boxes.

    QLabel * projectLabel = new QLabel("Project:");
    projectComboBox = new QComboBox();
    initComboBox(ITableType::Project, -1, projectComboBox);

    QLabel * bundleLabel = new QLabel("Bundle:");
    bundleComboBox = new QComboBox();
    bundleComboBox->setEnabled(false);

    // Lay out the combo boxes.

    QGridLayout * comboBoxLayout = new QGridLayout();
    comboBoxLayout->addWidget(projectLabel,     0, 0);
    comboBoxLayout->addWidget(projectComboBox,  0, 2);
    comboBoxLayout->addWidget(bundleLabel,       1, 0);
    comboBoxLayout->addWidget(bundleComboBox,    1, 2);
    comboBoxLayout->setColumnStretch(           2, 3);  // Stretch combo box
    comboBoxLayout->setColumnStretch(           3, 1);  // Add padding on right

    // Return the layout.

    return comboBoxLayout;
}

QVBoxLayout * ResultBrowser::initListLayout()
{
    // Allocate the list of inspections and lay it out.

    inspectionsModel = new CheckableStringListModel();
    inspectionsListView = new QListView();
    inspectionsListView->setModel(inspectionsModel);
    inspectionsListView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    inspectionsListView->setToolTip("Select an inspection date/time and click Results to see its results.");
    inspectionsListView->setSelectionMode(QAbstractItemView::SingleSelection);

    // Lay out the pieces.

    QVBoxLayout * layout = new QVBoxLayout();
    layout->addWidget(inspectionsListView);

    // Return the layout.

    return layout;
}

QVBoxLayout *ResultBrowser::initButtonLayout()
{
    // Create the buttons.

    resultsButton = new QPushButton("Results...");
    resultsButton->setEnabled(false);
    deleteCheckedButton = new QPushButton("Delete checked...");
    deleteCheckedButton->setEnabled(false);
    closeButton = new QPushButton("Close");

    // Lay it out.

    QVBoxLayout * layout = new QVBoxLayout();
    layout->addWidget(resultsButton);
    layout->addWidget(deleteCheckedButton);
    layout->addStretch();
    layout->addWidget(closeButton);

    // Return the layout.

    return layout;
}

void ResultBrowser::setConnections()
{
    // Populate the combo box and the list.

    connect(projectComboBox, SIGNAL(activated(int)), this, SLOT(projectChanged(int)));

    connect(bundleComboBox, SIGNAL(activated(int)), this, SLOT(bundleChanged(int)));

    connect(inspectionsListView, &QListView::clicked,
            resultsButton,
            [this](const QModelIndex& idx){
                if (!idx.isValid())
                    return;
                QModelIndexList selectedIndices = this->inspectionsListView->selectionModel()->selectedRows();
                foreach(const QModelIndex &index, selectedIndices) {
                    if (index.row() != 0) {
                        this->resultsButton->setEnabled(true);
                        return;
                    }
                }
                this->resultsButton->setEnabled(false);
            });

    // We connect any change in the 'check state' with a lambda function
    // that checks whether the first (toggle) checkbox was altered.
    // We are using inspectionsModel (3rd arg) as a guard/context for this connection
    // so that the connection is automatically broken when the context is destroyed
    // and memory is reclaimed
    connect(inspectionsModel, &CheckableStringListModel::dataChanged,
            this->inspectionsModel, // inspectionsModel is the context object
            [this](const QModelIndex& index){
                QItemSelectionModel* m = this->inspectionsListView->selectionModel();
                if (index.row() == 0) {
                    QVariant var = this->inspectionsModel->data(index, Qt::CheckStateRole);
                    Qt::CheckState checkState = var == Qt::Checked ? Qt::Checked : Qt::Unchecked;
                    this->inspectionsModel->setCheckStates(checkState);
                    m->clearSelection();
                }
                this->deleteCheckedButtonEnabled(this->inspectionsModel->countChecked());
            });

    // View the results.

    connect(resultsButton, SIGNAL(clicked()), this, SLOT(viewInspections()));

    // Delete checked inspections
    connect(deleteCheckedButton, SIGNAL(clicked()), this, SLOT(deleteCheckedInspections()));

    // Close the dialog.

    connect(closeButton, SIGNAL(clicked()), this, SLOT(close()));
}

/********************************************************************/
/*                                                                  */
/*  Methods to support GUI slots                                    */
/*                                                                  */
/********************************************************************/

void ResultBrowser::initComboBox(ITableType::Enum tableType, int parentSrl, QComboBox * comboBox)
{
    // Clear the combo box and add a blank item.

    comboBox->clear();
    comboBox->addItem("Choose..", QVariant("-1;"));

    // Build the query to populate the combo box. Note that
    // we allow folders to be empty.

    QString query, bundleType;
    switch (tableType)
    {
        case ITableType::Project :
            query = "SELECT srl, cptproj, cptproj_img_foldr FROM cptproj "
                    "WHERE cptproj <> '' AND deleted IS DISTINCT FROM TRUE ORDER BY LOWER(cptproj)";
            break;

        case ITableType::Bundle :
            bundleType = InspectorType::toDBString(this->inspectorType);
            query = "SELECT s.srl, s.cptsess, s.cptsess_img_foldr FROM cptsess s "
                    "INNER JOIN cptsess_type t ON (s.cptsess_type_srl = t.srl) "
                    "WHERE s.cptsess <> '' AND s.deleted IS DISTINCT FROM TRUE AND t.deleted IS DISTINCT FROM TRUE"
                    " AND s.cptproj_srl = " + QString::number(parentSrl) +
                    " AND t.cptsess_type = '" + bundleType +
                    "' ORDER BY LOWER(s.cptsess)";
            break;

        case ITableType::ImageSet :
            query = "SELECT srl, cptset, cptset_img_foldr "
                    "FROM cptset "
                    "WHERE cptset <> '' AND deleted IS DISTINCT FROM TRUE"
                    " AND cptsess_srl = " + QString::number(parentSrl) +
                    " ORDER BY LOWER(cptset)";
            break;
    }

    // Open the database and execute the query, then add
    // items/data to the combo box.

    QSqlQuery q(QSqlDatabase::database(dlnDBName));
    execQuery(q, query);
    while (q.next())
    {
        // Retrieve the data and build the user data. We need to
        // store both the serial number and path, but QVariant
        // only accepts a single user data value. Although it is
        // possible to store custom objects, such as a structure
        // containing both values, it's easier to build a string
        // with both pieces of data.

        int srl = q.value(0).toInt();
        QString item = q.value(1).toString();
        QString dirPath = q.value(2).toString();
        QString userData;
        userData.setNum(srl).append(";").append(dirPath);

        // Add the item to the combo box.

        comboBox->addItem(item, QVariant(userData));
    }

    comboBox->setCurrentIndex(0);
}

void ResultBrowser::updateComboBoxes(int index, QComboBox * comboBox)
{

    resultsButton->setEnabled(false);
    deleteCheckedButton->setEnabled(false);

    // Clear the list model.

    clearModel(inspectionsModel);

    // If the bundle combo box changed there's nothing else to do:
    if (comboBox == bundleComboBox) return;

    // So now we are dealing with the projectComboBox:

    // Clear the lower-level combo box(es) and disable them.

    bundleComboBox->setEnabled(false);
    bundleComboBox->clear();

    // If the index is 0, there is nothing to get, so just return.

    if (index == 0) return;

    // Get the serial number from the combo box that changed.

    int parentSrl = getSerial(comboBox);

    // Set the child combo box and table type.

    QComboBox * childComboBox = bundleComboBox;
    ITableType::Enum childTableType = ITableType::Bundle;

    // Update and enable the child combo box.

    initComboBox(childTableType, parentSrl, childComboBox);
    childComboBox->setEnabled(true);
}

void ResultBrowser::updateInspectionsList()
{
    // Clear the current model.

    clearModel(inspectionsModel);
    resultsButton->setEnabled(false);
    deleteCheckedButton->setEnabled(false);

    // Get the image set serial number.

    int cptsessSrl = getSerial(bundleComboBox);
    if (cptsessSrl < 0) {
        // The first "Choose.." option has -1 as serial, so
        // no image set is selected, just return:
        return;
    }

    // Execute the query to get the inspections

    QString query = "SELECT cptsess_srl, inspection_datetime,  string_agg(srl::text, ',') srls "
                    "FROM inspection_2xml "
                    "WHERE cptsess_srl = " + QString::number(cptsessSrl) + " "
                        "AND inspection_datetime IS NOT NULL "
                    "GROUP BY cptsess_srl, inspection_datetime "
                    "ORDER BY inspection_datetime ASC";
    QSqlQuery q(QSqlDatabase::database(dlnDBName));
    execQuery(q, query);

    // Read the results and add a row for each inspection.
    // We retreive the inspections in ASCending order based on
    // inspection datetime, but we always insert them in the
    // first row of the inspectionModel, so the most recent
    // inspection will be in the first position (row).

    while (q.next())
    {
        int srl = q.value(0).toInt();
        QDateTime dt = q.value(1).toDateTime();
        QString dtStr = dt.toString(Qt::TextDate);
        QString srls = q.value(2).toString(); // The inspection srls joined with ','
        QPersistentModelIndex idx = addRow(inspectionsModel, 0, dtStr);
        inspectionsHash.insert(dtStr, srl);
        inspectionsModel->setData(idx, srls, Qt::UserRole);
    }

    // If no inspections were found, display a warning.

    if (inspectionsModel->count() == 0)
    {
        QMessageBox::warning(this, "No inspections found", "No inspections found");
    }
    else {
        // Add a selector toggle as first row:
        addRow(inspectionsModel, 0, " -- Select all for removal --");
    }
}

void ResultBrowser::deleteCheckedButtonEnabled(int count)
{
    this->deleteCheckedButton->setEnabled(count > 0);
    this->deleteCheckedButton->setText(count > 0 ? QString("Delete %1 checked..").arg(count) : "Delete checked..");
}

/********************************************************************/
/*                                                                  */
/*  Methods to support GUI slots                                    */
/*                                                                  */
/********************************************************************/

int ResultBrowser::getSerial(const QComboBox * comboBox)
{
    QString userData = comboBox->itemData(comboBox->currentIndex()).toString();
    return userData.leftRef(userData.indexOf(';')).toInt();
}

QString ResultBrowser::getPath(const QComboBox * comboBox)
{
    QString userData = comboBox->itemData(comboBox->currentIndex()).toString();
    return userData.mid(userData.indexOf(';') + 1);
}

QString ResultBrowser::getNullableValue(QSqlQuery & q, int index)
{
    // It's not clear what is returned for a NULL value. The documentation
    // doesn't say and I'm too lazy to figure it out...

    QString value = q.value(index).toString();
    return q.isNull(index) ? QString() : value;
}


/********************************************************************/
/*                                                                  */
/*  Methods and functions for updating the combo boxes when         */
/*  Inspector is used with DLN                                      */
/*                                                                  */
/********************************************************************/

namespace {

// Selects and makes 'current' the item in the given combo box
// that contains the given serial:
int selectComboBoxItem(QComboBox* comboBox, int srl) {
    for (int i=0; i<comboBox->count(); ++i) {
        QString userData = comboBox->itemData(i).toString();
        if (srl == userData.leftRef(userData.indexOf(';')).toInt()) {
            comboBox->setCurrentIndex(i);
            return i;
        }
    }
    return -1;
}
}

void ResultBrowser::selectBundle(int project_srl, int bundle_srl)
{
    int project_index = ::selectComboBoxItem(this->projectComboBox, project_srl);
    updateComboBoxes(project_index, projectComboBox);
    int group_index = ::selectComboBoxItem(this->bundleComboBox, bundle_srl);
    updateComboBoxes(group_index, bundleComboBox);
    updateInspectionsList();
}
