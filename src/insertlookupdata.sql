-- *****************************************
--
-- POPULATE LOOKUP TABLES
--
-- *****************************************

-- *****************************************
-- Table: compare_type
-- *****************************************

INSERT INTO compare_type
(compare_type, description, deleted)
VALUES (0, 'None', 'f');

INSERT INTO compare_type
(compare_type, description, deleted)
VALUES (1, 'Restrictions', 'f');

INSERT INTO compare_type
(compare_type, description, deleted)
VALUES (2, 'Within file type', 'f');

INSERT INTO compare_type
(compare_type, description, deleted)
VALUES (4, 'Across file types', 'f');

INSERT INTO compare_type
(compare_type, description, deleted)
VALUES (255, 'Unknown', 'f');

-- *****************************************
-- Table: file_type
-- *****************************************

INSERT INTO file_type
(file_type, type_name, deleted)
VALUES (0, 'JPEG', 'f');

INSERT INTO file_type
(file_type, type_name, deleted)
VALUES (1, 'TIFF', 'f');

INSERT INTO file_type
(file_type, type_name, deleted)
VALUES (2, 'PNG', 'f');

INSERT INTO file_type
(file_type, type_name, deleted)
VALUES (3, 'DNG', 'f');

INSERT INTO file_type
(file_type, type_name, deleted)
VALUES (4, 'RDF', 'f');

INSERT INTO file_type
(file_type, type_name, deleted)
VALUES (255, 'UNKNOWN', 'f');

-- *****************************************
-- Table: filelist_type
-- *****************************************

INSERT INTO filelist_type
(filelist_type, type_name, deleted)
VALUES ('a', 'Archival images', 'f');

INSERT INTO filelist_type
(filelist_type, type_name, deleted)
VALUES ('p', 'Processed images', 'f');

-- *****************************************
-- Table: msg_type
-- *****************************************

INSERT INTO msg_type
(msg_type, description, deleted)
VALUES (0, 'Two values', 'f');

INSERT INTO msg_type
(msg_type, description, deleted)
VALUES (1, 'Restriction', 'f');

INSERT INTO msg_type
(msg_type, description, deleted)
VALUES (2, 'Invalid value', 'f');

INSERT INTO msg_type
(msg_type, description, deleted)
VALUES (3, 'Text', 'f');

INSERT INTO msg_type
(msg_type, description, deleted)
VALUES (255, 'Unknown', 'f');

-- *****************************************
-- Table: result_type
-- *****************************************

INSERT INTO result_type
(result_type, description, deleted)
VALUES (0, 'Warning', 'f');

INSERT INTO result_type
(result_type, description, deleted)
VALUES (1, 'File error', 'f');

INSERT INTO result_type
(result_type, description, deleted)
VALUES (2, 'Capture error', 'f');

INSERT INTO result_type
(result_type, description, deleted)
VALUES (3, 'Transformation error', 'f');

INSERT INTO result_type
(result_type, description, deleted)
VALUES (4, 'OK', 'f');

INSERT INTO result_type
(result_type, description, deleted)
VALUES (5, 'Fatal error', 'f');

INSERT INTO result_type
(result_type, description, deleted)
VALUES (255, 'Unknown', 'f');