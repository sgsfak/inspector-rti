/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

// Local headers

#include "iexception.h"
#include "iglobals.h"
#include "results.h"

// Qt headers

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QUuid>
#include <QXmlStreamReader>

#include <QDebug>

// C++ headers

#include <algorithm>

/********************************************************************/
/*                                                                  */
/*  IResultDiff class                                               */
/*                                                                  */
/********************************************************************/

IResultDiff::IResultDiff()
{

}

IResultDiff::~IResultDiff()
{

}

void IResultDiff::initialize(const IResult & result)
{
    savedResult = result;
}

int IResultDiff::getBreakField(const IResult & result)
{
    // Results are assumed to be sorted in ascending order
    // by the calling method. Because of this, each result
    // must be "greater than or equal to" the previous result
    // and we only need to check for inequality, which is
    // presumably faster than checking for greater-than.

    int breakField = -1;

    // Check for breaks. The order follows the sort order
    // in operator< for IResult. (See iresult.cpp.)

    // With IResult.propQName, note that we actually sort
    // on the local name, not the prefix plus local name.
    // Since we are testing for inequality, not less-than,
    // we can safely check the entire name.

    if (result.imageSetNum != savedResult.imageSetNum)
        breakField = 0;
    else if (result.testGroup != savedResult.testGroup)
        breakField = 1;
    else if (result.resultType != savedResult.resultType)
        breakField = 2;
    else if (result.propQName != savedResult.propQName)
        breakField = 3;
    else if (result.title != savedResult.title)
        breakField = 4;

    // If there was a break, set the saved result to the
    // current result.

    if (breakField != -1)
        savedResult = result;

    // Return the break field.

    return breakField;
}

/********************************************************************/
/*                                                                  */
/*  Results class                                                   */
/*                                                                  */
/********************************************************************/

/********************************************************************/
/*                                                                  */
/*  Public methods                                                  */
/*                                                                  */
/********************************************************************/

/********************************************************************/
/*                                                                  */
/*  Constructor and destructor                                      */
/*                                                                  */
/********************************************************************/

Results::Results() :
    sorted(false),
    totalImageCount(0)
{

}

Results::~Results()
{

}

/********************************************************************/
/*                                                                  */
/*  Getters and setters                                             */
/*                                                                  */
/********************************************************************/

void Results::setInspectionTime(const QDateTime & inspectionTime)
{
    this->inspectionTime = inspectionTime;
}

QDateTime Results::getInspectionTime()
{
    return inspectionTime;
}

void Results::setProject(const QString & projectDirectory)
{
    this->projectDirectory = projectDirectory;
}

QString Results::getProject()
{
    return projectDirectory;
}

void Results::increaseImageCount(const QString& imageSetName, int increment)
{
    auto it = this->imageSetCount.find(imageSetName);
    if (it != this->imageSetCount.end())
        *it += increment;
    else
        this->imageSetCount.insert(imageSetName, increment);
    this->totalImageCount += increment;
}

int Results::getTotalImageCount()
{
    return this->totalImageCount;
}

/********************************************************************/
/*                                                                  */
/*  Manage results                                                  */
/*                                                                  */
/********************************************************************/

void Results::clear()
{
    results.clear();
    totalImageCount = 0;
}

/*void Results::addResults(const QList<IResult> & results)
{
    // Add the results.

    for (int i = 0; i < results.size(); i++)
    {
        addResult(results[i]);
    }
}*/

void Results::addResult(const IResult &result)
{
    // Add an individual result. Set sorted to false
    // to indicate we can't skip the sort routine.

    results.append(result);
    sorted = false;
}

/********************************************************************/
/*                                                                  */
/*  Read and write results                                          */
/*                                                                  */
/********************************************************************/

void Results::readResults(const QString & filename)
{
    // Open the file for reading.

    QFile resultsFile(filename);
    if (!resultsFile.open(QIODevice::ReadOnly))
    {
        throw IOException("File open error", "Error opening file:\n\nFile: " + filename + "\n\nError: " + resultsFile.errorString());
    }

    // Read the results from the file using a stream.

    QTextStream stream(&resultsFile);
    readResults(stream, resultsFile.fileName());

    // Close the file.

    resultsFile.close();
}

void Results::readResults(QTextStream &stream, QString filename)
{
    // This method uses a stream because QTextStreams are the only
    // way to use read from stdin / write to stdout. (We don't currently
    // read from stdin but we do write to stdout.)

    // Clear the current results.

    clear();

    // Read the first line from the file and discard it. This line
    // contains headers used for spreadsheet programs.

    QString line = stream.readLine(0);

    // Read the results from the file.

    bool firstLine = true;
    while (!(line = stream.readLine(0)).isEmpty())
    {
        // Split the line on tab characters.

        QStringList fields = line.split("\t");
        if (fields.size() != 14)
        {
            throw IOException("Invalid line", "Invalid line found in the file: " + filename +
                                               "\n\nA valid line contains the following tab-separated fields:\n\n"
                                               "image-set\nimage-set-number\ntest-group\nresult-type\nproperty-qname\ntitle\ndescription-type\nfilepath1\nfilepath2\nvalue1\nvalue2\ndescription\n" + projectTermLC + "-directory\ninspection-time\n\n"
                                               "The invalid line is:\n\n" + line);
        }

        if (firstLine)
        {
            // Set the project directory and inspection time. These
            // are duplicated on every line, but we only need to
            // read them once.

            setProject(fields[12]);
            setInspectionTime(QDateTime::fromString(fields[13], "yyyy-MM-ddThh:mm"));
            firstLine = false;
        }

        // Create a new IResult. Note that the order in which results
        // are saved does not match the structure/constructor order.
        // This is because the results are saved in sort order so they
        // will be useful in a spreadsheet.

        IResult result(fields[0],                              // Image set name
                       fields[1].toInt(),                      // Image set number
                       ITestGroup::fromString(fields[2]),      // Test group
                       IResultType::fromString(fields[3]),     // Result type
                       IMessageType::fromString(fields[10]),   // Message type
                       fields[5],                              // Title
                       atSignToLineBreak(fields[11]),          // Description
                       fields[4],                              // Property path
                       fields[14].toInt(),                     // Severity
                       QFileInfo(fields[6]),                   // FileInfo1
                       QFileInfo(fields[7]),                   // FileInfo2
                       atSignToLineBreak(fields[8]),           // Value 1
                       atSignToLineBreak(fields[9]));          // Value 2

        // Add the IResult to the list.

        addResult(result);
    }

    // Check if there were any results.

    if (results.size() == 0)
    {
        throw IException("No results", "No results found in the file:\n\n" + filename);
    }
}

void Results::writeResults(const QString & filename)
{
    // Open the file for writing.

    QFile resultsFile(filename);
    if (!resultsFile.open(QIODevice::WriteOnly))
    {
        throw IOException("File open error", "Error opening file:\n\nFile: " + filename + "\n\nError: " + resultsFile.errorString());
    }

    // Write the results to the file using a stream.

    QTextStream stream(&resultsFile);
    writeResults(stream, this->results);

    // Close the file.

    resultsFile.close();
}

void Results::writeResults(QTextStream & stream, const QList<IResult>& results)
{
    // This method uses a stream because QTextStreams are the only
    // way to use read/write to stdin/stdout. (We don't currently
    // read from stdin but we do write to stdout.)

    // Sort the results.

    sort();

    // Write the result header.

    writeResultHeader(stream);

    // Write the results.

    for (int i = 0; i < results.size(); i++)
    {
        writeResult(stream, results[i]);
    }

    // Flush the stream.

    stream.flush();
}

void Results::writeHTMLFile(const QString & filename)
{
    // It's unlikely anyone will ever want to write HTML to
    // stdout, so it's safe to just use a filename here.

    // Sort the results.

    sort();

    // Open the file.

    QFile htmlFile(filename);
    if (!htmlFile.open(QIODevice::WriteOnly))
    {
        throw IOException("File open error", "Error opening file:\n\nFile: " + filename + "\n\nError: " + htmlFile.errorString());
    }

    QTextStream stream(&htmlFile);
    // Write the HTML report.

    try
    {
        writeHTML(stream, results);
    }
    catch (IOException io)
    {
        // If there is an exception, catch it, close the file,
        // and rethrow it.

        htmlFile.close();
        throw io;
    }

    // Close the file.

    htmlFile.close();
}


void Results::writeHTML(QTextStream & stream, const QList<IResult>& results)
{
    // Write the HTML report.
    writeHTMLHeadings(stream);
    if (results.size() == 0)
    {
        write(stream, "<p>No results</p>");
    }
    else
    {
        // Write the table of contents and the rows.

        writeHTMLTOC(stream, results);
        writeHTMLRows(stream, results);
    }

    // Finish the HTML wrapper.

    write(stream, "</body></html>");

}

/********************************************************************/
/*                                                                  */
/*  Update the DLN database                                         */
/*                                                                  */
/********************************************************************/

// The logical structure of the DLN database is as follows:
//
// Inspection (time, Inspector version)
//    Files inspected (archival, file type)
//       Path
//       ...
//    Files inspected (processed, file type)
//       Path
//       ...
//    Test group (File tests)
//       Property group (Files)
//          Property (name, restrictions, etc.)
//          ...
//       Results
//          Result (OK/Warning/etc., title, description, file1, file2, etc.)
//          ...
//    Test group (Capture tests)
//       ...
//    Test group (Transformation tests)
//       ...
//
// The physical structure of the database is as follows, with non-Inspector
// tables omitted for clarity. Arrows point from FK to PK.
//
//                            cptproj
//                               ^
//                               |
//                            cptsess
//                               ^
//                               |
//                            cptset
//                            ^    ^
//                            |    |____________cptset_vs_filelist
//                            |                                |
//                       inspections                           |
//                       ^    ^    ^                           |
//            ___________|    |    |__inspection_vs_filelist   |
//           |                |                 |              |
// inspection_vs_testgroup    |                 |              |
//           |                |                 |              |
//           v                |                 |              |
//       testgroup <-------   |                 |              |
//           ^             |  |                 v              |
//           |             |  |              filelist <--------
//     propertygroup       |  |                 ^
//           |             |  |                 |
//           v             |  |                 |
//       property <------ inspresult ------> filepath
//
// cptset_vs_filelist is used to save the files in an image set when
// the user adds or edits the files with SetManager.
//
// Not shown are the lookup tables:
//
// - compare_type (pointed to by property)
// - file_type (pointed to by filelist)
// - filelist_type (pointed to by filelist)
// - msg_type (pointed to by inspresult)
// - result_type (pointed to by inspresult)
//
// filelist/filepath are used to avoid creating multiple copies of
// the lists of files used by inspections. (Each inspection adds
// another row or two to inspection_vs_filelist.) filelist rows
// are deleted when the number of pointers to them in cptset_vs_filelist
// and inspection_vs_filelist falls to 0.
//
// testgroup, propertygroup, and property are used similarly -- to
// avoid multiple copies of the same data. Note, however, that these
// are never deleted, as the chance of nothing pointing to them is
// almost 0. New values are created when a new version of any property
// file is used, such as in a new version of the Inspector.

void Results::updateDatabase(const QList<ImageSet> &imageSets, const PropInfoLists * propInfoLists, bool removeResults, bool writeResults)
{
    // If we are not removing or writing results, just return.

    if (!removeResults && !writeResults) return;

    // If there are no image sets, just return. This shouldn't happen,
    // but we need to be safe.

    if (imageSets.size() == 0) return;

    // Sort the results.

    sort();

    // Build a QList of QLists. The outer QList corresponds to image sets
    // and the inner QList contains the results for each image set. This
    // allows processing of results by image set. (The results list does
    // not use the same index as the imageSets list -- there can be zero
    // or more results per image set.)

    // BUG: Rewrite Results so it uses this strategy instead of a single
    // QList of results. Also rewrite the Inspector class to store results
    // this way.

    QList<QList<IResult>> resultGroups = groupResults(imageSets);

    // Open the database connection, start a transaction, and get a query.

    QSqlDatabase db = QSqlDatabase::database(dlnDBName);
    startTransaction(db);
    QSqlQuery q(db);

    // Loop through the image sets and remove/add results.

    for (int i = 0; i < imageSets.size(); i++)
    {
        // Get the image set and its results.

        ImageSet imageSet = imageSets[i];
        QList<IResult> resultGroup = resultGroups[i];

        // Remove the old results if needed.

        if (removeResults)
        {
            deleteResults(q, imageSet, writeResults);
        }

        // Write the results if needed.

        if (writeResults)
        {
            // Create a document in the database for the specific imageset:
            QString htmlDoc;
            QTextStream stream(&htmlDoc, QIODevice::WriteOnly);
            this->writeHTML(stream, resultGroup);
            int docSrl = addInspectionDoc(q, imageSet.srl, htmlDoc);

            // Write an inspection row and link it with the document

            int inspSrl = addInspection(q, imageSet.srl, docSrl);

            // Associate the inspection with the test groups.

            addInspVsTestgroups(q, inspSrl, propInfoLists->testgroupSrlsHash);

            // Associate the inspection with the file lists.

            QHash<QString, int> fileSrlsHash;
            addInspVsFilelists(q, inspSrl, imageSet, fileSrlsHash);

            // Add the results

            addInspectionResults(q, inspSrl, resultGroup, propInfoLists->testgroupSrlsHash, propInfoLists->propSrlsHash, fileSrlsHash);
        }
    }

    // Commit the transaction

    commitTransaction(db);

    // Add a result to show the database was updated.

    addDLNResult(removeResults, writeResults);
}

/********************************************************************/
/*                                                                  */
/*  Protected methods                                               */
/*                                                                  */
/********************************************************************/

/********************************************************************/
/*                                                                  */
/*  Sorting                                                         */
/*                                                                  */
/********************************************************************/

void Results::sort()
{
    // We need sorted results in order to efficiently update the
    // DLN and write the HTML report.

    // If the list has already been sorted, just return.

    if (sorted) return;

    // Sort the list of results using std::sort. Note that
    // QList.begin() does not return the first item in the list
    // (as one might expect), but an STL-style iterator pointing
    // to the first item in the list. (Similarly, QList.end()
    // returns an STL-style iterator pointing to an imaginary
    // item after the last item in the list.)

    // For sort order, see the operator< function in iresult.cpp.

    std::sort(results.begin(), results.end());

    // Set the sorted flag so we don't do unnecessary sorts.

    sorted = true;
}

/********************************************************************/
/*                                                                  */
/*  Private methods                                                 */
/*                                                                  */
/********************************************************************/

/********************************************************************/
/*                                                                  */
/*  IO utility methods                                              */
/*                                                                  */
/********************************************************************/

void Results::write(QFile & file, const QString & str)
{
    // Check the return from write() because QFile does
    // not (appear to) throw exceptions.

    if (file.write(str.toStdString().c_str()) == -1)
        throw IOException("Error writing file", "Error writing to the file:\n\nFile: " + file.fileName() + "\n\nError: " + file.errorString());
}

void Results::write(QFile & file, const char * ptr_to_char)
{
    // Check the return from write() because QFile does
    // not (appear to) throw exceptions.

    if (file.write(ptr_to_char) == -1)
        throw IOException("Error writing file", "Error writing to the file:\n\nFile: " + file.fileName() + "\n\nError: " + file.errorString());
}

void Results::write(QTextStream & stream, const QString & str)
{
    // Check the stream status after every call because
    // QTextStream does not (appear to) throw exceptions.

    stream << str;
    if (stream.status() != QTextStream::Ok)
        throw IOException("File error", "Unknown error writing to file");
}

void Results::write(QTextStream & stream, char c)
{
    // Check the stream status after every call because
    // QTextStream does not (appear to) throw exceptions.

    stream << c;
    if (stream.status() != QTextStream::Ok)
        throw IOException("File error", "Unknown error writing to file");
}

/********************************************************************/
/*                                                                  */
/*  Write results                                                   */
/*                                                                  */
/********************************************************************/

void Results::writeResultHeader(QTextStream &stream)
{
    // Write the result header for use in a spreadsheet.

    write(stream, imageSetTermIC + " set");
    write(stream, '\t');
    write(stream, imageSetTermIC + " set number");
    write(stream, '\t');
    write(stream, "Test group");
    write(stream, '\t');
    write(stream, "Result type");
    write(stream, '\t');
    write(stream, "Property");
    write(stream, '\t');
    write(stream, "Title");
    write(stream, '\t');
    write(stream, "Filename 1");
    write(stream, '\t');
    write(stream, "Filename 2");
    write(stream, '\t');
    write(stream, "Value 1");
    write(stream, '\t');
    write(stream, "Value 2");
    write(stream, '\t');
    write(stream, "Message type");
    write(stream, '\t');
    write(stream, "Description");
    write(stream, '\t');
    write(stream, projectTermIC);
    write(stream, '\t');
    write(stream, "Inspection time");
    write(stream, '\t');
    write(stream, "Severity");
    write(stream, '\n');
}

void Results::writeResult(QTextStream & stream, const IResult & result)
{
    // Write the result separated by tabs. (We can't use commas
    // because some error messages contain commas.) The order
    // in which we write the information is the most useful
    // order for use in a spreadsheet. It follows the same order
    // as that in which the results are sorted.

    // Note that the calling method must flush the output stream.

    write(stream, result.imageSetName);
    write(stream, '\t');
    write(stream, QString::number(result.imageSetNum));
    write(stream, '\t');
    write(stream, ITestGroup::toString(result.testGroup));
    write(stream, '\t');
    write(stream, IResultType::toString(result.resultType));
    write(stream, '\t');
    write(stream, result.propQName);
    write(stream, '\t');
    write(stream, result.title);
    write(stream, '\t');
    write(stream, result.fileInfo1.canonicalFilePath());
    write(stream, '\t');
    write(stream, result.fileInfo2.canonicalFilePath());
    write(stream, '\t');
    write(stream, lineBreakToAtSign(result.value1));
    write(stream, '\t');
    write(stream, lineBreakToAtSign(result.value2));
    write(stream, '\t');
    write(stream, IMessageType::toString(result.msgType));
    write(stream, '\t');
    write(stream, lineBreakToAtSign(result.description));
    write(stream, '\t');
    write(stream, projectDirectory);
    write(stream, '\t');
    write(stream, inspectionTime.toString("yyyy-MM-ddThh:mm"));
    write(stream, '\t');
    write(stream, result.resultType == IResultType::OK ? "--" : QString::number(result.severity));
    write(stream, '\n');
}

QString Results::lineBreakToAtSign(const QString & str)
{
    // When storing results in a text file, we must replace
    // line breaks in values and descriptions with at signs.
    // Otherwise, methods reading those descriptions from the
    // text file will wrongly interpret the length of a line.

    // We hope nobody uses @@@ in a metadata property value
    // or error message...

    QString tmp = str;
    tmp.replace("\n", "@@@");
    return tmp;
}

QString Results::atSignToLineBreak(const QString & str)
{
    QString tmp = str;
    tmp.replace("@@@", "\n");
    return tmp;
}


/********************************************************************/
/*                                                                  */
/*  Utility methods to write the HTML report                        */
/*                                                                  */
/********************************************************************/

void Results::writeHTMLHeadings(QTextStream &htmlFile)
{
    // Write out the HTML wrapper and headings. Note that for the
    // Custom tab of the PG Inspector, the project directory is empty
    // because the files can be anywhere.

    write(htmlFile, "<html>\n<head>");
    writeCSSStyles(htmlFile);
    write(htmlFile, "</head>\n<body>\n");
    write(htmlFile, "<h1>DLN Inspector Report, ");
    write(htmlFile, inspectionTime.toString("yyyy-MM-dd, hh:mm") + "</h1>\n");
    if (!projectDirectory.isEmpty())
    {
        write(htmlFile, "<h2>" + projectTermIC + " Directory: ");
        write(htmlFile, projectDirectory);
        write(htmlFile, "</h2>\n");
    }
}

void Results::writeCSSStyles(QTextStream & htmlFile)
{
    // Write the CSS styles to format the HTML report.
    // The only non-obvious style is the ec class, which
    // is used to suppress borders on leading empty cells.
    // This makes it easy to see the result hierarchy.

    write(htmlFile, "<style>\n");
    write(htmlFile, "table.toc {\n"
                    "  border: none;\n"
                    "  border-spacing: 1px;\n"
                    "}\n");
    write(htmlFile, "table.toc th {\n"
                    "  text-align: left;\n"
                    "  border-bottom: 1px solid black;\n"
                    "  padding: 0px 10px;\n"
                    "}\n");
    write(htmlFile, "table.toc td {\n"
                    "  vertical-align: top;\n"
                    "  padding: 0px 10px;\n"
                    "}\n");
    write(htmlFile, "table.results {\n"
                    "  border-collapse: collapse;\n"
                    "  border: none;\n"
                    "}\n");
    write(htmlFile, "table.results th {\n"
                    "  vertical-align: bottom;\n"
                    "  border: 1px solid black;\n"
                    "  layout: fixed;\n"
                    "}\n");
    write(htmlFile, "table.results td {\n"
                    "  vertical-align: top;\n"
                    "  border: 1px solid black;\n"
                    "  layout: fixed;\n"
                    "  padding: 2px 4px 2px 4px;\n"
                    "}\n");
    write(htmlFile, "td.ec {\n"           // ec = empty cell
                    "  border: none !important;\n"
                    "}\n");
    write(htmlFile, ":link {\n"
                    "  color: Blue;\n"
                    "  text-decoration: none;\n"
                    "}\n");
    write(htmlFile, ":visited  {\n"
                    "  color: DarkSlateBlue;\n"
                    "}\n");
    write(htmlFile, ":hover {\n"
                    "  text-decoration: underline;\n"
                    "}\n");
    write(htmlFile, "</style>\n");
}

void Results::writeHTMLTOC(QTextStream & htmlFile, const QList<IResult>& results)
{
    // Create an IResultDiff object.

    IResultDiff diff;

    // Create the variables used to build href values.

    QString csHref,
            tgHref,
            pHref,
            tiHref;

    // Initialize the IResultDiff object and set the break
    // field to 0.

    diff.initialize(results.at(0));
    int breakField = 0;

    // Cycle through the results and write the table of contents.

    for (int i = 0; i < results.size(); i++)
    {
        // Get the next result.

        IResult result = results.at(i);

        // If this is not the first result, get the break field
        // and (if necessary) finish the previous result.

        if (i != 0)
        {
            // Get the break field, which is the field in which this
            // result differs from the previous result.

            breakField = diff.getBreakField(result);

            // If there was a break, finish the previous result
            // and start the current result.

            if (breakField != -1)
            {
                // Finish the previous image set.

                if (breakField == 0)
                {
                    write(htmlFile, "</table>\n");
                }

                // Calculate the number of empty cells we need
                // to property indent the table of contents entry.

                int numEmptyCells;
                if (breakField == 4)
                    numEmptyCells = 2;
                else if (breakField > 1) // breakField == 2 or 3
                    numEmptyCells = 1;
                else                     // breakField == 0 or 1
                    numEmptyCells = 0;

                // If there are any empty cells, start a new row
                // and write the empty cells.

                if (numEmptyCells > 0)
                {
                    // Start the table row.

                    write(htmlFile, "<tr>");

                    // Write the empty cells.

                    for (int j = 0; j < numEmptyCells; j++)
                    {
                        write(htmlFile, "<td />");
                    }
                }
            }
        }

        // Write the current table of contents row. We only write
        // fields from the current break field to the end of the
        // row. This indents the table of content entries.

        switch (breakField)
        {
            case 0: // Inspection set

                // Write an image set header and link it to the
                // image set data.

                csHref = "c" + QString::number(result.imageSetNum);
                write(htmlFile, "<h2><a href='#");
                write(htmlFile, csHref);
                write(htmlFile, "'>" + imageSetTermIC + " Set: ");
                write(htmlFile, result.imageSetName);
                write(htmlFile, " (" + QString::number(this->imageSetCount.value(result.imageSetName)) + " images)");
                write(htmlFile, "</a></h2>\n");

                // Start the TOC table for the image set.

                write(htmlFile, "<table class='toc'>\n");

                // Write the column headers.

                write(htmlFile, "<tr>"
                                "<th style='min-width: 150px;'>Test Group</th>"
                                "<th style='min-width: 250px;'>Property</th>"
                                "<th style='min-width: 250px;'>Result</th>"
                                "</tr>\n");

                // Fall through to the test group.
                Q_FALLTHROUGH();

            case 1: // Test group

                // Start the row for the test group, and write the
                // test group cell, linking it to to the test group data.

                // Note that we pad the tops of the cells in the
                // first row of each test group (except for the first
                // test group in the image set (breakField == 0)).
                // This visually separates the test groups, increasing
                // readability.

                tgHref = "t" + QString::number(result.testGroup);
                write(htmlFile, "<tr><td");
                if (breakField == 1) write(htmlFile, " style='padding-top: 10px'");
                write(htmlFile, "><a href='#");
                write(htmlFile, csHref);
                write(htmlFile, tgHref);
                write(htmlFile, "'>");
                write(htmlFile, ITestGroup::toString(result.testGroup));
                write(htmlFile, "</a>:");
                write(htmlFile, "</td>");

                // Fall through to property.
                Q_FALLTHROUGH();

            case 2: // Result type
            case 3: // Property

                // Because we don't list the result type in the table
                // of contents, we treat a break in result type the same
                // as a break in property.

                // Write the property name cell, linking it to the
                // property data. Notice that we do not allow
                // slashes or colons in the link name, as these
                // are not allowed in fragment identifiers.

                // Note the extra vertical padding when breakField == 1.

                write(htmlFile, "<td");
                if (breakField == 1) write(htmlFile, " style='padding-top: 10px'");
                write(htmlFile, "><a href='#");
                write(htmlFile, csHref);
                write(htmlFile, tgHref);
                pHref = result.propQName;
                pHref.remove("/").remove(":");
                write(htmlFile, pHref);
                write(htmlFile, "'>");
                write(htmlFile, result.propQName);
                write(htmlFile, "</a></td>");

                // Fall through to title.
                Q_FALLTHROUGH();

            case 4:  // Title

                // Write the title cell, linking it to the title data.

                // Note the extra vertical padding when breakField == 1.

                write(htmlFile, "<td");
                if (breakField == 1) write(htmlFile, " style='padding-top: 10px'");
                write(htmlFile, "><a href='#");
                write(htmlFile, csHref);
                write(htmlFile, tgHref);
                write(htmlFile, pHref);
                tiHref = "ti" + QString::number(i);
                write(htmlFile, tiHref);
                write(htmlFile, "'>");
                write(htmlFile, result.title);
                write(htmlFile, "</a></td></tr>\n");
                break;

            case -1: // No break.
            default:

                break;
        }
    }

    // End the table of contents table.

    write(htmlFile, "</table>\n");
}

void Results::writeHTMLRows(QTextStream & htmlFile, const QList<IResult>& results)
{

    // Create an IResultDiff object.

    IResultDiff diff;

    // Create the variables used to build name values.

    QString csName,
            tgName,
            pName,
            tiName;

    // Initialize the IResultDiff object and set the break
    // field to 0.

    diff.initialize(results.at(0));
    int breakField = 0;

    // Cycle through the results. Write headers for
    // inspection sets and rows for everything else.

    for (int i = 0; i < results.size(); i++)
    {
        // Get the next result.

        IResult result = results.at(i);

        // If this is not the first result, get the break field.

        if (i != 0)
        {
            breakField = diff.getBreakField(result);

            // Calculate the number of empty cells we need
            // to property indent the table of contents entry.
            // On breaks, we use breakField - 1 empty cells
            // (instead of breakField empty cells) because
            // there is no column for the inspection set.

            int numEmptyCells = (breakField == -1) ? 5 : breakField - 1;

            // If there are any empty cells, start a new row
            // and write the empty cells.

            if (numEmptyCells > 0)
            {
                // Start the table row.

                write(htmlFile, "<tr>");

                // Write the empty cells.

                for (int j = 0; j < numEmptyCells; j++)
                {
                    write(htmlFile, "<td class='ec' />");
                }
            }
        }

        // Write the current data row. We only write fields
        // from the current break field to the end of the
        // row. This indents the data entries.

        switch (breakField)
        {
            case 0: // Image set

                // If this is not the first result, finish the table
                // for the previous image set.

                if (i != 0)
                {
                    write(htmlFile, "</table>\n");
                }

                // Write the image set subheader. Note that the
                // image set name is a link target.

                csName = "c" + QString::number(result.imageSetNum);
                write(htmlFile, "<h2><a name='");
                write(htmlFile, csName);
                write(htmlFile, "'>" + imageSetTermIC + " Set: ");
                write(htmlFile, result.imageSetName);
                write(htmlFile, "</a></h2>");

                // Start the image set results table.

                write(htmlFile, "<table class='results'>\n");

                // Write the header row.

                write(htmlFile, "<tr>\n"
                                "<th style='min-width: 150px;'>Test Group</th>\n"
                                "<th style='min-width: 150px;'>Result</th>\n"
                                "<th style='min-width: 150px;'>Property</th>\n"
                                "<th style='min-width: 60px;'>Severity</th>\n"
                                "<th style='min-width: 200px;'>Title</th>\n"
                                "<th style='min-width: 125px;'>File 1</th>\n"
                                "<th style='min-width: 125px;'>File 2</th>\n"
                                "<th style='min-width: 100px;'>Value 1</th>\n"
                                "<th style='min-width: 100px;'>Value 2 /<br />Restrictions</th>\n"
                                "<th style='min-width: 500px;'>Description</th>\n"
                                "</tr>\n");

                // Fall through to test group.
                Q_FALLTHROUGH();

            case 1: // Test group

                // Start a new row and write the test group name.
                // Note that the test group name is a link target.

                tgName = "t" + QString::number(result.testGroup);
                write(htmlFile, "<tr><td><a name='");
                write(htmlFile, csName);
                write(htmlFile, tgName);
                write(htmlFile, "'>");
                write(htmlFile, ITestGroup::toString(result.testGroup));
                write(htmlFile, "</a></td>");

                // Fall through to result type.
                Q_FALLTHROUGH();

            case 2: // Result type

                // Write the result type name.

                write(htmlFile, "<td>");
                write(htmlFile, IResultType::toString(result.resultType));
                write(htmlFile, "</td>");

                // Fall through to property.
                Q_FALLTHROUGH();

            case 3: // Property

                // Write the property name. Note that the property
                // name is a link target.

                pName = result.propQName;
                pName.remove("/").remove(":");
                write(htmlFile, "<td><a name='");
                write(htmlFile, csName);
                write(htmlFile, tgName);
                write(htmlFile, pName);
                write(htmlFile, "'>");
                write(htmlFile, result.propQName);
                write(htmlFile, "</a></td>");

                // Fall through to severity.
                Q_FALLTHROUGH();

            case 4: // Severity

                write(htmlFile, "<td>");
                write(htmlFile, result.resultType == IResultType::OK ? "--" : QString::number(result.severity));
                write(htmlFile, "</td>");

                // Fall through to title
                Q_FALLTHROUGH();

            case 5: // Title


                // Write the title. Note that the title
                // is a link target.

                write(htmlFile, "<td><a name='");
                write(htmlFile, csName);
                write(htmlFile, tgName);
                write(htmlFile, pName);
                tiName = "ti" + QString::number(i);
                write(htmlFile, tiName);
                write(htmlFile, "'>");
                write(htmlFile, result.title);
                write(htmlFile, "</td>");
                break;

            case -1: // No break.
            default:
                break;
        }

        // For every row, write the filenames, the values, and
        // the description. End the row after the description.

        write(htmlFile, "<td>");
        write(htmlFile, result.fileInfo1.fileName());
        write(htmlFile, "</td>");

        write(htmlFile, "<td>");
        write(htmlFile, result.fileInfo2.fileName());
        write(htmlFile, "</td>");

        write(htmlFile, "<td>");
        write(htmlFile, result.value1);
        write(htmlFile, "</td>");

        write(htmlFile, "<td>");
        write(htmlFile, result.value2);
        write(htmlFile, "</td>");

        write(htmlFile, "<td>");
        write(htmlFile, result.description);
        write(htmlFile, "</td></tr>\n");
    }

    // End the table.

    write(htmlFile, "</table>\n");
}

/********************************************************************/
/*                                                                  */
/*  Methods to update the DLN database                              */
/*                                                                  */
/********************************************************************/

/********************************************************************/
/*  Methods to delete inspections                                   */
/********************************************************************/

void Results::deleteResults(QSqlQuery & q, ImageSet & imageSet, bool writeResults)
{
    // This method removes previous inspections from the DLN.

    // Delete the associations between previous inspections and
    // test groups.

    // NOTE: We do not remove the test groups. This is because
    // it would be extremely rare for some other inspection
    // not to be using them, and they would usually be added back
    // at the next inspection anyway. (The only time they would
    // not be added back is if the next inspection used properties
    // with a new version.)

    deleteInspVsTestgroup(q, imageSet.srl);

    // Delete the associations between previous inspections
    // and filelists.

    deleteInspVsFilelist(q, imageSet.srl);

    // Delete the inspection results.

    deleteInspectionResults(q, imageSet.srl);

    // Calculate the hashes of the archival and processed
    // filelists in the image set.

    QString hash_a = getHash(imageSet.archivalFileInfos);
    QString hash_p = getHash(imageSet.processedFileInfos);

    // Delete unused filelists except for filelists that
    // will be used by this inspection. Note that if we are
    // not writing results, no filelists will be used by
    // this inspection.

    deleteFilelistsAndPaths(q, hash_a, "a", writeResults);
    deleteFilelistsAndPaths(q, hash_p, "p", writeResults);

    // Delete the inspections.

    deleteInspections(q, imageSet.srl);
}

void Results::deleteInspVsTestgroup(QSqlQuery & q, int imageSetSrl)
{
    // Delete rows from inspection_vs_testgroup for the
    // image set. Note that the subquery returns multiple
    // rows if there have been multiple previous inspections.

    QString query = "DELETE FROM inspection_vs_testgroup "
                    "WHERE inspection_srl IN "
                    "(SELECT srl "
                    " FROM inspection "
                    " WHERE cptset_srl = " + QString::number(imageSetSrl) + ")";
    execQuery(q, query);
    q.finish();
}

void Results::deleteInspVsFilelist(QSqlQuery & q, int imageSetSrl)
{
    // Delete rows from inspection_vs_filelist for the
    // image set. Note that the subquery returns multiple
    // rows if there have been multiple previous inspections.

    QString query = "DELETE FROM inspection_vs_filelist "
                    "WHERE inspection_srl IN "
                    "(SELECT srl "
                    " FROM inspection "
                    " WHERE cptset_srl = " + QString::number(imageSetSrl) + ")";
    execQuery(q, query);
    q.finish();
}

void Results::deleteInspectionResults(QSqlQuery & q, int imageSetSrl)
{
    // Delete the previous inspection results for this image set. Note
    // that the subquery will return multiple rows if there have been
    // multiple previous inspections.

    QString query = "DELETE FROM inspresult "
                    "WHERE inspection_srl IN "
                    "(SELECT srl "
                     "FROM inspection "
                     "WHERE cptset_srl = " + QString::number(imageSetSrl) + ")";
    execQuery(q, query);
    q.finish();
}

void Results::deleteFilelistsAndPaths(QSqlQuery & q, const QString & hash, const QString & filelistType, bool writeResults)
{
    // Delete the unused filepaths.

    deleteFilepaths(q, hash, filelistType, writeResults);

    // Delete the unused filelists.

    deleteFilelists(q, hash, filelistType, writeResults);
}

void Results::deleteFilepaths(QSqlQuery & q, const QString & hash, const QString & filelistType, bool writeResults)
{
    // Delete any unused filepaths. (Note that this query will delete
    // ALL unused filepaths, not just those freed up by removing the
    // associations between the previous inspections for this image set.)

    // If we are writing results, we also do not delete filepaths
    // that we want to reuse -- that is, filepaths belonging to a filelist
    // whose hash matches the hash of the current (archival or processed)
    // files, if any.

    QString hashCheck;
    if (writeResults)
    {
        if (!hash.isEmpty()) hashCheck = " AND fl.filepath_hash <> '" + hash + "'";
    }

    // The query performs three subqueries:
    //
    // a) Check that the filepath belongs to a filelist of the specified
    //    type and (if we are writing results) does not have the same
    //    hash as the filelist of this type in the image set.
    // b) Check that the filepath belongs to a filelist that is not
    //    in inspection_vs_filelist.
    // c) Check that the filepath belongs to a filelist that is not
    //    in cptset_vs_filelist.
    //
    // The latter two subqueries ensure that the filepath belongs to
    // a filelist that is not being used.

    QString query = "DELETE FROM filepath fp "
                    "WHERE fp.filelist_srl IN "
                        "(SELECT srl "
                         "FROM filelist fl "
                         "WHERE fl.srl = fp.filelist_srl "
                            "AND fl.filelist_type = '" + filelistType + "' " +
                            hashCheck + ") " +
                    "AND NOT EXISTS "
                        "(SELECT NULL "
                         "FROM inspection_vs_filelist vs1 "
                         "WHERE fp.filelist_srl = vs1.filelist_srl) "
                    "AND NOT EXISTS "
                        "(SELECT NULL "
                         "FROM cptset_vs_filelist vs2 "
                         "WHERE fp.filelist_srl = vs2.filelist_srl)";

    // Execute the query.

    execQuery(q, query);
    q.finish();
}

void Results::deleteFilelists(QSqlQuery & q, const QString & hash, const QString & filelistType, bool writeResults)
{
    // Delete unused filelists. See notes in deleteFilepaths().

    QString hashCheck;
    if (writeResults)
    {
        if (!hash.isEmpty()) hashCheck = " AND fl.filepath_hash <> '" + hash + "'";
    }

    QString query = "DELETE FROM filelist fl "
                    "WHERE fl.filelist_type = '" + filelistType + "'" + hashCheck + " "
                    "AND NOT EXISTS "
                        "(SELECT NULL "
                         "FROM inspection_vs_filelist vs1 "
                         "WHERE fl.srl = vs1.filelist_srl) "
                    "AND NOT EXISTS "
                        "(SELECT NULL "
                         "FROM cptset_vs_filelist vs2 "
                         "WHERE fl.srl = vs2.filelist_srl) ";
    execQuery(q, query);
    q.finish();
}

void Results::deleteInspections(QSqlQuery & q, int imageSetSrl)
{
    // Delete the previous inspections for this image set.

    QString query = "DELETE FROM inspection "
                    "WHERE cptset_srl = " + QString::number(imageSetSrl);
    execQuery(q, query);
    q.finish();
}

/********************************************************************/
/*  Methods to add inspections                                      */
/********************************************************************/

int Results::addInspection(QSqlQuery & q, int imageSetSrl, int docSrl)
{
    // Add a new row to the inspection table.

    QString query = "INSERT INTO inspection "
                    "(cptset_srl, inspection_datetime, inspector_version, inspection_uuid, documt_srl) "
                    "VALUES (:imageSetSrl, :inspectionTime, :version, DEFAULT, :docSrl) "
                    "RETURNING srl";

    prepareQuery(q, query);
    q.bindValue(":imageSetSrl", imageSetSrl);
    q.bindValue(":inspectionTime", inspectionTime);
    q.bindValue(":version", inspectorVersionNumber);
    q.bindValue(":docSrl", docSrl);
    execPrepared(q);
    // Return the srl of the row.
    q.next();
    int srl = q.value(0).toInt();
    q.finish();
    return srl;
}

void Results::addInspVsTestgroups(QSqlQuery & q, int inspSrl, const QHash<QString, int> & testgroupSrlHash)
{
    // Associate the inspection with all of the test groups used
    // during the inspection. The test groups contain property
    // groups, which contain the actual properties tested.

    QString query = "INSERT INTO inspection_vs_testgroup "
                    "(inspection_srl, testgroup_srl) "
                    "VALUES (" + QString::number(inspSrl) + ", ?)";
    prepareQuery(q, query);

    QList<int> testgroupSrlList = testgroupSrlHash.values();
    for (int i = 0; i < testgroupSrlList.size(); i++)
    {
        q.bindValue(0, testgroupSrlList[i]);
        execPrepared(q);
    }
}

void Results::addInspVsFilelists(QSqlQuery & q, int inspSrl, ImageSet & imageSet, QHash<QString, int> &fileSrlsHash)
{
    addInspVsFilelist(q, inspSrl, imageSet.archivalFileInfos, imageSet.docIds, imageSet.archivalType, "a", fileSrlsHash);
    addInspVsFilelist(q, inspSrl, imageSet.processedFileInfos, imageSet.docIds, imageSet.processedType, "p", fileSrlsHash);
}

void Results::addInspVsFilelist(QSqlQuery & q, int inspSrl, const QList<QFileInfo> & fileInfos, const QHash<QFileInfo, ImageSet::DocumentIds> & docIds, int fileType, const QString & filelistType, QHash<QString, int> &fileSrlsHash)
{
    // If there are no files, just return.

    if (fileInfos.size() == 0) return;

    // Get the hash of the files.

    QString hash = getHash(fileInfos);

    // Check if there is an existing filelist with this type and hash.
    // This is common if there are multiple inspections of the same
    // image set.

    int filelistSrl = getFilelistSrl(q, hash, filelistType);

    // If there is not a filelist, add it now.

    if (filelistSrl == -1)
    {
        filelistSrl = addFilelist(q, hash, fileInfos, docIds, fileType, filelistType);
    }

    // Build a hashtable of the srls of the files in it.

    getFilepathSrls(q, fileSrlsHash, filelistSrl);

    // Associate the inspection with the filelist.

    addFilelistVsOther(q, "inspection_vs_filelist", "inspection_srl", inspSrl, filelistSrl);
}

void Results::getFilepathSrls(QSqlQuery & q, QHash<QString, int> &fileSrlsHash, int filelistSrl)
{
    // Get the srls and paths for the paths in a filelist and
    // store them in a QHash.

    QString query = "SELECT filepath, srl FROM filepath "
                    "WHERE filelist_srl = " + QString::number(filelistSrl);
    execQuery(q, query);

    while (q.next())
    {
        fileSrlsHash.insert(q.value(0).toString(), q.value(1).toInt());
    }
    q.finish();
}

void Results::addInspectionResults(QSqlQuery & q, int inspSrl, const QList<IResult> & resultGroup, const QHash<QString, int> & testgroupSrlsHash, const QHash<QString, int> & propSrlsHash, const QHash<QString, int> &fileSrlsHash)
{
    // Loop through the results and add them to the database.

    QString query = "INSERT INTO inspresult "
                    "(inspection_srl, testgroup_srl, result_type, msg_type, title, "
                     "description, property_srl, filepath1_srl, filepath2_srl, "
                     "value1, value2, inspresult_uuid) "
                     "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    prepareQuery(q, query);
    for (int i = 0; i < resultGroup.size(); i++)
    {
        // Get the result.

        IResult result = resultGroup[i];

        // Get the srls of the test group, property, and files, if any. Note
        // that we do not necessarily have a property or files. There is always
        // a test group.

        int testgroupSrl = testgroupSrlsHash.value(ITestGroup::toString(result.testGroup));
        int propSrl = propSrlsHash.value(result.propQName, -1);
        int file1Srl = fileSrlsHash.value(result.fileInfo1.canonicalFilePath(), -1);
        int file2Srl = fileSrlsHash.value(result.fileInfo2.canonicalFilePath(), -1);

        // Bind the values for the result.

        // BUG: Note that QVariant::Int tells QVariant to construct a
        // null QVariant of type int. QVariant::Int is obsolete [1]
        // and the documentation doesn't say what to use instead. We
        // might be able to use QMetaType::Int instead, but there is
        // some weirdness between the two [2], it's working now, and
        // I don't have time to fix this.
        //
        // [1] http://doc.qt.io/qt-5/qvariant-obsolete.html#Type-enum
        // [2] https://stackoverflow.com/questions/31290606/qmetatypefloat-not-in-qvarianttype

        q.bindValue(0, inspSrl);
        q.bindValue(1, testgroupSrl);
        q.bindValue(2, result.resultType);
        q.bindValue(3, result.msgType);
        q.bindValue(4, result.title);
        q.bindValue(5, result.description);
        if (propSrl == -1) q.bindValue(6, QVariant(QVariant::Int)); else q.bindValue(6, propSrl);
        if (file1Srl == -1) q.bindValue(7, QVariant(QVariant::Int)); else q.bindValue(7, file1Srl);
        if (file2Srl == -1) q.bindValue(8, QVariant(QVariant::Int)); else q.bindValue(8, file2Srl);
        q.bindValue(9, result.value1);
        q.bindValue(10, result.value2);
        q.bindValue(11, QUuid::createUuid().toString());

        // Execute the query.

        execPrepared(q);
    }
    q.finish();
}

int Results::addInspectionDoc(QSqlQuery& q, int imageSetSrl, const QString& htmlDoc)
{

    static const QString inp_documt_type_uuid = "bf381726-9db9-11ea-a105-5cf9dd66309a";

    QString query = "INSERT INTO documt "
                    "(documt_uri, documt_title, documt_authr, documt_itself, documt_date, deleted, documt_type_srl) "
                    "SELECT :uri, :title, :author, :doc, tsrange(:inspTime, :inspTime, '[]'), :deleted, srl FROM documt_type "
                    "WHERE documt_type_uuid=:uuid AND deleted IS NOT TRUE ORDER BY srl LIMIT 1";
    prepareQuery(q, query);
    q.bindValue(":uri", QString("urn:uuid:%1").arg(QUuid::createUuid().toString(QUuid::WithoutBraces)));
    q.bindValue(":title", QString("Inspection results for Image Set %1 - %2").arg(imageSetSrl).arg(inspectionTime.toString("yyyy.MM.dd-hh:mm:ss")));
    q.bindValue(":author", QString("DLN Inspector (%1)").arg(inspectorVersionNumber));
    q.bindValue(":doc", htmlDoc);
    q.bindValue(":inspTime", inspectionTime);
    q.bindValue(":deleted", true); // Hack, to hide the Inspection reports from the Bundle's doc list in DLN-CC
    q.bindValue(":uuid", inp_documt_type_uuid);

    // Execute the query.
    execPrepared(q);
    int docSrl = q.lastInsertId().toInt();
    q.finish();

    // Now insert the association with the image set:

    query = "INSERT INTO cptset_vs_documt "
            "(documt_srl, cptset_srl) "
            "VALUES(?,?)";
    prepareQuery(q, query);
    q.bindValue(0, docSrl);
    q.bindValue(1, imageSetSrl);
    // Execute the query.
    execPrepared(q);
    q.finish();

    return docSrl;
}

/********************************************************************/
/*  Utility methods to update the DLN database                      */
/********************************************************************/

QList<QList<IResult>> Results::groupResults(const QList<ImageSet> imageSets)
{
    // This isn't very efficient -- lots of copying -- but
    // I don't have time to write anything better.

    // Note that this code is fragile. It relies on the fact that
    // IResult.imageSetNum is the loop counter through the QList
    // of image sets passed to Inspector.inspect().

    // Loop through the image sets and get the results that
    // apply to them.

    int imageSetsCount = imageSets.size();
    QList<QList<IResult>> resultGroups;
    // Allocate space for all image sets:
    resultGroups.reserve(imageSetsCount);
    for (int i = 0; i < imageSetsCount; i++)
    {
        // Def. construct the results for each image set:
        resultGroups.append(QList<IResult>{});
    }

    // Now fill in the results in the proper slot (image set):
    for(const IResult& result: qAsConst(results)) {
        Q_ASSERT(0<=result.imageSetNum && result.imageSetNum < imageSetsCount);
        resultGroups[result.imageSetNum].append(result);
    }

    // Return the grouped results.

    return resultGroups;
}

int Results::getResultGroup(int imageSetNum, int resultIndexStart, QList<IResult> & resultGroup)
{
    // Start at the previous stopping index.

    int resultIndex = resultIndexStart;

    // Get the first result that might apply to this image set.

    IResult result = results[resultIndex];

    // Add results to the list until we reach a result that
    // applies to the next image set or we reach the last result.

    while (result.imageSetNum == imageSetNum)
    {
        // Add the result to the list.

        resultGroup.append(result);

        // Increment the result and check that we haven't
        // reached the last result.

        resultIndex++;
        if (resultIndex == results.size()) break;

        // Get the next result.

        result = results[resultIndex];
    }

    // Return the index we stopped at. This will be the starting
    // index for the next image set.

    return resultIndex;
}

void Results::addDLNResult(bool removeResults, bool writeResults)
{
    QString description("For these image sets, ");
    if (removeResults)
    {
        description.append("previous inspection results have been removed");
    }
    if (writeResults)
    {
        if (removeResults)
        {
            description.append(" and ");
        }
        description.append("new inspection results have been saved.");
    }
    addResult(IResult("ALL IMAGE SETS",
                      std::numeric_limits<int>::max(),
                      ITestGroup::UpdateDLN,
                      IResultType::OK,
                      "DLN updated",
                      description));
}
