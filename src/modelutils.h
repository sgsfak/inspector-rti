/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2019                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

#ifndef MODELUTILS_H
#define MODELUTILS_H

// Qt headers

#include <QListView>
#include <QStringListModel>

class ModelUtils
{
public:
    ModelUtils();
    ~ModelUtils();

    // Convenience methods for working with Qt models

    void clearModel(QStringListModel * model);
    QStringList getSelectedData(QListView * view, QStringListModel * model);
    QStringList getAllData(QStringListModel * model);
    void removeSelectedRows(QListView * view, QStringListModel * model);

//    void moveSelectedRow(QListView * fromList, QStringListModel * fromModel, QStringListModel * toModel);
//    void moveRow(QStringListModel * fromModel, QModelIndex fromIndex, QStringListModel * toModel, int toRowNum);
//    void moveAllRows(QStringListModel * fromModel, QStringListModel * toModel);
    QPersistentModelIndex addRow(QStringListModel * model, int rowNum, const QVariant & data);
    void setModelToolTip(QStringListModel * model, int rowNum, const QVariant & data);

private:
    QModelIndexList getSelectedIndexes(QListView * view);

};

#endif // MODELUTILS_H
