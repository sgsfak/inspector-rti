/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

#ifndef ENUMS_H
#define ENUMS_H

// Qt headers

#include <QColor>
#include <QHash>
#include <QString>
#include <QStringRef>

// This file contains all enums used by the Inspector (in alphabetical
// order), even if they are used only by one class. They are listed
// here because it is too difficult to find them when they are
// scattered across multiple files.

// The typing used by these enums isn't as strong as if we'd
// used an enum class, but enum classes can't contain methods
// and are also supported only in C++11, so this seems likes
// a good compromise. For more information, see:
//
//    http://gamesfromwithin.com/stupid-c-tricks-2-better-enums

/********************************************************************/
/*                                                                  */
/*  IComparisonType enum                                            */
/*                                                                  */
/********************************************************************/

// Enum describing the kinds of comparisons to make. For more
// information, search for PropElementToken::Compare in
// propertyinfo.cpp.

namespace IComparisonType
{
    // These match values stored in the DLN database. DO NOT CHANGE NUMBERS.

    enum Enum {None = 0x00,
               Restrictions = 0x01,
               WithinFileType = 0x02,
               AcrossFileTypes = 0x04,
               AgainstDLN = 0x08,
               All = Restrictions | WithinFileType | AcrossFileTypes | AgainstDLN,
               Unknown = 0xff // Since we're ANDing, this value is probably a bug...
              };
    Enum fromString(const QString & str);
}

/********************************************************************/
/*                                                                  */
/*  IErrorCode enum and methods                                     */
/*                                                                  */
/********************************************************************/

// Enum describing an error, usually one that occurs
// when comparing two property values.

namespace IErrorCode
{
    enum Enum {OK,
               DiffIsNull,
               DiffTypes,
               DiffIsArray,
               DiffArraySizes,
               DiffStructSizes,
               Equal,
               Unequal,
               DateDiffTooBig,
               LessThan,
               GreaterThan,
               InvalidValue,
               OpNotSupported,
               UnknownOperator
              };

    QString toString(Enum code);
}

/********************************************************************/
/*                                                                  */
/*  IFileType enum and methods                                      */
/*                                                                  */
/********************************************************************/

namespace IFileType
{
    // These are numbered because they match values stored in
    // the DLN database. DO NOT CHANGE NUMBERS.

    enum Enum {JPEG = 0,
               TIFF = 1,
//               PNG = 2,
               DNG = 3,
//               RDF = 4,
               UNKNOWN = 0xff};

    QString toString(Enum type);
    Enum fromString(const QString & str);
    QStringList getExtensions(Enum type);
    QStringList getProcessedExtensions();
    QStringList getArchivalExtensions();
}

/********************************************************************/
/*                                                                  */
/*  IMessageType enum and methods                                   */
/*                                                                  */
/********************************************************************/

// Enum used to tell the Inspector how to construct a
// message that describes a result.

namespace IMessageType
{
    enum Enum {CompareValues = 0,
               Restriction = 1,
               InvalidValue = 2,
               Text = 3,
               None = 0xff};

    QString toString(Enum msgType);
    Enum fromString(const QString & str);
    QString getFilenameLabel(IMessageType::Enum msgType, int num);
    QString getValueLabel(IMessageType::Enum msgType, int num);
}

/********************************************************************/
/*                                                                  */
/*  InspectorType enum and methods                                  */
/*                                                                  */
/********************************************************************/

// Enum used to discriminate between the RTI and photogrammetry Inspectors.

namespace InspectorType
{
    enum Enum {RTI,
               Photogrammetry,
               Documentary,
               MultiSpectral};

    QString toString(Enum inspectorType);
    QString toDBString(Enum inspectorType);
}

/********************************************************************/
/*                                                                  */
/*  IOperator enum and methods                                      */
/*                                                                  */
/********************************************************************/

// Enum used to describe the comparison operators.

namespace IOperator
{
    enum Enum {EQ,
               NE,
               LT,
               LE,
               GT,
               GE,
               Unknown};

    // Methods to convert to/from QString.

    QString toString(Enum op);
    Enum fromString(const QString & str);
    QString toSymbol(Enum op, bool html);
}

/********************************************************************/
/*                                                                  */
/*  IResultType enum and methods                                    */
/*                                                                  */
/********************************************************************/

// Enum used to describe the type of a result.

namespace IResultType
{
    // The order of the enum values is important because they
    // are used to sort IResults. See operator< in iresult.cpp.
    // They also match values stored in the DLN database. DO
    // NOT CHANGE NUMBERS.

    enum Enum {Warning = 0,
               FileError = 1,
               CaptureError = 2,
               TransformationError = 3,
               OK = 4,
               FatalError = 5,
               Unknown = 0xff};

    // Methods to convert to/from QString and QColor.

    QString toString(Enum resultType);
    Enum fromString(const QString & str);
    QColor toColor(Enum resultType);
}

/********************************************************************/
/*                                                                  */
/*  IStatus enum and methods                                        */
/*                                                                  */
/********************************************************************/

// Enum used to describe the status of the Inspector.

namespace IStatus
{
    // The order of the values is important, as code assumes
    // them to increase with the level of severity.

    enum Enum {Success = 0x01,
               Warning = 0x02,
               Error = 0x04};
}

/********************************************************************/
/*                                                                  */
/*  ITableType enum and methods                                     */
/*                                                                  */
/********************************************************************/

// Enum used to differentiate between the project (cptproj) and bundle
// (cptsess) tables.

namespace ITableType
{
    enum Enum {Project,
               Bundle,
               ImageSet};
}

/********************************************************************/
/*                                                                  */
/*  ITestGroup enum and methods                                     */
/*                                                                  */
/********************************************************************/

// Enum used to describe the test group a property belongs to.

namespace ITestGroup
{
    // The order of the enum values is important because they
    // are used to sort IResults. See operator< in iresult.cpp.

    enum Enum {File,
               Capture,
               Transformation,
               UpdateDLN,
               None};

    // Methods to convert to/from QString

    QString toString(Enum type);
    Enum fromString(const QString & str);
    QString fingerprint(Enum type);
}

/********************************************************************/
/*                                                                  */
/*  PropAttrToken enum and methods                                  */
/*                                                                  */
/********************************************************************/

// Enum used to identify attributes when parsing property files.

namespace PropAttrToken
{
    enum Enum {Unknown,
               Version};

    Enum fromString(const QStringRef & str);
    QString toString(Enum token);
}

/********************************************************************/
/*                                                                  */
/*  PropElementToken enum and methods                               */
/*                                                                  */
/********************************************************************/

// Enum used to identify element types when parsing XML properties file.

namespace PropElementToken
{
    enum Enum {Unknown,
               PropInfoDoc,
               PropInfo,
               Namespace,
               Name,
               IsArray,
               Severity,
               Compare,
               SimpleType,
               StructType,
               Type,
               Restrictions,
               ResultType,
               Restriction,
               Message,
               Operator,
               Value,
               AndOr,
               DLNQuery,
               ErrorMessage};

    Enum fromString(const QStringRef & str);
}

/********************************************************************/
/*                                                                  */
/*  XMPValueType enum and methods                                   */
/*                                                                  */
/********************************************************************/

// Enum used to discriminate among the union fields in XMPValue.

namespace XMPUnionType
{
    enum Enum {Simple,
               Array,
               Struct,
               Unknown};

    QString toString(Enum type);
    bool fromString(const QString & str, Enum & type);
}

/********************************************************************/
/*                                                                  */
/*  XMPType enum and methods                                        */
/*                                                                  */
/********************************************************************/

// Enum used to describe the data type of a property value.

namespace XMPType
{
    enum Enum {Boolean,
               Integer,
               Rational,
               Real,
               Text,
               Datetime,
               Unknown};

    QString toString(Enum type);
    bool fromString(const QString & str, Enum & type);
}

#endif // ENUMS_H
