/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

#ifndef OPTIONS_H
#define OPTIONS_H

// Local headers

#include "dbutils.h"
#include "ienums.h"

// Qt headers

#include <QSqlError>

/********************************************************************/
/*                                                                  */
/*  Options struct                                                  */
/*                                                                  */
/********************************************************************/

struct Options
{
    // Structure to hold processing options. These can be set through
    // the command line or the Options dialog.

    // Variables

    bool useDatabase;
    bool logging;
    bool stopOnError;
    int writeToDLN;
    bool removePreviousResults;
    int epsilon; // Allowable difference between two datetimes, in seconds

    Options() :
        useDatabase(true),
        logging(false),
        stopOnError(false),
        writeToDLN(IStatus::Success | IStatus::Warning | IStatus::Error),
        removePreviousResults(false),
        epsilon(60*60)
    {

    }

    ~Options()
    {

    }
};

/********************************************************************/
/*                                                                  */
/*  OptionsFactory class                                            */
/*                                                                  */
/********************************************************************/

class OptionsFactory : protected DBUtils
{
private:
    InspectorType::Enum inspectorType;
public:
    OptionsFactory(InspectorType::Enum type):
        inspectorType(type)
    {}
    Options fromOptionsDB();
    void toOptionsDB(const Options &options);

private:
    Options getExistingOptions(QString optionsPath);
    Options getNewOptions(QString optionsPath);
    QString getOptionsDBPath();
    static int intFromBool(bool b);
};

#endif // OPTIONS_H

