/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

#ifndef RESULTS_H
#define RESULTS_H

// Local headers

#include "dbutils.h"
#include "ienums.h"
#include "iexception.h"
#include "imageset.h"
#include "iresult.h"
#include "propertyinfo.h"
#include "xmpvalue.h"

// Qt headers

#include <QDateTime>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QList>
#include <QString>
#include <QTextStream>
#include <QXmlStreamWriter>
#include <QHash>

/********************************************************************/
/*                                                                  */
/*  IResultDiff class                                               */
/*                                                                  */
/********************************************************************/

// IResultDiff is a simple class that determines the field
// in which one result differs from the previous results. It
// assumes that the results it is passed have been sorted
// in ascending order.

class IResultDiff
{
public:
    IResultDiff();
    ~IResultDiff();

    void initialize(const IResult & result);
    int getBreakField(const IResult & result);

private:
    IResult savedResult;
};

/********************************************************************/
/*                                                                  */
/*  Results class                                                   */
/*                                                                  */
/********************************************************************/

// Results is used by the Inspector to store results. It is
// also the base class for ResultsDialog, which displays
// results in a dialog.

class Results : protected DBUtils
{
public:
    Results();
    virtual ~Results();

    // Public methods

    // Getters and setters

    virtual void setInspectionTime(const QDateTime & inspectionTime);
    QDateTime getInspectionTime();
    virtual void setProject(const QString & projectDirectory);
    QString getProject();

    virtual void increaseImageCount(const QString& imageSetName, int increment);
    int getTotalImageCount();

    // Manage results

    virtual void clear();
//    void addResults(const QList<IResult> &results);
    void addResult(const IResult & result);

    // Read and write results

    void readResults(const QString &filename);
    void readResults(QTextStream & stream, QString filename);
    void writeResults(const QString &filename);
    void writeResults(QTextStream & stream, const QList<IResult>& results);
    void writeHTMLFile(const QString & filename);

    // Update DLN database

    void updateDatabase(const QList<ImageSet> &imageSets, const PropInfoLists * propInfoLists, bool removeResults, bool writeResults);

protected:

    // Member variables

    QString projectDirectory;
    QDateTime inspectionTime;
    QList<IResult> results;
    bool sorted;
    int totalImageCount; // the total number of images inspected
    QHash<QString, int> imageSetCount; // Number of images per ImageSet name

    // Sorting methods

    void sort();

private:
    // IO utility methods

    static void write(QFile & file, const QString & str);
    static void write(QFile & file, const char * ptr_to_char);
    static void write(QTextStream & stream, const QString & str);
    static void write(QTextStream & stream, char c);

    // Write results

    void writeResultHeader(QTextStream &stream);
    void writeResult(QTextStream & stream, const IResult & result);
    QString lineBreakToAtSign(const QString & str);
    QString atSignToLineBreak(const QString & str);

    // Utility methods to write the HTML report

    void writeHTML(QTextStream &stream, const QList<IResult>& results);
    void writeHTMLHeadings(QTextStream &stream);
    void writeCSSStyles(QTextStream &stream);
    void writeHTMLTOC(QTextStream &stream, const QList<IResult>& results);
    void writeHTMLRows(QTextStream &stream, const QList<IResult>& results);
    int getBreakField(const IResult & result);

    // Methods to update the DLN database

    // Method to delete inspections.

    void deleteResults(QSqlQuery & q, ImageSet &imageSet, bool writeResults);
    void deleteInspVsTestgroup(QSqlQuery & q, int imageSetSrl);
    void deleteInspVsFilelist(QSqlQuery & q, int imageSetSrl);
    void deleteInspectionResults(QSqlQuery & q, int imageSetSrl);
    void deleteFilelistsAndPaths(QSqlQuery & q, const QString & hash, const QString & filelistType, bool writeResults);
    void deleteFilepaths(QSqlQuery & q, const QString & hash, const QString & filelistType, bool writeResults);
    void deleteFilelists(QSqlQuery & q, const QString & hash, const QString & filelistType, bool writeResults);
    void deleteInspections(QSqlQuery & q, int imageSetSrl);

    // Methods to add inspections.

    int addInspection(QSqlQuery & q, int imageSetSrl, int docSrl);
    void addInspVsTestgroups(QSqlQuery & q, int inspSrl, const QHash<QString, int> &testgroupSrlHash);
    void addInspVsFilelists(QSqlQuery & q, int inspSrl, ImageSet &imageSet, QHash<QString, int> &fileSrlsHash);
    void addInspVsFilelist(QSqlQuery & q, int inspSrl, const QList<QFileInfo> &fileInfos, const QHash<QFileInfo, ImageSet::DocumentIds> &docIds, int fileType, const QString & filelistType, QHash<QString, int> &fileSrlsHash);
    void getFilepathSrls(QSqlQuery & q, QHash<QString, int> & fileSrlsHash, int filelistSrl);
    void addInspectionResults(QSqlQuery & q, int inspSrl, const QList<IResult> & resultGroup, const QHash<QString, int> & testgroupSrlsHash, const QHash<QString, int> & propSrlsHash, const QHash<QString, int> &fileSrlsHash);

    // Add the HTML report to the Database:
    int addInspectionDoc(QSqlQuery& q, int imageSetSrl, const QString& htmlDoc);

    // Utility methods

    QList<QList<IResult>> groupResults(const QList<ImageSet> imageSets);
    int getResultGroup(int imageSetNum, int resultIndexStart, QList<IResult> & resultGroup);
    void addDLNResult(bool removeResults, bool writeResults);
};

#endif // RESULTS_H
