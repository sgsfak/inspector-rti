/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

// Local headers

#include "definesetdialog.h"
#include "iexception.h"
#include "iglobals.h"
#include "setmanager.h"

// Qt headers

#include <QCryptographicHash>
#include <QFileDialog>
#include <QLabel>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QTextStream>

#include <QDebug>

// C++ headers

#include <algorithm>

/********************************************************************/
/*                                                                  */
/*  Public methods                                                  */
/*                                                                  */
/********************************************************************/

/********************************************************************/
/*                                                                  */
/*  Constructor and destructor                                      */
/*                                                                  */
/********************************************************************/

// Note that SetManager works directly on the model and
// hash table that are passed to it. One side-effect of
// this is that the main list of image sets is updated
// at the same time as the list of image sets displayed
// by the SetManager.

SetManager::SetManager(QListView * setsList, QStringListModel *& imageSetsModel, QHash<QString, ImageSet> & imageSetsHash, QWidget *parent) :
    QFrame(parent),
    setsList(setsList),
    imageSetsModel(imageSetsModel),
    imageSetsHash(imageSetsHash)
{
    initGUI();
    setConnections();
}

SetManager::~SetManager()
{
}

/********************************************************************/
/*                                                                  */
/*  Method overrides                                                */
/*                                                                  */
/********************************************************************/

void SetManager::showEvent(QShowEvent* e)
{
    if (!usingDB)
    {
        QFrame::showEvent(e);
        addButton->setFocus();
    }
}

/********************************************************************/
/*                                                                  */
/*  Slots                                                           */
/*                                                                  */
/********************************************************************/

void SetManager::addImageSet()
{
    // Create a dialog to define an image set.

    DefineSetDialog defineDialog(this);
    bool okClicked = defineDialog.exec();

    // If Cancel was clicked, return now.

    if (!okClicked) return;

    // Get the image set defined in the dialog and add
    // it to the sets model and hash table.

    ImageSet imageSet = defineDialog.getImageSet();
    if (imageSetsHash.contains(imageSet.name))
    {
        QMessageBox msg(QMessageBox::Warning,
                        imageSetTermIC + " set already added",
                        "The " + imageSetTermLC + " set " + imageSet.name + " has already been added. Please use a different name.",
                        QMessageBox::Ok);
        msg.exec();
    }
    else
    {
        imageSetsHash.insert(imageSet.name, imageSet);
        addRow(imageSetsModel, 0, QVariant(imageSet.name));
        imageSetsModel->sort(0);
    }
}

void SetManager::editImageSet()
{
    // Get the selected row(s).

    QStringList setNames = getSelectedData(setsList, imageSetsModel);

    // If more than one row or no rows were selected,
    // display an error and return.

    if (setNames.size() > 1)
    {
        QMessageBox::warning(this, "Multiple " + imageSetTermLC + " sets selected", "Select one " + imageSetTermLC + " set to edit.");
        return;
    }
    else if (setNames.size() == 0)
    {
        QMessageBox::warning(this, "No " + imageSetTermLC + " sets selected", "Select an " + imageSetTermLC + " set to edit.");
        return;
    }

    // Create a dialog to edit the image set and
    // pass in the selected image set.

    QString name = setNames[0];
    DefineSetDialog defineDialog(this);
    defineDialog.setImageSet(imageSetsHash.value(name));
    bool okClicked = defineDialog.exec();

    // If Cancel was clicked, return now.

    if (!okClicked) return;

    // Update the hash table of image sets. Note that QHash.insert()
    // replaces the value when the key already exists.

    ImageSet imageSet = defineDialog.getImageSet();
    imageSetsHash.insert(name, imageSet);

    // If we are using the database, update it now with the
    // lists of files in the image set.

    if (usingDB)
    {
        updateDatabase(imageSet);
    }
}

void SetManager::removeImageSets()
{
    // Get the list of selected image sets and check
    // that one or more sets was selected

    QStringList removedSets = getSelectedData(setsList, imageSetsModel);
    if (removedSets.size() == 0)
    {
       QMessageBox::warning(this, "No " + imageSetTermLC + " sets selected", "Select one or more " + imageSetTermLC + " sets to remove.");
       return;
    }

    // Remove the selected image sets.

    for (int i = 0; i < removedSets.size(); i++)
    {
        imageSetsHash.remove(removedSets[i]);
    }

    // Remove the selected rows from the sets model.

    removeSelectedRows(setsList, imageSetsModel);
}

void SetManager::saveImageSets()
{
    // If there are no image sets to save, just return.

    if (imageSetsModel->rowCount() == 0)
    {
        QMessageBox::warning(this, "No " + imageSetTermLC + " sets", "No " + imageSetTermLC + " sets to save.");
        return;
    }

    // Get the list of selected image sets. If no sets are selected,
    // get all image sets.

    QStringList setsToSave = getSelectedData(setsList, imageSetsModel);
    if (setsToSave.size() == 0)
    {
        setsToSave = getAllData(imageSetsModel);
    }

    // Display a dialog to get the name of the file
    // to which to save the image sets.

    QString saveFilepath = QFileDialog::getSaveFileName(this, "Save " + imageSetTermLC + " sets", "", imageSetTermIC + " sets (*.sets)");

    // If a name was entered, open the file and save the
    // image sets.

    if (!saveFilepath.isEmpty())
    {
        QFile setsFile(saveFilepath);

        try
        {
            if (!setsFile.open(QIODevice::WriteOnly))
            {
                throw IOException("File open error", "Error opening file:\n\nFile: " + saveFilepath + "\n\nError: " + setsFile.errorString());
            }

            // For each row to be saved, get the ImageSet
            // for that row and save it.

            for (int i = 0; i < setsToSave.size(); i++)
            {
                ImageSet imageSet = imageSetsHash.value(setsToSave[i]);
                saveImageSet(setsFile, imageSet);
            }

            // Close the file.

            setsFile.close();
        }
        catch (IOException e)
        {
            // If an error occurred, display the error and
            // close the file.

            QMessageBox::warning(this, e.title, e.msg);
            setsFile.close();
            return;
        }
    }
}

void SetManager::loadImageSets()
{
    // If the list already contains image sets, warn the user that
    // loading new image sets will replace the existing image sets.

    if (imageSetsModel->rowCount() != 0)
    {
        QMessageBox::StandardButton reply = QMessageBox::question(this, "Replace image sets?", "WARNING! Loading image sets from a file will replace the current list of image sets. Continue?", QMessageBox::Yes | QMessageBox::No, QMessageBox::No);
        if (reply == QMessageBox::No) return;
    }

    // Display a dialog to get the name of a file containing
    // image set definitions.

    QString loadFilepath = QFileDialog::getOpenFileName(this, "Load " + imageSetTermLC + " sets", "", imageSetTermIC + " sets (*.sets)");

    // If the path is empty, the user cancelled, so just return.

    if (loadFilepath.isEmpty()) return;

    // Remove the existing image sets. We do this because it is
    // not clear what to do when we load an image set whose name
    // matches the name of an image set already in the list.

    clearModel(imageSetsModel);

    // Open the file.

    QFile setsFile(loadFilepath);
    if (!setsFile.open(QIODevice::ReadOnly))
    {
        throw IOException("File open error", "Error opening file:\n\nFile: " + loadFilepath + "\n\nError: " + setsFile.errorString());
    }

    // Open a text stream over the file. We use a text stream because
    // it allows lines to be any length. If we read lines directly
    // from the QFile, we need to allocate a buffer in which to read
    // each line. If the line is longer than this, we need to join data
    // from multiple buffers, which is a pain.

    QTextStream stream(&setsFile);
    QString line;

    // Read and process each line from the file.

    while (!(line = stream.readLine(0)).isEmpty())
    {
        // Split the line using '*' as a delimiter.

        QStringList fields = line.split("*");

        // Allocate an ImageSet object.

        ImageSet imageSet;

        // Set the name and image type.

        imageSet.name = fields[0];
        imageSet.processedType = IFileType::fromString(fields[1]);

        // Read file paths until we reach an empty field. For
        // each file path, create a QFileInfo object. These
        // are the paths of the processed files.

        int i = 2;
        while (!fields[i].isEmpty())
        {
            imageSet.processedFileInfos.append(QFileInfo(fields[i]));
            i++;
        }

        // Increment the index to skip the empty field, then read
        // file paths until we reach another empty field. For each
        // file path, create a QFileObject. These are the paths of
        // the archival files.

        i++;
        while (!fields[i].isEmpty())
        {
            imageSet.archivalFileInfos.append(QFileInfo(fields[i]));
            i++;
        }

        // Add the ImageSet object to the hash table and
        // its name to the sets model.

        imageSetsHash.insert(imageSet.name, imageSet);
        addRow(imageSetsModel, 0, QVariant(imageSet.name));
    }

    // Close the file.

    setsFile.close();

    // Sort the sets model.

    imageSetsModel->sort(0);
}

/********************************************************************/
/*                                                                  */
/*  Private methods                                                 */
/*                                                                  */
/********************************************************************/

/********************************************************************/
/*                                                                  */
/*  Methods to initialize the GUI                                   */
/*                                                                  */
/********************************************************************/

void SetManager::initGUI()
{
    // Lay out the buttons to manage the image sets.

    QVBoxLayout * manageButtonsLayout = initManageButtonsLayout();

    // Set the frame's layout.

    setLayout(manageButtonsLayout);
}

QVBoxLayout * SetManager::initManageButtonsLayout()
{
    // Create the buttons.

    editButton = new QPushButton("Edit...");
    editButton->setToolTip("Edit the selected " + imageSetTermLC + " set.");

    if (!usingDB)
    {
        addButton = new QPushButton("Add...");
        addButton->setToolTip("Add a new " + imageSetTermLC + " to the list of " + imageSetTermLC + " sets.");
        removeButton = new QPushButton("Remove");
        removeButton->setToolTip("Remove the selected " + imageSetTermLC + " sets.");
        saveButton = new QPushButton("Save...");
        saveButton->setToolTip("Save the selected " + imageSetTermLC + " sets to a file. If no " + imageSetTermLC + " sets are selected, save all " + imageSetTermLC + " sets to a file.");
        loadButton = new QPushButton("Load...");
        loadButton->setToolTip("Load " + imageSetTermLC + " sets from a file.");
    }

    // Add the buttons to a layout.

    QVBoxLayout * buttonLayout = new QVBoxLayout();
    if (!usingDB) buttonLayout->addWidget(addButton);
    buttonLayout->addWidget(editButton);
    if (!usingDB)
    {
        buttonLayout->addWidget(removeButton);
        buttonLayout->addWidget(saveButton);
        buttonLayout->addWidget(loadButton);
    }
    buttonLayout->addStretch();

    // Return the button layout.

    return buttonLayout;
}

void SetManager::setConnections()
{
    // Call methods to manage image sets. Note that if we are using the
    // database, only the edit button is available. This is because image
    // sets are added and removed with the DLNCC tool and saved to and
    // loaded from the database automatically.

    connect(editButton, SIGNAL(clicked()), this, SLOT(editImageSet()));
    if (!usingDB)
    {
        connect(addButton, SIGNAL(clicked()), this, SLOT(addImageSet()));
        connect(removeButton, SIGNAL(clicked()), this, SLOT(removeImageSets()));
        connect(saveButton, SIGNAL(clicked()), this, SLOT(saveImageSets()));
        connect(loadButton, SIGNAL(clicked()), this, SLOT(loadImageSets()));
    }
}

/********************************************************************/
/*                                                                  */
/*  Utility methods                                                 */
/*                                                                  */
/********************************************************************/

void SetManager::saveImageSet(QFile & file, ImageSet & imageSet)
{
    // Write the set name and image type
    // to the file, separated by asterisks (*).

    write(file, imageSet.name);
    write(file, "*");
    write(file, IFileType::toString(imageSet.processedType));
    write(file, "*");

    // For each processed file, write the file path to the file,
    // followed by an asterisk. After writing all the files,
    // write another asterisk. Thus, two asterisks (an empty
    // field) will separate the processed files from the archival files.

    for (int i = 0; i < imageSet.processedFileInfos.size(); i++)
    {
        write(file, imageSet.processedFileInfos[i].filePath());
        write(file, "*");
    }
    write(file, "*");

    // For each archival file, write the file path to the file,
    // followed by an asterisk. After writing all the files,
    // write another asterisk. Thus, two asterisks (an empty
    // field) will indicate the end of the archival files.

    for (int i = 0; i < imageSet.archivalFileInfos.size(); i++)
    {
        write(file, imageSet.archivalFileInfos[i].filePath());
        write(file, "*");
    }
    write(file, "*");

    // Write a line feed to end the image set.

    write(file, "\n");

    // Flush the data to the file.

    file.flush();
}

void SetManager::write(QFile & file, const QString & str)
{
    // Check the return from write() because QFile does
    // not (appear to) throw exceptions.

    if (file.write(str.toStdString().c_str()) == -1)
        throw IOException("Error writing file", "Error writing to the file:\n\nFile: " + file.fileName() + "\n\nError: " + file.errorString());
}

void SetManager::write(QFile & file, const char * ptr_to_char)
{
    // Check the return from write() because QFile does
    // not (appear to) throw exceptions.

    if (file.write(ptr_to_char) == -1)
        throw IOException("Error writing file", "Error writing to the file:\n\nFile: " + file.fileName() + "\n\nError: " + file.errorString());
}

/********************************************************************/
/*                                                                  */
/*  Database methods                                                */
/*                                                                  */
/********************************************************************/

void SetManager::updateDatabase(ImageSet & imageSet)
{
    // Open a connection to the database and start a transaction.

    QSqlDatabase db = QSqlDatabase::database(dlnDBName);
    startTransaction(db);
    QSqlQuery q(db);

    // Update the lists of processed and archival files.

    updateDatabase(q, imageSet.srl, imageSet.archivalFileInfos, imageSet.docIds, imageSet.archivalType, "a");
    updateDatabase(q, imageSet.srl, imageSet.processedFileInfos, imageSet.docIds, imageSet.processedType, "p");

    // Commit the transaction.

    commitTransaction(db);
}

void SetManager::updateDatabase(QSqlQuery & q, int imageSetSrl, const QList<QFileInfo> & fileInfos, const QHash<QFileInfo, ImageSet::DocumentIds>& docIds, IFileType::Enum fileType, const QString &filelistType)
{
    // Get the hash of the files in the list.

    QString hash = getHash(fileInfos);

    // Check if this image set already has a list of (archival or
    // processed) files. There are three possibilities:
    //
    // a) No filelist exists. Add the new filelist.
    //
    // b) A filelist exists and its hash matches the new hash. This
    //    occurs when the user doesn't change a filelist and clicks
    //    OK. For example, they change the processed filelist but
    //    not the archival filelist. In this case, we can continue to
    //    use the existing filelist and association row. Just return.
    //
    // c) A filelist exists and its hash is different from the new hash.
    //    Possibly delete the old filelist and add the new filelist.

    QString existingHash;
    int vsSrl(-1);
    int filelistSrl(-1);
    if (getFilelistForOther(q, "cptset_vs_filelist", "cptset_srl", imageSetSrl, filelistType, existingHash, vsSrl, filelistSrl))
    {
        // Case (b)

        if (hash == existingHash) return;

        // Case (c)

        // Remove the association between the filelist and
        // the image set, then get the reference count for
        // the filelist. If no one is using it, delete it.

        deleteFilelistVsOther(q, "cptset_vs_filelist", vsSrl);
        if (getFilelistRefCount(q, filelistSrl) == 0)
        {
            deleteFilelist(q, filelistSrl);
        }
    }

    // Cases (a) and (c) -- add the new filelist.

    if (fileInfos.size() != 0)
    {
        // Check if there is a filelist with this hash and type (unlikely).

        int filelistSrl = getFilelistSrl(q, hash, filelistType);

        // If there is not filelist, add one.

        if (filelistSrl == -1)
        {
            filelistSrl = addFilelist(q, hash, fileInfos, docIds, fileType, filelistType);
        }

        // Associate the filelist and the image set.

        addFilelistVsOther(q, "cptset_vs_filelist", "cptset_srl", imageSetSrl, filelistSrl);
    }
}
