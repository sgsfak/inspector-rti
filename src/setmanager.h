/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

#ifndef SETMANAGER_H
#define SETMANAGER_H

// Local headers

#include "dbutils.h"
#include "imageset.h"
#include "modelutils.h"

// Qt headers

#include <QFrame>
#include <QHBoxLayout>
#include <QPushButton>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QVBoxLayout>

class SetManager : public QFrame, protected ModelUtils, protected DBUtils
{
    Q_OBJECT
public:
    SetManager(QListView * setsList, QStringListModel *& imageSetsModel, QHash<QString, ImageSet> & imageSetsHash, QWidget *parent = 0);
    ~SetManager() override;

    // Method overrides

    void showEvent(QShowEvent* e) override;

public slots:
    void addImageSet();
    void editImageSet();
    void removeImageSets();
    void saveImageSets();
    void loadImageSets();

signals:

private:

    // GUI member variables

    QPushButton * addButton;
    QPushButton * editButton;
    QPushButton * removeButton;
    QPushButton * saveButton;
    QPushButton * loadButton;
    QListView * setsList;

    // Non-GUI member variables

    QStringListModel *& imageSetsModel;
    QHash<QString, ImageSet> & imageSetsHash;

    // GUI initialization methods

    void initGUI();
    QVBoxLayout * initListLayout(QVBoxLayout *manageButtonsLayout);
    QVBoxLayout * initManageButtonsLayout();
    QHBoxLayout * initOKButtonsLayout();

    // Connect slots

    void setConnections();

    // Utility methods

    void saveImageSet(QFile & file, ImageSet & imageSet);
    void write(QFile & file, const QString & str);
    void write(QFile & file, const char * ptr_to_char);

    // Database methods

    void updateDatabase(ImageSet & imageSet);
    void updateDatabase(QSqlQuery &q, int imageSetSrl, const QList<QFileInfo> &fileInfos, const QHash<QFileInfo, ImageSet::DocumentIds> &docIds, IFileType::Enum fileType, const QString &filelistType);
};

#endif // SETMANAGER_H
