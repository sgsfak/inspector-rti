/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2017                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

#ifndef IGLOBALS_H
#define IGLOBALS_H

// There are two executables for the DLN:Inspector, one for RTI files
// and one for photogrammetry. Each executable has its own .pro file and
// the only difference between these files is whether the RTI or PHOTO
// #define is set. These #defines are used to set version-specific values
// for the following global variables in main.cpp. The global variables
// are used at run time to determine which version is being run, as there
// are small processing and UI differences between the versions.

// iglobals.h is included in other files as needed.

// Local headers

#include "ienums.h"

// Qt headers

#include <QString>

// Global constants

extern const QString inspectorVersionNumber;
extern bool usingDB;

// Global variables

extern InspectorType::Enum globalInspectorType;
extern QString dlnDBName;

extern QString projectTermIC;
extern QString projectTermLC;
extern QString projectTermUC;
extern QString imageSetTermIC;
extern QString imageSetTermLC;
extern QString imageSetTermUC;

#endif // IGLOBALS_H
