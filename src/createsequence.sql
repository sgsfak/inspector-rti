CREATE SEQUENCE public.cptset_vs_filelist_srl_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.cptset_vs_filelist_srl_seq
  OWNER TO postgres;

CREATE SEQUENCE public.filelist_srl_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.filelist_srl_seq
  OWNER TO postgres;

CREATE SEQUENCE public.filepath_srl_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.filepath_srl_seq
  OWNER TO postgres;

CREATE SEQUENCE public.inspection_srl_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.inspection_srl_seq
  OWNER TO postgres;

CREATE SEQUENCE public.inspection_vs_filelist_srl_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.inspection_vs_filelist_srl_seq
  OWNER TO postgres;

CREATE SEQUENCE public.inspection_vs_testgroup_srl_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.inspection_vs_testgroup_srl_seq
  OWNER TO postgres;

CREATE SEQUENCE public.inspresult_srl_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.inspresult_srl_seq
  OWNER TO postgres;

CREATE SEQUENCE public.property_srl_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.property_srl_seq
  OWNER TO postgres;

CREATE SEQUENCE public.propertygroup_srl_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.propertygroup_srl_seq
  OWNER TO postgres;

CREATE SEQUENCE public.testgroup_srl_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.testgroup_srl_seq
  OWNER TO postgres;