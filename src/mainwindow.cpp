/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

// Local headers

#include "iexception.h"
#include "iglobals.h"
#include "mainwindow.h"
#include "resultbrowser.h"

// Qt headers

#include <QGuiApplication>
#include <QCoreApplication>
#include <QDesktopServices>
#include <QFileDialog>
#include <QFont>
#include <QLabel>
#include <QMessageBox>
#include <QPushButton>
#include <QStringList>

#include <QSqlDatabase>
#include <QSqlQuery>

#include <QUdpSocket>
#include <QNetworkDatagram>
#include <QBuffer>

#include <QDebug>

/********************************************************************/
/*                                                                  */
/*  Constructor and destructor                                      */
/*                                                                  */
/********************************************************************/

MainWindow::MainWindow(InspectorType::Enum type, PropInfoLists *propInfoLists, Options &options, const QList<DirPattern> & dirPatterns, bool isStandalone, QWidget *parent) :
    QWidget(parent),
    inspectorType(type),
    isStandalone_(isStandalone),
    selected_project_srl(0),
    selected_group_srl(0),
    propInfoLists(propInfoLists),
    options(options)
{
    initGUI(dirPatterns);
    // bindUdpComm();
}

MainWindow::~MainWindow()
{
    // We do not explicitly delete the QWidgets allocated in this class.
    // This is because Qt parents automatically delete their children.
}

/********************************************************************/
/*                                                                  */
/*  Slots                                                           */
/*                                                                  */
/********************************************************************/

bool MainWindow::bindUdpComm()
{
    // Open a UDP port to accept requests from DLN-CC
    QUdpSocket* udpSocket = new QUdpSocket(this);
    const uint PORT = 5240; // IANA unassigned, see
    // https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml
    bool bound = udpSocket->bind(QHostAddress::LocalHost, PORT);
    if (!bound) {
        qDebug() << "Could not bind";
        return false;
    }
    qDebug() << "Now listening on UDP " << PORT;
    auto conn = QObject::connect(udpSocket, &QUdpSocket::readyRead,
                                 [udpSocket, this]() {
        QHostAddress sender;
        quint16 senderPort;
        QByteArray buf(udpSocket->pendingDatagramSize(), Qt::Uninitialized);
        // See documentation of QtNetworkDatagram at
        // https://doc.qt.io/qt-5/qnetworkdatagram.html
        QDataStream stream(&buf, QIODevice::ReadOnly);
        stream.setVersion(QDataStream::Qt_5_5);
        udpSocket->readDatagram(buf.data(), buf.size(), &sender, &senderPort);
        qDebug() << "Someone connected! Pending bytes:"<< buf.size();
        quint32 project_srl = 0;
        quint32 session_srl = 0;
        quint32 set_srl = 0;
        quint32 session_type = 0;
        stream >> project_srl >> session_srl >> set_srl >> session_type;
        if (stream.status() != QDataStream::Ok)
            return;
        qDebug() << "Received for inspection: "
                 << project_srl << session_srl << set_srl << session_type;

        QByteArray outBuf;
        QDataStream outStream(&outBuf, QIODevice::WriteOnly);
        outStream.setVersion(QDataStream::Qt_5_5);

        quint32 response = 200;
        outStream << response;
        udpSocket->writeDatagram(outBuf.data(), outBuf.size(), sender, senderPort);

        InspectorType::Enum inspectorType;
        switch (session_type) {
        case InspectorType::Enum::Photogrammetry:
            inspectorType = InspectorType::Enum::Photogrammetry; break;
        case InspectorType::Enum::Documentary:
            inspectorType = InspectorType::Enum::Documentary; break;
        case InspectorType::Enum::MultiSpectral:
            inspectorType = InspectorType::Enum::MultiSpectral; break;
        default:
            inspectorType = InspectorType::Enum::RTI; break;
        }
        QString basePath;
        QList<ImageSet> imageSets = SetChooser::getImageSetsByDB(static_cast<int>(project_srl),
                                                                 static_cast<int>(session_srl),
                                                                 basePath);

        removeEmptySets(imageSets);
        if (imageSets.size() == 0)
            return;
        inspector.inspect(propInfoLists, &resultsDialog, options,
                          basePath, imageSets);
    });
    return true;
}

void MainWindow::inspect()
{
    // Inspect the selected image set(s).

    QList<ImageSet> imageSets = setChooser->getSelectedImageSets();
    if (imageSets.size() == 0)
    {
        QMessageBox msg(QMessageBox::Warning,
                        "No " + imageSetTermLC + " sets chosen",
                        "You must select one or more " + imageSetTermLC + " sets before you can inspect them.",
                        QMessageBox::Ok);
        msg.exec();
    }
    else
    {
        removeEmptySets(imageSets);
        if (imageSets.size() == 0) return;
        try
        {
            inspector.inspect(propInfoLists, &resultsDialog, options, setChooser->getProjectDirectory(), imageSets);
        }
        catch (IException iexception)
        {
            QMessageBox::warning(this, iexception.title, iexception.msg);
        }
    }
}

void MainWindow::displaySavedResults()
{
    if (usingDB)
    {
        displayResultsBrowser();
    }
    else
    {
        displayResultsFromFile();
    }
}

void MainWindow::displayOptions()
{
    // Display the Options dialog.

    OptionsDialog optionsDialog(this->isStandalone(), this->inspectorType, options, this);
    optionsDialog.exec();
}

void MainWindow::displayHelp()
{
    // Display help.

    QString currentDir = QDir::currentPath();
    QString path = "file:///" + currentDir + "/manual.pdf";
    QDesktopServices::openUrl(QUrl(path));

#if defined(Q_OS_OSX)
    QString appDir = QCoreApplication::applicationDirPath() + "/../Resources";
#else
    QString appDir = QCoreApplication::applicationDirPath();
#endif
    QString filePath = appDir + "/manual.pdf";

    if (!QFileInfo::exists(filePath)) {
        QMessageBox::information(this, QCoreApplication::applicationName(),
                                 "PDF manual not found in application's folder!");
        return;
    }

    QUrl fileUrl = QUrl::fromLocalFile(filePath);
    QDesktopServices::openUrl(fileUrl);
}

/********************************************************************/
/*                                                                  */
/*  Methods to initialize the GUI                                   */
/*                                                                  */
/********************************************************************/

void MainWindow::initGUI(const QList<DirPattern> &dirPatterns)
{
    // Set the window title

    setWindowTitle("DLN:Inspector for " + InspectorType::toString(this->inspectorType) + ", Version " + inspectorVersionNumber);

    // Set the minimum dialog size.

    setMinimumSize(800, 500);

    // Create the title layout, the image set controls and list, and the button frame.

    QHBoxLayout * titleLayout = initTitleLayout();
    setChooser = new SetChooser(this->inspectorType, dirPatterns, isStandalone());
    QVBoxLayout * buttonsLayout = initButtonsLayout();

    // Place the image set chooser and the buttons side-by-side in a
    // horizontal box layout. This will form the main part of the dialog.

    QHBoxLayout * hLayout = new QHBoxLayout();
    hLayout->addSpacing(10);
    hLayout->addWidget(setChooser);
    hLayout->addSpacing(20);
    hLayout->addLayout(buttonsLayout);
    hLayout->addSpacing(10);

    // Place the title and the horizontal layout in a vertical box layout.
    // This will center the title over the image set controls and the buttons.

    QVBoxLayout * vLayout = new QVBoxLayout();
    vLayout->addLayout(titleLayout);
    vLayout->addLayout(hLayout);
    vLayout->addSpacing(10);

    // Set the dialog's layout.

    setLayout(vLayout);
}

QHBoxLayout * MainWindow::initTitleLayout()
{
    // Create the label and set the font.

    QFont font(QGuiApplication::font().family(), 20, QFont::Bold);
    QLabel * title = new QLabel("DLN:Inspector for "+ InspectorType::toString(this->inspectorType));
    title->setFont(font);

    // Create the title layout.

    QHBoxLayout * titleLayout = new QHBoxLayout();
    titleLayout->addStretch();
    titleLayout->addWidget(title);
    titleLayout->addStretch();

    // Return the layout

    return titleLayout;
}

QVBoxLayout * MainWindow::initButtonsLayout()
{
    // Allocate the buttons.

    QPushButton * inspectButton = new QPushButton("Inspect...");
    inspectButton->setToolTip("Inspect the selected " + imageSetTermLC + " sets");
    QPushButton * resultsButton = new QPushButton("Results...");
    QString tip = usingDB ? "View results stored in DLN" : "View results saved to a file";
    resultsButton->setToolTip(tip);
    QPushButton * optionsButton = new QPushButton("Options...");
    optionsButton->setToolTip("Set inspection options");
    QPushButton * helpButton = new QPushButton("Help...");
    helpButton->setToolTip("Get help");
    QPushButton * exitButton = new QPushButton(isStandalone() ? "Exit" : "Close");
    exitButton->setToolTip("Exit the DLN:Inspector");

    // Connect the buttons to slots

    connect(inspectButton, SIGNAL(clicked()), this, SLOT(inspect()));
    connect(resultsButton, SIGNAL(clicked()), this, SLOT(displaySavedResults()));
    connect(helpButton, SIGNAL(clicked()), this, SLOT(displayHelp()));
    connect(optionsButton, SIGNAL(clicked()), this, SLOT(displayOptions()));
    connect(exitButton, SIGNAL(clicked()), this, SLOT(close()));

    // Lay out the buttons vertically, grouping them with spaces
    // into the groups {Inspect}, {Results, Options, Help}, {Exit}.

    QVBoxLayout * buttonsLayout = new QVBoxLayout();
    buttonsLayout->addStretch();
    buttonsLayout->addWidget(inspectButton);
    buttonsLayout->addStretch();
    buttonsLayout->addWidget(resultsButton);
    buttonsLayout->addWidget(optionsButton);
    buttonsLayout->addWidget(helpButton);
    buttonsLayout->addStretch();
    buttonsLayout->addWidget(exitButton);
    buttonsLayout->addStretch();

    // Return the buttons layout.

    return buttonsLayout;
}

/********************************************************************/
/*                                                                  */
/*  Methods to support slots                                        */
/*                                                                  */
/********************************************************************/

void MainWindow::displayResultsBrowser()
{
    ResultBrowser browser(this->inspectorType);
    if (this->selected_project_srl && this->selected_group_srl)
        browser.selectBundle(this->selected_project_srl, this->selected_group_srl);
    browser.exec();
}

void MainWindow::displayResultsFromFile()
{
    // Retrieve results from a file and display them.

    QString startDir = setChooser->getProjectDirectory();
    if (startDir != "Custom") startDir.clear();
    QString selectedFilter("Result files (*.txt)");
    QString filename = QFileDialog::getOpenFileName(this, "Open Results", startDir, "Result files (*.txt);;All files (*.*)", &selectedFilter);
    if (filename.isEmpty()) return;

    // Read the results from the file.

    try
    {
        resultsDialog.readResults(filename);
    }
    catch (IException iexception)
    {
        // If there is an exception, display it to the user.

        QMessageBox::warning(this, iexception.title, iexception.msg);
    }

    // Display the results.

    resultsDialog.showDialog();
}

void MainWindow::removeEmptySets(QList<ImageSet> & imageSets)
{
    // Loop backward through the image sets and remove
    // any that are empty.

    for (int i = imageSets.size() - 1; i >= 0; i--)
    {
        if (isSetEmpty(imageSets[i])) imageSets.removeAt(i);
    }
}

bool MainWindow::isSetEmpty(const ImageSet & imageSet)
{
    // Check if the image set contains any processed files.

    if (imageSet.processedFileInfos.size() == 0 && imageSet.archivalFileInfos.size() == 0)
    {
        QString text("The " + imageSetTermLC + " set " + imageSet.name + " does not contain any processed image files to inspect.");
        QMessageBox msg(QMessageBox::Warning,
                        imageSetTermIC + " directory is empty",
                        text,
                        QMessageBox::Ok);
        msg.exec();

        return true;
    }

    return false;
}

void MainWindow::selectBundle(int project_srl, int bundle_srl)
{
    this->selected_project_srl = project_srl;
    this->selected_group_srl = bundle_srl;

    this->setChooser->selectBundle(selected_project_srl, selected_group_srl);
}
