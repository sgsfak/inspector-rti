/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

// Local headers

#include "imageset.h"

// Qt headers

#include <QDebug>

/********************************************************************/
/*                                                                  */
/*  ImageSet class                                                  */
/*                                                                  */
/********************************************************************/

ImageSet::ImageSet() :
    srl(0),
    archivalType(IFileType::DNG)
{

}

ImageSet::ImageSet(int srl, QString name, QString archivalPath, QList<QFileInfo> archivalFileInfos, IFileType::Enum archivalType, QString processedPath, QList<QFileInfo> processedFileInfos, IFileType::Enum processedType) :
    srl(srl),
    name(name),
    archivalPath(archivalPath),
    archivalFileInfos(archivalFileInfos),
    archivalType(archivalType),
    processedPath(processedPath),
    processedFileInfos(processedFileInfos),
    processedType(processedType)
{
}

ImageSet::ImageSet(int srl, QString name, QString archivalPath, IFileType::Enum archivalType, QString processedPath, IFileType::Enum processedType) :
    srl(srl),
    name(name),
    archivalPath(archivalPath),
    archivalType(archivalType),
    processedPath(processedPath),
    processedType(processedType)
{
    getFileInfos(archivalPath, archivalFileInfos, archivalType);
    getFileInfos(processedPath, processedFileInfos, processedType);
}

ImageSet::~ImageSet()
{

}

void ImageSet::getFileInfos(const QString & path, QList<QFileInfo> & fileInfos, const IFileType::Enum imgType)
{
    // If the image type is unknown or the directory doesn't exist, just
    // return. This will result in an empty QFileInfo list.

    if (imgType == IFileType::UNKNOWN) return;
    if (path.isEmpty()) return;

    QDir dir(path);
    if (!dir.exists()) return;

    // Set the filters according to the file type.

    // Notice that the filename filters do not (appear to) consider case.
    // The evidence for this is that the function used to retrieve filenames
    // (QDir::entryList()) requires an explicit flag (QDir::CaseSensitive)
    // to do a case sensitive search.

    QStringList filenameFilters = IFileType::getExtensions(imgType);

    // Get the list of QFileInfos.

    fileInfos = dir.entryInfoList(filenameFilters, QDir::Files);
}

