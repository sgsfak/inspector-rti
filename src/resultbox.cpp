/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2017                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

// Local headers

#include "iglobals.h"
#include "resultbox.h"

// Qt headers

#include <QLabel>
#include <QPushButton>
#include <QTextEdit>
#include <QVBoxLayout>

/********************************************************************/
/*                                                                  */
/*  Constructor and destructor                                      */
/*                                                                  */
/********************************************************************/

ResultBox::ResultBox(const IResult & result, const QString & projectDirectory, QWidget *parent) :
    QDialog(parent),
    result(result),
    projectDirectory(projectDirectory)
{
    setWindowTitle(IResultType::toString(result.resultType) + ": " + result.title);
    setMinimumWidth(600);
    setMinimumHeight(400);
    initGUI();
}

ResultBox::~ResultBox()
{

}

/********************************************************************/
/*                                                                  */
/*  GUI initialization methods                                      */
/*                                                                  */
/********************************************************************/

void ResultBox::initGUI()
{
    // Create the layouts.

    QHBoxLayout * titleLayout = initTitle();
    QHBoxLayout * infoLayout = initInfoLayout();
    QHBoxLayout * descriptionLayout = initDescriptionLayout();
    QHBoxLayout * buttonLayout = initButtonLayout();

    // Create the main layout.

    QVBoxLayout * layout = new QVBoxLayout();
    layout->addLayout(titleLayout);
    layout->addSpacing(10);
    layout->addLayout(infoLayout);
    layout->addSpacing(20);
    layout->addLayout(descriptionLayout);
    layout->addLayout(buttonLayout);

    // Return the main layout.

    setLayout(layout);
}

QHBoxLayout * ResultBox::initTitle()
{
    // Create the label and set the font.

    QFont font("Times New Roman", 12);
    QLabel * title = new QLabel(IResultType::toString(result.resultType) + ": " + result.title);
    title->setFont(font);

    // Create the title layout.

    QHBoxLayout * titleLayout = new QHBoxLayout();
    titleLayout->addStretch();
    titleLayout->addWidget(title);
    titleLayout->addStretch();

    // Return the title layout

    return titleLayout;
}

QHBoxLayout * ResultBox::initInfoLayout()
{
    // Create the project directory and image set labels. Note that for
    // the Custom tab of the PG Inspector, the project directory is empty
    // because the files can be anywhere.

    QLabel * directoryLabel(nullptr);
    QLabel * directory(nullptr);
    if (!projectDirectory.isEmpty())
    {
        directoryLabel = new QLabel(projectTermIC + " directory:");
        directoryLabel->setToolTip(projectTermIC + " containing the " + imageSetTermLC + " set");
        directory = new QLabel(projectDirectory);
        directory->setToolTip(projectTermIC + " containing the " + imageSetTermLC + " set");
    }

    QLabel * imageSetLabel = new QLabel(imageSetTermIC + " set:");
    imageSetLabel->setToolTip(imageSetTermIC + " set to which the result applies");
    QLabel * imageSet = new QLabel(result.imageSetName);
    imageSet->setToolTip(imageSetTermIC + " set to which the result applies");

    // Lay out the information labels.

    QVBoxLayout * labelLayout = new QVBoxLayout();
    if (directoryLabel != nullptr) labelLayout->addWidget(directoryLabel);
    labelLayout->addWidget(imageSetLabel);

    QVBoxLayout * valueLayout = new QVBoxLayout();
    if (directory != nullptr) valueLayout->addWidget(directory);
    valueLayout->addWidget(imageSet);

    QHBoxLayout * infoLayout = new QHBoxLayout();
    infoLayout->addLayout(labelLayout);
    infoLayout->addLayout(valueLayout);
    infoLayout->addStretch();

    // Return the layout.

    return infoLayout;
}

QHBoxLayout * ResultBox::initDescriptionLayout()
{
    // Create the description label.

    QLabel * descriptionLabel = new QLabel("Description:");
    QTextEdit * htmlText = new QTextEdit();
    htmlText->setHtml(result.getMessage(true, false));
    htmlText->setReadOnly(true);
    htmlText->setToolTip("Description of the result");

    // Create the layout.

    QVBoxLayout * labelLayout = new QVBoxLayout();
    labelLayout->addWidget(descriptionLabel);
    labelLayout->addStretch();

    QHBoxLayout * descriptionLayout = new QHBoxLayout();
    descriptionLayout->addLayout(labelLayout);
    descriptionLayout->addWidget(htmlText);

    // Return the layout.

    return descriptionLayout;
}

QHBoxLayout * ResultBox::initButtonLayout()
{
    // Create the Close button.

    QPushButton * closeButton = new QPushButton("Close");
    connect(closeButton, SIGNAL(clicked()), this, SLOT(close()));

    // Lay out the Close button.

    QHBoxLayout * buttonLayout = new QHBoxLayout();
    buttonLayout->addStretch();
    buttonLayout->addWidget(closeButton);

    // Return the layout.

    return buttonLayout;
}

