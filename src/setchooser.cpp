/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

// Local headers

#include "definesetdialog.h"
#include "ienums.h"
#include "iexception.h"
#include "iglobals.h"
#include "setchooser.h"
#include "setmanager.h"

// Qt headers

#include <QFileDialog>
#include <QFileInfo>
#include <QFileInfoList>
#include <QGridLayout>
#include <QMessageBox>
#include <QRegExp>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>

#include <QDebug>

// C++ headers

#include <algorithm>
#include <vector>


/********************************************************************/
/*                                                                  */
/*  ClickableLabel class                                            */
/*                                                                  */
/********************************************************************/

ClickableLabel::ClickableLabel(QWidget* parent, Qt::WindowFlags)
    : QLabel(parent)
{

}

ClickableLabel::~ClickableLabel()
{

}

void ClickableLabel::mousePressEvent(QMouseEvent*)
{
    emit clicked();
}

/********************************************************************/
/*                                                                  */
/*  SetChooser class                                                */
/*                                                                  */
/********************************************************************/

/********************************************************************/
/*                                                                  */
/*  Public methods                                                  */
/*                                                                  */
/********************************************************************/

/********************************************************************/
/*                                                                  */
/*  Constructor and destructor                                      */
/*                                                                  */
/********************************************************************/

SetChooser::SetChooser(InspectorType::Enum type, const QList<DirPattern> &dirPatterns, bool withEditSet, QWidget *parent) :
    QGroupBox(parent),
    inspectorType(type),
    withEditSet(withEditSet),
    dirPatterns(dirPatterns),
    projectDirectory("")
{
    // Initialize the GUI and set the connections.

    initGUI();
    setConnections();
}

SetChooser::~SetChooser()
{
    // Delete the model used by the view class. Because multiple view classes
    // can share the same model, view classes do not delete model objects.
    // Instead, the programmer must delete model objects explicitly.

    delete imageSetsModel;
    imageSetsModel = nullptr;
}

/********************************************************************/
/*                                                                  */
/*  Accessor methods                                                */
/*                                                                  */
/********************************************************************/

QString SetChooser::getProjectDirectory()
{
    return projectDirectory;
}

QList<ImageSet> SetChooser::getSelectedImageSets()
{
    QList<ImageSet> imageSets;

#if 0
    // Get the names of the selected sets and retrieve the
    // corresponding ImageSets from the hash.

    QStringList selectedSets = getSelectedData(imageSetsListView, imageSetsModel);
#else
    // Based on Carla's comment, it's better to return all the image
    // sets contained in this Bundle:

    QStringList selectedSets = getAllData(imageSetsModel);
#endif

    for (int i = 0; i < selectedSets.size(); i++)
    {
        imageSets.append(imageSetsHash.value(selectedSets[i]));
    }

    // Return the list of selected sets.

    return imageSets;
}

/********************************************************************/
/*                                                                  */
/*  Method overrides                                                */
/*                                                                  */
/********************************************************************/

void SetChooser::showEvent(QShowEvent* e)
{
    if (!usingDB)
    {
        QGroupBox::showEvent(e);
        directoryEditBox->setFocus();
    }
}

/********************************************************************/
/*                                                                  */
/*  Slots                                                           */
/*                                                                  */
/********************************************************************/

/********************************************************************/
/*  Slots used without the database                                 */
/********************************************************************/

void SetChooser::chooseDirectory()
{
    // Display a directory chooser dialog and get the project/group directory.

    QString dirName = QFileDialog::getExistingDirectory(this,
                                                    projectTermIC + " Directory",
                                                    "",
                                                    QFileDialog::ShowDirsOnly);

    // If the filename is empty, the user canceled the dialog,
    // so just return.

    if (dirName.isEmpty()) return;

    // Get the selected pattern.

    DirPattern dirPattern = getSelectedPattern();

    // Update the list of image sets.

    getImageSetsByDir(dirName, dirPattern, IFileType::DNG);

    // Update the edit box.

    if (imageSetsModel->rowCount() == 0)
    {
        directoryEditBox->clear();
    }
    else
    {
        directoryEditBox->setText(dirName);
        projectDirectory = dirName;
    }
}

void SetChooser::checkDirectory()
{
    // This method is connected to the QLineEdit::editingFinished()
    // signal. A bug in this signal causes it to be fired twice. To
    // work around this, we use blockSignals() to ignore the second
    // call. For more information, see:
    //
    //    https://forum.qt.io/topic/39141/qlineedit-editingfinished-signal-is-emitted-twice/4
    //    https://stackoverflow.com/questions/26782211/qt-qlineedit-editingfinished-signal-twice-when-changing-focus
    //    https://bugreports.qt.io/browse/QTBUG-40

    directoryEditBox->blockSignals(true);

    // Retrieve the value from the project directory edit box and
    // check that it has changed.

    QString dirName = directoryEditBox->text();
    if (dirName != projectDirectory)
    {
        // Get the selected pattern.

        DirPattern dirPattern = getSelectedPattern();

        // Update the list of image sets.

        getImageSetsByDir(dirName, dirPattern, IFileType::DNG);

        // Update the edit box.

        if (imageSetsModel->rowCount() == 0)
        {
            directoryEditBox->clear();
        }
        else
        {
            directoryEditBox->setText(dirName);
            projectDirectory = dirName;
        }
    }

    // Unblock the signals.

    directoryEditBox->blockSignals(false);
}

void SetChooser::radioButtonClicked()
{
    int newRadioIndex = getSelectedRadioButton();
    if (newRadioIndex != currRadioIndex)
    {
        clearModels();
        directoryEditBox->clear();
        currRadioIndex = newRadioIndex;
    }
}

void SetChooser::exampleClicked()
{
    ClickableLabel * labelPtr = qobject_cast<ClickableLabel*>(sender());
    int i = examplesHash.value(labelPtr);
    QMessageBox::information(this, dirPatterns[i].summary + " example", dirPatterns[i].example);
}

/********************************************************************/
/*  Slots used with the database                                    */
/********************************************************************/

void SetChooser::projectChanged(int projectIndex)
{
    updateGroupComboBox(projectIndex, projectComboBox, groupComboBox);
}

void SetChooser::groupChanged(int)
{
    getImageSetsByDB(projectComboBox, groupComboBox);
}

/********************************************************************/
/*                                                                  */
/*  Private methods                                                 */
/*                                                                  */
/********************************************************************/

/********************************************************************/
/*                                                                  */
/*  Methods to initialize the GUI                                   */
/*                                                                  */
/********************************************************************/

void SetChooser::initGUI()
{
    // Note that the if/else blocks contain duplicate code, but
    // it's horribly confusing otherwise.

    QVBoxLayout * layout = new QVBoxLayout();
    if (this->inspectorType == InspectorType::RTI)
    {
        // In the RTI Inspector, the top of the dialog box
        // is either a frame with combo boxes for the project
        // and directory (usingDB) or a frame used to browse
        // for a directory (!usingDB).

        if (usingDB)
        {
            comboBoxFrame = initComboBoxFrame();
            layout->addWidget(comboBoxFrame);
        }
        else
        {
            browserFrame = initBrowserFrame();
            layout->addWidget(browserFrame);
        }

        // The bottom of the dialog box contains the
        // image sets list layout (lable and list).

        QLayout * listLayout = initListLayout();
        layout->addLayout(listLayout);
    }
    else
    {
        // In the Photogrammetry Inspector, the very top
        // of the dialog box has the toggle button (!usingDB).
        // The next level is the combo box frame (usingDB)
        // or the browse frame (!usingDB).

        if (usingDB)
        {
            comboBoxFrame = initComboBoxFrame();
            layout->addWidget(comboBoxFrame);
        }
        else
        {
            QHBoxLayout * toggleLayout = initToggleLayout();
            layout->addLayout(toggleLayout);

            browserFrame = initBrowserFrame();
            layout->addWidget(browserFrame);
        }

        // The bottom of the dialog box contains the
        // image sets list layout (label, list, and SetManager).

        QLayout * listLayout = initListLayout();
        layout->addLayout(listLayout);
    }

    setLayout(layout);
}

/********************************************************************/
/*  Main dialog - toggle layout                                     */
/********************************************************************/

QHBoxLayout * SetChooser::initToggleLayout()
{
    toggleComboBox = new QComboBox();
    toggleComboBox->addItem("Browse");
    toggleComboBox->addItem("Custom");

    QHBoxLayout * toggleLayout = new QHBoxLayout();
    toggleLayout->addStretch();
    toggleLayout->addWidget(toggleComboBox);

    return toggleLayout;
}

/********************************************************************/
/*  Main dialog - project/group area                                */
/********************************************************************/

QFrame * SetChooser::initBrowserFrame()
{
    // Create the label, directory name edit box, and browse button.

    QLabel * directoryLabel = new QLabel(projectTermIC + " directory:");
    directoryEditBox = new QLineEdit();
    directoryEditBox->setToolTip("Enter a " + projectTermLC + " directory");
    dirBrowseButton = new QPushButton("Browse...");
    dirBrowseButton->setToolTip("Choose a " + projectTermLC + " directory");

    // Create the list of directory patterns.

    QGroupBox * patternsGroup = initDirectoryPatterns();

    // Lay out the label, edit box, and button.

    QHBoxLayout * editBoxLayout = new QHBoxLayout();
    editBoxLayout->addWidget(directoryLabel);
    editBoxLayout->addWidget(directoryEditBox);
    editBoxLayout->addWidget(dirBrowseButton);

    // Lay out the edit box area and the patterns area.

    QVBoxLayout * frameLayout = new QVBoxLayout();
    frameLayout->addLayout(editBoxLayout);
    frameLayout->addWidget(patternsGroup);

    // Create the frame and set the layout.

    QFrame * frame = new QFrame();
    frame->setLayout(frameLayout);

    // Return the frame.

    return frame;
}

QGroupBox * SetChooser::initDirectoryPatterns()
{
    QVBoxLayout * patternsLayout = new QVBoxLayout();

    for (int i = 0; i < dirPatterns.size(); i++)
    {
        QRadioButton * radioButton = new QRadioButton(dirPatterns[i].summary);
        if (i == 0)
        {
            radioButton->setChecked(true);
            currRadioIndex = i;
        }
        radioButtons.append(radioButton);

        ClickableLabel * example = new ClickableLabel();
        example->setText("(example)");
        example->setStyleSheet("text-decoration: underline; color: DodgerBlue;");
        exampleLabels.append(example);
        examplesHash.insert(example, i);

        QHBoxLayout * buttonLayout = new QHBoxLayout();
        buttonLayout->addWidget(radioButton);
        buttonLayout->addWidget(example);
        buttonLayout->addStretch();

        patternsLayout->addLayout(buttonLayout);
    }

    // Add the radio buttons to the GroupBox. In the RTI Inspector,
    // hide the group box. This makes it usable interally but not
    // visible to or usable by the user -- RTI supports only a
    // single directory pattern.

    QGroupBox * patternsBox = new QGroupBox("Directory patterns");
    patternsBox->setLayout(patternsLayout);
    if ((this->inspectorType == InspectorType::RTI) && (radioButtons.size() == 1))
    {
        patternsBox->hide();
    }

    return patternsBox;
}

QFrame * SetChooser::initComboBoxFrame()
{
    // Create the project and group combo boxes.

    QLabel * projectLabel = new QLabel("Project:");
    projectComboBox = new QComboBox();
    initComboBox(ITableType::Project, -1, projectComboBox);

    QLabel * groupLabel = new QLabel("Bundle:");
    groupComboBox = new QComboBox();
    groupComboBox->setEnabled(false);

    // Lay out the combo boxes.

    QGridLayout * comboBoxLayout = new QGridLayout();
    comboBoxLayout->addWidget(projectLabel, 0, 0);
    comboBoxLayout->addWidget(projectComboBox, 0, 2);
    comboBoxLayout->addWidget(groupLabel, 1, 0);
    comboBoxLayout->addWidget(groupComboBox, 1, 2);
    comboBoxLayout->setColumnStretch(2, 1);  // Stretch combo box
    comboBoxLayout->setColumnStretch(3, 1);  // Add padding on right

    // Create a QFrame and set the layout.

    QFrame * frame = new QFrame();
    frame->setLayout(comboBoxLayout);

    // Return the frame.

    return frame;
}

namespace {

// Selects and makes 'current' the item in the given combo box
// that contains the given serial:
void selectComboBoxItem(QComboBox* comboBox, int srl) {
    for (int i=0; i<comboBox->count(); ++i) {
        QString userData = comboBox->itemData(i).toString();
        if (srl == userData.leftRef(userData.indexOf(';')).toInt()) {
            comboBox->setCurrentIndex(i);
            return;
        }
    }
}
}

void SetChooser::selectBundle(int project_srl, int bundle_srl)
{
    ::selectComboBoxItem(this->projectComboBox, project_srl);
    ::selectComboBoxItem(this->groupComboBox, bundle_srl);
}

void SetChooser::initComboBox(ITableType::Enum tableType, int parentSrl, QComboBox * comboBox)
{
    // Clear the combo box and add a blank item.

    comboBox->clear();
    comboBox->addItem("Choose..", QVariant("-1;"));

    // Build the query to populate the combo box. Note that
    // we allow folders to be empty.

    QString query, groupType("project");
    switch (tableType)
    {
        case ITableType::Project :
            query = "SELECT srl, cptproj, cptproj_img_foldr FROM cptproj "
                    "WHERE cptproj <> '' AND deleted IS DISTINCT FROM TRUE ORDER BY LOWER(cptproj)";
            break;

        case ITableType::Bundle :
            groupType = InspectorType::toDBString(this->inspectorType);
            query = "SELECT c.srl, c.cptsess, c.cptsess_img_foldr FROM cptsess c "
                    "INNER JOIN cptsess_type t ON (c.cptsess_type_srl = t.srl) "
                    "WHERE c.cptsess <> '' AND c.deleted IS DISTINCT FROM TRUE AND t.deleted IS DISTINCT FROM TRUE"
                    " AND c.cptproj_srl = " + QString::number(parentSrl) +
                    " AND t.cptsess_type = '" + groupType +
                    "' ORDER BY LOWER(c.cptsess)";
            break;
        default: // ImageSet
            break;
    }
    qDebug() << "Search for type "<<groupType << "with query" << query;

    // Open the database and execute the query, then add
    // items/data to the combo box.

    QSqlQuery q(QSqlDatabase::database(dlnDBName));
    execQuery(q, query);
    while (q.next())
    {
        // Retrieve the data and build the user data. We need to
        // store both the serial number and path, but QVariant
        // only accepts a single user data value. Although it is
        // possible to store custom objects, such as a structure
        // containing both values, it's easier to build a string
        // with both pieces of data.

        int srl = q.value(0).toInt();
        QString item = q.value(1).toString();
        QString dirPath = q.value(2).toString();
        QString userData;
        userData.setNum(srl).append(";").append(dirPath);

        // Add the item to the combo box.

        comboBox->addItem(item, QVariant(userData));
    }

    comboBox->setCurrentIndex(0);
}

/********************************************************************/
/*  Main dialog - sets lists                                        */
/********************************************************************/

QLayout * SetChooser::initListLayout()
{
    // BUG: QStringListModel only supports case-sensitive sorts. We want
    // case-insensitive sorts. To do this, we need to use QSortFilterProxyModel
    // between the QListView and the QStringListModel. Note that this may
    // cause problems to the model indexes, so we are not fixing it now. More
    // research needed...

    // Allocate the list of image sets and lay it out.

    imageSetsModel = new QStringListModel();
    imageSetsListView = new QListView();
    imageSetsListView->setModel(imageSetsModel);
    imageSetsListView->setEditTriggers(QAbstractItemView::NoEditTriggers);
//    imageSetsListView->setToolTip("Select one or more " + imageSetTermLC + " sets to inspect.");
    imageSetsListView->setSelectionMode(QAbstractItemView::NoSelection);

    // Create the list label.

    QLabel * imageSetsLabel = new QLabel(imageSetTermIC + " sets:");

    // Lay out the pieces.

    if (this->inspectorType == InspectorType::RTI || this->inspectorType == InspectorType::Documentary)
    {
        QVBoxLayout * layout = new QVBoxLayout();
        layout->addWidget(imageSetsLabel);
        layout->addWidget(imageSetsListView);
        return layout;
    }
    else if (this->inspectorType == InspectorType::Photogrammetry)
    {
        setManager = new SetManager(imageSetsListView, imageSetsModel, imageSetsHash);
        if (!usingDB || withEditSet == false) setManager->hide();

        QHBoxLayout * listAndMgrLayout = new QHBoxLayout();
        listAndMgrLayout->addWidget(imageSetsListView);
        listAndMgrLayout->addWidget(setManager);

        QVBoxLayout * layout = new QVBoxLayout();
        layout->addWidget(imageSetsLabel);
        layout->addLayout(listAndMgrLayout);
        return layout;
    }
    else // Documentary -> ???
    {
        return nullptr;
    }
}


/********************************************************************/
/*  Connect slots                                                   */
/********************************************************************/

void SetChooser::setConnections()
{
    if (usingDB)
    {
        // Populate the combo box and the list.

        connect(projectComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(projectChanged(int)));

        connect(groupComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(groupChanged(int)));
    }
    else
    {
        // Display a directory chooser dialog when dirBrowseButton is clicked.

        connect(dirBrowseButton, SIGNAL(clicked()), this, SLOT(chooseDirectory()));

        // When the directory text is edited, check the new project directory.

        connect(directoryEditBox, SIGNAL(editingFinished()), this, SLOT(checkDirectory()));

        // Connect the "(example)" labels to example popups.

        for (int i = 0; i < exampleLabels.size(); i++)
        {
            connect(radioButtons[i], SIGNAL(clicked()), this, SLOT(radioButtonClicked()));
            connect(exampleLabels[i], SIGNAL(clicked()), this, SLOT(exampleClicked()));
        }

        // Connect the toggle combo box in the Photogrammetry Inspector.

        if (this->inspectorType == InspectorType::Photogrammetry)
        {
            connect(toggleComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(toggleChanged(int)));
        }
    }
}

/********************************************************************/
/*                                                                  */
/*  Methods to support GUI slots                                    */
/*                                                                  */
/********************************************************************/

void SetChooser::toggleChanged(int index)
{
    if (index == 0) // New UI is directory browser
    {
        setManager->hide();
        clearModels();
        browserFrame->show();
        directoryEditBox->setFocus();
    }
    else // New UI is SetManager
    {
        browserFrame->hide();
        directoryEditBox->clear();
        clearModels();
        setManager->show();
        projectDirectory = "Custom";
    }
}

void SetChooser::updateGroupComboBox(int projectIndex, const QComboBox * projectComboBox, QComboBox * groupComboBox)
{
    // If the index of the project combo box is 0, clear the group combo
    // box and disable it. If it is anything else, update the data
    // in the group combo box and enable it.

    if  (projectIndex == 0)
    {
        groupComboBox->clear();
        groupComboBox->setEnabled(false);
    }
    else
    {
        // Get the project serial number from the project combo box.

        int projectSrl = getSerial(projectComboBox);

        // Update and enable the group combo box.

        initComboBox(ITableType::Bundle, projectSrl, groupComboBox);
        groupComboBox->setEnabled(true);
    }
}

int SetChooser::getSelectedRadioButton()
{
    for (int i = 0; i < radioButtons.size(); i++)
    {
        if (radioButtons[i]->isChecked()) return i;
    }

    throw IException("Programming error", "No pattern radio button checked.");
}

DirPattern SetChooser::getSelectedPattern()
{
    return dirPatterns[getSelectedRadioButton()];

    // throw IException("Programming error", "No pattern radio button checked.");
}


/********************************************************************/
/*                                                                  */
/*  Methods to get image sets - without database                    */
/*                                                                  */
/********************************************************************/

void SetChooser::getImageSetsByDir(const QString &dirName, DirPattern dirPattern, IFileType::Enum archivalType)
{
    // Clear the current model and hash table of image sets.

    clearModels();

    // Search the directory structure and build the hash of file sets.

    QHash<QString, FileSet> fileSets;
    QString currentSetName;
    searchDir(fileSets, dirName, dirPattern.patterns, 0, currentSetName, IFileType::UNKNOWN, archivalType);

    // Loop through the file sets and build image sets. Three
    // cases are legal:
    //
    // - Processed set + archival set
    // - Processed set only
    // - Archival set only (generates a warning, no ImageSet created)

    QList<QString> setNames = fileSets.uniqueKeys();
    for (int i = 0; i < setNames.size(); i++)
    {
        // Get the file sets for a particular set name. In most cases,
        // there will be two sets -- one archival and one processed. It
        // is also possible to have just an archival set (warning) or
        // just a processed set (OK).

        QString setName = setNames[i];
        QList<FileSet> setsWithSameName = fileSets.values(setName);

        // Get the processed and archival file sets for this name. If no
        // processed file set was found or more than two sets were found,
        // just continue.

        FileSet processedSet, archivalSet;
        bool processedSetFound = getFileSets(setName, setsWithSameName, archivalSet, processedSet);
        if (!processedSetFound) continue;

        // Add a row for the set name to the image sets model and
        // add an ImageSet to the image sets hash.

        addRow(imageSetsModel, 0, QVariant(setName));
        ImageSet imageSet(0, setName, archivalSet.path, archivalSet.fileInfos, archivalSet.fileType, processedSet.path, processedSet.fileInfos, processedSet.fileType);
        imageSetsHash.insert(setName, imageSet);
    }

    // If any image sets were found, sort them. Otherwise,
    // display a warning.

    if (imageSetsModel->rowCount() != 0)
    {
        // BUG: This sort is case-sensitive. See initImageSetsListLayout().

        imageSetsModel->sort(0);
    }
    else
    {
        QString msg = "<p>No " + imageSetTermLC +
                      " sets found in the directory:</p><pre>   " +
                      dirName +
                      "</pre><p>The directory must use the selected structure:</p><pre>   " +
                      dirPattern.summary +
                      "</pre><p>For example:</p><pre>" +
                      dirPattern.example +
                      "</pre>";
        QMessageBox::warning(this, "No " + imageSetTermLC + " sets found", msg);
    }
}

void SetChooser::searchDir(QHash<QString, FileSet> & fileSets, const QString & dirName, const QList<Pattern> & patterns, int patternIndex, const QString & currentSetName, IFileType::Enum currentFileType, IFileType::Enum archivalType)
{
    // Get a QDir for the directory;

    QDir dir(dirName);

    // Get the pattern.

    Pattern pattern = patterns[patternIndex];

    // Get the image set name and file set type from the directory
    // name (or use the current values).

    QString setName = getSetName(dir, pattern, currentSetName);
    IFileType::Enum fileType = getFileType(dir, pattern, currentFileType);

    // If the current directory contains files, build the file set.
    // If it contains subdirectories, search those subdirectories.

    if (pattern.hasFiles)
    {
        addFileSet(fileSets, dir, setName, fileType, archivalType);
    }
    else
    {
        // Recursively search the child directories. Note that the
        // search ends if there are no child directories, even if
        // the directory pattern expects there to be. (This happens
        // when there are extra directories in a tree that aren't
        // part of the structure designed to hold image sets.)

        QStringList childDirs = dir.entryList(QDir::AllDirs | QDir::NoDotAndDotDot);
        for (int i = 0; i < childDirs.size(); i ++)
        {
            searchDir(fileSets, dirName + "/" + childDirs[i], patterns, patternIndex + 1, setName, fileType, archivalType);
        }
    }
}

QString SetChooser::getSetName(const QDir & dir, const Pattern & pattern, const QString & currentSetName)
{
    // BUG: Windows directory names are case-insensitive, so two
    // parallel subdirectories with the same names in different
    // cases should be considered the same. For example:
    //
    //    my-project
    //       dng-files
    //          bar
    //       jpg-files
    //          bar      // matches dng-files/bar
    //       tif-files
    //          BAR      // does not match dng-files/bar;
    //                   // should match on Windows (not Mac)
    //
    // On Windows, this can lead to extra image sets being created
    // instead of an error occurring. In the example above, the image
    // set bar will have DNG and JPEG files; the image set BAR will
    // have only TIFF files.
    //
    // Since hashes are case-sensitive, the solution to this is
    // probably to store lower-case directory names in imageSetsHash
    // on Windows and mixed-case directory names on Mac. We would
    // continue to store mixed-case names in FileSet/ImageSet and use
    // these as the actual image set names.
    //
    // Before doing this, we need to check everywhere that
    // imageSetsHash is used, as some set names in it are supposed to
    // be mixed case, such as the Manage Set Dialog, Mac directories,
    // and the DLN database. Also need to check case in other places
    // that use QListView, such as the files in the Manage Set Dialog.

    // If this directory is supposed to be used as the image set
    // name, return that. Otherwise, return the current name.

    return pattern.hasSetName ? dir.dirName() : currentSetName;
}

IFileType::Enum SetChooser::getFileType(const QDir & dir, const Pattern & pattern, IFileType::Enum currentFileType)
{
    // If this directory does not contain file type information
    // in its name, just return the current file type.

    if (!pattern.hasTypeName) return currentFileType;

    // Get the file type from the directory name.

    // Pattern.typeRegexes is a hash table of type names and
    // case-insensitive QRegExps used to a find type names
    // in a directory name. For example:
    //
    // - "TIFF" and "(TIFF|TIF)"
    // - "JPEG" and "^(jpeg-exports)$"
    //
    // The first matches any directory name that includes the
    // (case-insensitive) strings "TIFF" or "TIF". The second
    // matches the (case-insensitive) directory name "jpeg-exports".
    //
    // Loop through the regexes and set the file type according
    // to the first regex that matches the directory name. Note
    // that we don't look for more matches, so "(TIFF|TIF)" matches
    // "JPEG-and-TIFF-files". We also don't look at meaning, so it
    // also matches "Tiffany Glass Images".

    QString dirName(dir.dirName());
    QList<QString> typeNames = pattern.typeRegexes.keys();
    for (int i = 0; i < typeNames.size(); i++)
    {
        QString typeName = typeNames[i];
        QRegExp regex = pattern.typeRegexes.value(typeName);
        if (regex.indexIn(dirName) != -1)
        {
            return IFileType::fromString(typeName);
        }
    }

    // If we haven't found a file type, just return the
    // current file type. This happens if there are directories
    // in the tree that are unrelated to image sets.

    return currentFileType;
}

void SetChooser::addFileSet(QHash<QString, FileSet> & fileSets, const QDir & dir, const QString & setName, IFileType::Enum fileType, IFileType::Enum archivalType)
{
    // If we don't have a file type, just return. This happens
    // when there are directories in a tree that are not related
    // to image sets and thus have no file type to be extracted.

    if (fileType == IFileType::UNKNOWN) return;

    // If we don't have a file set name, just return. Currently,
    // this will never happen because:
    //
    // (a) the initialization code checks that:
    //     (i) the DirPattern has exactly one directory from
    //         which the file set name is taken and
    //    (ii) the last step (and only the last step) of the
    //         DirPattern has a flag saying it contains files and
    // (b) to get to this method, we have traversed to the
    //     directory containing the last step.
    //
    // We have this code here because, in the future, we may
    // add regexes for retrieving the image set name from a
    // directory name. For branches of the directory tree that
    // do not contain image sets, it is possible that no name
    // will be extracted.

    if (setName.isEmpty()) return;

    // Build a filter and get the image files.

    QStringList nameFilters = IFileType::getExtensions(fileType);
    QFileInfoList fileInfos = dir.entryInfoList(nameFilters, QDir::Files);

    // If there are no image files, just return.

    if (fileInfos.size() == 0) return;

    // Add the image set to the hash of image sets.

    fileSets.insertMulti(setName, FileSet(setName, fileType == archivalType, dir.canonicalPath(), fileType, fileInfos));
}

bool SetChooser::getFileSets(const QString & setName, const QList<FileSet> & candidateSets, FileSet & archivalSet, FileSet & processedSet)
{
    // Go through the list of FileSets that have the same set name and
    // find the processed and archived file sets. There should only be
    // one or two sets in the list, so display a warning and return
    // if we find more than one set of processed or archival files.

    bool haveProcessed(false), haveArchival(false);
    for (int j = 0; j < candidateSets.size(); j++)
    {
        FileSet fileSet = candidateSets[j];
        if (fileSet.isArchival)
        {
            if (haveArchival)
            {
                // If we already have an archival file set, display
                // a warning and return, skipping this set name.

                // Note that we can safely use the first QFileInfo in
                // the file set because file sets aren't created if
                // no files are found (see addFileSet()).

                QString msg = "The image set " + setName +
                              " has two archival image directories: \n\n" +
                              fileSet.fileInfos[0].canonicalPath() +
                              "\n" +
                              archivalSet.fileInfos[0].canonicalPath();

                QMessageBox::warning(this, "Multiple archival images directories", msg);
                return false;
            }
            haveArchival = true;
            archivalSet = fileSet;
        }
        else
        {
            if (haveProcessed)
            {
                // If we already have a processed file set, display
                // a warning and return, skipping this set name.

                QString msg = "The image set " + setName +
                              " has two processed image directories: " +
                              fileSet.fileInfos[0].canonicalPath() +
                              " and " +
                              processedSet.fileInfos[0].canonicalPath();

                QMessageBox::warning(this, "Multiple processed images directories", msg);
                return false;
            }
            haveProcessed = true;
            processedSet = fileSet;
        }
    }

    // If we have an archival set but not a processed set,
    // post a warning and continue.

    if (haveArchival && !haveProcessed)
    {
        QMessageBox::warning(this,
                             "Unused archival images folder",
                             "The following archival images folder does not "
                             "correspond to any processed images folders:\n\n" +
                             archivalSet.fileInfos[0].canonicalPath());
    }

    // Return whether we have a processed file set.

    return haveProcessed;
}

QString SetChooser::getProjectPathDB(int projectSrl)
{
    QString query =
            "SELECT p.cptproj_img_foldr"
            " FROM cptproj p"
            " WHERE p.srl = " +
            QString::number(projectSrl);
    // qDebug() << projectSrl << query;
    QSqlQuery q(QSqlDatabase::database(dlnDBName));
    execQuery(q, query);
    if (q.next()) {
        QString basePath = q.value(0).toString();
        if (!basePath.endsWith("/"))
            basePath.append("/");
        return basePath;
    }
    return "";

}

/********************************************************************/
/*                                                                  */
/*  Methods to get image sets -- with database                      */
/*                                                                  */
/********************************************************************/

QList<ImageSet> SetChooser::getImageSetsByDB(int projectSrl, int groupSrl, QString& projectDir)
{

    // Execute the query to get the image set directories and
    // files (if any) that are stored in the database.

    QString query = QString(
                "SELECT c.cptset_srl AS srl, p.cptproj_img_foldr, "
                "CASE WHEN multi_set IS TRUE THEN cptset ELSE img_bundle END AS name, "
                "c.arch_img_foldr, c.work_img_foldr, fl.file_type, fp.filepath "
                "FROM %1 c JOIN cptproj p on p.srl = c.cptproj_srl "
                "LEFT OUTER JOIN cptset_vs_filelist vs ON c.cptset_srl = vs.cptset_srl "
                "LEFT OUTER JOIN filelist fl ON vs.filelist_srl = fl.srl "
                "LEFT OUTER JOIN filepath fp ON fl.srl = fp.filelist_srl "
                "WHERE c.cptproj_srl = %2 AND c.cptsess_srl = %3 "
                "ORDER BY LOWER(c.cptset)")
            .arg(QSysInfo::productType() == "windows" ? "get_folders_win" : "get_folders_unix")
            .arg(projectSrl).arg(groupSrl);
    qDebug() << "Getting images from DB for project" << projectSrl << "and bundle" << groupSrl << query;
    QSqlQuery q(QSqlDatabase::database(dlnDBName));
    execQuery(q, query);

    // Read the results and add a row for each image set.

    int srl = -1, currSrl = -1;
    QList<QFileInfo> archivalInfos, processedInfos;
    IFileType::Enum processedType(IFileType::UNKNOWN);
    QString setName;
    QString archivalPath, processedPath;
    QList<ImageSet> sets;
    while (q.next())
    {

        bool firstRow = currSrl == -1;

        // If this is the first row, retrieve the project's folder from the DB.
        // (All retrieved rows should have the same value):
        if (firstRow) {
            projectDir = q.value("cptproj_img_foldr").toString();
            if (!projectDir.endsWith("/"))
                projectDir.append("/");
        }

        // Get the image set key.

        srl = q.value("srl").toInt();

        // If this row is from a new image set, create an ImageSet
        // from the existing data (unless it is the first row).
        if (currSrl != srl)
        {
            if (!firstRow)
            {
                ImageSet set = newImageSet(currSrl, setName, archivalPath, archivalInfos, IFileType::DNG, processedPath, processedInfos, processedType);
                sets.append(set);
            }

            // Reset variables.
            archivalInfos.clear();
            archivalPath.clear();
            processedInfos.clear();
            processedPath.clear();
            processedType = IFileType::UNKNOWN;

            // Remember the new image set key.

            currSrl = srl;

            // Get the new image set name.

            setName = q.value("name").toString();

            // Build the paths to the new archival and processed images
            // directories. Use empty paths if the database does not
            // contain any directories. For RTI, append the correct
            // subdirectories. (Note that the RTI image directory stored
            // in the database is structurally equivalent to the PG
            // group directory.)

            archivalPath = q.value("arch_img_foldr").toString();
            // qDebug() << "Image set" << currSrl << "(" << setName << ") Archival path:" << archivalPath;

            processedPath = q.value("work_img_foldr").toString();
            // qDebug() << "Image set" << currSrl << "(" << setName << ") Processed path:" << processedPath;

        }

        // If there are any files (filelist.filetype is not NULL), add
        // them to the approriate list. NOTE: This should actually check
        // the filelists.filelist_type field, which has values for
        // archival and processed.

        if (!q.isNull("file_type"))
        {
            IFileType::Enum fileType = static_cast<IFileType::Enum>(q.value("file_type").toInt());
            if (fileType == IFileType::DNG)
            {
                archivalInfos.append(q.value("filepath").toString());
            }
            else
            {
                processedType = fileType;
                processedInfos.append(q.value("filepath").toString());
            }
        }
    }

    // Create an ImageSet for the last set, only if got any from the DB
    if (currSrl != -1) {
        ImageSet set = newImageSet(currSrl, setName, archivalPath, archivalInfos, IFileType::DNG, processedPath, processedInfos, processedType);
        sets.append(set);
    }
    return sets;
}
void SetChooser::getImageSetsByDB(const QComboBox * projectComboBox, const QComboBox * groupComboBox)
{
    // Clear the current model and hash table of image sets.

    clearModels();

    // If no group is selected, just return.

    if (groupComboBox->currentIndex() <= 0) return;

    // Get the project and group serial numbers.

    int projectSrl = getSerial(projectComboBox);
    int groupSrl = getSerial(groupComboBox);

    QList<ImageSet> sets = getImageSetsByDB(projectSrl, groupSrl, this->projectDirectory);
    foreach (ImageSet imageSet, sets) {
        addImageSet(imageSet);
    }
    // If any image sets were found, sort them. Otherwise,
    // display a warning.

    if (imageSetsModel->rowCount() != 0)
    {
        imageSetsListView->selectAll();
    }
    else
    {
        QMessageBox::warning(this, "No " + imageSetTermLC + " sets found", "No " + imageSetTermLC + " sets found");
    }
}

/*
Method doesn't work because QStringListModel doesn't support the
tool tip role.  Need to use QStandardItemModel.

https://stackoverflow.com/questions/39439636/unable-to-set-a-tooltip-with-setdata-when-using-qlistview-with-qstringlistmodel

void SetChooser::setItemToolTip(const QString & archivalImagesPath, const QString & processedImagesPath)
{
    QString dirToolTip;
    dirToolTip.append("Processed images directory: ")
              .append(processedImagesPath)
              .append("\n")
              .append("Archival images directory: ")
              .append(archivalImagesPath);
    setModelToolTip(availableSetsModel, 0, QVariant(dirToolTip));
}*/

int SetChooser::getSerial(const QComboBox * comboBox)
{
    QString userData = comboBox->itemData(comboBox->currentIndex()).toString();
    return userData.leftRef(userData.indexOf(';')).toInt();
}

QString SetChooser::getPath(const QComboBox * comboBox)
{
    QString userData = comboBox->itemData(comboBox->currentIndex()).toString();
    return userData.mid(userData.indexOf(';') + 1);
}

ImageSet SetChooser::newImageSet(int imageSetSrl, const QString & setName, const QString & archivalPath, const QList<QFileInfo> & archivalInfos, IFileType::Enum archivalType, const QString & processedPath, const QList<QFileInfo> & processedInfos, IFileType::Enum processedType)
{
    // If there were no files in the database, create an ImageSet
    // using the files (if any) found on the disk. If there were
    // files in the database, create an ImageSet with them.

    if (processedInfos.size() == 0)
    {
        // Determine the processed image type from the files in
        // the directory. getTypeFromFiles() throws an exception
        // if the directory contains files of more than one type.
        // If there are no files, the file type will be IFileType::Unknown.

        processedType = getTypeFromFiles(processedPath, setName); // could throw: (IException e)

        // Create the image set and add it to the hash.

        ImageSet imageSet(imageSetSrl, setName, archivalPath, archivalType, processedPath, processedType);
        return imageSet;
    }
    else
    {
        // Create the Image set and add it to the hash.

        ImageSet imageSet(imageSetSrl, setName, archivalPath, archivalInfos, archivalType, processedPath, processedInfos, processedType);
        return imageSet;
    }
}

void SetChooser::addImageSet(const ImageSet& imageSet)
{
    try {
        // Create the image set and add it to the hash.
        imageSetsHash.insert(imageSet.name, imageSet);
    }
    catch (IException e)
    {
        QMessageBox::warning(this, e.title, e.msg);
        return;
    }

    // Add the image set to the model.
    addRow(imageSetsModel, 0, imageSet.name);
    // setItemToolTip(archivalImagesPath, processedImagesPath);
}

IFileType::Enum SetChooser::getTypeFromFiles(const QString & path, const QString & setName)
{
    // If the path is empty, return IFileType::UNKNOWN.

    if (path.isEmpty()) return IFileType::UNKNOWN;

    // Get a list of QFileInfos for the images in the path.

    QList<QFileInfo> fileInfos = QDir(path).entryInfoList(IFileType::getProcessedExtensions(), QDir::Files);

    // If all files are of the same type, use that type. Otherwise,
    // display a warning and throw an exception.

    IFileType::Enum imageFileType(IFileType::UNKNOWN), thisFileType;
    for (int i = 0; i < fileInfos.size(); i++)
    {
        thisFileType = IFileType::fromString(fileInfos[i].suffix());
        if (imageFileType == IFileType::UNKNOWN)
        {
            imageFileType = thisFileType;
        }
        else if (imageFileType != thisFileType)
        {
            QString msg;
            msg = "Unable to determine the image type for the processed images in the " +
                    setName + " image set. The directory " + path + " contains both " +
                    IFileType::toString(imageFileType) + " and " +
                    IFileType::toString(thisFileType) + " images. Skipping image set.";
            // QMessageBox::warning(this, "Multiple file types", msg);
            throw InspectorException("Multiple file types", msg);
        }
    }

    // Return the file type of the file set.

    return imageFileType;
}

/********************************************************************/
/*                                                                  */
/*  Utility methods                                                 */
/*                                                                  */
/********************************************************************/

void SetChooser::clearModels()
{
    // Clear the existing list of available image sets.

    clearModel(imageSetsModel);
    imageSetsHash.clear();
    projectDirectory.clear();
}
