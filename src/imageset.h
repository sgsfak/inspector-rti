/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

#ifndef IMAGESET_H
#define IMAGESET_H

// Local headers

#include "ienums.h"

// Qt headers

#include <QDir>
#include <QFileInfo>
#include <QList>
#include <QString>

inline uint qHash(const QFileInfo& fi, uint seed)
{
    return qHash(fi.absoluteFilePath(), seed);
}

class ImageSet
{
public:

    // ImageSet is a simple structure that describes an RTI or
    // photogrammetry image set -- that is, a set of files to
    // be inspected together. An image set consists of two file
    // sets: the archival files and the processed image files.
    // Each file set has its own type (JPEG, TIFF, DNG).

    // Constructors

    ImageSet();

    ImageSet(int srl, QString name, QString archivalPath, QList<QFileInfo> archivalFileInfos, IFileType::Enum archivalType, QString processedPath, QList<QFileInfo> processedFileInfos, IFileType::Enum processedType);

    ImageSet(int srl, QString name, QString archivalPath, IFileType::Enum archivalType, QString processedPath, IFileType::Enum processedType);

    ~ImageSet();

    // Variables

    // Notes:
    //
    // - archivalType is currently always set to DNG. We may use it to support
    //   TIFF as an archival type in the future. Note that this variable is not
    //   always used where it should be. To do that, search files for "DNG" and
    //   fix as needed.

    int srl;
    QString name;
    QString archivalPath;
    QList<QFileInfo> archivalFileInfos;
    IFileType::Enum archivalType;
    QString processedPath;
    QList<QFileInfo> processedFileInfos;
    IFileType::Enum processedType;

    struct DocumentIds {
        QString documentId;
        QString origDocumentId;

        DocumentIds() = default;
        DocumentIds(const QString& dId, const QString& oid)
            :documentId(dId), origDocumentId(oid)
        {}
    };

    // Map from the image file path to the pair of Document and OriginalDocument IDs
    // of the images:
    QHash<QFileInfo, DocumentIds> docIds;

private:
    void getFileInfos(const QString & path, QList<QFileInfo> & fileInfos, const IFileType::Enum imgType);
};

#endif // IMAGESET_H
