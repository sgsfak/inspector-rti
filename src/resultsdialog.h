/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2015                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

#ifndef RESULTSDIALOG_H
#define RESULTSDIALOG_H

// Local headers

#include "results.h"
#include "treemodel.h"

// Qt headers

#include <QColor>
#include <QDateTime>
#include <QDialog>
#include <QLabel>
#include <QModelIndex>
#include <QMouseEvent>
#include <QStandardItemModel>
#include <QString>
#include <QTableView>
#include <QVBoxLayout>
#include <QTreeView>

/********************************************************************/
/*                                                                  */
/*  ITableView                                                      */
/*                                                                  */
/********************************************************************/

// ITableView intercepts mouse tracking events so that the
// results dialog can display a pointing-hand cursor when
// the mouse is in the fourth column of the table. This cues
// the user that they can click on the entries in this column.

class ITableView : public QTableView
{
public:
    ITableView(QWidget *parent=nullptr);
    void mouseMoveEvent(QMouseEvent* event) override;
};

/********************************************************************/
/*                                                                  */
/*  ResultsDialog                                                   */
/*                                                                  */
/********************************************************************/

class ResultsDialog : public QDialog, public Results
{
    Q_OBJECT
public:
    ResultsDialog(QWidget *parent = nullptr);
    ~ResultsDialog() override;

    // Methods to set/clear variables

    virtual void setProject(const QString & projectDirectory) override;
    virtual void setInspectionTime(const QDateTime & inspectionTime) override;
    virtual void clear() override;

    // Method to display the dialog

    void showDialog();

signals:
    
public slots:
    void displayResult(const QModelIndex &index);
    void saveResults();
    void saveHTML();
    void closeDialog();
    void displayHelp();
    void doubleClickOnTree(const QModelIndex &index);
private:
    // Member variables

    QLabel * directoryLabel;
    QLabel * dateTimeLabel;
    QStandardItemModel * tableModel;
    ITableView * resultsTable;
    TreeModel * treeModel;
    class QTreeView* treeView;

    // GUI initialization methods

    void initGUI();
    QVBoxLayout * initInfoLayout();
    ITableView * initResultsTable();
    void initTreeView();
    QVBoxLayout * initButtonsLayout();

    void buildTreeModel();

    // Methods to work with table models

    void addCell(QStandardItemModel * model, int rowNum, int colNum, const QString &data);
    void setUnderline(QStandardItemModel * model, int rowNum, int colNum);
    void setTextColor(QStandardItemModel * model, int rowNum, int colNum, const QColor &color);

    // Utility methods

    QString getFilename(const QString & caption, const QString & extension, const QString & filters, QString *selectedFilter);

};

#endif // RESULTSDIALOG_H
