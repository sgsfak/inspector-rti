/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

#ifndef DBUTILS_H
#define DBUTILS_H

// Local headers
#include "imageset.h"

// Qt headers

#include <QFileInfo>
#include <QSqlQuery>

class DBUtils
{
public:
    DBUtils();
    ~DBUtils();

    // Database methods

    static QSqlDatabase openDatabase(const QString &type, const QString &connectionName, const QString &dbName);
    static void startTransaction(QSqlDatabase & db);
    static void startTransaction(QSqlDatabase & db, const QString &dbName);
    static void commitTransaction(QSqlDatabase & db);
    static void commitTransaction(QSqlDatabase & db, const QString &dbName);

    // Query methods

    static void prepareQuery(QSqlQuery & q, const QString & query);
    static void prepareQuery(QSqlQuery & q, const QString & query, const QString & dbTitle);

    static void execPrepared(QSqlQuery & q);
    static void execPrepared(QSqlQuery & q, const QString & dbTitle);

    static void execQuery(QSqlQuery & q, const QString & query);
    static void execQuery(QSqlQuery & q, const QString & query, const QString & dbTitle);

    // Methods for working with filelists

    int addFilelist(QSqlQuery & q, const QString & hash, const QList<QFileInfo> &fileInfos, const QHash<QFileInfo, ImageSet::DocumentIds> &docIds, int fileType, const QString &filelistType);
    void addFilelistVsOther(QSqlQuery & q, const QString & vsTable, const QString & otherColumn, int otherSrl, int filelistSrl);

    void deleteFilelistVsOther(QSqlQuery & q, const QString & vsTable, int vsSrl);
    void deleteFilelist(QSqlQuery & q, int filelistSrl);

    bool getFilelistForOther(QSqlQuery & q, const QString & vsTable, const QString & otherColumn, int otherSrl, const QString & filelistType, QString & hash, int & vsSrl, int & filelistSrl);
    int getFilelistSrl(QSqlQuery & q, const QString & hash, const QString &filelistType);
    int getFilelistRefCount(QSqlQuery & q, int filelistSrl);

    // Utilities

    static QString getHash(QList<QFileInfo> fileInfos);
    static bool comparePaths(const QFileInfo &fileInfo1, const QFileInfo &fileInfo2);
};

#endif // DBUTILS_H
