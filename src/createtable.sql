-- *****************************************
--
-- CREATE TABLE STATEMENTS
--
-- *****************************************

-- *****************************************
-- Table: compare_type
-- *****************************************

CREATE TABLE compare_type (
    compare_type integer NOT NULL,
    description text NOT NULL,
    deleted text NOT NULL
);

ALTER TABLE compare_type OWNER TO postgres;

ALTER TABLE ONLY compare_type
    ADD CONSTRAINT compare_type_pkey PRIMARY KEY (compare_type);

-- *****************************************
-- Table: cptset_vs_filelist
-- *****************************************

CREATE TABLE cptset_vs_filelist (
    srl integer DEFAULT nextval('cptset_vs_filelist_srl_seq'::regclass) NOT NULL,
    cptset_srl integer NOT NULL,
    filelist_srl integer NOT NULL
);

ALTER TABLE cptset_vs_filelist OWNER TO postgres;

ALTER TABLE ONLY cptset_vs_filelist
    ADD CONSTRAINT cptset_vs_filelist_pkey PRIMARY KEY (srl);

-- *****************************************
-- Table: file_type
-- *****************************************

CREATE TABLE file_type (
    file_type integer NOT NULL,
    type_name text NOT NULL,
    deleted boolean NOT NULL
);

ALTER TABLE file_type OWNER TO postgres;

ALTER TABLE ONLY file_type
    ADD CONSTRAINT file_type_pkey PRIMARY KEY (file_type);

-- *****************************************
-- Table: filelist
-- *****************************************

CREATE TABLE filelist (
    srl integer DEFAULT nextval('filelist_srl_seq'::regclass) NOT NULL,
    file_type integer NOT NULL,
    filelist_type character(1) NOT NULL,
    filepath_hash character(32) NOT NULL,
    filelist_uuid uuid NOT NULL
);

ALTER TABLE filelist OWNER TO postgres;

ALTER TABLE ONLY filelist
    ADD CONSTRAINT filelist_pkey PRIMARY KEY (srl);

-- *****************************************
-- Table: filelist_type
-- *****************************************

CREATE TABLE filelist_type (
    filelist_type character(1) NOT NULL,
    type_name text NOT NULL,
    deleted boolean NOT NULL
);

ALTER TABLE filelist_type OWNER TO postgres;

ALTER TABLE ONLY filelist_type
    ADD CONSTRAINT filelist_type_pkey PRIMARY KEY (filelist_type);

-- *****************************************
-- Table: filepath
-- *****************************************

CREATE TABLE filepath (
    srl integer DEFAULT nextval('filepath_srl_seq'::regclass) NOT NULL,
    filelist_srl integer NOT NULL,
    filepath text NOT NULL,
    filepath_uuid uuid NOT NULL
);

ALTER TABLE filepath OWNER TO postgres;

ALTER TABLE ONLY filepath
    ADD CONSTRAINT filepath_pkey PRIMARY KEY (srl);

-- *****************************************
-- Table: inspection
-- *****************************************

CREATE TABLE inspection (
    srl integer DEFAULT nextval('inspection_srl_seq'::regclass) NOT NULL,
    cptset_srl integer NOT NULL,
    inspection_datetime timestamp(0) with time zone NOT NULL,
    inspector_version text NOT NULL,
    inspection_uuid uuid NOT NULL
);

ALTER TABLE inspection OWNER TO postgres;

ALTER TABLE ONLY inspection
    ADD CONSTRAINT inspection_pkey PRIMARY KEY (srl);

-- *****************************************
-- Table: inspection_vs_filelist
-- *****************************************

CREATE TABLE inspection_vs_filelist (
    srl integer DEFAULT nextval('inspection_vs_filelist_srl_seq'::regclass) NOT NULL,
    inspection_srl integer NOT NULL,
    filelist_srl integer NOT NULL
);

ALTER TABLE inspection_vs_filelist OWNER TO postgres;

ALTER TABLE ONLY inspection_vs_filelist
    ADD CONSTRAINT inspection_vs_filelist_pkey PRIMARY KEY (srl);

-- *****************************************
-- Table: inspection_vs_testgroup
-- *****************************************

CREATE TABLE inspection_vs_testgroup (
    srl integer DEFAULT nextval('inspection_vs_testgroup_srl_seq'::regclass) NOT NULL,
    inspection_srl integer NOT NULL,
    testgroup_srl integer NOT NULL
);

ALTER TABLE inspection_vs_testgroup OWNER TO postgres;

ALTER TABLE ONLY inspection_vs_testgroup
    ADD CONSTRAINT inspection_vs_testgroup_pkey PRIMARY KEY (srl);

-- *****************************************
-- Table: inspresult
-- *****************************************

CREATE TABLE inspresult (
    srl integer DEFAULT nextval('inspresult_srl_seq'::regclass) NOT NULL,
    inspection_srl integer NOT NULL,
    testgroup_srl integer NOT NULL,
    result_type integer NOT NULL,
    msg_type integer NOT NULL,
    title text NOT NULL,
    description text,
    property_srl integer,
    filepath1_srl integer,
    filepath2_srl integer,
    value1 text,
    value2 text,
    inspresult_uuid uuid NOT NULL
);

ALTER TABLE inspresult OWNER TO postgres;

ALTER TABLE ONLY inspresult
    ADD CONSTRAINT inspresult_pkey PRIMARY KEY (srl);

-- *****************************************
-- Table: msg_type
-- *****************************************

CREATE TABLE msg_type (
    description text NOT NULL,
    msg_type integer NOT NULL,
    deleted boolean NOT NULL
);

ALTER TABLE msg_type OWNER TO postgres;

ALTER TABLE ONLY msg_type
    ADD CONSTRAINT msg_type_pkey PRIMARY KEY (msg_type);

-- *****************************************
-- Table: property
-- *****************************************

CREATE TABLE property (
    srl integer DEFAULT nextval('property_srl_seq'::regclass) NOT NULL,
    propertygroup_srl integer NOT NULL,
    property_name text NOT NULL,
    restrictions text,
    compare integer NOT NULL,
    property_uuid uuid NOT NULL
);

ALTER TABLE property OWNER TO postgres;

ALTER TABLE ONLY property
    ADD CONSTRAINT property_pkey PRIMARY KEY (srl);

-- *****************************************
-- Table: propertygroup
-- *****************************************

CREATE TABLE propertygroup (
    srl integer DEFAULT nextval('propertygroup_srl_seq'::regclass) NOT NULL,
    testgroup_srl integer NOT NULL,
    propertygroup_name text NOT NULL,
    propertygroup_version text NOT NULL,
    propertygroup_uuid uuid NOT NULL
);

ALTER TABLE propertygroup OWNER TO postgres;

ALTER TABLE ONLY propertygroup
    ADD CONSTRAINT propertygroup_pkey PRIMARY KEY (srl);

-- *****************************************
-- Table: result_type
-- *****************************************

CREATE TABLE result_type (
    result_type integer NOT NULL,
    description text NOT NULL,
    deleted boolean NOT NULL
);

ALTER TABLE result_type OWNER TO postgres;

ALTER TABLE ONLY result_type
    ADD CONSTRAINT result_type_pkey PRIMARY KEY (result_type);

-- *****************************************
-- Table: testgroup
-- *****************************************

CREATE TABLE testgroup (
    srl integer DEFAULT nextval('testgroup_srl_seq'::regclass) NOT NULL,
    testgroup_name text NOT NULL,
    testgroup_uuid uuid NOT NULL
);

ALTER TABLE testgroup OWNER TO postgres;

ALTER TABLE ONLY testgroup
    ADD CONSTRAINT testgroup_pkey PRIMARY KEY (srl);

-- *****************************************
--
-- CREATE FOREIGN KEYS
--
-- *****************************************

-- *****************************************
-- Table: cptset_vs_filelist
-- *****************************************

ALTER TABLE ONLY cptset_vs_filelist
    ADD CONSTRAINT cptset_cptset_vs_filelist_fk FOREIGN KEY (cptset_srl) REFERENCES cptset(srl);

ALTER TABLE ONLY cptset_vs_filelist
    ADD CONSTRAINT filelist_cptset_vs_filelist_fk FOREIGN KEY (filelist_srl) REFERENCES filelist(srl);

-- *****************************************
-- Table: filelist
-- *****************************************

ALTER TABLE ONLY filelist
    ADD CONSTRAINT file_type_filelist_fk FOREIGN KEY (file_type) REFERENCES file_type(file_type);

ALTER TABLE ONLY filelist
    ADD CONSTRAINT filelist_type_filelist_fk FOREIGN KEY (filelist_type) REFERENCES filelist_type(filelist_type);

-- *****************************************
-- Table: filepath
-- *****************************************

ALTER TABLE ONLY filepath
    ADD CONSTRAINT filelist_filepath_fk FOREIGN KEY (filelist_srl) REFERENCES filelist(srl);

-- *****************************************
-- Table: inspection
-- *****************************************

ALTER TABLE ONLY inspection
    ADD CONSTRAINT cptset_inspection_fk FOREIGN KEY (cptset_srl) REFERENCES cptset(srl);

-- *****************************************
-- Table: inspection_vs_filelist
-- *****************************************

ALTER TABLE ONLY inspection_vs_filelist
    ADD CONSTRAINT filelist_inspection_vs_filelist_fk FOREIGN KEY (filelist_srl) REFERENCES filelist(srl);

ALTER TABLE ONLY inspection_vs_filelist
    ADD CONSTRAINT inspection_inspection_vs_filelist_fk FOREIGN KEY (inspection_srl) REFERENCES inspection(srl);

-- *****************************************
-- Table: inspection_vs_testgroup
-- *****************************************

ALTER TABLE ONLY inspection_vs_testgroup
    ADD CONSTRAINT inspection_inspection_vs_testgroup_fk FOREIGN KEY (inspection_srl) REFERENCES inspection(srl);

ALTER TABLE ONLY inspection_vs_testgroup
    ADD CONSTRAINT testgroup_inspection_vs_testgroup_fk FOREIGN KEY (testgroup_srl) REFERENCES testgroup(srl);

-- *****************************************
-- Table: inspresult
-- *****************************************

ALTER TABLE ONLY inspresult
    ADD CONSTRAINT filepath1_inspresult_fk FOREIGN KEY (filepath1_srl) REFERENCES filepath(srl);

ALTER TABLE ONLY inspresult
    ADD CONSTRAINT filepath2_inspresult_fk FOREIGN KEY (filepath2_srl) REFERENCES filepath(srl);

ALTER TABLE ONLY inspresult
    ADD CONSTRAINT inspection_inspresult_fk FOREIGN KEY (inspection_srl) REFERENCES inspection(srl);

ALTER TABLE ONLY inspresult
    ADD CONSTRAINT msg_type_inspresult_fk FOREIGN KEY (msg_type) REFERENCES msg_type(msg_type);

ALTER TABLE ONLY inspresult
    ADD CONSTRAINT property_inspresult_fk FOREIGN KEY (property_srl) REFERENCES property(srl);

ALTER TABLE ONLY inspresult
    ADD CONSTRAINT result_type_inspresult_fk FOREIGN KEY (result_type) REFERENCES result_type(result_type);

ALTER TABLE ONLY inspresult
    ADD CONSTRAINT testgroup_inspresult_fk FOREIGN KEY (testgroup_srl) REFERENCES testgroup(srl);

-- *****************************************
-- Table: property
-- *****************************************

ALTER TABLE ONLY property
    ADD CONSTRAINT propertygroup_property_fk FOREIGN KEY (propertygroup_srl) REFERENCES propertygroup(srl);

-- *****************************************
-- Table: propertygroup
-- *****************************************

ALTER TABLE ONLY propertygroup
    ADD CONSTRAINT testgroup_propertygroup_fk FOREIGN KEY (testgroup_srl) REFERENCES testgroup(srl);