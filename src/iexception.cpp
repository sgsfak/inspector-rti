/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

// Local headers

#include "iexception.h"

/********************************************************************/
/*                                                                  */
/*  Constructors                                                    */
/*                                                                  */
/********************************************************************/

IException::IException(QString title, QString msg) :
    title(title),
    msg(msg)
{

}

IException::~IException()
{

}

IOException::IOException(QString title, QString msg) :
    IException(title, msg)
{

}

XMLException::XMLException(QString title, QString msg) :
    IException(title, msg)
{

}

XMPException::XMPException(QString title, QString msg) :
    IException(title, msg)
{

}

DBException::DBException(QString title, QString msg) :
    IException(title, msg)
{

}

DBException::DBException(QString title, QSqlError e)
{
    this->title = title;
    msg.append("Driver error: ").append(e.driverText()).append("\n");
    msg.append("Database error: ").append(e.databaseText()).append("\n");
    msg.append("Error code: ").append(e.nativeErrorCode());
}


InspectorException::InspectorException(QString title, QString msg) :
    IException(title, msg)
{

}
