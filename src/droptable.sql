drop table inspresult;
drop table inspection_vs_filelist;
drop table inspection_vs_testgroup;
drop table inspection;

drop table cptset_vs_filelist;
drop table filepath;
drop table filelist;

drop table property;
drop table propertygroup;
drop table testgroup;

drop table compare_type;
drop table file_type;
drop table filelist_type;
drop table msg_type;
drop table result_type;