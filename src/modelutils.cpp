/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

// Local headers

#include "modelutils.h"

#include <QDebug>

/********************************************************************/
/*                                                                  */
/*  Public methods                                                  */
/*                                                                  */
/********************************************************************/

/********************************************************************/
/*                                                                  */
/*  Constructor and destructor                                      */
/*                                                                  */
/********************************************************************/

ModelUtils::ModelUtils()
{

}

ModelUtils::~ModelUtils()
{

}

/********************************************************************/
/*                                                                  */
/*  Convenience methods for working with Qt models                  */
/*                                                                  */
/********************************************************************/

// Qt has a remarkably byzantine way of working with list models. Instead
// of just using row number (or row and column numbers) to set or retrieve
// the data in a model, you must instead retrieve the "index" (QModelIndex
// object) for a given row (and column) and use that to set or retrieve
// data. To make matters more confusing, you can retrieve data by passing
// the index to the model or directly from the index itself, but can only
// set data by passing the index to the model. Furthermore, indexes are
// brittle and may become invalid over time. Bleagh.
//
// Indexes exist to separate "the representation of the data ... from the
// way it is accessed." While this is useful in some cases, it is overkill
// in many others and it is surprising that there isn't a simple list model
// that simply hides indexes altogether, using only rows and columns for
// access.

void ModelUtils::clearModel(QStringListModel * model)
{
    // Convenience method to remove all of the rows in an item model.

    model->removeRows(0, model->rowCount());
}

QStringList ModelUtils::getSelectedData(QListView * view, QStringListModel * model)
{
    // Convenience method to get the data for the currently
    // selected rows.

    // Get the indexes of the selected rows.

    QModelIndexList indexList = getSelectedIndexes(view);

    // Build a list of the data for the selected rows.

    QStringList stringList;
    for (int i = 0; i < indexList.size(); i++)
    {
        QModelIndex index = indexList[i];
        QString string = model->data(index, Qt::DisplayRole).toString();
        stringList.append(string);
    }

    // Return the list.

    return stringList;
}

QStringList ModelUtils::getAllData(QStringListModel * model)
{
    // Get all of the data from the model.

    QStringList stringList;
    for (int i = 0; i < model->rowCount(); i++)
    {
        stringList.append(model->index(i).data().toString());
    }

    // Return the data.

    return stringList;
}

void ModelUtils::removeSelectedRows(QListView * view, QStringListModel * model)
{
    // Get the list of indexes for the selected items.

    QModelIndexList selectedIndexes = getSelectedIndexes(view);

    // Thanks to Qt's remarkably Byzantine model access methods,
    // QModelIndexList is not sorted and the QModelIndex objects
    // inside it are not guaranteed to remain valid over time. Thus,
    // if we simply loop through the list of QModelIndex objects and
    // delete the row associated with each, there is a significant
    // chance of failure. This is because (a) deleting a row changes
    // the numbers of any rows that occur later in the list and
    // (b) the QModelIndex's of those rows do not appear to be
    // updated to use the new row number.

    // A simple solution would be to sort the QModelIndexList, but
    // there is no built-in method to do this and, since row numbers
    // are brittle (as seen above), writing our own comparison
    // function may be brittle as well. (I didn't try.)

    // To solve this problem, build a vector of row numbers and
    // sort them...

    std::vector<int> indexVector;
    for (int i = 0; i < selectedIndexes.size(); i++)
    {
        indexVector.insert(indexVector.begin(), selectedIndexes[i].row());
    }
    std::sort(indexVector.begin(), indexVector.end() );

    // ... then remove the rows in reverse order so any row that
    // is removed does not affect the numbers of the remaining rows.

    for (int i = indexVector.size() - 1; i >= 0 ; i--)
    {
        model->removeRow(indexVector[i]);
    }
}

// These methods are commented out because we no longer need
// them. We are keeping the commented versions because they
// were painful to write and we may need them in the future.

/*void ModelUtils::moveSelectedRow(QListView * fromList,
                                        QStringListModel * fromModel,
                                        QStringListModel * toModel)
{
    // Convenience method to move the currently selected image set
    // from one list of image sets to the other list of image sets.

    // Get the model representing the current selection in the
    // list of "from" image sets.

    QItemSelectionModel * selectionModel = fromList->selectionModel();
    if (selectionModel->hasSelection())
    {
        // If an image set has been selected, get the index pointing
        // to that image set and move it to the "to" model, then
        // sort the "to" model.

        QModelIndex fromIndex = selectionModel->currentIndex();
        moveRow(fromModel, fromIndex, toModel, 0);
        toModel->sort(0);
    }
}

void ModelUtils::moveRow(QStringListModel * fromModel,
                                QModelIndex fromIndex,
                                QStringListModel * toModel,
                                int toRowNum)
{
    // Convenience method for move a row from one model to another model.

    // Add a row to the "to" model. Note that fromIndex points
    // to the row to be removed.

    addRow(toModel, toRowNum, fromIndex.data());

    // Remove the row from the "from" model.

    fromModel->removeRow(fromIndex.row());
}

void ModelUtils::moveAllRows(QStringListModel * fromModel, QStringListModel * toModel)
{
    // Convenience method for moving all the rows in one model to another model.

    // For each row in the "from" model, get the index of the row and add
    // the row to the "to" model.

    for (int i = 0; i < fromModel->rowCount(); i++)
    {
        QModelIndex index = fromModel->index(i);
        addRow(toModel, 0, fromModel->data(index, Qt::DisplayRole));
    }

    // Sort the "to" model.

    toModel->sort(0);

    // Clear the "from" model.

    clearModel(fromModel);
}*/

QPersistentModelIndex ModelUtils::addRow(QStringListModel * model,
                                         int rowNum,
                                         const QVariant & data)
{
    // Convenience method for adding a row to a model.

    // Check if the data is already in the list. We perform case-sensitive
    // matching because filenames on Unix (Mac) are case-sensitive and this
    // method is sometimes used by filenames.

    Qt::MatchFlags flags = Qt::MatchExactly | Qt::MatchWrap;
#ifdef Q_OS_MACOS
    flags = flags | Qt::MatchCaseSensitive;
#endif

    QModelIndex startIndex = model->index(0, 0);
    QModelIndexList modelIndexList = model->match(startIndex, Qt::DisplayRole, data, 1, flags);

    // If the data is not in the list, add a new (empty) row to
    // the list and get the index for that row, then set the
    // data for that row.

    if (modelIndexList.size() == 0)
    {
        model->insertRow(rowNum);
        QModelIndex newIndex = model->index(rowNum, 0);
        model->setData(newIndex, data, Qt::DisplayRole);
        return newIndex;
    }
    return QModelIndex();
}

void ModelUtils::setModelToolTip(QStringListModel * model,
                            int rowNum,
                            const QVariant & data)
{
    // Convenience method for setting the tooltip on a row in a model.

    QModelIndex index = model->index(rowNum, 0);
    model->setData(index, data, Qt::ToolTipRole);
}

QModelIndexList ModelUtils::getSelectedIndexes(QListView * view)
{
    // Get the indexes for the currently selected rows.

    return view->selectionModel()->selectedIndexes();
}
