/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

// Local headers

#include "iexception.h"
#include "iglobals.h"
#include "inspector.h"
#include "resultsdialog.h"

// Qt headers

#include <QApplication>
#include <QDate>
#include <QFile>
#include <QFileInfo>
#include <QList>
#include <QTime>
#include <QTimeZone>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
//#include <QXmlQuery>
//#include <QXmlResultItems>

#include <QDebug>

// C++ headers

#include <algorithm>
#include <limits>
#include <cmath>
#include <memory>

/********************************************************************/
/*                                                                  */
/*  Inspector class                                                 */
/*                                                                  */
/********************************************************************/

/********************************************************************/
/*                                                                  */
/*  Constructor and destructor                                      */
/*                                                                  */
/********************************************************************/

Inspector::Inspector() :
    progressDialog(nullptr),
    propInfoLists(nullptr)
{
    initSpecialHandling();
}

Inspector::~Inspector()
{
}

/********************************************************************/
/*                                                                  */
/*  Public methods                                                  */
/*                                                                  */
/********************************************************************/
void Inspector::inspect(PropInfoLists *propInfoLists, Results * results, const Options &options, const QString &projectDirectory, QList<ImageSet> &imageSets)
{
    // Set the initial error context.

    e.reset();
    e.projectDirectory = projectDirectory;

    // Set the global variables.

    this->propInfoLists = propInfoLists;
    this->results = results;
    this->options = options;

    // Initialize the results.

    results->clear();
    results->setProject(projectDirectory);
    results->setInspectionTime(QDateTime::currentDateTime());

    // BUG: An earlier version of the Inspector had both command-line
    // and GUI interfaces. It now only has a GUI interface. The code
    // needs to be cleaned up:
    //
    // a) Get rid of the gui variable.
    // b) ResultsDialog should not extend Results. Instead, these
    //    two classes should probably be separate, with ResultsDialog
    //    encapsulating Results (passed on constructor to member variable).

    // If we are using the GUI interface, create and use a progress
    // dialog. Note that dynamic_cast returns a null pointer if it
    // is unable to cast results to ResultsDialog*.

    ResultsDialog * resultsDialog = dynamic_cast<ResultsDialog *>(results);
    gui = (resultsDialog != nullptr);
    if (gui)
    {
        progressDialog = new QProgressDialog(resultsDialog->parentWidget());
    }

    // Set the status to OK. The status is used to track whether
    // any warnings or errors occurred. We need to know this
    // because there are options to (a) stop if an error occurs
    // and (b) not write to the DLN database if an error or warning occurs.

    status.reset();

    // Initialize the exception variables. These are used to
    // delay exception handling.

    bool exceptionThrown = false;
    IException savedException;

    // Sort the files in each image set according to their base names.
    // Because the filenames come from multiple sources, it is cleaner to
    // do this here.

    sortFiles(imageSets);

    // Inspect the image sets.

    try
    {
        // Set up the log file, if any.

        if (options.logging)
        {
            // Create a log file.

            QString path;
            if (!projectDirectory.isEmpty() && (projectDirectory != "Custom"))
                path = projectDirectory + "/";
            logFile.setFileName(path + "inspect" + results->getInspectionTime().toString("yyyy-MM-ddThh-mm") + ".log");
            if (!logFile.open(QIODevice::WriteOnly | QIODevice::Text))
            {
                throw IOException("Error opening file", "Error opening log file: " + logFile.fileName() + ". Error: " + logFile.errorString());
            }

            // Write out the log file header.

            write(logFile, "INSPECTING " + imageSetTermUC + " SET(S)\nDATE/TIME: ");
            write(logFile, results->getInspectionTime().toString("yyyy-MM-ddThh:mm"));
            if (!projectDirectory.isEmpty())
            {
                write(logFile, "\n" + projectTermUC + " DIRECTORY: " + projectDirectory);
            }
            write(logFile, "\n\n");
        }

        // Inspect the image sets.


        for (int i = 0; i < imageSets.size(); i++)
        {
            ImageSet& imageSet = imageSets[i];
            for(const QFileInfo& finfo: qAsConst(imageSet.archivalFileInfos)) {
                // p == (docId, origDocId);
                auto p = getMediaManagementIds(finfo);
                imageSet.docIds.insert(finfo, p);
            }
            for(const QFileInfo& finfo: qAsConst(imageSet.processedFileInfos)) {
                auto p = getMediaManagementIds(finfo);
                imageSet.docIds.insert(finfo, p);
            }

            inspectSet(imageSet, i);
        }
    }
    catch (IException iexception)
    {
        // Exceptions are thrown for four reasons:
        //
        // a) An error occurs above when opening the log file.
        //
        // b) The Inspector reports a fatal error. This error
        //    is added to the results and the Inspector then
        //    throws an exception to stop processing.
        //
        // c) The Inspector reports a recoverable error and the
        //    user has selected the stop-on-error option. The
        //    error is added to the results and the Inspector
        //    then throws an exception to stop processing.
        //
        // d) An unanticipated exception was thrown, such as by
        //    the IO subsystem or the XMP SDK. Some of these are
        //    caught and rethrown, others might simply occur.

        // If an exception is thrown, we catch it here so we can clean up
        // before rethrowing it later.

        exceptionThrown = true;
        savedException = iexception;

        // Fall through to finish the inspection.
    }

    try
    {
        // Add the results for each image set to the DLN database. We
        // do this in a separate try-catch block so we can add error
        // information to the DLN.

        if (usingDB)
        {
            updateDatabase(imageSets);
        }
    }
    catch (IException iexception)
    {
        // If we get an error updating the DLN, let that override
        // the earlier exception (if any).

        exceptionThrown = true;
        savedException = iexception;

        // Fall through to clean up.
    }

    // Close the log file, if any.

    if (options.logging)
    {
        logFile.close();
    }

    // If we are in GUI mode, close the progress dialog
    // and display the results.

    if (gui)
    {
        // Call setValue with the maximum to tell the progress
        // dialog that the operation is done.

        progressDialog->setValue(progressDialog->maximum());

        // Show the results dialog. We call showDialog() instead of
        // QWidget.show() so that ResultsDialog can sort the results
        // and add them to its table model before displaying them.

        resultsDialog->showDialog();
    }

    // If an exception occurred, rethrow it now.

    if (exceptionThrown)
    {
        throw savedException;
    }
}

/********************************************************************/
/*                                                                  */
/*  Private methods                                                 */
/*                                                                  */
/********************************************************************/

/********************************************************************/
/*                                                                  */
/*  Initialize GUI                                                  */
/*                                                                  */
/********************************************************************/

void Inspector::initProgressDialog(const QString & imageSetName, int size)
{
    // Initialize the progress dialog. Note that setting the cancel
    // button text to an empty QString hides the cancel button.

    progressDialog->setWindowTitle("Inspecting " + imageSetName + " ...");
    progressDialog->setCancelButtonText(QString());
    progressDialog->setLabelText("Inspecting " + imageSetTermLC + " set: " + imageSetName);
    progressDialog->setMinimumDuration(0);
    progressDialog->setModal(true);

    // Set the range of the progress dialog and show it.

    progressDialog->setRange(0, size);
    progressDialog->show();

    // Initialize the variable that tracks the progress.

    progress = 0;

    // Call setValue and processEvents to force the progress
    // dialog to immediately display.

    progressDialog->setValue(0);
    QApplication::processEvents(QEventLoop::ExcludeUserInputEvents | QEventLoop::ExcludeSocketNotifiers);
}

/********************************************************************/
/*                                                                  */
/*  Inspection methods                                              */
/*                                                                  */
/********************************************************************/

// These methods have the following call hierarchy:
//
// inspectSet
//   |
//   +-->inspectFilenames
//   |     |
//   |     +-->compareFilenames
//   |
//   +-->inspectCaptureProps
//   |     |
//   |     +-->inspectDLN
//   |     |     |
//   |     |     +-->getDLNValues
//   |     |     |
//   |     |     +-->getValues
//   |     |     |
//   |     |     +-->validateValues
//   |     |     |
//   |     |     +-->compareValueLists
//   |     |
//   |     +-->inspectImages (see below)
//   |
//   +-->inspectXformProps
//         |
//         +-->inspectImages
//               |
//               +-->compareWithinFileType
//               |     |
//               |     +-->getValues
//               |     |
//               |     +-->validateValues
//               |     |
//               |     +-->compareValueLists
//               |
//               +-->compareAcrossFileTypes
//                     |
//                     +-->getValues
//                     |
//                     +-->compareValueLists
//
// See also Get Property Values and Compare Property Values sections

void Inspector::inspectSet(const ImageSet &imageSet, const int imageSetNum)
{
    // Inspect a single image set.

    results->increaseImageCount(imageSet.name, imageSet.archivalFileInfos.length());
    results->increaseImageCount(imageSet.name, imageSet.processedFileInfos.length());
    // Set the error context.

    e.imageSetName = imageSet.name;
    e.imageSetNum = imageSetNum;

    // Write the image set name to the log file.

    if (options.logging)
    {
        write(logFile, "***************************************************\n");
        write(logFile, imageSetTermUC + " SET: " + imageSet.name + "\n");
        write(logFile, "***************************************************\n\n");
    }

    // Inspect the filenames and get the names of the first
    // archival and processed files with the same basename.

    QFileInfo firstArchivalFileInfo;
    QFileInfo firstProcessedFileInfo;
    inspectFilenames(imageSet, firstArchivalFileInfo, firstProcessedFileInfo);

    // Initialize the progress dialog. We estimate the process size as the
    // number of non-DLN properties times the number of processed and archival
    // files. This isn't exact, but it is close enough.

    if (gui)
    {
        initProgressDialog(imageSet.name,
                           (imageSet.archivalFileInfos.size() + imageSet.processedFileInfos.size()) *
                           (propInfoLists->cameraIDPropInfoList->size() + propInfoLists->capturePropInfoList->size() + propInfoLists->xformPropInfoList->size()));
    }

    // Inspect the capture and tranformation properties.

    inspectCaptureProps(imageSet, firstArchivalFileInfo, firstProcessedFileInfo);
    inspectXformProps(imageSet, firstArchivalFileInfo, firstProcessedFileInfo);

    // Reset the error context.

    e.imageSetName.clear();
    e.imageSetNum = -1;
}

void Inspector::inspectFilenames(const ImageSet &imageSet, QFileInfo &firstArchivalFileInfo, QFileInfo &firstProcessedFileInfo)
{
    // Set the test group to filenames.

    e.testGroup = ITestGroup::File;

    // Check that there are files to inspect. Add a warning if no archival
    // files are found. Add a fatal error if no processed files are found.

    if (imageSet.archivalFileInfos.size() == 0)
    {
        addResult(IResultType::Warning, "No DNG files", "<p>No DNG files found in " + imageSetTermLC + " set " + imageSet.name + "</p>", "");
    }

    if (imageSet.processedFileInfos.size() == 0)
    {
        QString fileType =
                imageSet.processedType == IFileType::UNKNOWN ? "processed" : IFileType::toString(imageSet.processedType);
        addResult(IResultType::Warning,
                  QString("No %1 files").arg(fileType),
                  QString("<p>No %1 files found in %2 set %3</p>").arg(fileType, imageSetTermLC, imageSet.name ),
                  "");
    }

    // Check that there are corresponding archival and processed files. Also, return
    // the names of the first archival and processed files that have the same basename.
    // We will use these later to compare properties in the archival files to
    // properties in the processed files.

    compareFilenames(imageSet.name, imageSet.archivalFileInfos, imageSet.processedFileInfos, imageSet.processedType, firstArchivalFileInfo, firstProcessedFileInfo);

    // NOTE: In earlier versions of the inspector, we checked at this point
    // that the directory being used matched the directory in the DLN RDF
    // file. We no longer do this because:
    //
    // - If the DLN is used, we get the directory from the DLN, so they are
    //   the same.
    //
    // - If the DLN is not used, the user selects the directory, but there is
    //   no DLN to compare against.

    // If no errors were found, add an OK result.

    if ((status.file == IStatus::Success) || (status.file == IStatus::Warning))
    {
        addResult(IResultType::OK, "File tests OK", "<p>No file errors found in this " + imageSetTermLC + " set.</p>", "");
    }

    // Reset the error context.

    e.testGroup = ITestGroup::None;
}

void Inspector::inspectCaptureProps(const ImageSet &imageSet, const QFileInfo & firstArchivalFileInfo, const QFileInfo & firstProcessedFileInfo)
{
    // Set the test group to capture.

    qDebug() << "inspectCaptureProps" << "usingDB=" << usingDB;

    e.testGroup = ITestGroup::Capture;

    // Check the property values in the DLN database against property values
    // in the archival and processed files. If there is no DLN database, add
    // a warning and check that these values are the same in the archival and
    // processed files.

    if (usingDB)
    {
        if (options.logging) write(logFile, "Using the DLN database\n\n");
        inspectDLN(*propInfoLists->dlnPropInfoList, imageSet);
    }
    else
    {
        if (options.logging) write(logFile, "Not using the DLN database\n\n");
        addResult(IResultType::Warning, "Not using DLN", "<p>Not using the DLN database.</p>", "");
        inspectImages(*propInfoLists->dlnPropInfoList, imageSet, firstArchivalFileInfo, firstProcessedFileInfo);
    }

    // Check that the capture properties in the archival and processed
    // files have the same values and that those values are legal.

    inspectImages(*propInfoLists->cameraIDPropInfoList, imageSet, firstArchivalFileInfo, firstProcessedFileInfo);
    inspectImages(*propInfoLists->capturePropInfoList, imageSet, firstArchivalFileInfo, firstProcessedFileInfo);

    // If no errors were found, add an OK result.

    if ((status.capture == IStatus::Success) || (status.capture == IStatus::Warning))
    {
        addResult(IResultType::OK, "Capture tests OK", "<p>No capture errors found in this " + imageSetTermLC + " set.</p>", "");
    }

    // Reset the error context.

    e.testGroup = ITestGroup::None;
}

void Inspector::inspectXformProps(const ImageSet & imageSet, const QFileInfo & firstArchivalFileInfo, const QFileInfo & firstProcessedFileInfo)
{
    // Set the test group to transformation.

    e.testGroup = ITestGroup::Transformation;

    // Check that the transformation properties in the archival and processed
    // files have the same values and that those values are legal.

    inspectImages(*propInfoLists->xformPropInfoList, imageSet, firstArchivalFileInfo, firstProcessedFileInfo);

    // If no errors were found, add an OK result.

    if ((status.xform == IStatus::Success) || (status.xform == IStatus::Warning))
    {
        addResult(IResultType::OK, "Transformation tests OK", "<p>No transformation errors found in this " + imageSetTermLC + " set.</p>", "");
    }

    // Reset the error context.

    e.testGroup = ITestGroup::None;
}

void Inspector::inspectDLN(const QList<PropInfo *> &propInfoList, const ImageSet & imageSet)
{
    // Get the property values from the DLN database.

    QList<XMPValue *> dlnValues = getDLNValues(propInfoList, imageSet);

    // Get the property values from the archival files and check them against the
    // property values in the DLN database. Delete the archival values after they
    // have been used. Note that nothing happens if there are no archival files.

    for (const QFileInfo& finfo: imageSet.archivalFileInfos)
    {
        QList<XMPValue *> archivalValues = getValues(propInfoList, finfo);
        validateValues(propInfoList, finfo, archivalValues);
        compareValueLists(propInfoList, QFileInfo("DLN database"), dlnValues, finfo, archivalValues, IComparisonType::AgainstDLN);
        qDeleteAll(archivalValues);
    }

    // Get the property values from the processed files and check them against the
    // property values in the DLN database. Delete the processed values after they
    // have been used.

    for (const QFileInfo& finfo: imageSet.processedFileInfos)
    {
        QList<XMPValue *> processedValues = getValues(propInfoList, finfo);
        validateValues(propInfoList, finfo, processedValues);
        compareValueLists(propInfoList, QFileInfo("DLN database"), dlnValues, finfo, processedValues, IComparisonType::AgainstDLN);
        qDeleteAll(processedValues);
    }

    // Delete the DLN property values.

    qDeleteAll(dlnValues);
}

void Inspector::inspectImages(const QList<PropInfo *> &propInfoList, const ImageSet &imageSet, const QFileInfo &firstArchivalFileInfo, const QFileInfo &firstProcessedFileInfo)
{
    // For the archival and processed files, check that a set of property values is:
    //
    // a) The same across all archival files,
    // b) The same across all processed files, and
    // c) The same for the first archival file and the first processed file.
    //
    // This ensures that each property has the same value in all archival
    // and processed files.

    if (imageSet.archivalFileInfos.size())
        compareWithinFileType(propInfoList, imageSet.archivalFileInfos);
    if (imageSet.processedFileInfos.size())
        compareWithinFileType(propInfoList, imageSet.processedFileInfos);
    if (firstArchivalFileInfo.exists() && firstProcessedFileInfo.exists())
        compareAcrossFileTypes(propInfoList, firstArchivalFileInfo, firstProcessedFileInfo);
}

void Inspector::compareWithinFileType(const QList<PropInfo *> &propInfoList, const QList<QFileInfo> &fileInfos)
{
    // If there are no files, just return.

    qDebug() << "compareWithinFileType" << fileInfos.size() << (fileInfos.size()?fileInfos.at(0).absolutePath() : "--");
    if (fileInfos.size() == 0) return;

    // Get the property values for the first file, then compare these to the property
    // values in each of the remaining files. Delete the values for the remaining files
    // after they have been used.

    QList<XMPValue *> firstFileValues = getValues(propInfoList, fileInfos[0]);
    validateValues(propInfoList, fileInfos[0], firstFileValues);
    for (int i = 1; i < fileInfos.size(); i++)
    {
        QList<XMPValue *> nextFileValues = getValues(propInfoList, fileInfos[i]);
        validateValues(propInfoList, fileInfos[i], nextFileValues);
        compareValueLists(propInfoList, fileInfos[0], firstFileValues, fileInfos[i], nextFileValues, IComparisonType::WithinFileType);
        qDeleteAll(nextFileValues);
    }

    // Delete the property values for the first file.

    qDeleteAll(firstFileValues);

    // If we are running in GUI mode, update the progress dialog.
    // The call to QApplication::processEvents forces the progress
    // dialog to display updates.
    //
    // Note that we subtract 1 from the progress amount. When setValue()
    // is called with the maximum, the progress dialog is closed. By
    // subtracting 1, we keep the progress dialog visible until after
    // the DLN is updated. (We don't update the progress dialog while
    // updating the DLN, but we want it to remain visible so the user knows
    // we are still processing things. See the call to setValue() at
    // the end of inspect().)

    if (gui)
    {
        progress += propInfoList.size() * fileInfos.size() - 1;
        progressDialog->setValue(progress);
        QApplication::processEvents(QEventLoop::ExcludeUserInputEvents | QEventLoop::ExcludeSocketNotifiers);
    }
}

void Inspector::compareAcrossFileTypes(const QList<PropInfo *> &propInfoList, const QFileInfo & archivalFileInfo, const QFileInfo & processedFileInfo)
{
    // If there is no archival or processed file, just return.

    if (archivalFileInfo.fileName().isEmpty() || processedFileInfo.fileName().isEmpty()) return;

    // Get the property values for the archival file and the processed file,
    // then compare these. Delete the values after they have been used.

    QList<XMPValue *> archivalValues = getValues(propInfoList, archivalFileInfo);
    QList<XMPValue *> processedValues = getValues(propInfoList, processedFileInfo);
    compareValueLists(propInfoList, archivalFileInfo, archivalValues, processedFileInfo, processedValues, IComparisonType::AcrossFileTypes);
    qDeleteAll(archivalValues);
    qDeleteAll(processedValues);
}

void Inspector::updateDatabase(const QList<ImageSet> &imageSets)
{
    // Write to the log file.

    if (options.logging) write(logFile, "Updating DLN database.\n\n");

    // Calculate the overall status from the status of each test
    // group. Perform a bitwise AND to determine whether to write
    // results to the DLN. (Probably too clever for our own good...)

    IStatus::Enum overallStatus = std::max(std::max(status.file, status.capture), status.xform);
    bool writeResults = (overallStatus & options.writeToDLN);

    // Update the DLN database.

    results->updateDatabase(imageSets, propInfoLists, options.removePreviousResults, writeResults);
}

/********************************************************************/
/*                                                                  */
/*  Get property values                                             */
/*                                                                  */
/********************************************************************/

// These methods have the following call hierarchy:
//
// getDLNValues
//
// getValues
//   |
//   +-->getValue <--------------+
//         |                     |
//         +-->getStructValue ---+ <--+
//                 -or-              -or-
//             getSimpleValue <-- or -+
//                 -or-               |
//             getArrayValues --------+

QList<XMPValue *> Inspector::getDLNValues(const QList<PropInfo *> &propInfoList, const ImageSet & imageSet)
{
    // Set the error context.

    e.pushFileInfos(QFileInfo("DLN database"), QFileInfo());
    e.pushMsgType(IMessageType::InvalidValue);

    // Do logging.

    if (options.logging) write(logFile, "Getting property values from the DLN database\n");

    // Allocate a new list of values.

    QList<XMPValue *> values;

    // Get the database connection and create a query.

    QSqlDatabase db = QSqlDatabase::database(dlnDBName);
    QSqlQuery q(db);

    // Loop through the list of DLN properties and retrieve
    // their values from the DLN database.

    for (int i = 0; i < propInfoList.size(); i++)
    {
        // Get the next property value.

        PropInfo * info = propInfoList.at(i);

        // Do logging.

        if (options.logging) write(logFile, "Getting value for property: " + propPath(info) + "\n");

        // Get the query used to retrieve a DLN property value and
        // pass it to the query engine. Return an error if there is
        // no query.

        if (info->simpleInfo->dlnQuery == nullptr)
        {
            addResult(IResultType::FatalError, "Configuration error", "<p>DLN properties must have an SQL query. No query was provided for the property " + propPath(info) + ".</p>", propPath(info));
        }

        // Set the image set serial number in the query, execute the query,
        // and check for errors.

        QString query = *info->simpleInfo->dlnQuery;
        query = query.arg(imageSet.srl);
        if (!q.exec(query))
        {
            QSqlError e = q.lastError();
            QString msg;
            msg.append("Driver error: ").append(e.driverText()).append("***");
            msg.append("Database error: ").append(e.databaseText()).append("***");
            msg.append("Error code: ").append(e.nativeErrorCode());
            addResult(IResultType::FatalError, "Error querying DLN database", msg, info, nullptr, nullptr);
        }

        // Process the query results.

        if (q.next()) // Results returned.
        {
            // Create a new XMPValue.

            XMPValue * value = new XMPValue();

            // Get the value. If it is NULL, clear the string. According to
            // the QSqlQuery.isNull() documentation: "[F]or some drivers,
            // isNull() will not return accurate information until after
            // an attempt is made to retrieve data."

            QString dbValue = q.value(0).toString();
            if (q.isNull(0)) dbValue.clear();

            // Set the XMPValue. Note that this will return a NULL
            // XMPValue if dbValue is empty.

            if (!XMPValue::fromString(info->simpleInfo->type, dbValue, *value))
            {
                addResult(getResultType(e.testGroup),
                          "Invalid DLN value",
                          "",
                          propPath(info),
                          dbValue,
                          QString());
            }
            values.append(value);

            QString multiple;
            while (q.next())
            {
                // Returning more than one result is not necessarily a fatal error.
                // There are two reasons it might occur:
                //
                // * The query is incorrect. That is, it is grammatically correct
                //   but it does not retrieve the value from the DLN database. This
                //   is a configuration error. After debugging PropsDLN.xml, this
                //   should never happen.
                //
                // * The query is correct but the DLN contains multiple values.
                //   This is allowed by the DLNCC tool but it is not clear if it
                //   is correct. In some cases it makes sense, such as associating
                //   multiple tripods with an image set. In other cases it does not,
                //   such as associating multiple cameras with an image set.
                //
                // Because the second case is an allowable error, we will flag the
                // error and use the first returned value.

                // Build a string of additional values.

                QString dbValue = q.value(0).toString();
                if (q.isNull(0)) dbValue = "NULL";

                multiple.append("<p>&nbsp;&nbsp;&nbsp;");
                multiple.append(dbValue);
                multiple.append("</p>");
            }

            // Add a warning if any additional rows were returned.

            if (!multiple.isEmpty())
            {
                addResult(IResultType::Warning,
                          "Multiple DLN values",
                          "<p>The DLN database contains multiple values for the " +
                          propPath(info) +
                          " property:</p><p></p>" + multiple +
                          "<p></p><p>The DLN:Inspector will use the first value when inspecting metadata. This may cause unintended errors.</p>",
                          info,
                          value,
                          nullptr);
            }
        }
        else // No rows returned.
        {
            // Returning zero results is not necessarily a fatal error.
            // There are two reasons it might occur:
            //
            // * The query is incorrect. That is, it is grammatically correct
            //   but it does not retrieve the value from the DLN database. This
            //   is a configuration error. After debugging PropsDLN.xml, this
            //   should never happen.
            //
            // * The query is correct but the value is missing from the DLN
            //   database. This is an error in the capture process.
            //
            // Because the second case is an allowable error, we will add a
            // null XMPValue for the tag. (If we don't add a value, we get
            // a null pointer dereference in later processing, which assumes
            // we have XMPValues for all tags.)

            // Create a null XMPValue.

            XMPValue * nullValue = new XMPValue();
            values.append(nullValue);

            // Add an error.

            addResult(IResultType::CaptureError,
                      "DLN database missing data",
                      "<p>No data was found in the DLN database for the " +
                      propPath(info)  +
                      " property. Please enter the data with the DLN:CaptureContext tool. " +
                      "If you have already entered data for this property, please contact CHI.</p>",
                      info,
                      nullValue,
                      nullptr);
        }

        // Finish the query.

        q.finish();
    }

    // Do more logging.

    if (options.logging) write(logFile, "\n");

    // Reset the error context.

    e.popFileInfos();
    e.popMsgType();

    // Return the DLN property values.

    return values;
}

ImageSet::DocumentIds Inspector::getMediaManagementIds(const QFileInfo &fileInfo)
{
    QList<PropInfo *> propInfoList{PropInfo::docIdPropInfo, PropInfo::origDocIdPropInfo};
    QList<XMPValue *> values = this->getValues(propInfoList, fileInfo);
    
    QString docId;
    QString origDocId;
    if (values.size() == 2) {
        QString s = values.at(0)->toString();
        docId = s.mid(s.indexOf(":")+1);
        s = values.at(1)->toString();
        origDocId = s.mid(s.indexOf(":")+1);
    }
    qDeleteAll(values);

    // qDebug() << "FILE" << fileInfo.fileName() << "DOC" << docId << "OrigDOC" << origDocId;
    return ImageSet::DocumentIds(docId, origDocId);
}

QList<XMPValue *> Inspector::getValues(const QList<PropInfo *> &propInfoList, const QFileInfo & fileInfo)
{
    // Set the error context

    e.pushFileInfos(fileInfo.fileName(),  QFileInfo());
    e.pushMsgType(IMessageType::InvalidValue);

    // Do logging.

    if (options.logging) write(logFile, "Getting property values from file " + fileInfo.fileName() + "\n");

    // Allocate a new list of values.

    QList<XMPValue *> values;

    // Catch errors thrown by the XMP SDK. These
    // may occur in this or lower-level methods.

    SXMPFiles file;
    try
    {
        // Open the image file. kXMPFiles_OpenStrictly tells the
        // XMP SDK to be strict about reconciling different forms
        // of metadata. (Or so it appears. The XMP documentation is
        // anything but thorough. In some places, the documentation says
        // that kXMPFiles_OpenStrictly means to use the specified file
        // format. In others, it says this means to "Be strict about
        // locating XMP and reconciling with other forms." We're hoping
        // for the latter meaning...)

        XMP_OptionBits opts = kXMPFiles_OpenForRead | kXMPFiles_OpenStrictly;
        if (!file.OpenFile(fileInfo.canonicalFilePath().toStdString().c_str(), kXMP_UnknownFile, opts))
        {
            // If an error occurs when opening the file, add a fatal error.

            addResult(IResultType::FatalError, "XMP SDK exception", "<p>Unknown XMP SDK exception thrown while opening file: " + e.fileInfo1.canonicalFilePath() + "</p>", "");
        }

        // Get the XMP metadata from the file.

        SXMPMeta meta;
        if (!file.GetXMP(&meta))
        {
            addResult(IResultType::Warning, "No metadata found", "<p>No metadata found in file: " + e.fileInfo1.canonicalFilePath() + "</p>", "");
        }

        // For each property in the list, get the property value
        // from the XMP metadata.

//        std::unique_ptr<XMPValue> docId {getValue(meta, PropInfo::docIdPropInfo, nullptr, nullptr)};
//        std::unique_ptr<XMPValue> instId {getValue(meta, PropInfo::instIdPropInfo, nullptr, nullptr)};
//        qDebug() << "File" << fileInfo.fileName() << "DocID " << docId->toString() << "InstID" << instId->toString();

        for (int i = 0; i < propInfoList.size(); i++)
        {
            PropInfo* p = propInfoList.at(i);
            XMPValue* w = getValue(meta, p, nullptr, nullptr);
            // qDebug() << "File" << fileInfo.fileName() << "Prop " << p->getQName() << "has value" << w->toString();
            values.append(w);
        }

        // Close the file.

        file.CloseFile();
    }
    catch (XMP_Error err)
    {
        // If the XMP SDK throws an exception, we catch it here. (Note that
        // XMP SDK exceptions thrown more deeply in the code -- all of which
        // are in the properties methods -- are caught here as well.)

        // Close the file.

        file.CloseFile();

        // Add a fatal error for the exception.

        addResult(IResultType::FatalError,
                  "XMP SDK error",
                  "<p>An XMP SDK error occurred while retrieving image metadata.</p>"
                  "<p>File: " + e.fileInfo1.canonicalFilePath() + "</p>" +
                  "<p>XMP error ID: " + QString::number(err.GetID()) +
                  "</p></p>XMP error message: " + err.GetErrMsg() + "</p>", "");
    }

    // Do logging.

    if (options.logging) write(logFile, "\n");

    // Reset the error context.

    e.popFileInfos();
    e.popMsgType();

    // Return the property values.

    return values;
}

XMPValue * Inspector::getValue(SXMPMeta &meta, PropInfo *info, const XMP_StringPtr rootNS, const XMP_StringPtr parentPath)
{
    // Get the value for a single property.
    //
    // This method is called recursively to handle structures
    // (including nested structures).

    // Do logging.

    if (options.logging) write(logFile, "Getting value for property: " + propPath(info) + "\n");

    // Allocate variables for the value, XMP namespace, and XMP path.

    XMPValue * value;
    const char * ns;
    const char * path;
    TXMP_STRING_TYPE tmpPath;

    // Check for invalid parameters. The root namespace and the parent path
    // will both be null for top-level properties and both be non-null for
    // properties nested inside one or more levels of structures.

    if (((rootNS == nullptr) && (parentPath != nullptr)) ||
        ((rootNS != nullptr) && (parentPath == nullptr)))
    {
        addResult(IResultType::FatalError, "Programming error", "<p>In Inspector::getValue(), rootNS is NULL and parentPath is not NULL or vice versa. Please contact CHI.</p>", propPath(info));
    }

    // We now need to determine the namespace and path properties to use,
    // then call one of three methods:
    //
    //   getSimpleValue()  // calls XMP SDK to get property value
    //   getStructValue()  // calls getValue() for each property in structure
    //   getArrayValues()  // loops through array, calls getSimpleValue() or getStructValue()

    // The XMP SDK constructs an XPath-like path to access property values. There
    // are several annoying things about this:
    //
    // - The language is not XPath, so we cannot construct the path ourselves.
    //   Instead, we must call a method to construct the path.
    //
    // - The method that constructs a path cannot construct paths to top-level
    //   properties, so we must special-case these.
    //
    // - The path (apparently) does not contain the namespace of the root node.
    //   (It does contain the namespaces of other nodes in the path.) Instead,
    //   we must pass this namespace separately.

    // Get the namespace and the path.

    if (rootNS == nullptr)
    {
        // If this is a top-level property, just use the namespace and
        // the name of the property.

        ns = TXMPSTRING_TO_CSTRING(info->ns);
        path = TXMPSTRING_TO_CSTRING(info->name);
    }
    else
    {
        // If this is not a top-level property -- that is, it is nested
        // one or more levels deep in structures -- use the root namespace
        // passed (recursively) to this method. Construct the path by
        // calling an XMP function and passing the root namespace and
        // path to the parent (both passed recursively to this method) and
        // this property's namespace and name.

        ns = rootNS;
        SXMPUtils::ComposeStructFieldPath(rootNS, parentPath, TXMPSTRING_TO_CSTRING(info->ns), TXMPSTRING_TO_CSTRING(info->name), &tmpPath);
        path = TXMPSTRING_TO_CSTRING(tmpPath);
    }

    // Call a method to process the property. Which method to call
    // depends on whether the method is simple or a structure, and
    // whether the property is a single value or an array.

    if (!info->isArray)
    {
        if (info->isStruct)
        {
            value = getStructValue(meta, info, ns, path);
        }
        else
        {
            value = getSimpleValue(meta, info, ns, path);
        }
    }
    else
    {
        value = getArrayValues(meta, info, ns, path);
    }

    // Return the value.

    return value;
}

XMPValue  * Inspector::getSimpleValue(SXMPMeta &meta, PropInfo *info, const XMP_StringPtr rootNS, const XMP_StringPtr path)
{
    // Allocate an XMPValue for the property.

    XMPValue  * value = new XMPValue();
    value->simpleType = info->simpleInfo->type;

    // Call the XMP SDK to get the property value.

    switch (info->simpleInfo->type)
    {
        case XMPType::Boolean:
            value->isNull = !meta.GetProperty_Bool(rootNS, path, &value->b, nullptr);
            break;

        case XMPType::Datetime:
            value->ptr_dt = new XMP_DateTime();
            value->isNull = !meta.GetProperty_Date(rootNS, path, value->ptr_dt, nullptr);
            if (value->isNull)
            {
                delete value->ptr_dt;
                value->ptr_dt = nullptr;
            }
            else
            {
                if (!value->ptr_dt->hasTimeZone)
                {
                    // Exif does not store timezone information, so we should
                    // always get here. We check just to be sure, since Exif
                    // may change in the future.

                    // We need timezone information to compare datetimes, so
                    // we assume that the timezone on the camera is the same
                    // as the timezone on the computer. This is clearly not
                    // always true, but it's the best we can do.

                    value->ptr_dt->hasTimeZone = true;
                    int secFromUTC = QDateTime::currentDateTime().offsetFromUtc();
                    if (secFromUTC == 0)
                    {
                        // If the offset from UTC is 0, set the time zone
                        // hour and minutes to 0 and set time zone sign
                        // to UTC. (No, this doesn't make sense. + and -
                        // are signs; UTC is not a sign, but they use it.)

                        value->ptr_dt->tzHour = 0;
                        value->ptr_dt->tzMinute = 0;
                        value->ptr_dt->tzSign = kXMP_TimeIsUTC;
                    }
                    else
                    {
                        if (secFromUTC < 0)
                        {
                            // If the offset from UTC is less than 0, set
                            // the time zone sign to negative and convert
                            // the seconds to a positive number.

                            value->ptr_dt->tzSign = kXMP_TimeWestOfUTC;
                            secFromUTC *= -1;
                        }
                        else
                        {
                            // If the offset from UTC is greater than 0
                            // set the time zone sign to positive.

                            value->ptr_dt->tzSign = kXMP_TimeEastOfUTC;
                        }

                        // Convert the timezone offset from seconds to
                        // hours and minutes.

                        value->ptr_dt->tzHour = secFromUTC / 3600;
                        value->ptr_dt->tzMinute = (secFromUTC % 3600) / 60;
                    }
                }
            }

            break;

        case XMPType::Real:
            value->isNull = !meta.GetProperty_Float(rootNS, path, &value->d, nullptr);
            break;

        case XMPType::Integer:
            value->isNull = !meta.GetProperty_Int(rootNS, path, &value->i, nullptr);
            break;

        case XMPType::Rational:
        {
            // Get the string form of the rational number and break if there
            // is no value.

            TXMP_STRING_TYPE r;
            value->isNull = !meta.GetProperty(rootNS, path, &r, nullptr);
            if (value->isNull) break;

            // Split the string on the slash (/). Get the numerator and denominator.

            // Note that 0/0 is a common value in exifEX:LensSpecification and
            // other tags, in spite of the fact that it is not a valid rational
            // number. Because of this, we assume that there may be other invalid
            // or incorrectly formatted rational numbers in the wild. When we
            // encounter one of these, we issue a warning and attempt to set the
            // numerator and denominator, using 0 if we don't have a value. Because
            // we never actually divide the numerator by the denominator, this
            // is safe to do.

            bool ok;
            QString msg;

            // Initialize the numerator and denominator to 0.

            value->r.numerator = 0;
            value->r.denominator = 0;

            // Parse the string and set the numerator and denominator.

            QStringList l = (TXMPSTRING_TO_QSTRING(r)).split("/");
            if (l.size() != 2)
            {
                msg = QString("Incorrectly formatted rational number.");
                ok = false;
            }
            else
            {
                value->r.numerator = l[0].toInt(&ok);
                if (!ok)
                {
                    msg = QString("Invalid numerator.");
                }
                else
                {
                    value->r.denominator = l[1].toInt(&ok);
                    if (!ok)
                    {
                        msg = QString("Invalid denominator.");
                    }
                    else if (value->r.denominator == 0)
                    {
                        ok = false;
                        msg = QString("Denominator is 0.");
                    }
                }
            }

            // If there is an error, add a warning.

            if (!ok)
            {
                addResult(IResultType::Warning,
                          "Invalid rational number",
                          msg,
                          PropInfo::getQName(rootNS, path),
                          TXMPSTRING_TO_QSTRING(r),
                          QString());
            }
            break;
        }

        case XMPType::Text:
        {
            value->ptr_str = NEW_TXMP_STRING_TYPE;
            value->isNull = !meta.GetProperty(rootNS, path, value->ptr_str, nullptr);
            if (value->isNull)
            {
                delete value->ptr_str;
                value->ptr_str = nullptr;
            }
            break;
        }

        case XMPType::Unknown:
        default:
            value->isNull = true;
            break;
    }

    // Return the XMPValue.

    return value;
}

XMPValue * Inspector::getStructValue(SXMPMeta &meta, PropInfo *info, const XMP_StringPtr rootNS, const XMP_StringPtr path)
{
    // Push the current property information on the stack. We do this so we can
    // display the full property path in error messages.

    propStack.push(info);

    // Allocate a new XMPValue. This contains a pointer to a QList
    // that contains the XMPValues of the properties in the structure.

    XMPValue * value = new XMPValue();
    value->unionType = XMPUnionType::Struct;
    value->isNull = false;
    value->ptr_struct = new QList<XMPValue *>();

    // For each property in the structure, call getValue(). Notice that we
    // pass the PropInfo for the individual property, as well as the root
    // namespace and the path to this structure.

    for (int i = 0; i < info->structInfo->propInfoList->size(); i++)
    {
        value->ptr_struct->append(getValue(meta, info->structInfo->propInfoList->at(i), rootNS, path));
    }

    // If all the values in the struct are null, we consider the
    // the struct itself to be null. First, check if the values are null.

    bool structIsNull = true;
    for (int i = 0; i < info->structInfo->propInfoList->size(); i++)
    {
        if (!value->ptr_array->at(i)->isNull)
        {
            structIsNull = false;
            break;
        }
    }

    // If all values are null, delete the struct value. We use
    // delete so the XMPValue destructor will deallocate memory
    // correctly. Allocate a new value (which has isNull set to
    // true by default) and set its type.

    if (structIsNull)
    {
        delete value;
        value = new XMPValue();
        value->unionType = XMPUnionType::Struct;
    }

    // Pop the current property information off the stack.

    propStack.pop();

    // Return the XMPValue for the structure.

    return value;
}

XMPValue * Inspector::getArrayValues(SXMPMeta & meta, PropInfo *info, const XMP_StringPtr rootNS, const XMP_StringPtr path)
{
    // Allocate a new XMPValue. This contains a pointer to a QList
    // that holds an array of XMPValues.

    XMPValue * value = new XMPValue();
    value->unionType = XMPUnionType::Array;

    // We don't know how big the array is, so we just keep getting
    // values until we fail. Note that XMP property arrays are 1-based.

    bool done = false;
    XMP_Index i = 1;
    TXMP_STRING_TYPE itemPath;
    XMPValue * itemValue;

    while (!done)
    {
        // Get the path to the array item.

        SXMPUtils::ComposeArrayItemPath(rootNS, path, i, &itemPath);

        // Call getSimpleValue() or getStructValue() to get the item value.

        if (info->isStruct)
        {
            itemValue = getStructValue(meta, info, rootNS, TXMPSTRING_TO_CSTRING(itemPath));
        }
        else
        {
            itemValue = getSimpleValue(meta, info, rootNS, TXMPSTRING_TO_CSTRING(itemPath));
        }

        // If the item value is not null, store it in the QList for the
        // array and increment the counter. Otherwise, stop.

        if (!itemValue->isNull)
        {
            // If this is the first value in the array, set isNull to
            // false and allocate the QList for the array.

            if (i == 1)
            {
                value->isNull = false;
                value->ptr_array = new QList<XMPValue *>();
            }

            // Add the value to the QList.

            value->ptr_array->append(itemValue);

            // Increment the array counter.

            i++;
        }
        else
        {
            done = true;
        }
    }

    // Return the XMPValue for the array.

    return value;
}

/********************************************************************/
/*                                                                  */
/*  Validate property values                                        */
/*                                                                  */
/********************************************************************/

// These methods have the following call hierarchy:
//
// validateValues
//   |
//   +--> validateValue
//          |
//          +--> evalRestriction <--+
//                 |                |
//                 +------ or ------+
//                 |
//               -or-
//                 |
//                 +--> evalPredicate

void Inspector::validateValues(const QList<PropInfo *> &propInfoList, const QFileInfo &fileInfo, const QList<XMPValue *> &values)
{
    // Set the error context.

    e.pushFileInfos(fileInfo, QFileInfo());

    // Log the test.

    if (options.logging) write(logFile, "Validating values for file " + fileInfo.fileName() + ".\n\n");

    // Cycle through the list and check if the property has a required
    // value or needs other validation.

    for (int i = 0; i < propInfoList.size(); i++)
    {
        // Get the PropInfo and see if we compare restrictions. For more
        // information, search for "DontCompare" in propertyinfo.cpp.


        PropInfo * info = propInfoList.at(i);
        if (!(info->compare & IComparisonType::Restrictions))
        {
            if (options.logging) write(logFile, "Restrictions do not apply to " + propPath(info) + ". Not validating.\n\n");
            continue;
        }

        // Get the value.

        XMPValue * value = values.at(i);

        // Check if the value is null. If so, continue to the
        // next property.

        if (value->isNull)
        {
            if (options.logging) write(logFile, propPath(info) + " is NULL. Not validating.\n\n");
            continue;
        }

        validateValue(info, value);
    }

    // Reset the error context.

    e.popFileInfos();
}

void Inspector::validateValue(const PropInfo *info, const XMPValue * value)
{
    // Set the error context.

    e.pushMsgType(IMessageType::Restriction);

    // Log the test.

    if (options.logging) write(logFile, "Validating property: " + propPath(info) + "\n");

    // Validate the value.
    //
    // WARNING! It is not currently possible to validate simple properties
    // that are inside a structure. This is a design decision based on the
    // fact that we have no current need to do this. To add it in the future,
    // add an else clause to the "if (!info->isStruct)" statement and place
    // a loop inside this statement to call validateValue for each field
    // in the structure. Probably need to worry about arrays of structures
    // as well...

    bool ok(true);
    if (!info->isStruct)
    {
        if (info->simpleInfo->hasRestrictions)
        {
            if (options.logging) write(logFile, "Evaluating restriction: " + info->simpleInfo->restrictionSet->restriction->toString(info->getQName(), false) + "\n");

            if (!info->isArray)
            {
                ok = evalRestriction(info, value, info->simpleInfo->restrictionSet->restriction);
            }
            else
            {
                for (int i = 0; i < value->ptr_array->size(); i++)
                {
                    // Array-valued properties are tested against array-valued
                    // restrictions on a pair-wise basis. All elements must
                    // satisfy the restriction for the array to satisfy the
                    // restriction.

                    // For example, a required value is expressed as the
                    // restriction (property = value). Only the value {0,255}
                    // satisfies the restriction (property = {0,255}). As
                    // another example, {1,2,8} satisfies:
                    //
                    //   ((property < {2,3,4}) OR (property > {5,6,7})).
                    //
                    // because each of the following pair-wise comparisons
                    // is true:
                    //
                    //    (1 < 2) OR (1 > 5)
                    //    (2 < 3) OR (2 > 6)
                    //    (8 < 4) OR (8 > 7)

                    if (!(ok = evalRestriction(info, value, info->simpleInfo->restrictionSet->restriction, i)))
                    {
                        break;
                    }
                }
            }
        }
    }

    if (ok)
    {
        if (options.logging)
        {
            // Log successful tests.

            write(logFile, propPath(info) + " value: " +  value->toString() + " ... OK \n");
        }
    }
    else
    {
        // Add a result for failed tests.

        // Add the restriction-specific message if there is one.

        QString msg;
        if (info->simpleInfo->restrictionSet->message != nullptr)
        {
            msg.append(*(info->simpleInfo->restrictionSet->message));
        }

        // Add the result.

        addResult(info->simpleInfo->restrictionSet->resultType,
                  "Does not satisfy restrictions",
                  msg,
                  propPath(info),
                  value->toString(),
                  info->simpleInfo->restrictionSet->restriction->toString(info->getQName(), true));
    }

    // Reset the error context.

    e.popMsgType();
}

bool Inspector::evalRestriction(const PropInfo * info, const XMPValue * value, const Restriction * restriction)
{
    if (restriction->isExpr)
    {
        // Evaluate the expression for a pair of array values.

        IErrorCode::Enum code = evalPredicate(info, value, restriction->op, restriction->value);
        // Return true or false.

        return (code == IErrorCode::OK) ? true : false;
    }
    else
    {
        // Recursively evaluate the nested restrictions.

        bool ret1 = evalRestriction(info, value, restriction->restriction1);
        bool ret2 = evalRestriction(info, value, restriction->restriction2);

        // AND or OR the returned values and return the result.

        if (restriction->useAnd)
            return ret1 && ret2;
        else
            return ret1 || ret2;
    }
}

bool Inspector::evalRestriction(const PropInfo * info, const XMPValue * value, const Restriction * restriction, int index)
{
    // Array values are evaluated in pair-wise fashion. That is, the first
    // entry in the value array must satisfy the first entry in the expression
    // array, the second entry in the value array ... and so on.

    // The only difference between this method and the previous method
    // is that this method passes the index of the array entry to use
    // when evaluating expressions.

    if (restriction->isExpr)
    {
        // Compare the size of the array in the expression with the size
        // of the array being evaluated. If they are different, add a
        // result and return false.

        // Because PropInfo::initPropInfoList() checks that all expressions
        // in a restriction use arrays with the same size, this will either
        // succeed or fail for all expressions. As a result, any mismatch
        // in array size is guaranteed to fail for the restriction as a whole.

        // Requiring all expressions in a restriction to use the same array
        // size is necessary to avoid false positives. For example, suppose
        // we have the restriction ((prop < {0, 1}) OR (prop > {5, 6, 7})). The
        // array {6, 7, 8} would satisfy this restriction because, even though
        // it fails the first expression due to size mismatch, it satisfies
        // the second expression.

        // We assume this method is called only for array values and therefore
        // do not check the isArray flag.

        if (value->ptr_array->size() != restriction->value->ptr_array->size())
        {
            return false;
        }

        // Evaluate the expression for a pair of array values.

        IErrorCode::Enum code = evalPredicate(info, value->ptr_array->at(index), restriction->op, restriction->value->ptr_array->at(index));

        // Return true or false.

        return (code == IErrorCode::OK) ? true : false;
    }
    else
    {
        // Recursively evaluate the nested restrictions.

        bool ret1 = evalRestriction(info, value, restriction->restriction1, index);
        bool ret2 = evalRestriction(info, value, restriction->restriction2, index);

        // AND or OR the returned values and return the result.

        if (restriction->useAnd)
            return ret1 && ret2;
        else
            return ret1 || ret2;
    }
}

/********************************************************************/
/*                                                                  */
/*  Compare property values                                         */
/*                                                                  */
/********************************************************************/

// These methods have the following call hierarchy:
//
// compareValueLists
//   |
//   +-->specialHandling
//   |
//   +-->compareValues <--------------+
//         |                          |
//         +-->checkNulls             |
//         |                          |
//         +-->compareStructValues ---+ <--+
//                   -or-                 -or-
//             compareArrayValues ---------+
//                   -or-                 -or-
//             compareSimpleValues <-------+
//               |
//               +-->evalPredicate

void Inspector::compareValueLists(const QList<PropInfo *> &propInfoList, const QFileInfo & fileInfo1, const QList<XMPValue *>& values1, const QFileInfo & fileInfo2, const QList<XMPValue *> &values2, IComparisonType::Enum comparisonType)
{
    // Set the error context.

    e.pushFileInfos(fileInfo1, fileInfo2);
    e.pushMsgType(IMessageType::CompareValues);

    // Log the test.

    if (options.logging)
    {
        write(logFile, "Comparing property values in files " + fileInfo1.fileName() + " and " + fileInfo2.fileName() + ".\n\n");
    }

    // Set the property information list and value lists
    // for special handling.

    this->currPropInfoList = propInfoList;
    this->currValues1 = values1;
    this->currValues2 = values2;

    // Compare the values of the properties in the property info list.

    for (int i = 0; i < propInfoList.size(); i++)
    {
        PropInfo * info = propInfoList.at(i);

        // If this property is not supposed to be compared, go to the next property.
        // For more information, search for "PropElementToken::Compare" in
        // PropertyInfo.

        if (!(info->compare & comparisonType))
        {
            if (options.logging) write(logFile, "Not comparing property " + propPath(info) + "\n");
            continue;
        }

        // If this property is handled specially, go to the next property.

        if (specialHandling(info, fileInfo1, values1.at(i), fileInfo2, values2.at(i)))
        {
            if (options.logging) write(logFile, "\n");
            continue;
        }

        // If the property is handled normally, compare the property values.

        compareValues(info, values1.at(i), values2.at(i));
    }

    // Reset the error context.

    e.popFileInfos();
    e.popMsgType();
}

void Inspector::compareValues(PropInfo *info, const XMPValue * value1, const XMPValue * value2)
{
    if (options.logging) write(logFile, "Comparing property " + propPath(info) + "\n");

    // Check the null values. If the nulls don't match or
    // both are null, then return.

    if (!checkNulls(info, value1, value2))
    {
        if (options.logging) write(logFile, "\n");
        return;
    }

    // Compare the property values.

    if (!info->isArray)
    {
        if (info->isStruct)
          {
              compareStructValues(info, value1, value2);
          }
          else
          {
              compareSimpleValues(info, value1, value2);
          }
    }
    else
    {
       compareArrayValues(info, value1, value2);
    }

    if (options.logging) write(logFile, "\n");
}

void Inspector::compareSimpleValues(PropInfo *info, const XMPValue * value1, const XMPValue * value2)
{
    // When compareSimpleValues() is called from checkRequiredValue(), the
    // second argument must be the required value. This is to make sure
    // that error messages and logging are labeled correctly.

    // Compare the values.

    IErrorCode::Enum code = evalPredicate(info, value1, IOperator::EQ, value2);

    // Handle the result.

    if (code == IErrorCode::OK)
    {
        if (options.logging)
        {
            QString value1Label = IMessageType::getValueLabel(e.msgType, 1);
            QString value2Label = IMessageType::getValueLabel(e.msgType, 2);
            write(logFile, value1Label + ": " + value1->toString() + "  " +
                           value2Label + ": " + value2->toString() +
                           " ... OK\n");
        }
    }
    else
    {
        addResult(getResultType(e.testGroup), code, info, value1, value2);
    }
}

void Inspector::compareStructValues(PropInfo *info, const XMPValue * value1, const XMPValue * value2)
{
    // Do logging.

    if (options.logging) write(logFile, "\nComparing structure values.\n\n");

    // Push the current property information on the stack. We do this so we can
    // display our position in error messages.

    propStack.push(info);

    // Process the properties in the current property (structure).

    for (int i = 0; i < info->structInfo->propInfoList->size(); i++)
    {
        compareValues(info->structInfo->propInfoList->at(i), value1->ptr_struct->at(i), value2->ptr_struct->at(i));
    }

    // Pop the current property information off the stack.

    propStack.pop();

    // Do logging.

    if (options.logging) write(logFile, "\nDone comparing structure values.\n\n");
}

void Inspector::compareArrayValues(PropInfo *info, const XMPValue * value1, const XMPValue * value2)
{
    // Do logging.

    if (options.logging) write(logFile, "Comparing array values.\n");

    // Check that the array sizes match.

    if (value1->ptr_array->size() != value2->ptr_array->size())
    {
        addResult(getResultType(e.testGroup), IErrorCode::DiffArraySizes, info, value1, value2);
        return;
    }
    else
    {
        if (options.logging)
        {
            write(logFile, "Array 1 size: " + QString::number(value1->ptr_array->size()) +
                                           "  Array 2 size: " + QString::number(value2->ptr_array->size()) +
                                           " ... OK\n");
        }
    }

    // This is ugly. We use the same PropInfo to describe an array and
    // the individual members of the array, but we use different XMPValues
    // to contain the array itself and the members of the array. This causes
    // a problem when processing the array members, as the PropInfo says
    // that these are arrays as well. (They aren't.) As a workaround,
    // make a copy of the PropInfo and change its isArray flag.

    PropInfo tInfo = *info;
    tInfo.isArray = false;

    // Compare the values in the array.

    for (int i = 0; i < value1->ptr_array->size(); i++)
    {
        if (info->isStruct)
        {
            compareStructValues(&tInfo, value1->ptr_array->at(i), value2->ptr_array->at(i));
        }
        else
        {
            compareSimpleValues(&tInfo, value1->ptr_array->at(i), value2->ptr_array->at(i));
        }
    }

}

bool Inspector::checkNulls(const PropInfo *info, const XMPValue * value1, const XMPValue * value2)
{
    // Check if one property value is null and the other is not.

    if (value1->isNull != value2->isNull)
    {
        addResult(getResultType(e.testGroup), IErrorCode::DiffIsNull, info, value1, value2);
        return false;
    }
    else if (options.logging)
    {
        // checkNulls is called from compareValues(), checkRequiredValue(),
        // and specialHandling(). The labels we use in the log file
        // depend on which method it is called from.

        QString value1Label = IMessageType::getValueLabel(e.msgType, 1);
        QString value2Label = IMessageType::getValueLabel(e.msgType, 2);
        write(logFile, value1Label + ": " + value1->isNullToString() + "  " +
                       value2Label + ": " + value2->isNullToString() +
                       " ... OK\n");
    }

    // If both property values are null, skip the property.

    return !(value1->isNull && value2->isNull);
}

/********************************************************************/
/*                                                                  */
/*  Evaluate predicates                                             */
/*                                                                  */
/********************************************************************/

// These methods have the following call hierarchy:
//
// evalPredicate
//   |
//   +-->evalBooleanPred
//   |
//   +-->evalDatetimePred
//   |
//   +-->evalIntegerPred
//   |
//   +-->evalRationalPred
//   |
//   +-->evalDoublePred
//   |
//   +-->evalTextPred

IErrorCode::Enum Inspector::evalPredicate(const PropInfo *info, const XMPValue * value1, IOperator::Enum op, const XMPValue * value2)
{
    // Compare the values.

    switch (info->simpleInfo->type)
    {
        case XMPType::Boolean:
            return evalBooleanPred(value1->b, op, value2->b);

        case XMPType::Datetime:
            return evalDatetimePred(*value1->ptr_dt, op, *value2->ptr_dt);

        case XMPType::Integer:
            return evalIntegerPred(value1->i, op, value2->i);

        case XMPType::Rational:
            return evalRationalPred(value1->r, op, value2->r);

        case XMPType::Real:
            return evalDoublePred(value1->d, op, value2->d);

        case XMPType::Text:
            return evalTextPred(*value1->ptr_str, op, *value2->ptr_str);

        case XMPType::Unknown:
        default:
            addResult(IResultType::FatalError, "Invalid data type", "<p>Programming error. Unknown data type in PropInfo for " + info->getQName() + ".</p>", propPath(info));
            break;
    }

    // Return OK.

    return IErrorCode::OK;
}

IErrorCode::Enum Inspector::evalBooleanPred(bool value1, IOperator::Enum op, bool value2)
{
    switch (op)
    {
        case IOperator::EQ:
            if (value1 == value2)
                return IErrorCode::OK;
            else
                return IErrorCode::Unequal;

        case IOperator::NE:
            if (value1 != value2)
                return IErrorCode::OK;
            else
                return IErrorCode::Equal;

        case IOperator::LE:
        case IOperator::LT:
        case IOperator::GE:
        case IOperator::GT:
            return IErrorCode::OpNotSupported;

        case IOperator::Unknown:
        default:
            return IErrorCode::UnknownOperator;
    }
}

IErrorCode::Enum Inspector::evalDatetimePred(const XMP_DateTime & value1, IOperator::Enum op, const XMP_DateTime & value2)
{
    // Check that both date-times either have a date or don't
    // have a date.

    if (value1.hasDate != value2.hasDate)
    {
        return IErrorCode::Unequal;
    }

    // Check that both date-times either have a time zone or
    // don't have a time zone. It is not possible to compare
    // a time with a time zone to a time without a time zone.

    if (value1.hasTimeZone != value2.hasTimeZone)
    {
        return IErrorCode::Unequal;
    }

    // If the date-times do not have a date, make sure they
    // both have times.

    if (!value1.hasDate && (!(value1.hasTime && value2.hasTime)))
    {
        return IErrorCode::Unequal;
    }


    // Get the difference between the date-times.

    qint64 diff = dateTimeDiff(value1, value2);

    /* The following check for the difference between the date-times
       has been disabled. Carla says:
        "It was originally created when Inspector was a separate tool,
         and was used a check to aid in determining if you had a correct
         image set associated with the DLN data.  However, with the new system
         it doesn’t make sense and could cause confusion.  For PG IBs’s for
         example these could be captured over several days for the same subject."
    */

//    switch (op)
//    {
//        case IOperator::EQ:
//            if (abs(diff) <= options.epsilon)
//                return IErrorCode::OK;
//            else
//                return IErrorCode::DateDiffTooBig;
//            break;
//
//        case IOperator::NE:
//            if (abs(diff) > options.epsilon)
//                return IErrorCode::OK;
//            else
//                return IErrorCode::Equal;
//
//        case IOperator::LE:
//        case IOperator::GE:
//            if (abs(diff) <= options.epsilon)
//                return IErrorCode::OK;
//            break;
//
//        case IOperator::LT:
//        case IOperator::GT:
//            if (abs(diff) <= options.epsilon)
//                return IErrorCode::Equal;
//            break;
//
//        case IOperator::Unknown:
//        default:
//            return IErrorCode::UnknownOperator;
//    }


    switch (op)
    {
        case IOperator::LE:
        case IOperator::LT:
            if (diff < 0)
                return IErrorCode::OK;
            else
                return IErrorCode::GreaterThan;

        case IOperator::GE:
        case IOperator::GT:
            if (diff > 0)
                return IErrorCode::OK;
            else
                return IErrorCode::LessThan;

        default:
            // Here for forward compatibility. We never reach it.

            break;
    }

    // Here to keep the compiler happy. We never reach it.

    return IErrorCode::OK;
}

IErrorCode::Enum Inspector::evalIntegerPred(XMP_Int32 value1, IOperator::Enum op, XMP_Int32 value2)
{
    switch (op)
    {
        case IOperator::EQ:
            return (value1 == value2) ? IErrorCode::OK : IErrorCode::Unequal;

        case IOperator::NE:
            return (value1 != value2) ? IErrorCode::OK : IErrorCode::Equal;

        case IOperator::LE:
        case IOperator::GE:
            if (value1 == value2)
                return IErrorCode::OK;
            break;

        case IOperator::LT:
        case IOperator::GT:
            if (value1 == value2)
                return IErrorCode::Equal;
            break;

        case IOperator::Unknown:
        default:
            return IErrorCode::UnknownOperator;
    }

    switch (op)
    {
        case IOperator::LE:
        case IOperator::LT:
            if (value1 < value2)
                return IErrorCode::OK;
            else
                return IErrorCode::GreaterThan;

        case IOperator::GE:
        case IOperator::GT:
            if (value1 > value2)
                return IErrorCode::OK;
            else
                return IErrorCode::LessThan;

        default:
            // Here for forward compatibility. We never reach it.

            break;
    }

    // Here to keep the compiler happy. We never reach it.

    return IErrorCode::OK;
}

IErrorCode::Enum Inspector::evalRationalPred(const XMPValue::rational & value1, IOperator::Enum op, const XMPValue::rational & value2)
{
    // Do quick tests for the equals operator.

    if (op == IOperator::EQ)
    {
        // Return if the numerators and denominators are the same.

        if ((value1.numerator == value2.numerator) &&
            (value1.denominator == value2.denominator))
            return IErrorCode::OK;

        // Return if both have a numerator of 0.

        if ((value1.numerator == 0) && (value2.numerator == 0))
            return IErrorCode::OK;
    }

    // Reduce both fractions.

    int n1 = value1.numerator;
    int d1 = value1.denominator;
    int n2 = value2.numerator;
    int d2 = value2.denominator;

    reduce(n1, d1);
    reduce(n2, d2);

    // Test for equality / inequality.

    switch (op)
    {
        case IOperator::EQ:
            // Since both fractions are reduced, we can directly
            // compare their numerators and denominators for equality.

            return((n1 == n2) && (d1 == d2)) ? IErrorCode::OK : IErrorCode::Unequal;

        case IOperator::NE:
            return ((n1 != n2) || (d1 != d2)) ? IErrorCode::OK : IErrorCode::Equal;

        case IOperator::LE:
        case IOperator::GE:
            if ((n1 == n2) && (d1 == d2))
                return IErrorCode::OK;
            break;

        case IOperator::LT:
        case IOperator::GT:
            if ((n1 == n2) && (d1 == d2))
                return IErrorCode::Equal;
            break;

        case IOperator::Unknown:
        default:
            return IErrorCode::UnknownOperator;
    }

    // If we made it this far, we know that (a) the fractions
    // are not equal and (b) we have eliminated the EQ and NE
    // operators. This means we need to test greater/less than,
    // for which we need a common denominator. We will use the
    // least common multiple of the two denominators, which is
    // their product divided by their greatest common divisor.

    int gcd = greatestCommonDivisor(d1, d2);

    // We calculate new fractions as follows:
    //
    // 1) Factor the denominators into gcd and non-gcd parts:
    //
    //    n1         n1         n2         n2
    //    -- => ------------ ,  -- => ------------
    //    d1    (d1/gcd)*gcd    d2    (d2/gcd)*gcd
    //
    // 2) Multiply each fraction by the other fraction's
    //    non-gcd part:
    //
    //         n1        (d2/gcd)    n1*(d2/gcd)
    //    ------------ * -------- => ----------- ,
    //    (d1/gcd)*gcd   (d2/gcd)    (d1/gcd)*d2
    //
    //         n2        (d1/gcd)    n2*(d1/gcd)
    //    ------------ * -------- => -----------
    //    (d2/gcd)*gcd   (d1/gcd)    (d1/gcd)*d2
    //
    // Now compare the numerators:
    //
    //    n1*(d2/gcd), n2*(d1/gcd)
    //
    // Because there is a risk of integer overflow, perform
    // the division before the multiplication and check that
    // it is safe to multiply before actually doing so. Also,
    // use a long to hold the product, although this might
    // be the same width as an int.

    bool ok1, ok2;
    long n1Long, n2Long;
    n1Long = safeProduct(n1, d2/gcd, ok1);
    n2Long = safeProduct(n2, d1/gcd, ok2);

    if (ok1 && ok2)
    {
        switch (op)
        {
            case IOperator::LE:
            case IOperator::LT:
                if (n1Long < n2Long)
                    return IErrorCode::OK;
                else
                    return IErrorCode::GreaterThan;

            case IOperator::GE:
            case IOperator::GT:
                if (n1Long > n2Long)
                    return IErrorCode::OK;
                else
                    return IErrorCode::LessThan;

            default:
                // Here for forward compatibility. We never reach it.

                break;
        }
    }
    else
    {
        // If we can't do the comparison with integers,
        // perform it with real numbers and hope it isn't
        // too close...

        // To simplify things, just multiple the first fraction
        // by d2/d2 and the second by d1/d1 and compare the
        // numerators.

        double n1Double = n1*d2;
        double n2Double = n2*d1;

        switch (op)
        {
            case IOperator::LE:
            case IOperator::LT:
                if (doubleLessThan(n1Double, n2Double, 0.00001))
                    return IErrorCode::OK;
                else
                    return IErrorCode::GreaterThan;

            case IOperator::GE:
            case IOperator::GT:
                // Note reversed positions of n1Double and n2Double.

                if (doubleLessThan(n2Double, n1Double, 0.00001))
                    return IErrorCode::OK;
                else
                    return IErrorCode::LessThan;

            default:
                // Here for forward compatibility. We never reach it.

                break;
        }
    }

    // Here to keep the compiler happy. We never reach it.

    return IErrorCode::OK;
}

IErrorCode::Enum Inspector::evalDoublePred(double value1, IOperator::Enum op, double value2)
{
    switch (op)
    {
        case IOperator::EQ:
            if (doubleEquals(value1, value2, 0.00001))
                return IErrorCode::OK;
            else
                return IErrorCode::Unequal;
            break;

        case IOperator::NE:
            if (!doubleEquals(value1, value2, 0.00001))
                return IErrorCode::OK;
            else
                return IErrorCode::Equal;

        case IOperator::LE:
        case IOperator::GE:
            if (doubleEquals(value1, value2, 0.00001))
                return IErrorCode::OK;
            break;

        case IOperator::LT:
        case IOperator::GT:
            if (doubleEquals(value1, value2, 0.00001))
                return IErrorCode::Equal;
            break;

        case IOperator::Unknown:
        default:
            return IErrorCode::UnknownOperator;
    }

    switch (op)
    {
        case IOperator::LE:
        case IOperator::LT:
            if (doubleLessThan(value1, value2, 0.00001))
                return IErrorCode::OK;
            else
                return IErrorCode::GreaterThan;

        case IOperator::GE:
        case IOperator::GT:
            // Note reversed positions of value1 and value2.

            if (doubleLessThan(value2, value1, 0.00001))
                return IErrorCode::OK;
            else
                return IErrorCode::LessThan;

        default:
            // Here for forward compatibility. We never reach it.

            break;
    }

    // Here to keep the compiler happy. We never reach it.

    return IErrorCode::OK;
}

IErrorCode::Enum Inspector::evalTextPred(const TXMP_STRING_TYPE & value1, IOperator::Enum op, const TXMP_STRING_TYPE & value2)
{
    // Perform a case-sensitive comparison.

    int compare = value1.compare(value2);

    switch (op)
    {
        case IOperator::EQ:
            if (compare == 0)
                return IErrorCode::OK;
            else
                return IErrorCode::Unequal;

        case IOperator::NE:
            if (compare != 0)
                return IErrorCode::OK;
            else
                return IErrorCode::Equal;

        case IOperator::LE:
        case IOperator::GE:
            if (compare == 0)
                return IErrorCode::OK;
            break;

        case IOperator::LT:
        case IOperator::GT:
            if (compare == 0)
                return IErrorCode::Equal;
            break;

        case IOperator::Unknown:
        default:
            return IErrorCode::UnknownOperator;
    }

    switch (op)
    {
        case IOperator::LE:
        case IOperator::LT:
            if (compare < 0)
                return IErrorCode::OK;
            else
                return IErrorCode::GreaterThan;

        case IOperator::GE:
        case IOperator::GT:
            if (compare > 0)
                return IErrorCode::OK;
            else
                return IErrorCode::LessThan;

        default:
            // Here for forward compatibility. We never reach it.

            break;
    }

    // Here to keep the compiler happy. We never reach it.

    return IErrorCode::OK;
}

/********************************************************************/
/*                                                                  */
/*  Predicate utilities                                             */
/*                                                                  */
/********************************************************************/

void Inspector::reduce(int & num, int & den)
{
    // If the numerator or denominator is 0, we can't reduce the
    // fraction further, so just return.

    if ((num == 0) || (den == 0)) return;

    // Divide the numerator and the denominator by their greatest common
    // divisor. The result is a reduced fraction.

    int gcd = greatestCommonDivisor(num, den);
    num /= gcd;
    den /= gcd;
}

int Inspector::greatestCommonDivisor(const int v1, const int v2)
{
    // Use the Euclidean algorithm (http://en.wikipedia.org/wiki/Euclidean_algorithm)
    // to find the greatest common divisor.

    int larger = v1 >= v2 ? v1 : v2;
    int smaller = v1 < v2 ? v1 : v2;

    while (int rem = larger % smaller != 0)
    {
        larger = smaller;
        smaller = rem;
    }

    return smaller;
}

long Inspector::safeProduct(const int value1, const int value2, bool & ok)
{
    long v1 = value1, v2 = value2;

    // Check that an overflow won't occur. Note that:
    //
    //    abs(v1*v2) <= max implies abs(v1) <= max/abs(v2)

    ok = (abs(v1) <= std::numeric_limits<long>::max()/abs(v2));

    // Return the product or 0.

    return ok ? v1*v1 : 0;
}

bool Inspector::doubleEquals(const double d1, const double d2, const double epsilon)
{
    // If d1 and d2 happen to be exactly equal, we're done. In our
    // case, this is possible, since any floating point numbers
    // probably derive from identical hardware and software chains.

    if (d1 == d2) return true;

    // If d1 and d2 are not exactly equal, return whether they are
    // within a small relative amount of each other. That is, is their
    // difference small relative to their size?

    double absD1 = std::abs(d1);
    double absD2 = std::abs(d2);

    double max = absD1 > absD2 ? absD1 : absD2;

    return (std::abs(d1 - d2)/max < epsilon);
}

bool Inspector::doubleLessThan(const double v1, const double v2, const double epsilon)
{
    // Return false if the numbers are "equal".

    if (doubleEquals(v1, v2, epsilon)) return false;

    // Return the result of a normal comparison.

    return v1 < v2;
}

qint64 Inspector::dateTimeDiff(const XMP_DateTime &dt1, const XMP_DateTime &dt2)
{
    // Create QDateTimes that use the UTC time zone for each date.

    QDateTime qdt1 = createUTCQDateTime(dt1);
    QDateTime qdt2 = createUTCQDateTime(dt2);

    // Return dt1 - dt2 in seconds.

    return qdt2.secsTo(qdt1);
}

QDateTime Inspector::createUTCQDateTime(const XMP_DateTime &dt)
{
    // This method creates a QDateTime object from an XMP_DateTime object.
    // The QDateTime object can then be used to do date/time math.

    // Create QDate and QTime objects that correspond to the XMP_DateTime.
    // If there is no date, use the current date. If there is no time, use
    // midnight.

    QDate qd = dt.hasDate ? QDate(dt.year, dt.month, dt.day) : QDate::currentDate();
    QTime qt = dt.hasTime ? QTime(dt.hour, dt.minute, dt.second) : QTime(0, 0, 0);

    // Create a QDateTime. If there is a time zone, use it.

    if (dt.hasTimeZone)
    {
        // The XMP SDK and Qt both describe a time zone in hours and minutes
        // from UTC. Unfortunately, they use different ranges. Qt allows values
        // between -14 and +14 hours, expressed as seconds. The XMP SDK allows
        // values between -23:59 and +23:59; to further confuse things, it
        // stores the absolute values of the hours and minutes in separate fields,
        // and these are separate from their sign. We therefore need to convert
        // XMP SDK values that have an absolute value greater than 14:00 to values
        // that are allowed by Qt.

        int tzh, tzm;
        if (dt.tzHour >= 14)
        {
            // If the absolute value of the time is greater than or equal
            // to 14 hours, convert the hours and minutes to a time with
            // an absolute value less than 14 hours.

            if (dt.tzMinute == 0)
            {
                tzh = dt.tzHour - 24;
                tzm = 0;
            }
            else
            {
                tzh = dt.tzHour - 23;
                tzm = dt.tzMinute - 60;
            }
        }
        else
        {
            // If the absolute value of the time is less than 14 hours,
            // just use the existing hour and minutes.

            tzh = dt.tzHour;
            tzm = dt.tzMinute;
        }

        // If the hours and minutes are west of UTC, then convert them
        // to hours and minutes east of UTC.

        if (dt.tzSign == kXMP_TimeWestOfUTC)
        {
            tzh *= -1;
            tzm *= -1;
        }

        // Construct a QTimeZone object, then construct and return a
        // QDateTime object.

        QTimeZone qtz((3600 * tzh) + (60 * tzm));
        return QDateTime(qd, qt, qtz).toUTC();
    }
    else
    {
        // There is no time zone, so just create a QDateTime from
        // the date and time. (This appears to use the local time zone.)
        // Note that we should never get to this code, as we always
        // set the timezone to local time.

        return QDateTime(qd, qt).toUTC();
    }
}

/********************************************************************/
/*                                                                  */
/*  Special handling of property values                             */
/*                                                                  */
/********************************************************************/

// These methods have the following call hierarchy:
//
//   specialHandling
//     |
//     +-->checkSynonymsAgainstDLN
//     |     |
//     |     +-->getNonNullSynonyms
//     |     |     |
//     |     |     +-->getOtherValue
//     |     |     |
//     |     |     +-->getOtherInfo
//     |     |
//     |     +-->checkSynonymValues
//     |     |     |
//     |     |     +-->evalPredicate
//     |     |
//     |     +-->compareSimpleValues
//     |
//     +-->checkNulls
//     |
//     +-->checkDimensions
//           |
//           +-->getDimensions
//                 |
//                 +-->getOtherValue

void Inspector::initSpecialHandling()
{
    QString tmp;

    // Insert the names of properties we need to handle specially.
    // Note that we do not actually store anything useful here.
    // Instead, we just use the hash table as a way to quickly
    // determine if special processing is needed.

    // (We could probably insert pointers to methods that actually
    // do the special handling, but that's more work than I want
    // to do. It would only be worthwhile if we did a lot of special
    // handling.)

    specialAuxProps.insert("Lens", tmp);
    specialAuxProps.insert("SerialNumber", tmp);

    specialExifProps.insert("Lens", tmp);
    specialExifProps.insert("SerialNumber", tmp);
    specialExifProps.insert("PixelXDimension", tmp);
    specialExifProps.insert("PixelYDimension", tmp);

    specialExifEXProps.insert("BodySerialNumber", tmp);
    specialExifEXProps.insert("LensModel", tmp);
    specialExifEXProps.insert("SerialNumber", tmp);

    specialTiffProps.insert("ImageLength", tmp);
    specialTiffProps.insert("ImageWidth", tmp);

    // Add the hash tables to specialProps. This allows us to do
    // a two-part lookup: first by namespace and then by name. This
    // is faster than concatenating the namespace to the name and
    // then looking up the resulting string.

    specialProps.insert("http://ns.adobe.com/exif/1.0/aux/", &specialAuxProps);
    specialProps.insert("http://ns.adobe.com/exif/1.0/", &specialExifProps);
    specialProps.insert("http://cipa.jp/exif/1.0/", &specialExifEXProps);
    specialProps.insert("http://ns.adobe.com/tiff/1.0/", &specialTiffProps);

    // Build the lists of synonyms.

    lensNamespaces.append(CSTRING_TO_TXMPSTRING("http://ns.adobe.com/exif/1.0/aux/"));
    lensNamespaces.append(CSTRING_TO_TXMPSTRING("http://ns.adobe.com/exif/1.0/"));
    lensNamespaces.append(CSTRING_TO_TXMPSTRING("http://cipa.jp/exif/1.0/"));

    lensNames.append(CSTRING_TO_TXMPSTRING("Lens"));
    lensNames.append(CSTRING_TO_TXMPSTRING("Lens"));
    lensNames.append(CSTRING_TO_TXMPSTRING("LensModel"));

    serialNumberNamespaces.append(CSTRING_TO_TXMPSTRING("http://ns.adobe.com/exif/1.0/aux/"));
    serialNumberNamespaces.append(CSTRING_TO_TXMPSTRING("http://ns.adobe.com/exif/1.0/"));
    serialNumberNamespaces.append(CSTRING_TO_TXMPSTRING("http://cipa.jp/exif/1.0/"));
    serialNumberNamespaces.append(CSTRING_TO_TXMPSTRING("http://cipa.jp/exif/1.0/"));

    serialNumberNames.append(CSTRING_TO_TXMPSTRING("SerialNumber"));
    serialNumberNames.append(CSTRING_TO_TXMPSTRING("SerialNumber"));
    serialNumberNames.append(CSTRING_TO_TXMPSTRING("BodySerialNumber"));
    serialNumberNames.append(CSTRING_TO_TXMPSTRING("SerialNumber"));
}

bool Inspector::specialHandling(PropInfo *info, const QFileInfo & fileInfo1, const XMPValue * value1, const QFileInfo & fileInfo2, const XMPValue * value2)
{
    // Some properties cannot be compared directly and need special
    // handling. For example, multiple Exif properties can store the
    // lens model, but a given image will probably use only one of
    // these. Thus, when to compare the image lens model against the
    // DLN lens model, we need to determine which Exif property is
    // not null and use that.

    // WARNING! The special handling code is currently used only for
    // top-level, simple-value properties. If it is used for lower
    // level properties, the following might be issues:
    //
    // - The globals currPropInfoList, currValues1, and currValues2
    //   are used to look up properties other than the current property.
    //   For example, they are used to look up all of the properties
    //   that store lens model. They are set for top-level properties,
    //   so special handling code that needs to look up lower-level
    //   properties, such as those inside a structure, must be prepared
    //   to traverse the structures themselves.
    //
    // - specialHandling() is called from compareValueLists(), which
    //   is called only for top-level properties. It cannot be called
    //   from lower-level compare methods (e.g. compareValues()) because
    //   it requires filenames and the lower-level methods do not have
    //   filenames in their signatures.

    // specialHandling() returns true if compareValueLists() (the
    // calling method) should not call compareValues() for the
    // properties. This is done when specialHandling() does the
    // entire property comparison.
    //
    // specialHandling() returns false if compareValueLists() should
    // call compareValues(). This is done when specialHandling() does
    // not compare the properties (the most common case) or only
    // partially compares the properties.

    // Do logging.

    if (options.logging)
    {
        write(logFile, "Checking property for special handling: " + propPath(info) + "\n");
    }

    // Check if the namespace has any properties that require
    // special handling.

    QHash<QString, QString> * hash = specialProps.value(TXMPSTRING_TO_QSTRING(info->ns));
    if (hash == nullptr)
    {
        if (options.logging) write(logFile, "No special handling needed.\n");
        return false;
    }

    // Check if the property requires special handling. If not,
    // return false. Notice that the hashtable allows us to
    // quickly decide whether the property needs special handling.

    if (!hash->contains(TXMPSTRING_TO_QSTRING(info->name)))
    {
        if (options.logging) write(logFile, "No special handling needed.\n");
        return false;
    }

    // Delegate the special handling. The string comparisons are
    // inefficient, but we only do a few of them. If this needs
    // to be more efficient in the future, we can hash pointers
    // to the methods that do the special handling. At the moment
    // that's more complex than is necessary.

    QString name = TXMPSTRING_TO_QSTRING(info->name);
    if ((name == "SerialNumber") || (name == "BodySerialNumber"))
    {
        return checkSynonymsAgainstDLN("BodySerialNumber", serialNumberNamespaces, serialNumberNames, info, fileInfo1, value1, fileInfo2, value2);
    }
    else if ((name == "Lens") || (name == "LensModel"))
    {
        return checkSynonymsAgainstDLN("LensModel", lensNamespaces, lensNames, info, fileInfo1, value1, fileInfo2, value2);
    }

    // Check the null values. If the nulls don't match or
    // both are null, then return. Notice that we did not
    // check nulls for synonyms. This is because it is legal
    // for synonyms to have null values.

    if (!checkNulls(info, value1, value2)) return true;

    // Continue delegating the special handling.

    if ((name == "PixelXDimension") ||
        (name == "PixelYDimension") ||
        (name == "ImageLength") ||
        (name == "ImageWidth"))
    {
        return checkDimensions(info, fileInfo1, value1, fileInfo2, value2);
    }
    else
    {
        // It is a bug if we get here, as it means that a property
        // was included in the hash tables but no code handled it.

        addResult(IResultType::FatalError,
                  "Programming Error",
                  "<p>" + propPath(info) + " was included in the hash tables of special properties, but there is no code to handle it.</p>",
                  propPath(info));
    }

    // We never reach this statement because we either return or
    // call addResult with a fatal error, which throws an exception,
    // but it is needed to keep the compiler happy.

    return true;
}

bool Inspector::checkSynonymsAgainstDLN(const QString & mainSynonym,
                                        const QList<TXMP_STRING_TYPE> & synNamespaces,
                                        const QList<TXMP_STRING_TYPE> & synNames,
                                        PropInfo *info,
                                        const QFileInfo & fileInfo1,
                                        const XMPValue *value1,
                                        const QFileInfo & fileInfo2,
                                        const XMPValue *value2)
{
    // Check properties that have multiple synonyms against
    // a value in the DLN.

    // These properties apparently have one or more synonyms.
    // Examples are aux:Lens, exif:Lens, and exifEX:Lens.
    // (I say "apparently" because the specs aren't written
    // clearly and some of these properties, such as exif:Lens,
    // aren't even in specifications.)
    //
    // When comparing two image files (as opposed to an image
    // file and the DLN), we immediately return false so that the
    // values from these files are compared normally. We
    // expect all image files to have the same value for a given
    // property because all of the image files derive from the
    // same hardware/software chain. (The most likely case is
    // that one synonym -- e.g. exifEX:Lens -- will have a value
    // and the other synonyms -- e.g. aux:Lens and exif:Lens --
    // will be null.)
    //
    // When comparing an image file against the DLN, we need
    // to discover which property (if any) is non-null and
    // compare it against the value in the DLN. Note that
    // the DLN should have non-null values for each of these
    // properties because we use the same query for each
    // synonym.

    // Do logging.

    if (options.logging) write(logFile, "Checking for synonyms.\n");

    // Check if the first file is the DLN database. (When we are comparing
    // values from the DLN database, the first "file" MUST be the DLN database.)
    // If not, this means we are processing two image files, so return
    // false to compare their values normally.

    if (fileInfo1.fileName() != "DLN database")
    {
        if (options.logging)
        {
            write(logFile, "Both files are image files and will be checked normally.\n");
        }
        return false;
    }

    // Process the synonyms only when we encounter the main synonym
    // name (e.g. exifEX:BodySerialNumber). For other synonyms,
    // return true to skip further comparisons.

    if (TXMPSTRING_TO_QSTRING(info->name) != mainSynonym)
    {
        if (options.logging)
        {
            write(logFile, "Property ignored. Synonyms will be checked when handling the " + mainSynonym + " property.\n");
        }
        return true;
    }

    // Get the information about and values of any synonyms
    // (including the main synonym) that have non-null values.

    QList<PropInfo *> synPropInfoList;
    QList<XMPValue *> synValues;
    getNonNullSynonyms(fileInfo2, currValues2, synNamespaces, synNames, synPropInfoList, synValues);

    // Compare the synonym values against the DLN value.

    if (synValues.size() > 0)
    {
        // If there are any non-null synonyms, first check that
        // all synonyms have the same value, then check the
        // first synonym value against the DLN value.

        if (checkSynonymValues(synPropInfoList, synValues))
        {
            if (checkNulls(info, value1, synValues.at(0)))
            {
                compareSimpleValues(info, value1, synValues.at(0));
            }
        }
    }
    else
    {
        // If all synonym values are null, check the DLN value.

        if (value1->isNull)
        {
            // If the DLN value is null, add an error.

            e.pushMsgType(IMessageType::InvalidValue);
            addResult(IResultType::CaptureError, "Required DLN value is NULL", "", info, value1, nullptr);
            e.popMsgType();
        }
        else
        {
            // If the DLN value is not null, add an error.

            QString msg("The following synonymous properties in " + fileInfo2.fileName() + " are all null: ");
            for (int i = 0; i < synNamespaces.size(); i++)
            {
                msg.append(PropInfo::getQName(synNamespaces.at(i), synNames.at(i)));
                QString separator = (i < synNamespaces.size() - 1) ? ", " : ".";
                msg.append(separator);
            }

            addResult(IResultType::CaptureError,
                      IErrorCode::toString(IErrorCode::DiffIsNull),
                      msg,
                      info,
                      value1,
                      value2);
        }
    }

    // Return true to suppress further comparisons.

    return true;
}

void Inspector::getNonNullSynonyms(const QFileInfo & fileInfo, const QList<XMPValue *> &values, const QList<TXMP_STRING_TYPE> & synNamespaces, const QList<TXMP_STRING_TYPE> & synNames, QList<PropInfo *> &synPropInfoList, QList<XMPValue *> & synValues)
{
    // Set the error context.

    e.pushFileInfos(fileInfo, QFileInfo());
    e.pushMsgType(IMessageType::InvalidValue);

    // Do logging.

    if (options.logging) write(logFile, "Getting synonym values and property information.\n");

    // Return the values and property information for the
    // synonyms with non-null values.

    PropInfo * firstInfo;
    for (int i = 0; i < synNamespaces.size(); i++)
    {
        // Get the value and the info for the synonym.

        XMPValue * value = getOtherValue(values, synNamespaces.at(i), synNames.at(i));
        PropInfo * info = getOtherInfo(synNamespaces.at(i), synNames.at(i));

        // Check that the synonyms have the same type. Return
        // an error if they do not.

        if (i == 0)
        {
            firstInfo = info;
        }
        else
        {
            if (firstInfo->simpleInfo->type != info->simpleInfo->type)
            {
                addResult(IResultType::FatalError,
                          "Synonyms must have same type",
                          "<p>" + info->getQName() + " has type " +
                          XMPType::toString(info->simpleInfo->type) +
                          " and " + firstInfo->getQName() + " has type " +
                          XMPType::toString(firstInfo->simpleInfo->type) + ".</p>",
                          info,
                          value,
                          nullptr);
            }
        }

        // If the value is non-null, append its information to
        // the lists.

        if (!value->isNull)
        {
            // If the value is non-null, append it to the list.

            synValues.append(value);
            synPropInfoList.append(info);
        }
    }

    // Restore the error context.

    e.popFileInfos();
    e.popMsgType();
}

bool Inspector::checkSynonymValues(const QList<PropInfo *> & synPropInfoList, const QList<XMPValue *> & synValues)
{
    // Check that all synonyms have the same value.

    // If there aren't any synonyms in the list, return true.

    if (synPropInfoList.size() == 0) return true;

    // Get the first value.

    XMPValue * firstValue = synValues.at(0);
    PropInfo * firstInfo = synPropInfoList.at(0);
    bool ok = true;

    // Cycle through the remaining values and compare them to
    // the first value.

    for (int i = 1; i < synValues.size(); i++)
    {
        // Get the next value and info and call compareScalarValues().
        // We can safely pass firstInfo because getNonNullSynonyms()
        // checked that all synonyms have the same type.

        XMPValue * nextValue = synValues.at(i);
        PropInfo * nextInfo = synPropInfoList.at(i);
        IErrorCode::Enum code = evalPredicate(firstInfo, firstValue, IOperator::EQ, nextValue);

        // If there was an error, return an error message. Otherwise,
        // log the success.

        if (code != IErrorCode::OK)
        {
            addResult(IResultType::CaptureError,
                      "Different synonym values",
                      "<p>In " + e.fileInfo2.fileName() +
                      ", the following synonymous properties have different values: </p>" +
                      "<table><tr><td>Property:</td><td>" + firstInfo->getQName() + "</td></tr>" +
                      "<tr><td>Value:</td><td>" + firstValue->toString() + "</td></tr>" +
                      "<tr><td>&nbsp;</td><td>&nbsp;</td></tr>" +
                      "<tr><td>Property:</td><td>" + nextInfo->getQName() + "</td></tr>" +
                      "<tr><td>Value:</td><td>" + nextValue->toString() + "</td></tr></table>",
                      propPath(firstInfo));
            ok = false;
        }
        else
        {
            if (options.logging)
            {
                write(logFile, "Synonyms have same value. " +
                                firstInfo->getQName() + ": " + firstValue->toString() + " " +
                                nextInfo->getQName() + ": " + nextValue->toString() + " ... OK\n");
            }

        }
    }

    // Return success or failure.

    return ok;
}

XMPValue * Inspector::getOtherValue(const QList<XMPValue *> &values, const TXMP_STRING_TYPE ns, const TXMP_STRING_TYPE name)
{
    // Do a linear search to find the requested value. Obviously,
    // this is very slow, but it is rarely used.

    for (int i = 0; i < currPropInfoList.size(); i++)
    {
        PropInfo * info = currPropInfoList.at(i);
        if (TXMP_STRING_EQUALS(info->ns, ns))
        {
            if (TXMP_STRING_EQUALS(info->name, name))
            {
                if (options.logging)
                {
                    write(logFile, "Got value for " + PropInfo::getQName(ns, name) + ": " + values.at(i)->toString() + "\n");
                }
                return values.at(i);
            }
        }
    }

    // Add a fatal error if the XMPValue is not found. Note that the XMPValue
    // not being found is different from a null XMPValue being found. In a
    // null XMPValue, the XMPValue object exists, but its isNull flag is
    // set to true. We only add a fatal error if no XMPValue is found, which
    // is a configuration error in the PropsXxxx.xml files. In particular,
    // the information for the property we are searching for is not in the files.

    addResult(IResultType::FatalError,
              "No value found",
              "<p>While processing properties that require special handling, no value was found for " + PropInfo::getQName(ns, name) +
              ". The property information is missing from the PropsXxxx.xml configuration files.</p>",
              PropInfo::getQName(ns, name));

    // The following code is never reached -- addResult throws
    // an exception on a fatal error -- but is needed to keep
    // the compiler happy.

    return nullptr;
}

PropInfo * Inspector::getOtherInfo(const TXMP_STRING_TYPE ns, const TXMP_STRING_TYPE name)
{
    // Do a linear search to find the requested PropInfo. Obviously,
    // this is very slow, but it is rarely used.

    for (int i = 0; i < currPropInfoList.size(); i++)
    {
        PropInfo * info = currPropInfoList.at(i);
        if (TXMP_STRING_EQUALS(info->ns, ns))
        {
            if (TXMP_STRING_EQUALS(info->name, name))
            {
                if (options.logging)
                {
                    write(logFile, "Got property info for " + info->getQName() + "\n");
                }
                return info;
            }
        }
    }

    // Add a fatal error if the property information is not found. Note
    // that the information should always exist. It is a configuration
    // error in the PropsXxxx.xml files if the property information does
    // not exist. In particular, the information for the property we are
    // searching for is not in the files

    addResult(IResultType::FatalError,
              "No property information found",
              "<p>While processing properties that require special handling, no property information was found for " + PropInfo::getQName(ns, name) +
              ". The property information is missing from the PropsXxxx.xml configuration files.</p>",
              PropInfo::getQName(ns, name));

    // The following code is never reached -- addResult throws
    // an exception on a fatal error -- but is needed to keep
    // the compiler happy.

    return nullptr;
}

bool Inspector::checkDimensions(const PropInfo *info, const QFileInfo & fileInfo1, const XMPValue *value1, const QFileInfo & fileInfo2, const XMPValue *value2)
{
    // Image dimensions require special handling because it is
    // OK for archival and processed images to have different
    // rotations or sizes.

    // The rules are as follows:
    //
    // 1) If we are comparing two images of the same type (both
    //    processed or both archival), return false to continue normal
    //    processing. All images of the same type must have the
    //    same image sizes.
    //
    // 2) If we are comparing two images of different types (one
    //    processed and one archival), we need to allow for rotation and
    //    scaling. The algorithm is as follows.
    //
    // 3) Unless we are processing the image width (tiff:ImageWidth
    //    or exif:PixelXDimension), return true. This skips duplicate
    //    comparisons when we encounter the image height properties
    //    (tiff:ImageLength and exif:PixelYDimension).
    //
    //    (It is not clear why there are two different sets of
    //    properties for height and width. We assume that if one
    //    member of a pair is set, so is the other, and that if
    //    both pairs are used, they contain the same values.)
    //
    // 4) Determine which width property is being compared and retrieve
    //    the corresponding height property.
    //
    // 5) Compare the X and Y dimensions as follows:
    //
    //    a) If X1=X2 and Y1=Y2, the images are the same size.
    //       Return true.
    //
    //    b) If X1=Y2 and X2=Y1, the images are the same size
    //       and are rotated. Return true.
    //
    //    c) If X1/Y1 = X2/Y2 (i.e. X1*Y2 = X2*Y1), the images have
    //       the same aspect ratios, which means they were resized.
    //       Return true.
    //
    //    d) If X1/Y1 = Y2/X2 (i.e. X1*X2 = Y2*Y1), the images have
    //       inverted aspect rations, which means they were rotated
    //       and resized. Return true.
    //
    //    e) Add an error stating that the images are different sizes.
    //       Return true.

    // Log the test.

    if (options.logging) write(logFile, "Checking dimensions.\n");

    // If the file types are the same, return false to continue
    // normal processing. This is because all files of a given
    // type -- e.g. all image files -- should have the same values
    // for these properties.

    if (IFileType::fromString(fileInfo1.suffix()) == IFileType::fromString(fileInfo2.suffix()))
    {
        if (options.logging)
        {
            write(logFile, "Files have same type: " + IFileType::toString(IFileType::fromString(fileInfo1.suffix())) + ". Property will be handled normally.\n");
        }
        return false;
    }

    // If we are not processing image width, return true to stop
    // further processing. This saves us from comparing the
    // dimensions twice.

    if ((info->name != "ImageWidth") && (info->name != "PixelXDimension"))
    {
        if (options.logging)
        {
            write(logFile, "Property ignored. Dimensions will be checked when handling tiff:ImageWidth and exif:PixelXDimension.\n");
        }
        return true;
    }

    // Get the x and y dimensions. Return true to stop further processing
    // if the Y dimension is null.

    int x1, y1, x2, y2;
    if (getDimensions(fileInfo1.fileName(), currValues1, info, value1, x1, y1)) return true;
    if (getDimensions(fileInfo2.fileName(), currValues2, info, value2, x2, y2)) return true;

    // Make the comparisions.

    // NOTE: It is not clear what the relation between tiff:rotation and the X
    // and Y dimension properties is. However, it does not seem that we need to
    // care. The reason is that, even if rotation affects the dimension properties
    // -- for example, it switches the X and Y dimension values -- we take this
    // into account in our rotated cases, checking x1 against y2 and 1/x1 against
    // 1/y2.

    if ((x1 == x2) && (y1 == y2))
    {
        if (options.logging) write(logFile, "Image sizes match.\n");
        return true;
    }

    if ((x1 == y2) && (y1 == x2))
    {
        if (options.logging) write(logFile, "Images rotated; sizes match.\n");
        return true;
    }

    if (x1*y2 == x2*y1) // Same as x1/y1 == x2/y2
    {
        if (options.logging) write(logFile, "Images resized; aspect ratios match.\n");
        return true;
    }

    if (x1*x2 == y1*y2) // Same as x1/y1 == y2/x2
    {
        if (options.logging) write(logFile, "Images resized and rotated; aspect ratios match.\n");
        return true;
    }

    // Add an error if all the comparisons fail.

    addResult(IResultType::TransformationError,
              "Different dimensions",
              "File " + e.fileInfo1.fileName() + " has width " + QString::number(x1) +
              " and height " + QString::number(y1) +
              ". File " + e.fileInfo2.fileName() + " has width " + QString::number(x2) +
              " and height " + QString::number(y2) + ".",
              propPath(info));

    // Return true to suppress further comparisons.

    return true;
}

bool Inspector::getDimensions(const QString & filename, const QList<XMPValue *> &values, const PropInfo * info, const XMPValue * xValue, int & x, int & y)
{
    // Set the X dimension.

    x = xValue->i;

    // Get the Y dimension.

    TXMP_STRING_TYPE yName = (TXMPSTRING_TO_QSTRING(info->name) == "ImageWidth") ?
                              CSTRING_TO_TXMPSTRING("ImageLength") :
                              CSTRING_TO_TXMPSTRING("PixelYDimension");
    XMPValue * yValue = getOtherValue(values, info->ns, yName);

    // If the Y value is non-null, set it and return false to
    // continue processing. Otherwise, add an error and return
    // true to stop further processing.

    if (!yValue->isNull)
    {
        y = yValue->i;
        return false;
    }
    else
    {
        addResult(IResultType::CaptureError,
                  "Y dimension is null",
                  "<p>The X dimension of an image is non-null and the Y dimension is null.</p>"
                  "<tr><td>File:</td><td>" + filename + "</td></tr>"
                  "<tr><td>&nbsp;</td><td>&nbsp;</td></tr>"
                  "<tr><td>" + propPath(info) + ":</td><td>" + xValue->toString() + "</td></tr>"
                  "<tr><td>&nbsp;</td><td>&nbsp;</td></tr>"
                  "<tr><td>" + PropInfo::getQName(info->ns, yName) + ":</td><td>" + yValue->toString() + "</td></tr></table>",
                  QString());
        return true;
    }
}

/********************************************************************/
/*                                                                  */
/*  Property utilities                                              */
/*                                                                  */
/********************************************************************/

void Inspector::toLowerCase(XMPValue * value)
{
    // Get the current path and convert it to lower case.
    // Note that toLower() returns a lower-cased copy of the
    // QString; it does not modify the current QString.

    QString qLower = TXMPSTRING_TO_QSTRING(*value->ptr_str).toLower();
    TXMP_STRING_TYPE * xmpLower = NEW_TXMP_STRING_TYPE_Q(qLower);

    // Delete the old value and replace it with the new value.

    delete value->ptr_str;
    value->ptr_str = xmpLower;
}

/********************************************************************/
/*                                                                  */
/*  Compare filenames and directories                               */
/*                                                                  */
/********************************************************************/

void Inspector::compareFilenames(const QString& imageSetName, const QList<QFileInfo> &archivalFileInfos, const QList<QFileInfo> &processedFileInfos, IFileType::Enum processedType, QFileInfo & firstArchivalFileInfo, QFileInfo & firstProcessedFileInfo)
{
    // Check that there are corresponding archival and processed files.
    //
    // a) If an archival file has no corresponding processed file, issue
    //    a warning. See below for details.
    // b) If a processed file has no corresponding archival file, issue
    //    an error.

    // Do not compare filenames if there are no archival or processed files.

    if (archivalFileInfos.size() == 0 || processedFileInfos.size() == 0) return;

    // Do logging.

    if (options.logging) write(logFile, "Matching archival and processed filenames.\n");

    // Compare the filenames.

    bool foundFirstMatch = false;
    int archivalIndex = 0;
    int processedIndex = 0;

    while ((archivalIndex < archivalFileInfos.size()) && (processedIndex < processedFileInfos.size()))
    {
        int compare = archivalFileInfos[archivalIndex].baseName().compare(processedFileInfos[processedIndex].baseName(), Qt::CaseSensitive);
        if (compare == 0)
        {
            // If these are the first archival and processed files with the
            // same basename, set the firstArchivalFileInfo and
            // firstProcessedFileInfo variables. We use these later in
            // compareAcrossFileTypes() to check that the archival and
            // processed files have the same property values.

            if (!foundFirstMatch)
            {
                firstArchivalFileInfo = archivalFileInfos[archivalIndex];
                firstProcessedFileInfo = processedFileInfos[processedIndex];
                foundFirstMatch = true;
            }

            // Do logging.

            if (options.logging)
            {
                write(logFile, "Found match: " + archivalFileInfos[archivalIndex].fileName() + " and " + processedFileInfos[processedIndex].fileName() + "\n");
            }

            // If there is a processed file with the same basename as the
            // archival file, just continue.

            archivalIndex++;
            processedIndex++;
        }
        else if (compare < 1)
        {
            // If there is an archival file but no corresponding processed file,
            // issue a warning. This might be OK, such as when the archival
            // file is a color test and no corresponding processed file is
            // needed. It might also be an error, such as when the user
            // forgot to create a corresponding processed file. Since we can't
            // tell the difference, issue a warning and let the user
            // decide.
            QString fileType = processedType == IFileType::UNKNOWN ? "processed" : IFileType::toString(processedType);
            addResult(IResultType::Warning,
                      QString("Missing %1 file").arg(fileType),
                      QString("<p>No %1 file was found that corresponds to the DNG file %2</p>").arg(fileType, archivalFileInfos[archivalIndex].fileName()),
                      "");
            archivalIndex++;
        }
        else // if (compare > 1)
        {
            // If there is a processed file but no corresponding archival file,
            // issue an error. This is not allowed if archival files exist.

            addResult(IResultType::FileError, "Missing DNG file", "<p>No DNG file was found that corresponds to the " + IFileType::toString(processedType) + " file " + processedFileInfos[processedIndex].fileName() + ".</p>", "");
            processedIndex++;
        }

        // How many images were "inspected"? One pair? Or one archived and one processed, i.e. two?
        // results->increaseImageCount(1);
    }

    // Process any unmatched filenames.

    if (archivalIndex < archivalFileInfos.size())
    {
        for (; archivalIndex < archivalFileInfos.size(); archivalIndex++)
        {
            // Issue warnings for any remaining unmatched archival files.

            QString fileType = processedType == IFileType::UNKNOWN ? "processed" : IFileType::toString(processedType);
            addResult(IResultType::Warning,
                      QString("Missing %1 file").arg(fileType),
                      QString("<p>No %1 file was found that corresponds to the DNG file %2</p>").arg(fileType, archivalFileInfos[archivalIndex].fileName()),
                      "");
            results->increaseImageCount(imageSetName, 1);
        }
    }
    else if (processedIndex < processedFileInfos.size())
    {
        for (; processedIndex < processedFileInfos.size(); processedIndex++)
        {
            // Issue errors for any remaining unmatched image files.

            addResult(IResultType::FileError, "Missing DNG file", "<p>No DNG file was found that corresponds to the " + IFileType::toString(processedType) + " file " + processedFileInfos[processedIndex].fileName() + ".</p>", "");
            results->increaseImageCount(imageSetName, 1);
        }
    }

    // Do logging.

    if (options.logging) write(logFile, "\n");
}

/********************************************************************/
/*                                                                  */
/*  File utilities                                                  */
/*                                                                  */
/********************************************************************/

void Inspector::sortFiles(QList<ImageSet> & imageSets)
{
    for (int i = 0; i < imageSets.size(); i++)
    {
        // On Windows, lower-case all the file names so we can do
        // case insensitive comparisons.
        //
        // We assume that Windows filenames are always case insensitive and
        // Mac filenames are always case sensitive. There seem to be some
        // obscure cases where this isn't true, but there don't seem to
        // be easy ways to test for this. We will worry about this when/if
        // the problem actually occurs. For example, see:
        //
        // http://stackoverflow.com/questions/20080174/how-to-find-out-if-two-filenames-refer-to-the-same-file
        // http://www.archivum.info/qt-interest@trolltech.com/2007-05/00493/Determining-case-sensitivity-of-filesystem-in-Qt.html
        //

#ifdef Q_OS_WIN
        lowerFileCase(imageSets[i].archivalFileInfos);
        lowerFileCase(imageSets[i].processedFileInfos);
#endif

        // Sort the files using a custom function to compare base
        // names. We need this in the case that the files in an
        // image set come from multiple directories, such as when
        // photogrammetry images are spread across multiple directories.
        // Note that this assumes there are no duplicate basenames.

        std::sort(imageSets[i].archivalFileInfos.begin(), imageSets[i].archivalFileInfos.end(), compareBaseNames);
        std::sort(imageSets[i].processedFileInfos.begin(), imageSets[i].processedFileInfos.end(), compareBaseNames);
    }
}

bool Inspector::compareBaseNames(const QFileInfo &fileInfo1, const QFileInfo &fileInfo2)
{
    return (fileInfo1.baseName().compare(fileInfo2.baseName()) < 0);
}

void Inspector::lowerFileCase(QList<QFileInfo> & fileInfos)
{
    for (int i = 0; i < fileInfos.size(); i++)
    {
        fileInfos[i].setFile(fileInfos[i].canonicalFilePath().toLower());
    }
}

void Inspector::write(QFile & file, const QString str)
{
    // Check the return from write() because QFile does
    // not (appear to) throw exceptions.

    if (file.write(str.toStdString().c_str()) == -1)
    {
        addResult(IResultType::FatalError,
                  "Error writing file",
                  "<p>Unknown error writing to file.</p>"
                  "<p>Filename: " + file.fileName() +
                  "</p><p>Error: " + file.errorString() + "</p>",
                  "");
    }
    file.flush();
}

void Inspector::write(QFile & file, const char *ptr_to_char)
{
    // Check the return from write() because QFile does
    // not (appear to) throw exceptions.

    if (file.write(ptr_to_char) == -1)
    {
        addResult(IResultType::FatalError,
                  "Error writing file",
                  "<p>Unknown error writing to file.</p>"
                  "<p>Filename: " + file.fileName() +
                  "</p><p>Error: " + file.errorString() + "</p>",
                  "");
    }
    file.flush();
}

/********************************************************************/
/*                                                                  */
/*  Report results                                                  */
/*                                                                  */
/********************************************************************/

// Error handling in the Inspector has three basic parts: the ErrorContext
// struct, the status variable, and error reporting via addResult().
//
// (NOTE: The term "error" is used loosely here. In fact, this section
// describes result handling, but since most results are errors -- the
// only non-error results are OK and a few warnings -- it is easier to
// think in terms of errors.)
//
// ErrorContext struct
// -------------------
// ErrorContext tracks the information used to report errors, such
// as the current project directory, image set, and names of files
// being checked. It is kept at a global level to avoid cluttering
// method signatures. (Values are set throughout the call hierarchy
// and used at the bottom of the call hierarchy.) Because of this,
// care should be taken before changing the call hierarchy.
//
// ErrorContext members are generally set at the start of a method
// and unset at the end of a method. For example, inspectSet()
// sets and unsets the imageSet and imageSetNum members.
//
// Notable members of ErrorContext are:
//
// - testGroup. Whether we are testing files, capture properties, or
//   transformation properties. Used to set IResult.resultType.
//
// - fileInfo1 and fileInfo2. The QFileInfos of the files being tested.
//   If only one file is being tested, such as when testing a
//   required value, only fileInfo1 is used.
//
// - msgType. The format of the error message that is constructed
//   from filenames and values. Legal values are CompareValues (use
//   two filenames and two values), Restriction (use one filename
//   and two values), and InvalidValue (use one filename and one value.)
//
// status variable
// ---------------
// The status variable is a struct that contains three separate
// statuses -- file, capture, and transform -- to report the status
// of each of these test groups. This allows us to separately report
// the status of each test group.
//
// addResult
// ---------
// The addResult() methods create a new IResult object and perform
// the following additional error handling:
//
// - Add the result to the list of results (the Results object).
//
// - Log the result to the log file (if any).
//
// - Upgrade the correct member of the status variable to keep track
//   of the status of the inspection of each test group.
//
// - Throw an exception if (a) a fatal error occurs or (b) a recoverable
//   error occurs and the user selected the "Stop on Error" option.
//
// Error descriptions and formatting
// ---------------------------------
//
// Error descriptions are used in numerous places, such as the Results
// box, the HTML report, the log file, and the DLN database. There are three
// error formats: HTML, plain text with line breaks, and plain text
// without line breaks.
//
// To get an error message, methods call IResult.getMessage(), which
// accepts formatting flags for HTML and line breaks. For text-centric
// results (third version of addResult, no filenames or values),
// getMessage() simply returns the description field. For data-centric
// and mixed results (first, second and fourth versions of addResults)
// getMessage() constructs a message from the title, description,
// filename, and value fields; see getMessage() for details.
//
// Error messages are used in the following places with the following
// formatting options:
//
// Output location         HTML?  Line breaks?
// ----------------------  -----  ------------
// Results box               Y        --
// HTML report               Y        --
// Saved results file        Y        --
// stdout                    Y        --
// DLN database              N        N
// Log file                  N        Y
// Error dialog boxes        N        N
// stderr                    N        N

void Inspector::addResult(IResultType::Enum resultType, IErrorCode::Enum code, const PropInfo *info, const XMPValue *value1, const XMPValue *value2)
{
    // This version of addResult is for data-centric errors, in which
    // the error title plus the filenames and values is sufficient
    // to describe the error. It is primarily used by methods like
    // compareSimpleValues().
    //
    // The description field uses the error message (if any) from the
    // property configuration file. A text message describing the error
    // and suitable for use in the log file, dialog boxes, etc. can be
    // retrieved by calling IResult.getMessage(). The message is built
    // from the fields in the result.

    // Get the error message from the PropInfo struct, if any.

    QString description;
    if (info->simpleInfo != nullptr)
        if (info->simpleInfo->errorMessage != nullptr)
                description = *(info->simpleInfo->errorMessage);

    // Create a new IResult.

    IResult result(e.imageSetName,
                   e.imageSetNum,
                   e.testGroup,
                   resultType,
                   e.msgType,
                   IErrorCode::toString(code),
                   description,
                   propPath(info),
                   info->severity,
                   e.fileInfo1,
                   e.fileInfo2,
                   (value1 == nullptr) ? QString() : value1->toString(),
                   (value2 == nullptr) ? QString() : value2->toString());

    // Explain to users how to deal with date differences.

    if (code == IErrorCode::DateDiffTooBig)
    {
        QString msg("Set the allowable date difference in the Options dialog. Current date difference is " + QString::number(options.epsilon/(60 * 60)) + " hour(s).");
        if (result.description.isEmpty())
            result.description = msg;
        else
            result.description.append(" ").append(msg);
    }

    // Add the result to the Results object.

    addResult(result);
}

void Inspector::addResult(IResultType::Enum resultType, const QString &title, const QString &htmlDescription, const QString &propPath)
{
    // This version of addResult is for text-centric errors,
    // such as programming or configuration errors. In this case,
    // the filenames are of no importance and there are no values,
    // so the data fields are left blank.
    //
    // Note that we set the property path to "--" when there is no
    // property path. This is so the messages that do not apply to
    // any property will (a) sort together, (b) sort at the top of the
    // list, and (c) have a human-readable name.
    //
    // IMPORTANT: The htmlDescription parameter must contain HTML formatting.

    // Create a new IResult.

    int severity = resultType == IResultType::Enum::Warning ? PropInfo::SEVERITY_WARNING : PropInfo::SEVERITY_ERROR;
    IResult result(e.imageSetName,
                   e.imageSetNum,
                   e.testGroup,
                   resultType,
                   IMessageType::Text,
                   title,
                   htmlDescription,
                   propPath.isEmpty() ? "--" : propPath,
                   severity,
                   e.fileInfo1,
                   e.fileInfo2,
                   QString(),
                   QString());

    // Add the result to the Results object.

    addResult(result);
}

void Inspector::addResult(IResultType::Enum resultType, const QString & title, const QString & description, const PropInfo * info, const XMPValue * value1, const XMPValue * value2)
{
    // This version of addResult is for errors that contain
    // a mixture of text and data, such as those from properties
    // that are handled specially. In this case, the data fields
    // (filenames, values) are relevant, but the title alone is
    // insufficient to describe the error.

    // IMPORTANT: The description parameter may contain HTML
    // formatting.

    // Create a new IResult.

    IResult result(e.imageSetName,
                   e.imageSetNum,
                   e.testGroup,
                   resultType,
                   e.msgType,
                   title,
                   description,
                   propPath(info),
                   info->severity,
                   e.fileInfo1,
                   e.fileInfo2,
                   (value1 == nullptr) ? QString() : value1->toString(),
                   (value2 == nullptr) ? QString() : value2->toString());

    // Add the result to the Results object.

    addResult(result);
}

void Inspector::addResult(IResultType::Enum resultType, const QString & title, const QString & description, const QString & propPath, const QString & value1, const QString & value2)
{
    // This version of addResult() is the fallback for cases that
    // don't cleanly fit the other versions of addResult().

    // IMPORTANT: The description parameter may contain HTML
    // formatting.

    // Create a new IResult.
    int severity = resultType == IResultType::FatalError ? 2 : (resultType == IResultType::Warning ? 0 : 1);
    IResult result(e.imageSetName,
                   e.imageSetNum,
                   e.testGroup,
                   resultType,
                   e.msgType,
                   title,
                   description,
                   propPath,
                   severity,
                   e.fileInfo1,
                   e.fileInfo2,
                   value1,
                   value2);

    // Add the result to the Results object.

    addResult(result);
}

void Inspector::addResult(const IResult & result)
{
    // Log the result to the log file.

    if (options.logging)
    {
        write(logFile, IResultType::toString(result.resultType).toUpper() + ":\n" + result.title.toUpper() + ":\n" + result.getMessage(false, true) + "\n");
    }

    // Add the result to the Results object.

    results->addResult(result);

    // Upgrade the status as needed.

    switch (e.testGroup)
    {
        case ITestGroup::File:
            upgradeStatus(status.file, result.resultType);
            break;

        case ITestGroup::Capture:
            upgradeStatus(status.capture, result.resultType);
            break;

        case ITestGroup::Transformation:
            upgradeStatus(status.xform, result.resultType);
            break;

        case ITestGroup::UpdateDLN:
            upgradeStatus(status.updateDLN, result.resultType);
            break;

        default:
            break;
    }

    // If there is a recoverable error (file, photo, or processing),
    // throw an exception only if the "Stop on Error" option is set.
    // If there is a fatal error, always throw an exception.

    QString msg;
    switch (result.resultType)
    {
        case IResultType::FileError:
        case IResultType::CaptureError:
        case IResultType::TransformationError:
            if (options.stopOnError)
            {
                if (options.logging) write(logFile, "\nInspection stopped due to error.\n");
                msg = gui ? result.getMessage(true, true) : ": " + result.getMessage(false, false);
                throw InspectorException("Inspection stopped due to error", "Inspection stopped due to error: " + result.title + msg);
            }
            break;

        case IResultType::FatalError:
            if (options.logging) write(logFile, "\nInspection stopped due to fatal error.\n");
            msg = gui ? result.getMessage(true, true) : ": " + result.getMessage(false, false);
            throw InspectorException("Inspection stopped due to fatal error", "Inspection stopped due to fatal error. " + result.title + msg);

        case IResultType::Warning:
        default:
            break;
    }
}

void Inspector::upgradeStatus(IStatus::Enum & status, IResultType::Enum resultType)
{
    switch (resultType)
    {
        case IResultType::Warning:
            if (status == IStatus::Success) status = IStatus::Warning;
            break;

        case IResultType::FileError:
        case IResultType::CaptureError:
        case IResultType::TransformationError:
        case IResultType::FatalError:
            status = IStatus::Error;
            break;

        default:
            break;
    }
}

QString Inspector::propPath(const PropInfo *info)
{
    QString path("/");
    for (int i = 0; i < propStack.size(); i++)
    {
        path.append(propStack.at(i)->getQName());
        path.append("/");
    }
    path.append(info->getQName());
    return path;
}

IResultType::Enum Inspector::getResultType(ITestGroup::Enum testGroup)
{
    // This method isn't technically necessary, as there is a
    // one-to-one correspondence between the test group and the
    // result type used by that test group. We include it to
    // explicitly separate the two ideas in case they diverge
    // in the future.

    switch(testGroup)
    {
        case ITestGroup::Capture:
            return IResultType::CaptureError;

        case ITestGroup::Transformation:
            return IResultType::TransformationError;

        case ITestGroup::File:
            return IResultType::FileError;

        case ITestGroup::UpdateDLN:
            return IResultType::CaptureError;

        case ITestGroup::None:
        default:
            return IResultType::Unknown;
    }
}
