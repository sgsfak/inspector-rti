/****************************************************************************
* DLN:Inspector                                                             *
*                                                                           *
* Copyright (c) 2018                                                        *
*                                                                           *
* Cultural Heritage Imaging                                                 *
* http://culturalheritageimaging.org/                                       *
*																			*
* This file is part of the DLN:Inspector.                                   *
*                                                                           *
* The DLN:Inspector is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation, either version 3 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* The DLN:Inspector is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with the DLN:Inspector.  If not, see <http://www.gnu.org/licenses/>.*
*                                                                           *
* Written by Ronald Bourret, http://www.rpbourret.com                       *
*                                                                           *
****************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

// Local headers

#include "dirpattern.h"
#include "inspector.h"
#include "optionsdialog.h"
#include "resultsdialog.h"
#include "setchooser.h"
#include "ienums.h"

// Qt headers

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QWidget>

class MainWindow : public QWidget
{
    Q_OBJECT
    
public:
    MainWindow(InspectorType::Enum inspectorType, PropInfoLists *propInfoLists, Options &options, const QList<DirPattern> & dirPatterns, bool isStandalone = true, QWidget *parent = nullptr);
    ~MainWindow();

    void selectBundle(int project_srl, int bundle_srl);
    bool isStandalone() const {
        return this->isStandalone_;
    }

public slots:
    void inspect();
    void displaySavedResults();
    void displayOptions();
    void displayHelp();

signals:

private:

    // Inspector Type
    InspectorType::Enum inspectorType;

    // Does the Inspector runs as a standalone application?
    // (i.e. outside of DLN-CC)
    bool isStandalone_;

    // The following members are used when the Inspector is called from inside
    // the DLN. They store the selected project and bundle ids:
    int selected_project_srl;
    int selected_group_srl;

    // Inspector object

    Inspector inspector;

    // GUI member variables

    SetChooser * setChooser;
    ResultsDialog resultsDialog;

    // Member variables for property information lists and options

    PropInfoLists * propInfoLists;
    Options & options;

    // GUI initialization methods

    void initGUI(const QList<DirPattern> & dirPatterns);
    QHBoxLayout * initTitleLayout();
    QVBoxLayout * initButtonsLayout();

    // Methods to support slots

    void displayResultsBrowser();
    void displayResultsFromFile();

    void removeEmptySets(QList<ImageSet> & imageSets);
    bool isSetEmpty(const ImageSet & imageSet);
    bool bindUdpComm();
};

#endif // MAINWINDOW_H
