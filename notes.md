Notes:

* An image set is a set of processed images used to create an RTI file or a
  photogrammetry model, and an optional set of archived images from which the
  processed images were created. For RTI, the processed images are always JPEG
  files; for photogrammetry, they can be JPEG or TIFF files. The archived
  images are always DNG files.


* For RTI, the DLN:Inspector expects image sets to be gathered into projects,
  with a project directory containing related image set directories. In turn,
  each image set directory is expected to have a jpeg-exports subdirectory,
  which contains JPEG files, and an optional original- captures subdirectory,
  which contains DNG files. `cptset_img_foldr` is used for processed files, and
  `cptset_arch_img_foldr` is used for the originals
    
    - It seems that in Checking of Filenames order matters and therefore Ron
      sorts them in an OS-dependent way..


* For photogrammetry, the DLN:Inspector expects one or more related image sets
  to be gathered into a group. The DLN:Inspector is quite flexible in how image
  sets and files are stored within a group directory. As shipped, it supports
  several common directory structures. For example, the following structure has
  a group directory containing image set directories. These contain image type
  directories, which contain images

 * A useful tool for extracting metadata is [exiftool](https://exiftool.org/)
   e.g with `exiftool -tagsfromfile A.DNG meta.xmp` See more examples at
   https://exiftool.org/metafiles.html 
